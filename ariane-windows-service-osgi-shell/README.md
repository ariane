# Jellyfish #

This is a web cache based on java and netty.

## Description ##

This project aims to implement an HTTP based web cache, with the following features:

* Support for multiple protocols working together using something like apache tomcat connectors;

* Support for HTTP, HTTP/S and SPDY;

* Support for multiple cache replacement policies (with easy switching);

* Support for custom handlers, allowing a user to change the logic of a proxy, in order to implement something like a web crawler;

* Support for disconnected/connected mode, allowing the usage of a browser even when there is no network connection, and the enforcing of consistency rules when a connection is available;

* Code that is easy to understand and modify if one needs to.

## Todo ##

There are several things on the todo list, and among them:

* Improve the documentation, mainly create a user guide, some of the regular files on a tarball (like a install, news, authors, etc.) and create some developer or design documentation, something like an guide that explains the main building blocks of the project together with references to the classes that implement those blocks;

* Work with the gradle build system, in order to create a better release model (one that is capable of producing tarballs and zip files containing the binaries, sources and documentation of a release) and integrate other tools like checkstyle, findbugs, etc.

* Complete the code in order to create a cache, mainly test the existing cache system based on ehcache, create the modules implementing the consistency policies and some of the modules implementing the replacement strategies;

So that is the roadmap for now, but once that is done, some other things are on the list, like:

* HTTP/S and SPDY support;

* Support for filtering on the pages;

* Support for image transformation;

* Support for youtube and some other fancy sites (like facebook games);

* ...

## License ##

This project is licensed under the Apache License 2.0.

## Install ##

To install this software you must have gradle installed, and issue gradle distZip in order to create an release zip file. 

Then go to ${path_to_source_dir}/build/distributions and extract the zip file in the folder to the location where you want to install jellyfish.

Then open an console shell, go to the directory where you extracted the files and type bin/jellyfish -c configuration.xml.

If you want to customize the server, please edit the configuration.xml, logback.xml and ehcache.xml in order to configure the server, the logging system or the cache respectively.