/******************************************************************************
 * Copyright 2012 Fat Cat                                                     *
 *                                                                            *
 * Licensed under the Apache License, Version 2.0 (the "License");            *
 * you may not use this file except in compliance with the License.           *
 * You may obtain a copy of the License at                                    *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 * Unless required by applicable law or agreed to in writing, software        *
 * distributed under the License is distributed on an "AS IS" BASIS,          *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 * See the License for the specific language governing permissions and        *
 * limitations under the License.                                             *
 ******************************************************************************/
package com.assembla.coyote.ariane.jellyfish.plain;

import com.beust.jcommander.JCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class ArianeJellyfishMain
{

  private final Logger logger = LoggerFactory.getLogger( getClass() );

  private final String[] args;

  public ArianeJellyfishMain( String... args )
  {
    logger.debug( "Initialized ariane jellyfish using the local method." );
    this.args = Arrays.copyOf(args, args.length);
  }

  @SuppressWarnings( "FeatureEnvy" )
  public void run( )
  {
    //Parse the arguments
    ArianeJellyfishArguments arguments = new ArianeJellyfishArguments();
    JCommander commander = new JCommander( arguments, args );

    if(arguments.isVersion() )
    {
      ArianeJellyfishArguments.showVersion();
      System.exit( 0 );
    }

    if(arguments.isHelp())
    {
      commander.usage(ArianeUsefulConstants.ARIANE_NAME + ' ' +
          ArianeUsefulConstants.ARIANE_VERSION);
      System.exit( 0 );
    }

    //Load the osgi framework

    //Load the main bundle

    //Wait for osgi termination

    //Exit the server
  }

  public static void main(String[] args)
  {
    ArianeJellyfishMain main = new ArianeJellyfishMain( args );
    main.run();
  }

  @Override
  public String toString()
  {
    return "Main Class";
  }
}
