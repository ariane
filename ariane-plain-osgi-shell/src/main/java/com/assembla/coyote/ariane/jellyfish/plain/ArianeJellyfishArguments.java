/******************************************************************************
 * Copyright 2012 Fat Cat                                                     *
 *                                                                            *
 * Licensed under the Apache License, Version 2.0 (the "License");            *
 * you may not use this file except in compliance with the License.           *
 * You may obtain a copy of the License at                                    *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 * Unless required by applicable law or agreed to in writing, software        *
 * distributed under the License is distributed on an "AS IS" BASIS,          *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 * See the License for the specific language governing permissions and        *
 * limitations under the License.                                             *
 ******************************************************************************/

package com.assembla.coyote.ariane.jellyfish.plain;

import com.assembla.coyote.ariane.jellyfish.utilities.Type;
import com.beust.jcommander.Parameter;
import com.google.common.base.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * This is an class that contains the value of the parsed arguments.
 * @since 0.0.8
 * @version 1
 */
@SuppressWarnings( { "ClassWithoutLogger", "FieldMayBeFinal" } )
@Type
public class ArianeJellyfishArguments implements Serializable
{

  /**
   * The capacity of the string builder.
   */
  private static final int CAPACITY = 45;

  /**
   * The id of this class for serialization purposes.
   */
  private static final long serialVersionUID = 1934426400287989939L;

  /**
   * The calculated hash code, stored in a variable, because this type is
   * immutable.
   */
  private int hash_code = -1;

  /**
   * The name of the configuration file to use.
   */
  @Parameter(names = { "--configuration", "-c" }, description = "The name " +
      "and path of the configuration file to use", required = true)
  private String configurationFileName = "";

  /**
   * If we should run in debug mode.
   */
  @Parameter(names = {"-d", "--debug"}, description = "Debug mode")
  private boolean debug = false;

  /**
   * If we should show the help options.
   */
  @Parameter(names = {"-h", "--help"}, description = "Shows this help", help = true)
  private boolean help = false;

  /**
   * If we should show the version.
   */
  @Parameter(names = {"-v", "--version"}, description = "Shows the server " +
      "version information")
  private boolean version = false;

  /**
   * Constructor that creates a new argument value object.
   */
  public ArianeJellyfishArguments( )
  {
    Logger logger = LoggerFactory.getLogger( getClass() );
    logger.debug( "Created the configuration value object." );
  }

  /**
   * Shows the version information.
   */
  public static void showVersion()
  {
    StringBuilder sb = new StringBuilder( CAPACITY );
    sb.append( ArianeUsefulConstants.ARIANE_NAME  )
      .append( "  " )
      .append( ArianeUsefulConstants.ARIANE_VERSION );
    System.out.println( sb.toString() );
    System.out.println( "Copyright (C) 2013 Fat Cat" );
    System.out.println( "This program is licensed under the Apache 2.0 " +
        "License.");
  }

  /**
   * Calculates and returns an hash code for this argument type.
   * @return The calculated hash code.
   */
  @Override
  public int hashCode()
  {
    if(hash_code < 0) hash_code = Objects.hashCode( debug, configurationFileName );
    return hash_code;
  }

  /**
   * Checks if the given argument type object is equal to this one.
   * @param o The object to check for equality.
   * @return True, if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals( Object o )
  {
    if( this == o ) return true;
    if( o == null || getClass() != o.getClass() ) return false;

    ArianeJellyfishArguments that = ( ArianeJellyfishArguments ) o;

    return debug == that.isDebug() &&
        !( configurationFileName != null ?
            !configurationFileName.equals( that.getConfigurationFileName() ) :
            that.getConfigurationFileName() != null ) &&
           help == that.isHelp() &&
           version == that.isVersion();

  }

  /**
   * Checks if ariane jellyfish should show version information.
   * @return True, if it should, false otherwise.
   */
  public boolean isVersion()
  {
    return version;
  }

  /**
   * Checks if ariane jellyfish should show the options help information.
   * @return True, if it should, false otherwise.
   */
  public boolean isHelp()
  {
    return help;
  }

  /**
   * Returns an string representation of this type.
   * @return An string representing this type.
   */
  @Override
  public String toString()
  {
    return Objects.toStringHelper( this )
        .add( "configurationFileName", configurationFileName )
        .add( "debug", debug )
        .add( "help", help )
        .add( "version", version )
        .omitNullValues()
        .toString();
  }

  /**
   * Checks if this program is going to be run in debug mode.<br/>
   * This will enable extra checking.
   * @return True, if the debug mode is enabled, false otherwise.
   */
  public boolean isDebug()
  {
    return debug;
  }

  /**
   * Returns the name of the configuration file to use.
   * @return The name of the configuration file to use.
   */
  public String getConfigurationFileName()
  {
    return configurationFileName;
  }
}
