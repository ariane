This is a list of contributors to this package:

  * Fat Cat <carlos dot manuel250 at gmail dot com>

If you have done something for this package, please send me an email with your 
name and what you have done or send a patch to this file when posting an patch 
to this package.
