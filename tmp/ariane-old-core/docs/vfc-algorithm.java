﻿//We need the VFC vector in order to insert new pages properly, since pages 
//depend on the distance from a given pivot.

//Browser -> Cache Protocol:

//REGISTER USER => PUT jellyfish/users?username=NAME_OF_USER (200 OK)
//UNREGISTER USER => DELETE jellyfish/users?username=NAME_OF_USER
//ADD/CHANGE BOOKMARKS => PUT jellyfish/bookmarks?username=NAME_OF_USER (+ json content)
//ADD/CHANGE CONSISTENCY ZONES => PUT jellyfish/vfc?username=NAME_OF_USER (+ json content)

/*
 * Bookmark registering json:
 * 
 * {
 *   bookmarks:
 *   {
 *     number:3,
 *     bookmark:
 *     [
 *       {
 *         url:'ESCAPED_URL_1'
 *       },
 *       {
 *         url:'ESCAPED_URL_2'
 *       },
 *       {
 *         url:'ESCAPED_URL_3'
 *       }
 *     ]
 *   }
 * }
 * 
 */

/*
 * 
 * Consistency Zone json:
 * 
 * {
 *   pivot:{
 *     vector:'d;r;f',
 *     zones:3,
 *     zone:[
 *       {
 *         vector:'d;r;f'
 *       },
 *       {
 *         vector:'d;r;f'
 *       },
 *       {
 *         vector:'d;r;f'
 *       }
 *     ]
 *   }
 * }
 * 
 */

//Problems:

  //Check if a page exists somewhere else, before creating a new one!
    
//On user request. ( user is not new )

public void handle(ProxyPageElement page)
{

  //Add frequency += 1 to the user
  
  //Add age += 1 to all other users
  
  //(check) if user age > defined_threshold
  
    //remove the user (check this out)

}

//On new request ( !isConditional() && isVirtualCacheable() && !isRangeRequest() )

public void handle(ProxyPageElement page)
{

  //if request is new
  
    //(check) if the page is known for the given user
    
      //(check) if the page is already cached (isCached)
      
        //(check) if the page is fresh (page vfc-vector < consistency vfc-vector)
        
          //get the cached page
          
          //add one to the frequency counter
          
          //check (if the page needs to he remaped) (remap all users)
        
          //return the cached page to the user (2+)
        
        //else (if page is not fresh)
        
          //create an conditional server request (or a normal one it does not matter) (+3 incomplete)
          
      //else (if the page is not cached)
      
        //create an unconditional server request (+3)
        
    //else (the page is not known)
    
      //set an extra-value (direct=true)
      
      //create an unconditional server request (+3)
   
   //else (the request is from the server)
   
    //if direct is not set
   
      //get the corresponding page (from consistency map)
   
      //(check) if the response is 304 (unmodified)
        
        //set the vfc-vector as (current_distance, 0, current_frequency)
        
        //remap the page in the consistency zones (remap all users)
        
        //get the cached page
        
        //return the cached page to the user (2+)
        
      //else (the response is not 304)
      
        //(check) if the page is listed as cached
        
          //update the page in cache
          
          //set the vfc-vector as (current_distance, 0, current_frequency)
        
          //remap the page in the consistency zones (remap all users)
          
          //return the received page to the user (+3)
          
        //else (the page is listed as not cached)
        
          //(check) if the page is an html page
          
            //parse the page
            
            //add the parsed pages in the respective consistency zones 
            //
            //( use getPivotsFor( parent_page ) in the users, followed by createUsingParent(parent_page), and 
            //  addNewPage(page) on each parent pivot. )
            
          //set the vfc-vector as (current_distance, 0, current_frequency + 1)
          
          //remap the page in the consistency zones
          
          //cache the new page
          
          //return the received page to the user (+3)
          
    //else (direct is set)
    
       //return the received page to the user (+3)
}

//On conditional request ( isConditional() && isVirtualCacheable() && !isRangeRequest() )

public void handle(ProxyPageElement page)
{

  //if request is new
  
    //(check) if the page is known for the given user (get the page) (from all users)
    
      //(check) if the page is already cached (isCached)
      
        //(check) if the etags match
      
          //(check) if the page is fresh (page vfc-vector < consistency vfc-vector)
        
            //add one to the frequency counter
          
            //check (if the page needs to he remaped) (remap all users)
        
            //return response 304 to the user (+3)
        
          //else (page is not fresh)
                
            //create an unconditional server request (+4)
            
        //else (if the etags do not match)
        
          //create an unconditional server request (+4)
          
      //else (if the page is not cached)
      
        //create an unconditional server request (+4)
        
    //else (the page is not known)
    
      //set an extra-value (direct=true)
      
      //create an unconditional server request (+4)
  
  //else (if request is from the server)

    //if direct is not set
    
      //get the corresponding page (from consistency map) (from all users)
      
      //check if the etags match = store
      
      //(check) if the response is not an error
      
        //(check) if the page is listed as cached
        
          //update the page in cache
          
          //set the vfc-vector as (current_distance, 0, current_frequency)
        
          //remap the page in the consistency zones (remap all users)
          
        //else (the page is listed as not cached)
        
          //(check) if the page is an html page
          
            //parse the page
            
            //add the parsed pages in the respective consistency zones (see method bellow)
            
          //set the vfc-vector as (current_distance, 0, current_frequency + 1)
          
          //remap the page in the consistency zones
          
          //cache the new page
          
        //if the etags match
         
          //return response 304 to the user (+3)
          
        //else (if the etags do not match)
        
          //return the received response
          
      //else (if the response is an error) 
        
        //(check) if the response code is 504 (invalid gateway)
        
          //(check) if the page is listed as cached
          
            //Add an warning to the response.
          
            //return response 304 to the user (+3)
            
          //else (page is not listed as cached)
          
            //return the received page (+3)
            
        //else (response code is not 504)
        
          //return the received page (+3)
            
    //else (direct is set)

      //return the received page (+3)
}

//add the parsed pages in the respective consistency zones 
public void addParsedPage( String url, VFCPage parentPage )
{

  //get the pivot containing the parent page
  
  //(check) if the url exists somewhere else
  
    //get the existing page (from all users)
    
    //add the retrieved page to that pivots
    
  //else
  
    //create a new VFCPage
    
    //add the new page to the pivots
    
  //add a new page to the parent page.   
    
}

//use an CacheEventListener to update isCached on each Page (all methods should be synchronized for this)

public class Users //Has all registered users...
{

  private List<Users>;
  
  public Users() { }
  
  public List<Users> isRemapNeeded( VFCPage page ) { }
  
  /**
   * Don't use method above!
   */
  public void remapAsNeeded( VFCPage page ) { }
  
  /**
   * Use each user getPivotsFor( page ).
   */
  public Map<User, List<Pivot>> getPivotsFor( VFCPage page ) {  }

  /**
   * Use isPageKnown and see if the user that contains the page is the
   * same user that is in the argument.
   *
   * If it is set the second returned boolean as true.
   */
  public Pair<boolean, boolean> isPageKnown(String url, User user) { }
  
  /**
   * Use peekPageInformation and see if the user that contains the page is the
   * same user that is in the argument.
   *
   * If it is set the returned boolean as true.
   */
  public Pair<boolean, VFCPage> peekPageInformation(String url, User user) { }
  
}

//user => (id, pivots)

public class User //(comparator using user id)
{

  private UUID id;
  
  private int frequency;
  
  private int age;
  
  private List<Pivot> pivots;
  
  public User() { }
  
  /**
   * Return on first match
   */
  public boolean isPageKnown(String url) { }
  
  /**
   * Return null if the url is unknown.
   *
   * Check if VFCPages are equal!
   *
   * VFCPage newpage = ...
   * VFCPage oldpage = null;
   * 
   * if( (oldpage != null) && ( newpage.equals(oldpage) ) )
   * {
   *   oldpage = newpage;
   * }
   * else if(oldpage != null)
   * {
   *   this.logger.warn("There is an duplicated page {} in the consistency zones!", newpage.getUrl() );
   *   throw new DuplicatedPageException();
   * }
   */
  public VFCPage peekPageInformation(String url) { }
    
  /**
   * Return on first not fresh.
   */
  public boolean isPageFresh( VFCPage page ) { }
  
  /**
   * Return on first true.
   */
  public boolean isRemapNeeded( VFCPage page ) { }
  
  /**
   * Don't use method above!
   */
  public void remapAsNeeded( VFCPage page ) {  }
  
  /**
   * Use isPageKnown(page) on each pivot and add if true.
   */
  public List<Pivot> getPivotsFor( VFCPage page ) {  }
  
}

//pivot => (page, consistency_zones)

public class Pivot //(comparator using page url)
{

  private VFCPage page;
  
  private List<ConsistencyZone> consistency_zones;
  
  public Pivot() { }
  
  /**
   * When iterating return on first true consistency.isPageKnown(url).
   *
   * Return true if the url is equal to the one of this pivot.
   */
  public boolean isPageKnown(String url) { }
  
  /**
   * When iterating return on first true consistency.isPageKnown(page).
   *
   * Return true if the page is equal to the one of this pivot.
   */
  public boolean isPageKnown(VFCPage page) {  }
  
  /**
   * Return null if the url is unknown.
   *
   * Return this page if the url is equal to the one of this pivot
   */
  public VFCPage peekPageInformation(String url) { }
  
  /**
   * Return on first not fresh.
   *
   * Check if the page is this pivot.
   */
  public boolean isPageFresh( VFCPage page ) { }
  
  /**
   * Return on first shouldBelongHere(page) in collection (do not iterate over everything!)
   *
   * Return false if page is this pivot.
   */
  public boolean isRemapNeeded( VFCPage page ) { }
  
  /**
   * Don't use method above!
   *
   * record first shouldBelongHere(page) in collection and stop issuing shouldBelongHere(page).
   * locate the current consistency zone. (stop iterating once this is found!)
   * remove the page from current consistency zone.
   * reinsert page in the new consistency zone.
   *
   * Return imediatly is the page is this pivot.
   */
  public void remapAsNeeded( VFCPage page ) {  }
  
  /**
   * Check if page url is not present first!
   *
   * Use shouldBelongHere(page) to see where to add the new page.
   */
  public void addNewPage( VFCPage page ) {  }

}

//consistency_zones => (vfc-vector, pages)

public class ConsistencyZone //(comparator using VFCVector)
{

  private VFCVector vfc; //max accepted vfc
  
  /**
   * Before adding check if the page shouldBelongHere().
   */
  private List< VFCPage > pages;
  
  public ConsistencyZone() { }
  
  /**
   * Return on first true page in collection (do not iterate over everything!)
   */
  public boolean isPageKnown(String url) { }
  
  /**
   * Return on first true page in collection (do not iterate over everything!)
   *
   * Use page.getVector() <= page to filter pages, before iteration.
   */
  public boolean isPageKnown( VFCPage page ) { }
  
  /**
   * Return null if the url is unknown.
   */
  public VFCPage peekPageInformation(String url) { }
  
  /**
   * Locate the page first, and then use:
   *
   * page.getVector().recency < this.vector.recency && page.getVector().frequency < this.vector.frequency
   */
  public boolean isPageFresh( VFCPage page ) { }
  
  /**
   * If the page is not inside and : 
   *
   * page.getVector().recency < this.vector.recency && page.getVector().frequency < this.vector.frequency,
   *
   * then return true.
   */
  public boolean shouldBelongHere( VFCPage page ) { }
  
}

//page => (vfc-vector, page_data)

public class VFCPage //(comparator using url)
{

  private String url;
  
  private UUID pageId;
  
  private VFCVector vector;
  
  private List<VFCPage> linkedPages;
  
  private boolean isCached;
  
  public VFCPage( ) { }
    
  /**
   * Creates a new page, using the given one as parent.
   *
   * The new page vfc-vector is ( parentpage.getVector( ).getDistance( ) + 1, 0, 0 )
   */
  public static VFCPage createUsingParent( VFCPage page ) { }
  
  /**
   * This is not unsafe!<br/>&nbsp;<br/>
   *
   * Let me explain:<br/>&nbsp;<br/>
   * 
   * The structures used to store VFC data are really an direct acyclic graph (DAG),
   * in that each VFCPage is really a reference to the same page (like all objects in java).<br/>&nbsp;<br/>
   * 
   * The point, is that we need that whenever an VFCPage is *really* deleted (no more uses in the DAG),
   * then if that page has cached data, then we must destroy all the cached data with the page, why?<br/>&nbsp;<br/>
   *
   * Because suppose, that we don't delete the cached page only the VFCPage (for example when a pivot or user 
   * is removed), then at some other time, when a pivot uses that same page, he will detect that there is no
   * VFCPage for that url and will hapilly retrieve the page from the server, but when he tries to cache it,
   * hell will break loose, since there is an cached page (that is potencially stale and different from the 
   * new one), and nothing will be cached, so that we will have an new VFCPage with an old cached page hence
   * stored data is mismatched and potentially stale.
   */
  protected void finalize() throws Throwable { }

}

public class VFCVector //(natural order distance 1st, recency 2nd, frequency 3rd)
{

  private int distance;
  
  private int recency;
  
  private int frequency;
  
  public VFCVector( ) { }

}