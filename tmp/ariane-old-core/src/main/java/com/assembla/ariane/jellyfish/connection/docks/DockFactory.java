/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.docks;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that allows to create a new dock.
 * @author Fat Cat
 * @serial 0.0.1
 * @version 2
 */
public final class DockFactory
{
 
  private Map<String, Class> docks;
  
  private Logger logger = LoggerFactory.getLogger( DockFactory.class );
  
  /**
   * Private constructor to create a new factory. 
   */
  private DockFactory()
  {
    this.docks = new HashMap<>();
    this.docks.put( "http", HttpDock.class );
    this.docks.put( "null", NullDockImpl.class );
  }
  
  /**
   * Returns a new dock of the given type.
   * @param type The type of the dock.
   * @param config The dock configuration.
   * @return A new dock of the given type.
   * @throws InvocationTargetException If the dock couldn't be created.
   */
  public Dock get( final String type, final HierarchicalConfiguration config ) throws InvocationTargetException
  {
    switch( type )
    {
      case "http":
        return new HttpDock( config );
      case "null":
        return JellyfishConstants.NullDock;
      default:
        break;
    }

    return create( type, config );
  }

  /**
   * Returns a new dock of the given type, looking only in the dock type map.
   * @param type The type of the dock.
   * @param config The dock configuration.
   * @return A new dock of the given type.
   */
  @SuppressWarnings( { "unchecked", "rawtypes" } )
  private Dock create( final String type, final HierarchicalConfiguration config )
  {
    if( this.contains( type ) )
    {
      try
      {
        final Constructor constructor = this.docks.get( type ).getConstructor( HierarchicalConfiguration.class );
        return ( Dock ) constructor.newInstance( config );
      }
      catch ( InstantiationException | IllegalAccessException |
              IllegalArgumentException | InvocationTargetException |
              NoSuchMethodException | SecurityException e )
      { 
        this.logger.error( e.getMessage( ), e ); 
      }
    }
    return JellyfishConstants.NullDock;
  }
  
  /** The singleton instance. */
  private static final DockFactory inst = new DockFactory( );
  
  /**
   * Returns the instance of this singleton.
   * @return The factory singleton
   */
  public static DockFactory instance( )
  {
    return DockFactory.inst;
  }

  /**
   * Checks if the given dock type is known.
   * @param type The type to check.
   * @return True, if it is.
   */
  public boolean contains( final String type )
  {
    return this.docks.containsKey( type );
  }

  /**
   * Returns an set with all the known dock types.
   * @return An set with dock types.
   */
  public Set<String> stringSet( )
  {
    return this.docks.keySet( );
  }

  /**
   * Adds a new dock type.
   * @param type The dock type.
   * @param dock The dock to add.
   */
  @SuppressWarnings( "rawtypes" )
  public void add( final String type, final Class dock )
  {
    if( !this.docks.containsKey( type ) )
    {
      this.docks.put( type, dock );
    }    
  }

  /**
   * Removes the given dock.
   * @param type The dock type.
   */
  public void remove( final String type )
  {
    if( this.docks.containsKey( type ) )
    {
      this.docks.remove( type );
    }    
  }

  /**
   * Returns the number of known dock types.
   * @return The number of known docks.
   */
  public int size( )
  {
    return this.docks.size( );
  }
}
