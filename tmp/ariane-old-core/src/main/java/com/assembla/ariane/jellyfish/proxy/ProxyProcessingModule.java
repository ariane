/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.handling.ProxyResponseHandlingAction;
import com.assembla.ariane.jellyfish.proxy.handling.RequestDistributionModule;
import com.assembla.ariane.jellyfish.proxy.handling.ResponseDistributionModule;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * Class that implements the main processing module, that should retrieve
 * requests from the request queue in ClientMessageQueue, handle them 
 * to the cache chain of responsibility, and then send the response to the 
 * client.
 * By request of a cache consistency policy, it also sends requests to an
 * server in order to retrieve some needed information.
 * 
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 */
public class ProxyProcessingModule extends AbstractModule
{
  
  /** The default handler. */
  private int defaultHandlerIndex;
  
  /** The handler list. */
  private final List< IHandler< ProxyPageElement >> handlers;
  
  /** The request distributor. */
  private final RequestDistributionModule requestDistributor;
  
  /** The response distributor. */
  private final ResponseDistributionModule responseDistributor;
  
  /** The processor response handler. */
  private final ProxyResponseHandlingAction responseProcessor;
  
  /** The thread pool. */
  private final ExecutorService threadpool = Executors.newFixedThreadPool( 3 );
  
  /**
   * Creates a new proxy processing module.
   * @param configuration The configuration object.
   */
  public ProxyProcessingModule( final HierarchicalConfiguration configuration )
  {
    super( configuration );
    this.logger = LoggerFactory.getLogger( ProxyProcessingModule.class );
    this.handlers = Collections.synchronizedList( new ArrayList< IHandler< ProxyPageElement >>( ) );
    
    this.requestDistributor = new RequestDistributionModule( this.config );
    this.responseDistributor = new ResponseDistributionModule( this.config );
    this.responseProcessor = new ProxyResponseHandlingAction( this.config );
  }
  
  /**
   * Configures the proxy processing module, inserting all the processors.
   */
  public void configure( )
  {
    this.logger.info( "Configuring the processing module..." );
    for ( final HierarchicalConfiguration processorInfo : this.config.configurationsAt( "chain.processor" ) )
    {
      final ProxyAction action = ProxyActionFactory.instance( ).get( processorInfo.getString( "[@id]", "NULL" ), JellyfishConstants.NullProxyAction, processorInfo );
      
      if ( !action.equals( JellyfishConstants.NullProxyAction ) )
      {
        this.logger.info( "Adding a new proxy action." );
        this.add( action );
        
        if ( ( processorInfo.containsKey( "[@initial]" ) ) && ( processorInfo.getBoolean( "[@initial]" ) ) )
        {
          this.logger.info( "Proxy action is the startup one." );
          this.setDefaultHandlerIndex( this.size( ) - 1 );
        }
      }
    }
  }
  
  /**
   * Stops this module.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override( )
  public synchronized void stop( )
  {
    this.logger.info( "Stopping the distributor." );
    this.logger.info( "Request to stop received!" );
    this.requestDistributor.stop( );
    this.logger.info( "Stopped the request distributor." );
    
    this.responseDistributor.stop( );
    this.logger.info( "Stopped the response distributor." );
    
    this.responseProcessor.stop( );
    this.logger.info( "Executing the request distributor" );
    
    this.threadpool.shutdown( );
    this.logger.info( "Stopping the threads..." );
    try
    {
      if ( !this.threadpool.awaitTermination( 
          JellyfishConstants.DEFAULT_WAITING_TIME + 
          JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS ) )
      {
        this.logger.info( "Forcing the thread shutdown." );
        this.threadpool.shutdownNow( );
      }
    }
    catch( final InterruptedException e )
    {
    }
    this.logger.info( "Clearing the message queues." );
    this.clearHandlers( );
    this.logger.info( "Processing module is terminated" );
  }
  
  /**
   * Runs the module.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override( )
  public void run( )
  {
    this.logger.info( "Executing the request distributor" );
    this.threadpool.execute( this.requestDistributor );
    
    this.logger.info( "Executing the response distributor" );
    this.threadpool.execute( this.responseDistributor );
    
    this.logger.info( "Executing the response receiver" );
    this.threadpool.execute( this.responseProcessor );
    
    this.setRunning( true );
  }
  
  //Default handler
  
  /**
   * Obtains the default handler index.
   * @return The index of the default handler.
   */
  public int getDefaultHandlerIndex( )
  {
    return this.defaultHandlerIndex;
  }
  
  /**
   * Sets the default handler index.
   * @param defaultHandlerIndex The index of the default handler.
   */
  public void setDefaultHandlerIndex( final int defaultHandlerIndex )
  {
    if ( ( defaultHandlerIndex >= 0 ) && ( defaultHandlerIndex < this.size( ) ) )
    {
      this.logger.info( "Changing default handler!" );
      this.defaultHandlerIndex = defaultHandlerIndex;
      this.startingHandlerChanged( this.getHandlerAt( defaultHandlerIndex ) );
    }
  }
  
  /**
   * Method to change the startup handler of the distributors.
   * @param handler The new handler to use.
   */
  private void startingHandlerChanged( final IHandler< ProxyPageElement > handler )
  {
    this.requestDistributor.setStartingHandler( handler );
    this.responseDistributor.setStartingHandler( handler );
  }
  
  // Handlers
  
  /**
   * Adds a new handler to the end of the list.
   * @param handler The handler to add.
   */
  public void add( final IHandler< ProxyPageElement > handler )
  {
    this.logger.info( "Adding a new handler to the last position in the list." );
    if ( handler == null )
    {
      return;
    }
    else
    {
      if ( !this.handlers.isEmpty( ) )
      {
        this.handlers.get( this.handlers.size( ) - 1 ).setSuccessor( handler );
      }
      this.handlers.add( handler );
    }
  }
  
  /**
   * Adds a new handler at the given position.
   * @param index The position to add the handler to.
   * @param handler The handler to add.
   */
  public void add( final int index, final IHandler< ProxyPageElement > handler )
  {
    this.logger.info( "Adding a handler to {}", index );
    if ( ( handler != null ) && ( ( index >= 0 ) && ( index < this.handlers.size( ) ) ) )
    {
      if ( index < this.handlers.size( ) )
      {
        handler.setSuccessor( this.handlers.get( index ).getSuccessor( ) );
      }
      if ( index > 0 )
      {
        this.handlers.get( index ).setSuccessor( handler );
      }
      this.handlers.add( index, handler );
      if ( index == this.getDefaultHandlerIndex( ) )
      {
        this.startingHandlerChanged( handler );
      }
    }
  }
  
  /**
   * Clears the handler list.
   */
  public void clearHandlers( )
  {
    this.logger.info( "Clearing all handlers." );
    this.handlers.clear( );
  }
  
  /**
   * Sees if the given handler is contained inside this list. 
   * @param handler The handler to check.
   * @return True, if it is at least once.
   */
  public boolean contains( final IHandler< ProxyPageElement > handler )
  {
    return this.handlers.contains( handler );
  }
  
  /**
   * Returns the handler at the given index.
   * @param index The index from where to return the handler.
   * @return The handler at the given position.
   */
  public IHandler< ProxyPageElement > getHandlerAt( final int index )
  {
    if ( ( index < 0 ) || ( index >= this.size( ) ) ) 
    {
      throw new IndexOutOfBoundsException( );
    }
    else
    {
      return this.handlers.get( index );
    }
  }
  
  /**
   * Checks if the handler list is empty.
   * @return True if it is (This is an execution error...)
   */
  public boolean emptyHandlers( )
  {
    return this.handlers.isEmpty( );
  }
  
  /**
   * Removes the handler at the given index.
   * @param index The index of the handler to remove.
   * @return The removed handler. 
   */
  public IHandler< ProxyPageElement > removeHandlerAt( final int index )
  {
    if ( ( index < 0 ) || ( index >= this.size( ) ) )
    {
      throw new IndexOutOfBoundsException( );
    }
    else
    {
      this.logger.info( "Removing handler at {}", index );
      if ( index > 0 )
      {
        this.handlers.get( index - 1 ).setSuccessor( this.handlers.get( index ).getSuccessor( ) );
      }
      if ( index == this.getDefaultHandlerIndex( ) )
      {
        if ( this.size( ) == 1 ) 
        {
          throw new UnsupportedOperationException( "There must be at least one handler" );
        }
        else
        {
          this.startingHandlerChanged( this.handlers.get( this.getDefaultHandlerIndex( ) + 1 ) );
        }
      }
      return this.handlers.remove( index );
    }
  }
  
  /**
   * Changes the handler at the given index.
   * @param index The index of the handler to change.
   * @param handler The new handler.
   * @return The old handler.
   */
  public IHandler< ProxyPageElement > changeHandlerAt( final int index, final IHandler< ProxyPageElement > handler )
  {
    this.logger.info( "Changing the handler at {}", index );
    final IHandler< ProxyPageElement > old_handler = this.removeHandlerAt( index );
    this.add( index, handler );
    return old_handler;
  }
  
  /**
   * Returns the size of the handler list.
   * @return The number of handlers.
   */
  public int size( )
  {
    return this.handlers.size( );
  }
  
  /**
   * Returns the handler list as read-only.
   * @return The handler list.
   */
  public List< IHandler< ProxyPageElement >> getHandlerList( )
  {
    return Collections.unmodifiableList( this.handlers );
  }
}
