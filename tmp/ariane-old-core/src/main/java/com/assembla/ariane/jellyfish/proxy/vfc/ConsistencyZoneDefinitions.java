/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.vfc;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.exceptions.ArgumentException;
import org.json.JSONArray;
import org.json.JSONObject;
import com.google.common.base.Objects;

/**
 * This class represents an set of consistency zone definitions for a given 
 * user.<br/>
 * Since an user may configure only one set of maximum VFCVector for the 
 * consistency zones and pivot. 
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public class ConsistencyZoneDefinitions implements Serializable
{
  
  /** The class version. */
  private static final long serialVersionUID = 5303175024684256136L;

  /** The pivot VFC vector. */
  private VFCVector pivotVector;
  
  /** The consistency zone vectors. */
  private List<VFCVector> consistencyZoneVectors;
  
  /**
   * Creates a new consistency zone definition.
   * @param pivotVector The maximum VFC vector for the pivots under this definition.
   */
  public ConsistencyZoneDefinitions( final VFCVector pivotVector )
  {
    this.pivotVector = ( pivotVector.getDistance( ) == 0 ) ? pivotVector : 
      new VFCVector( 0, pivotVector.getRecency( ), pivotVector.getFrequency( ) );
    
    this.consistencyZoneVectors = Collections.synchronizedList( new LinkedList<VFCVector>(  ) );
  }
  
  /**
   * Creates a new consistency zone definition, given an json object.
   * @param json An json object describing the new definition.
   * @throws ArgumentException If there is an error in the given json.
   */
  public ConsistencyZoneDefinitions( final JSONObject json ) throws ArgumentException
  {
    try
    {
      final JSONObject rootNode = json.optJSONObject( "pivot" );
    
      if( rootNode != null )
      {
        String vector = rootNode.optString( "vector", JellyfishConstants.EMPTY_STRING );
      
        if( vector == null )
        {
          throw new NullPointerException( );
        }
        
        int distance = 0;
        int recency = Integer.parseInt( vector.split( ";" )[ 1 ] );
        int frequency = Integer.parseInt( vector.split( ";" )[ 2 ] );
        
        this.pivotVector = new VFCVector( distance, recency, frequency );
        
        final long numberZones = rootNode.optLong( "zones", -1L );
        final JSONArray array = rootNode.optJSONArray( "zone" );
        
        if( array.length( ) == numberZones )
        {
          for( long i = 0; i < numberZones; i++ )
          {
            
            final JSONObject vfc = array.optJSONObject( ( int  ) i ); 
            
            if( vfc == null )
            {
              continue;
            }
            
            vector = vfc.optString( "vector", JellyfishConstants.EMPTY_STRING );
            
            if( vector == null )
            {
              continue;
            }
            
            distance = Integer.parseInt( vector.split( ";" )[ 0 ] );
            recency = Integer.parseInt( vector.split( ";" )[ 1 ] );
            frequency = Integer.parseInt( vector.split( ";" )[ 2 ] );
            
            this.consistencyZoneVectors.add( new VFCVector( distance, recency, frequency ) );            
          }
        }
      }
    }
    catch( final ArrayIndexOutOfBoundsException | NullPointerException | NumberFormatException ex )
    {
      throw new ArgumentException( );
    }
  }
  
  /**
   * Returns the current maximum VFC-Vector that an pivot can have before it is
   * updated.
   * @return The current maximum pivot VFC-Vector.
   */
  public final synchronized VFCVector getPivotVector( )
  {
    return this.pivotVector;
  }

  /**
   * Changes the current maximum VFC-Vector that an pivot can have before it is
   * updated.<br/>
   * Note that the only possible distance for an pivot VFC-Vector is 0. (since this field is not used...)
   * @param pivotVector The new maximum pivot VFC-Vector.
   */
  public final synchronized void setPivotVector( final VFCVector pivotVector )
  {
    this.pivotVector = ( pivotVector.getDistance( ) == 0 ) ? pivotVector : 
        new VFCVector( 0, pivotVector.getRecency( ), pivotVector.getFrequency( ) );
  }

  /**
   * Returns an read-only list with all VFC-Vector that define the consistency 
   * zones.
   * @return An list of VFC-Vectors.
   */
  public final synchronized List< VFCVector > getConsistencyZoneVectors( )
  {
    return Collections.unmodifiableList( this.consistencyZoneVectors );
  }
  
  /**
   * Adds a new consistency zone vector to this list.<br/>
   * Note that this method does not change anything in the user pivots.<br/>
   * You will have to remap the consistency zones of each pivot to adapt it
   * to the new added consistency zone.
   * @param newConsistencyZoneVector The new consistency zone vector.
   */
  public final synchronized void addConsistencyZoneVector( final VFCVector newConsistencyZoneVector )
  {
    if ( newConsistencyZoneVector != null )
    {
      boolean hasConsistencyZone = false;
      for( VFCVector existingZone : this.consistencyZoneVectors )
      {
        if( VFCVector.CONSISTENCY_RULES_COMPARATOR.compare( existingZone, newConsistencyZoneVector ) == 0 )
        {
          hasConsistencyZone = true;
          break;
        }
      }
      if( !hasConsistencyZone )
      {
        this.consistencyZoneVectors.add( newConsistencyZoneVector );
        Collections.sort( this.consistencyZoneVectors, VFCVector.CONSISTENCY_RULES_COMPARATOR );
      }
    }
  }
  
  /**
   * Removes a consistency zone vector from this list.<br/>
   * Note that this method does not change anything in the user pivots.<br/>
   * You will have to remap the consistency zones of each pivot to adapt it
   * to the removed consistency zone.
   * @param consistencyZoneVector The consistency zone vector to remove.
   */
  public final synchronized void removeConsistencyZoneVector( final VFCVector consistencyZoneVector )
  {
    if ( consistencyZoneVector != null )
    {
      boolean hasConsistencyZone = false;
      for( VFCVector existingZone : this.consistencyZoneVectors )
      {
        if( VFCVector.CONSISTENCY_RULES_COMPARATOR.compare( existingZone, consistencyZoneVector ) == 0 )
        {
          hasConsistencyZone = true;
          break;
        }
      }
      if( !hasConsistencyZone )
      {
        this.consistencyZoneVectors.remove( consistencyZoneVector );
        Collections.sort( this.consistencyZoneVectors, VFCVector.CONSISTENCY_RULES_COMPARATOR );
      }
    }
  }

  /**
   * Compares this object and the given object, to see if they are equal.<br/>
   * The two objects are equal if they are both ConsistencyZoneDefinitions, not 
   * null, if the pivot vector is compatible (in terms of recency and frequency),
   * and if the consistency zone vectors are equal.
   * @param obj The other object to compare this one to.
   * @return True, if both objects are equal, false otherwise.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public synchronized boolean equals( final Object obj )
  {
    if( ( obj instanceof ConsistencyZoneDefinitions ) && ( obj != null ) )
    { 
      final ConsistencyZoneDefinitions other = ( ConsistencyZoneDefinitions ) obj;
      
      if( VFCVector.CONSISTENCY_RULES_COMPARATOR.compare( other.getPivotVector( ), this.pivotVector ) == 0 )
      { 
        if( other.getConsistencyZoneVectors( ).size( ) == this.consistencyZoneVectors.size( ) )
        {
          return other.getConsistencyZoneVectors( ).containsAll( this.consistencyZoneVectors );
        }
      }
    }
    return false;
  }

  /**
   * Calculates and returns an hash code for this object.
   * @return The calculates hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this.pivotVector, this.consistencyZoneVectors );
  }

  /**
   * Returns an string representation of this object.
   * @return An string representation of this object.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    final StringBuffer sb = new StringBuffer( );
    
    sb.append( String.format( "Pivot Maximum VFC-Vector: %s\n", this.pivotVector.toString( ) ) );
    sb.append(  String.format( "Consistency Zone Size: %d\n", this.consistencyZoneVectors.size( ) ) );
    
    return sb.toString( );
  }
}
