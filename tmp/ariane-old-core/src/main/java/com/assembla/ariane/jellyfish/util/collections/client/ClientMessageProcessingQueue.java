/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.client;

import com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import org.slf4j.LoggerFactory;

/**
 * This class implements an bidirectional queue in order to send messages
 * to be processed by the consistency modules and receive the responses
 * from the consistency modules.
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 */
public final class ClientMessageProcessingQueue extends JellyfishQueue< ProxyPageElement, ProxyPageElement >
{
  
  /** The singleton variable. */
  private static final ClientMessageProcessingQueue inst = new ClientMessageProcessingQueue( );
  
  /**
   * Returns this singleton.
   * @return The singleton.
   */
  public static ClientMessageProcessingQueue instance( )
  {
    return ClientMessageProcessingQueue.inst;
  }
  
  /**
   * Private constructor of this class.
   */
  private ClientMessageProcessingQueue( )
  {
    super( );
    this.logger = LoggerFactory.getLogger( ClientMessageProcessingQueue.class );
  }
  
  /**
   * Returns the size of this request queue.
   * @return The size of this request queue (1024).
   * @see com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue#sendSize()
   */
  @Override( )
  protected int sendSize( )
  {
    return JellyfishConstants.DEFAULT_QUEUE_SIZE;
  }
  
  /**
   * Returns the size of this response queue.
   * @return The size of this response queue (1024). 
   * @see com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue#receiveSize()
   */
  @Override( )
  protected int receiveSize( )
  {
    return JellyfishConstants.DEFAULT_QUEUE_SIZE;
  }
  
}
