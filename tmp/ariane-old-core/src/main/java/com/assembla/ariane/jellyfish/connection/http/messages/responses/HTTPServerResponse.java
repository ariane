/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.messages.responses;

import io.netty.buffer.ChannelBuffers;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.WarningValue;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;

/**
 * This class represents an http server response.
 * @author Fat Cat
 * @version 3
 * @since 0.0.2
 */
public class HTTPServerResponse extends HTTPGenericResponse 
{
  
  /**
   * The size of "bytes".
   */
  private static final int RANGE_BYTE_LEN = 5;

  /**
   * Converts an http response to a generic proxy response.
   * @param httpresp The http response to convert.
   * @return An corresponding generic proxy response.
   */
  public static ProxyResponseMessage messageConverter( final HTTPServerResponse httpresp )
  {
    final ProxyResponseMessage response = new ProxyResponseMessage( httpresp.getMessageId( ) );
    
    response.setResponseCode( 
        ProxyResponseCode.create( 
            httpresp.getNativeResponseCode( ).getCode( ), 
            httpresp.getNativeResponseCode( ).getReasonPhrase( ) ) );
    
    response.setProtocol( 
        ProxyProtocol.create( 
            httpresp.getNativeProtocolVersion( ).getMajorVersion( ), 
            httpresp.getNativeProtocolVersion( ).getMinorVersion( ), 
            httpresp.getNativeProtocolVersion( ).getProtocolName( ) ) );
    
    response.setContent( httpresp.getContent( ) );
    response.setDockName( httpresp.getDockName( ) );
    
    if ( httpresp.isKeepAlive( ) )
    {
      response.setKeepAlive( );
    }
    
    //headers
    for ( final String headerName : httpresp.getHeaderNames( ) )
    {
      response.addHeader( headerName, httpresp.getHeader( headerName ) );
    }
    
    //extra
    for ( final String extraName : httpresp.getExtraValueNames( ) )
    {
      response.addExtra( extraName, httpresp.getExtra( extraName ) );
    }
        
    //ranges
    for ( final Pair< Integer, Integer > pair : httpresp.getRanges( ) )
    {
      response.addRange( pair );
    }
    
    //warnings
    for ( WarningValue warning : httpresp.getWarnings( ) )
    {
      response.addWarning( warning );
    }
    
    //hop headers
    for ( String hopHeader : httpresp.getHopHeaders( ) )
    {
      response.addHopHeader( hopHeader );
    }
    
    return response;
  }
  
  /**
   * Converts an generic proxy response to a http response.
   * @param proxyresp The generic proxy response.
   * @return The corresponding http response.
   */
  public static HTTPServerResponse messageConverter( final ProxyResponseMessage proxyresp )
  {
    final HTTPServerResponse response = new HTTPServerResponse( proxyresp.getMessageId( ) );
    
    response.setContent( proxyresp.getContent( ) );
    response.setDockName( proxyresp.getDockName( ) );
    
    response.setResponseCode(
        HttpResponseStatus.valueOf( 
            proxyresp.getNativeResponseCode( ).getResponseCode( ) ) );
    
    response.setProtocolVersion( HttpVersion.valueOf( proxyresp.getProtocolVersion( ) ) );
    
    if ( proxyresp.isKeepAlive( ) )
    {
      response.setKeepAlive( );
    }
    
    //extra
    for ( final String extraName : proxyresp.getExtraValueNames( ) )
    {
      response.addExtra( extraName, proxyresp.getExtra( extraName ) );
    }
    
    //headers
    for ( final String headerName : proxyresp.getHeaderNames( ) )
    {
      response.addHeader( headerName, proxyresp.getHeader( headerName ) );
    }
    
    //ranges
    for ( final Pair< Integer, Integer > pair : proxyresp.getRanges( ) )
    {
      response.addRange( pair );
    }
    
    //warnings
    for ( WarningValue warning : proxyresp.getWarnings( ) )
    {
      response.addWarning( warning );
    }
    
    //hop headers
    for ( String hopHeader : proxyresp.getHopHeaders( ) )
    {
      response.addHopHeader( hopHeader );
    }
    
    return response;
  }
  
  /**
   * Adds the remaining headers to the response.
   * @param response The response to add the headers.
   * @return The response object with the added headers.
   */
  public static HTTPClientResponse addRemainingHeaders( final HTTPClientResponse response )
  {
    //Calculates content length header
    if( !response.containsHeader( "Content_Length" ) && 
        ( !response.getContent( ).equals( ChannelBuffers.EMPTY_BUFFER ) ) )
    {
      HttpHeaders.setContentLength( response.getResponse( ), 
          response.getContent( ).toString( CharsetUtil.US_ASCII ).length( ) );
    }
    
    StringBuffer sb = new StringBuffer( );
    
    //Adds the range header
    if ( !response.getRanges( ).isEmpty( ) )
    { 
      sb.append( "bytes " );
      for ( final Pair< Integer, Integer > pair : response.getRanges( ) )
      {
        if ( ( pair.getFirst( ) == -1 ) && ( pair.getSecond( ) != -1 ) ) // max only
        {
          sb.append( String.format( "-%d", pair.getSecond( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) == -1 ) ) // min only
        {
          sb.append( String.format( "%d-", pair.getFirst( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) != -1 ) ) // both
        {
          sb.append( String.format( "%d-%d", pair.getFirst( ), pair.getSecond( ) ) );
        }
      }
      if ( response.containsExtra( "max-range-content" ) )
      {
        sb.append( String.format( "/%s", response.getExtra( "max-range-content" ) ) );
      }
      else
      {
        sb.append( "/*" );
      }      
      response.addHeader( "Content-Range", sb.toString( ) );
    }
    
    //Adds the warning header
    if( response.hasAnyWarning( ) )
    {
      sb = new StringBuffer( );      
      final List< WarningValue > tmplist = response.getWarnings( );
      
      for( int i = 0; i < response.getNumberOfWarnings( ); i++ )
      {
        sb.append( tmplist.get( i ).toString( ) );
        if( i < response.getNumberOfWarnings( ) - 1 )
        {
          sb.append( "," );
        }
      }
      response.addHeader( "Warning", sb.toString( ) );      
    }
    
    return response;
  }
  
  /**
   * Creates a new empty server response, given the request id.
   * @param messageID The request id.
   */
  public HTTPServerResponse( final UUID messageID )
  {
    super( messageID );
    this.logger = LoggerFactory.getLogger( HTTPServerResponse.class );
  }
  
  /**
   * Creates a new server response.
   * @param messageID The request id.
   * @param resp The http response object to use.
   */
  public HTTPServerResponse( final UUID messageID, final HttpResponse resp )
  {
    super( messageID, resp );
    this.logger = LoggerFactory.getLogger( HTTPServerResponse.class );
  }
  
  /**
   * Creates a new server response.
   * @param messageID The request id.
   * @param version The response http version.
   * @param status The response status code.
   */
  public HTTPServerResponse( final UUID messageID, final HttpVersion version, final HttpResponseStatus status )
  {
    super( messageID, new DefaultHttpResponse( version, status ) );
    this.logger = LoggerFactory.getLogger( HTTPServerResponse.class );
  }
  
  /**
   * Creates a new server request.
   * @param messageID The request id.
   * @param version The response http version (string).
   * @param responsecode The http response code (int).
   */
  public HTTPServerResponse( final UUID messageID, final String version, final int responsecode )
  {
    super( messageID, 
        new DefaultHttpResponse( 
            HttpVersion.valueOf( version ), 
            HttpResponseStatus.valueOf( responsecode ) ) );
    
    this.logger = LoggerFactory.getLogger( HTTPServerResponse.class );
  }
  
  // Id
  
  /**
   * Changes the request id of this request.
   * @param messageId The new message id.
   */
  public void setRequestId( final UUID messageId )
  {
    if ( ( messageId != null ) && ( !this.messageId.equals( messageId ) ) )
    {
      this.messageId = messageId;
    }
  }
  
  // Regular
  
  /**
   * Checks if these two objects are equal.
   * @param obj The object to compare to.
   * @return True, if they are.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals( final Object obj )
  {
    if ( obj instanceof HTTPServerResponse )
    {
      if ( this.messageId.equals( ( ( HTTPServerResponse ) obj ).getMessageId( ) ) )
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Returns an hash code for this response.
   * @return An hash code for this response.
   * @see java.lang.Object#hashCode()
   */
  public int hashCode( )
  {
    return this.messageId.hashCode( );
  }
  
  /**
   * Returns the inner http response.
   * @return The http response.
   */
  public HttpResponse getResponse( )
  {
    return this.response;
  }
  
  /**
   * Method to check the server response, and extract things useful
   * for the cache.
   */
  public void check( )
  {
    // Parse the cache control header
    this.checkCacheControl( );
    
    // Parse the pragma header
    if ( this.containsHeader( HttpHeaders.Names.PRAGMA ) && 
         ( !this.containsHeader( HttpHeaders.Names.CACHE_CONTROL ) ) )
    {
      this.logger.warn( "{}: Parsing pragma header!", this.messageId );
      this.addExtra( "no-cache", "true" );
    }
    
    // Parsing the date header
    this.parseDate( "Date" );
    
    // Parsing the expires header
    this.parseDate( "Expires" );
    
    // Parsing the last modified header
    this.parseDate( "Last-Modified" );
    
    // Parsing the X-Polipo-Date header
    this.parseDate( "X-Polipo-Date" );
    
    // Parsing the content type header
    if ( this.containsHeader( "Content-Type" ) )
    {
      if ( this.getHeader( "Content-Type" ).contains( "multipart/byteranges" ) )
      {
        this.logger.warn( "The content type is multipart byteranges in response {}" );
        this.addExtra( "virtual-no-cache", "true" );
      }
    }
    
    //Don't cache error messages...
    if( ( this.getResponseCodeAsInteger( ) < JellyfishConstants.HTTPErrorCodes.OK.errorcode ) ||
        ( this.getResponseCodeAsInteger( ) >= JellyfishConstants.HTTPErrorCodes.MULTIPLE_CHOICES.errorcode ) )
    {
      this.addExtra( "no-cache", "true" );
    }
    
    
    // Not used since the response isn't sent in chunked mode (always)
    this.removeHeader( "Content-MD5" );
    
    if ( this.containsHeader( "Proxy-Autenticate" ) )
    {
      this.addExtra( "virtual-no-cache", "true" );
    }
    
    this.checkRanges( );
    
    this.checkWarnings( );
    
    this.checkConnection( );
  }
  
  /**
   * Parses the connection header if available.
   */
  public void checkConnection( )
  {
    for ( String connection : this.getHeaders( "Connection" ) )
    {
      for ( String value : connection.split( "," ) )
      {
        if ( ( !value.toLowerCase( ).equals( "close" ) ) && ( !value.toLowerCase( ).equals( "keep-alive" ) ) )
        {
          this.hopHeaders.add( value );
        }
      }
    }
    
    //Standard hop headers
    this.hopHeaders.add( "Connection" );
    this.hopHeaders.add( "Proxy-Authenticate" );
    this.hopHeaders.add( "Trailer" );
    this.hopHeaders.add( "Transfer-Encoding" );
  }
  
  /**
   * Parses the warning header.
   */
  private void checkWarnings( )
  {
    if ( this.containsHeader( "Warning" ) )
    {
      // There can be multiple warning headers, as per http standard.
      for ( String value : this.getHeaders( "Warning" ) )
      {
        
        for ( String warn : value.split( "," ) )
        {
          try
          {
            final int firstSpace = warn.indexOf( " " );
            final int secondSpace = warn.indexOf( " ", firstSpace + 1 );
            final int thirdSpace = warn.indexOf( " ", secondSpace + 1 );
            final int fourthSpace = warn.indexOf( " ", thirdSpace + 1 );
            
            final int code = Integer.parseInt( warn.substring( 0, firstSpace ) );
            final String agent = warn.substring( firstSpace + 1, secondSpace );
            final String text = warn.substring( secondSpace + 1, thirdSpace );
            
            if ( thirdSpace < warn.length( ) )
            {
              final DateTime date = DateTime.parse( warn.substring( thirdSpace + 1, fourthSpace ).replace( "\"", "" ), JellyfishConstants.HTTP_FORMATTER );
              this.addWarning( WarningValue.create( code, agent, text, date ) );
            }
            else
            {
              this.addWarning( WarningValue.create( code, agent, text ) );
            }
            
          }
          catch( final NumberFormatException ex )
          {
            
          }
        }        
      }
    }
    
    this.removeHeader( "Warning" );
  }
  
  /**
   * Parses an date field, and adds an error field in case the date format is not valid.
   * @param headername The name of the header to check.
   */
  private void parseDate( final String headername )
  {
    if ( this.containsHeader( headername ) )
    {
      try
      {
        HttpHeaders.getDateHeader( this.response, headername );
      }
      catch( final ParseException e )
      {
        this.logger.warn( "The {} header is invalid for response {}", headername, this.messageId );
        this.addExtra( String.format( "invalid-%s", headername ), Boolean.TRUE.toString( ) );
        this.removeHeader( headername );
      }
    }
  }
  
  /**
   * Parses the content-range header value.
   */
  private void checkRanges( )
  {
    if ( this.containsHeader( "Content-Range" ) )
    {
      this.logger.info( "Parsing range header in server response {}", this.messageId );
      
      final String headervalue = this.getHeader( "Content-Range" );
      
      if ( headervalue.startsWith( "bytes" ) )
      {
        final String rangeStr = headervalue.substring( HTTPServerResponse.RANGE_BYTE_LEN ).trim( );
        
        final String rangeList = rangeStr.split( "/" )[ 0 ];
        final List< String > ranges = Arrays.asList( rangeList.split( "," ) );
        
        // Get a list of ranges.
        int max, min = 0;
        for ( final String currange : ranges )
        {
          try
          {
            if ( currange.startsWith( "-" ) )
            {
              max = Integer.parseInt( currange.substring( 1 ) );
              this.addRangeMax( max );
            }
            else if ( currange.endsWith( "-" ) )
            {
              min = Integer.parseInt( currange.substring( 0, currange.length( ) - 1 ) );
              this.addRange( min );
            }
            else if ( !currange.contains( "*" ) )
            {
              min = Integer.parseInt( currange.split( "-" )[ 0 ] );
              max = Integer.parseInt( currange.split( "-" )[ 1 ] );
              this.addRange( min, max );
            }
          }
          catch( final NumberFormatException | IndexOutOfBoundsException ex )
          {
            this.logger.warn( "There is a badly formatted range in the response {}", this.messageId );
          }
        }
        
        // Get the max length of the entity...
        int length = -1;
        try
        {
          length = Integer.parseInt( rangeStr.split( "/" )[ 1 ] );
          
        }
        catch( final NumberFormatException ex )
        {
          this.logger.warn( "There is a badly max range size in the response {}", this.messageId );
        }
        finally
        {
          this.addExtra( "max-range-content", ( length == -1 ) ? "*" : Integer.toString( length ) );
        }
      }
    }
    
    response.removeHeader( "Content-Range" );
  }
  
  /**
   * Parses the cache control header.
   */
  private void checkCacheControl( )
  {
    if ( this.containsHeader( "Cache-Control" ) )
    {
      this.logger.debug( "Parsing cache-control from response {}", this.messageId );
      final List< String > list = Arrays.asList( this.getHeader( "Cache-Control" ).split( "," ) );
      
      checkSimpleCacheControlOptions( list );
      parseComplexCacheControlOptions( list );
      
    }
    this.logger.debug( "Finished parsing the cache-control header in server." );
  }
  
  /**
   * Parses the cache control options that have arguments.<br/>
   * Since one of the values that can appear is s-max-age and max-age and
   * since the first one override the second in the case of shared caches (like 
   * this one), then we check first for s-max-age and then for max-age.
   * 
   * @param list The cache control options.
   */
  private void parseComplexCacheControlOptions( final List< String > list )
  {
    for ( final String directive : list )
    {
      if ( directive.startsWith( "s-max-age" ) )
      {
        this.logger.debug( "Shared max-age specified in response {}", this.messageId );
        if ( directive.split( "=" ).length > 1 )
        {
          try
          {
            final int max_age = Integer.parseInt( directive.split( "=" )[ 1 ].trim( ) );
            this.addExtra( "max-age", Integer.toString( max_age ) );
            this.addExtra( "s-max-age", Integer.toString( max_age ) );
          }
          catch( final NumberFormatException ex )
          {
            this.logger.warn( "Shared max-age is not parseable in response {}", this.messageId );
          }
        }
      }
      else if ( directive.startsWith( "max-age" ) )
      {
        this.logger.debug( "Max-age specified in response {}", this.messageId );
        if ( directive.split( "=" ).length > 1 )
        {
          try
          {
            final int max_age = Integer.parseInt( directive.split( "=" )[ 1 ].trim( ) );
            this.addExtra( "max-age", Integer.toString( max_age ) );
          }
          catch( final NumberFormatException ex )
          {
            this.logger.warn( "Max age is not parseable in response {}", this.messageId );
          }
        }
      }
    }
  }
  
  /**
   * Parses the cache control options that don't have an argument.
   * @param list The cache control options.
   */
  private void checkSimpleCacheControlOptions( final List< String > list )
  {
    if ( list.contains( "public" ) )
    {
      this.logger.debug( "Public specified in response {}", this.messageId );
      this.addExtra( "public", "true" );
    }
    if ( list.contains( "no-cache" ) || list.contains( "private" ) )
    {
      this.logger.debug( "No-cache or private specified in response {}", this.messageId );
      this.addExtra( "no-cache", "true" );
    }
    if ( list.contains( "no-store" ) )
    {
      this.logger.debug( "No-store specified in response {}", this.messageId );
      this.addExtra( "no-cache", "true" );
      this.addExtra( "no-store", "true" );
    }
    if ( list.contains( "no-transform" ) )
    {
      this.logger.debug( "No-transform specified in response {}", this.messageId );
      this.addExtra( "no-transform", "true" );
    }
    if ( list.contains( "must-revalidate" ) || list.contains( "proxy-revalidate" ) )
    {
      this.logger.debug( "must-revalidate specified in response {}", this.messageId );
      this.addExtra( "no-cache", "true" );
      this.addExtra( "must-revalidate", "true" );
    }
  }
  
  //Utilities
  
  /**
   * Clones this http server response message.
   * @return A new identical http server response message.
   */
  public HTTPServerResponse copy( )
  {
    try //main clone method uses serialization
    {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream( );
      final ObjectOutputStream out = new ObjectOutputStream( baos );
      out.writeObject( this );
      
      final ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray( ) );
      final ObjectInputStream ois = new ObjectInputStream( bais );
      return ( HTTPServerResponse ) ois.readObject( );
    }
    catch( final IOException | ClassNotFoundException e ) //An alternate clone method, just in case...
    {
      final HTTPServerResponse response = new HTTPServerResponse( this.messageId );
      
      response.setContent( this.getContent( ).copy( ) );
      response.setDockName( this.getDockName( ) );
      response.setProtocolVersion( HttpVersion.valueOf( this.getProtocolVersion( ) ) );
      response.setResponseCode( HttpResponseStatus.valueOf( this.getResponseCodeAsInteger( ) ) );
      
      if ( response.isKeepAlive( ) )
      {
        response.setKeepAlive( );
      }
      
      //Add the headers
      for ( String header : this.getHeaderNames( ) )
      {
        for ( String value : this.getResponse( ).getHeaders( header ) )
        {
          response.getResponse( ).addHeader( header, value );
        }
      }
      
      //Add the extra
      for ( String extraname : this.getExtraValueNames( ) )
      {
        response.addExtra( extraname, this.getExtra( extraname ) );
      }
      
      //Add the ranges
      for ( Pair< Integer, Integer > range : this.getRanges( ) )
      {
        response.addRange( range );
      }
      
      //Add the hop headers
      for ( String header : this.hopHeaders )
      {
        response.addHopHeader( header );
      }
      
      //Add the warning header
      for ( WarningValue warning : this.warnings )
      {
        if( warning.hasDate( ) )
        {
          response.addWarning( WarningValue.create( warning.getCode( ), warning.getAgent( ), warning.getText( ), warning.getDate( ) ) );
        }
        else
        {
          response.addWarning( WarningValue.create( warning.getCode( ), warning.getAgent( ), warning.getText( ) ) );
        }
      }
      
      return response;
    }
  }
  
}
