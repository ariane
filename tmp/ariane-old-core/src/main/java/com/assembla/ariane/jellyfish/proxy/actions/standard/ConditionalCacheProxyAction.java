/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.standard;

import java.net.MalformedURLException;
import java.net.URL;
import com.assembla.ariane.jellyfish.util.collections.cache.CacheMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.RequestType;
import com.assembla.ariane.jellyfish.proxy.comparators.LastModifiedEquality;
import com.assembla.ariane.jellyfish.proxy.comparators.StrongEtagEquality;
import com.assembla.ariane.jellyfish.proxy.comparators.WeakEtagEquality;
import com.assembla.ariane.jellyfish.proxy.exceptions.ArgumentException;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQueryOperation;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponse;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.joda.time.DateTime;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * An proxy action processor that implements an action that deals with
 * conditional requests.
 * @author Fat Cat
 * @since 0.0.6
 * @version 3
 */
public class ConditionalCacheProxyAction extends ProxyAction
{
  
  /**
   * Creates a new conditional proxy action.
   * @param successor The next proxy action processor.
   * @param config The configuration to use for this conditional proxy action.
   */
  public ConditionalCacheProxyAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( ConditionalCacheProxyAction.class );
  }
  
  /**
   * Checks if this processor can handle the given request.<br/>
   * Is only true, if this request is conditional and is not a range request.
   * @param aRequest The request to check for.
   * @return True, if the conditions above are satisfied.
   * @throws IllegalArgumentException Never thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  { 
    return RequestType.isConditionalRequest( aRequest ) && !RequestType.isRangeRequest( aRequest );
  }
  
  /**
   * This method handles a request, by implementing the http standard for
   * conditional requests.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    try
    {
      final Object[ ] args = { aRequest.getCurrentUUID( ), aRequest.getRequest( ).getUri( ), aRequest.getState( ).toString( ) };
      
      this.logger.info( "Received an request:\nID: {}\nURI: {}\nState: {}", args );
      
      //When a request is new, we:
      if ( aRequest.getState( ).equals( ProxyRequestState.PROCESSING ) )
      {
        this.logger.info( "Request with id {} is a new request", aRequest.getCurrentUUID( ) );
        processNewRequest( aRequest );
      }
      else if ( aRequest.getState( ).equals( ProxyRequestState.MORE_INFO_RECEIVED ) )
      {
        //When the request is received from a server:
        this.logger.info( "Request with id {} is a server request", aRequest.getCurrentUUID( ) );
        processReceivedResponse( aRequest );
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Error in the conditional cache proxy action.", ex );
    }
  }

  /**
   * Method to handle a new request.
   * @param aRequest The request to handle.
   */
  private void processNewRequest( final ProxyPageElement aRequest )
  {
    //remove any hop-by-hop headers,
    this.logger.debug( "Removing hop-to-hop headers from request {}", aRequest.getRequest( ).getMessageId( ) );
    aRequest.removeHopToHopHeaders( true );
    
    //See if the request is in proxy (ignoring the method)
    final CacheResponse cachedresponse = getCachedPage( aRequest );
    
    if( cachedresponse != null )
    {
      this.logger.debug( "Cache response code is {}", cachedresponse.getResponse( ).toString( ) );
    }
    
    //Check the cached response, for the conditions.
    boolean match = false;
    if( ( cachedresponse != null ) && ( cachedresponse.getResponseElement( ) != null ) )
    {
      match = this.checkCondition( aRequest, cachedresponse.getResponseElement( ).getResponse( ) );
    }
    
    //If the request cannot be cached
    if ( !RequestType.isCacheableRequest( aRequest ) )
    {
      // we must ask for a new page.
      match = false;
    }
    
    this.logger.debug( "For request {}, is there a conditional match? {}", aRequest.getRequest( ).getMessageId( ), Boolean.toString( match ) );
    
    //If the existing entry match
    if ( match )
    {      
      this.logger.debug( "Request {} is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getMethod( ) );
      
      //If the method is get or head
      if ( RequestType.isGetOrHeadRequest( aRequest ) )
      {
        
        if( this.logger.isDebugEnabled( ) && ( cachedresponse.getResponseElement( ) != null ) )
        {
          StringBuilder tmp = new StringBuilder( );
          tmp.append( "Information for the cached response is:\n" );
          
          if( cachedresponse.getResponseElement( ).hasRequest( ) )
          {
            tmp.append( String.format( "Server Request Time: %s\n", cachedresponse.getResponseElement( ).getRequest( ).getRequestTime( ).toString( JellyfishConstants.DEBUG_FORMATTER ) ) );
          }
          
          if( cachedresponse.getResponseElement( ).hasResponse( ) )
          {
            tmp.append( String.format( "Server Response Time: %s\n", cachedresponse.getResponseElement( ).getResponse( ).getResponseTime( ).toString( JellyfishConstants.DEBUG_FORMATTER ) ) );
            tmp.append( String.format( "Corrected Received Age: %d\n", cachedresponse.getResponseElement( ).getResponse( ).getCorrectedReceivedAge( ) ) );
          }
          
          tmp.append( String.format( "Corrected Initial Age: %d\n", cachedresponse.getResponseElement( ).calculateCorrectedInitialAge( ) ) );
          tmp.append( String.format( "Resident Time: %d\n", cachedresponse.getResponseElement( ).calculateResidentTime( ) ) );
          tmp.append( String.format( "Page Current Age: %d\n", cachedresponse.getResponseElement( ).calculateCurrentAge( ) ) );
          tmp.append( String.format( "Page Freshness Lifetime: %d\n", cachedresponse.getResponseElement( ).calculateFreshnessLifetime( ) ) );
          tmp.append( String.format( "Page Expiration Lifetime: %d\n", cachedresponse.getResponseElement( ).calculateExpirationTime( ) ) );
          this.logger.debug( tmp.toString( ) );
          tmp.setLength(0);
        }
        
        //If the request in cache is fresh, or if an cache only response must be returned.
        final boolean isFresh = ( cachedresponse.getResponseElement( ) != null ) && 
                          cachedresponse.getResponseElement( ).calculateFreshnessLifetime( ) > cachedresponse.getResponseElement( ).calculateCurrentAge( );
        
        if ( isFresh || aRequest.getRequest( ).containsExtra( "only-if-cached" ) )
        {
          this.logger.info( "Page in cache is fresh for request {}",  aRequest.getRequest( ).getMessageId( ) );
          aRequest.addExtra("cached", "true");
          sendNotModifiedResponse( aRequest );
        }
        else //else, make an unconditional request to the server
        {
          this.logger.info( "Page in cache is NOT fresh for request {}",  aRequest.getRequest( ).getMessageId( ) );
          final ProxyRequestMessage request = aRequest.getRequest( ).copy( );
          
          request.removeHeader( "If-Match" );
          request.removeHeader( "If-None-Match" );
          request.removeHeader( "If-Modified-Since" );
          request.removeHeader( "If-Unmodified-Since" );
          
          aRequest.addExtra( "in-cache", Boolean.toString( Boolean.TRUE ) );
          
          this.sendServerRequest( aRequest, request );
        }        
      }
      else //else, if the method is different
      {
        this.logger.info( "Request {} method is not get.", aRequest.getRequest( ).getMessageId( ) );
        //delete the entry in cache
        if ( cachedresponse.getResponseElement( ) != null )
        {
          this.removeCachedPage( cachedresponse.getResponseElement( ) );
        }
       
        if ( aRequest.getRequest( ).containsExtra( "only-if-cached" ) )
        {
          this.logger.info( "Request {} must be satisfied by the cache, because of cache-control", aRequest.getRequest( ).getMessageId( ) );
          sendErrorMessage( aRequest );
        }
        else //else, send the request to the server, unmodified.
        {
          this.logger.info( "Request {} is being sent to the server", aRequest.getRequest( ).getMessageId( ) );
          final ProxyRequestMessage request = aRequest.getRequest( ).copy( );
          sendServerRequest( aRequest, request );
        }
      }
    }
    else //else,
    {      
      this.logger.info( "Request {} can't be verified by the cache. ", aRequest.getRequest( ).getMessageId( ) );
      if ( aRequest.getRequest( ).containsExtra( "only-if-cached" ) )
      {
        this.logger.info( "Request {} must be satisfied by the cache, because of cache-control", aRequest.getRequest( ).getMessageId( ) );
        sendErrorMessage( aRequest );
      }
      else //else, make the request to the server
      {   
        this.logger.info( "Request {} is being sent to the server", aRequest.getRequest( ).getMessageId( ) );
        final ProxyRequestMessage request = aRequest.getRequest( ).copy( );
        
        request.removeHeader( "If-Match" );
        request.removeHeader( "If-None-Match" );
        request.removeHeader( "If-Modified-Since" );
        request.removeHeader( "If-Unmodified-Since" );
        
        this.sendServerRequest( aRequest, request );
      }
    }
  }

  /**
   * Method to process an received server response.
   * @param aRequest The request with the received response.
   */
  private void processReceivedResponse( final ProxyPageElement aRequest )
  {
    //When the request is received from a server
    final ProxyResponseMessage serverResponse = aRequest.getTransitoryResponse( );
    
    this.logger.debug( "Request {} is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getMethod( ) );
    
    //If the method of the request is get, then
    if ( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      
      final boolean match = checkCondition( aRequest, serverResponse );
      
      this.logger.debug( "For request {}, is there a conditional match? {} ", aRequest.getRequest( ).getMessageId( ), Boolean.toString( match ) );
      
      //If we can cache the response...
      if( RequestType.isCacheable( aRequest ) )
      {
        this.logger.info( "Caching the response for request {} ", aRequest.getRequest( ).getMessageId( ) );
        
        //If the entry is in cache,
        final CacheResponse cachedresponse = getCachedPage( aRequest );
        
        if( cachedresponse != null )
        {
          this.logger.debug( "Cache response code is {}", cachedresponse.getResponse( ).toString( ) );
        }
        
        if ( ( cachedresponse != null ) && ( cachedresponse.getResponse( ) == com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponseOperation.OK ) )
        {
          //we replace it by the new entry,
          final ProxyPageElement element = cachedresponse.getResponseElement( );
          element.setResponse( serverResponse.copy( ) );
          element.getResponse( ).removeHeader( "Content-Transfer" );
          
          element.getRequest( ).removeHeader( "If-Match" );
          element.getRequest( ).removeHeader( "If-None-Match" );
          element.getRequest( ).removeHeader( "If-Modified-Since" );
          element.getRequest( ).removeHeader( "If-Unmodified-Since" );
          
          this.changeCachedPage( element );
        }
        else //else,
        {
          //if the response is cacheable, we cache it
          final ProxyPageElement element = new ProxyPageElement( );
          element.setRequest( aRequest.getRequest( ).copy( ) );
          element.setResponse( serverResponse.copy( ) );
          element.setState( ProxyRequestState.ARCHIVED );
          
          element.emptyTransitoryData( );
          
          //empty the request message, so that we don't cache it.
          element.getRequest( ).setContentString( "" );
          
          element.getRequest( ).removeHeader( "Content-Transfer" );
          element.getRequest( ).removeHeader( "If-Match" );
          element.getRequest( ).removeHeader( "If-None-Match" );
          element.getRequest( ).removeHeader( "If-Modified-Since" );
          element.getRequest( ).removeHeader( "If-Unmodified-Since" );
          
          element.getResponse( ).removeHeader( "Content-Transfer" );
          
          this.addCachedPage( element );
        }      
      }
      
      //If there is an match in the conditional request, we
      if( match )
      {
        this.logger.info( "Page in request {} is not modified.",  aRequest.getRequest( ).getMessageId( ) );
        sendNotModifiedResponse( aRequest );      
      }  
      else //else, we send the received response
      {
        this.logger.info( "Page in request {} is modified.",  aRequest.getRequest( ).getMessageId( ) );
        sendReceivedResponse( aRequest, serverResponse );
      }      
    }
    else //else, if the method is not get
    {
      this.logger.info( "Sending the received response for request {}",  aRequest.getRequest( ).getMessageId( ) );
      sendReceivedResponse( aRequest, serverResponse );
    }
  }

  /**
   * Sends the response received from the server.
   * @param aRequest The page request.
   * @param serverResponse The received response.
   */
  private void sendReceivedResponse( final ProxyPageElement aRequest, final ProxyResponseMessage serverResponse )
  {
    //we send the received response
    aRequest.setResponse( serverResponse );
    aRequest.setState( ProxyRequestState.PROCESSED );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    this.logger.debug( "Request {} dock is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getDockName( ) );
    aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
        
    aRequest.getResponse( ).setMessageId( aRequest.getRequest( ).getMessageId( ) );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }

  /**
   * Sends an not modified response to the client.
   * @param aRequest The page request.
   */
  private void sendNotModifiedResponse( final ProxyPageElement aRequest )
  {
    //send an 304 response to the client
    final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
    response.addHeader( "Date", DateTime.now( ).toString( JellyfishConstants.HTTP_FORMATTER ) );
    response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
    response.setResponseCode( ProxyResponseCode.create( JellyfishConstants.HTTPErrorCodes.NOT_MODIFIED.errorcode, "Not Modified" ) );
    this.sendReceivedResponse( aRequest, response );
  }

  /**
   * Checks if the conditions in the request hold.
   * @param aRequest The page of the request.
   * @param responseMessage The response message to use in the verification.
   * @return True, if they hold.
   */
  private boolean checkCondition( final ProxyPageElement aRequest, final ProxyResponseMessage responseMessage )
  {
    boolean match1 = true;
    boolean match2 = true;
    boolean match3 = true;
    boolean match4 = true;
    
    //Check for an If-Match condition
    if ( aRequest.getRequest( ).containsHeader( "If-Match" ) )
    {
      this.logger.debug( "There is an If-Match conditional request." );
      //Get the existing etag
      String etag = "";
      if ( responseMessage.containsHeader( "ETag" ) )
      {
        this.logger.debug( "There is an E-tag in the request." );
        etag = responseMessage.getHeader( "Etag" );
      }
      
      //Check if the tag does not match
      match1 = !this.ifMatchVerifier( aRequest.getRequest( ).getHeader( "If-Match" ), etag );
    }

    //Check for an If-None-Match condition
    if ( aRequest.getRequest( ).containsHeader( "If-None-Match" ) )
    {
      this.logger.debug( "There is an If-None-Match conditional request." );
      //Get the existing etag
      String etag = "";
      if ( responseMessage.containsHeader( "ETag" ) )
      {
        this.logger.debug( "There is an E-tag in the request." );
        etag = responseMessage.getHeader( "ETag" );
      }
      
      //Check if the tag matchs
      match2 = this.ifMatchVerifier( aRequest.getRequest( ).getHeader( "If-None-Match" ), etag );
    }
    
    //Check for an If-Modified-Since condition
    if ( aRequest.getRequest( ).containsHeader( "If-Modified-Since" ) )
    {
      this.logger.debug( "There is an If-Modified-Since conditional request." );
      //Get the existing modified since
      String modifiedDate = "";
      if ( responseMessage.containsHeader( "Last-Modified" ) )
      {
        this.logger.debug( "There is an Last Modified in the request." );
        modifiedDate = responseMessage.getHeader( "Last-Modified" );
      }
      
      //Check if the date does not match
      match3 = LastModifiedEquality.instance( ).equals( aRequest.getRequest( ).getHeader( "If-Modified-Since" ), modifiedDate );
    }
    
    //Check for an If-Unmodified-Since condition
    if ( aRequest.getRequest( ).containsHeader( "If-Unmodified-Since" ) )
    {
      this.logger.debug( "There is an If-Unmodified-Since conditional request." );
      
      //Get the existing modified since
      String modifiedDate = "";
      if ( responseMessage.containsHeader( "Last-Modified-Since" ) )
      {
        this.logger.debug( "There is an Last Modified in the request." );
        modifiedDate = responseMessage.getHeader( "Last-Modified-Since" );
      }
      
      //Check if the date matches
      match4 = !LastModifiedEquality.instance( ).equals( aRequest.getRequest( ).getHeader( "If-Unmodified-Since" ), modifiedDate );
    }
    
    return match1 && match2 && match3 && match4; 
  }
  
  /**
   * Sends an request to the server.
   * @param aRequest The request page.
   * @param request The request to send.
   */
  private void sendServerRequest( final ProxyPageElement aRequest, final ProxyRequestMessage request )
  {
    this.logger.info( "Preparing request {} to be sent to the server", aRequest.getRequest( ).getMessageId( ) );
    
    URL host;
    int port = JellyfishConstants.DEFAULT_HTTP_PORT;
    try
    {
      host = new URL( aRequest.getRequest( ).getCompleteUri( ) );
      port = host.getPort( );
      if ( port == -1 )
      {
        port = JellyfishConstants.DEFAULT_HTTP_PORT;
      }
    }
    catch( final MalformedURLException e )
    {
      //Do nothing
    }
    
    this.logger.debug( "Server port for request {} is {}", aRequest.getRequest( ).getMessageId( ), port );
    
    //Notice that since the server port can be different than the standard 80,
    //we will extract the port in order to use it in the HTTPServerSender channel.
    request.addExtra( "server-port", Integer.toString( port ) );
    aRequest.setTransitoryData( request );
    
    aRequest.setState( ProxyRequestState.MORE_INFO );
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} was sent to server", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }

  /**
   * Sends an error message to the client.
   * @param aRequest The request page.
   */
  private void sendErrorMessage( final ProxyPageElement aRequest )
  {
    //We return an error message
    final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
    response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
    response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
    response.setResponseCode( 
        ProxyResponseCode.create( JellyfishConstants.HTTPErrorCodes.GATEWAY_TIMEOUT.errorcode, 
            "Gateway Timeout" ) );
    
    response.setContentString( response.getNativeResponseCode( ).getHtmlPage( null ) );
    
    this.sendReceivedResponse( aRequest, response );
  }

  /**
   * Checks if any of the given etags matches this one.
   * @param header The etag values.
   * @param etag The etag to each of them to.
   * @return True, if any of them matches.
   */
  private boolean ifMatchVerifier( final String header, final String etag )
  {
    for ( String value : header.split( "," ) )
    {
      if ( value.equals( "*" ) ) 
      { 
        return true; 
      }
      
      if ( StrongEtagEquality.instance( ).isValidator( value.replace( "\"", "" ) ) )
      {
        if ( StrongEtagEquality.instance( ).equals( value.replace( "\"", "" ), etag.replace( "\"", "" ) ) ) 
        { 
          return true; 
        }
      }
      if ( WeakEtagEquality.instance( ).isValidator( value.replace( "\"", "" ) ) )
      {
        if ( WeakEtagEquality.instance( ).equals( value.replace( "\"", "" ), etag.replace( "\"", "" ) ) ) 
        { 
          return true; 
        }
      }
    }
    return false;
  }
  
  /**
   * Returns the page in cache, if existing.
   * @param aRequest The request to use for the query.
   * @return The page in cache or null, if something happens.
   */
  private CacheResponse getCachedPage( final ProxyPageElement aRequest )
  {
    CacheResponse response = null;
    final CacheQuery query = new CacheQuery(CacheQueryOperation.QUERY,
      aRequest.getCurrentUUID(), aRequest.getRequest().getMethod(),
      aRequest.getRequest().getCompleteUri(), "http");
    try
    {
      CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( "The standard cache consistency was interrupted.", ex );
    }
    
    try
    {
      response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( "The standard cache consistency was interrupted.", ex );
    }
    
    return response;
  }
  
  /**
   * Removes the page in cache, if existing.
   * @param aRequest The request to use for the query.
   * @return The query response or null, if something happens.
   */
  private CacheResponse removeCachedPage( final ProxyPageElement aRequest )
  {
    CacheResponse response = null;
    final CacheQuery query = new CacheQuery(CacheQueryOperation.REMOVE,
      aRequest.getCurrentUUID(), aRequest.getRequest().getMethod(),
      aRequest.getRequest().getCompleteUri(), "http");
    try
    {
      CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( "The standard cache consistency was interrupted.", ex );
    }
    
    try
    {
      response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( "The standard cache consistency was interrupted.", ex );
    }
    
    return response;
  }
  
  /**
   * Changes the page in cache, to the given one.
   * @param aRequest The request to use for the query.
   */
  private void changeCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.CHANGE, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return;
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
    }
  }
  
  /**
   * Adds the page to the cache.
   * @param aRequest The request to use for the query.
   */
  private void addCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.ADD, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return;
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
    }
  }
  
  /**
   * Returns the id of this proxy, that is "Conditional Cache Proxy Action".
   * @return The id of this proxy.
   */
  @Override( )
  public String getActionID( )
  {
    return "Conditional Cache Proxy Action";
  }
}
