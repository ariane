/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.html.visitors;

/**
 * Enumeration representing the type of link tag visited.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public enum VisitedLinkType
{
  /**
   * The visited tag was an a link.
   */
  ALINK,
  
  /**
   * The visited tag was an image tag.
   */
  IMAGE,
  
  /**
   * The visited tag was an script tag.
   */
  SCRIPT,
  
  /**
   * The visited tag was an frame tag.
   */
  FRAME,
  
  /**
   * The visited tag was an link tag.
   */
  LINK, 
  
  /**
   * The visited tag was unknown.
   */
  UNKNOWN
}
