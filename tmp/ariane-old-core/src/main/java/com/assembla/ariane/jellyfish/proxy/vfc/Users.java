/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.vfc;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import com.assembla.ariane.jellyfish.util.collections.Pair;

/**
 * This class contains an list of users.
 * @author Fat Cat
 * @version 1
 * @since 0.0.7
 */
public final class Users implements Serializable
{
  
  /** The class version. */
  private static final long serialVersionUID = -6247057140716941819L;

  /** The list of users. */
  private List<User> users;
  
  /** The singleton object. */
  private static final Users inst = new Users( );
  
  /**
   * Returns the instance of this singleton. 
   * @return The users singleton.
   */
  public static Users instance( )
  {
    return Users.inst;
  }
  
  /**
   * Creates a new user list.
   */
  private Users() 
  { 
    this.users = Collections.synchronizedList( new LinkedList< User >( ) );
  }
  
  /**
   * Returns an read-only list of all users.
   * @return An list of all users.
   */
  public synchronized List< User > getUsers( )
  {
    return Collections.unmodifiableList( this.users );
  }
  
  /**
   * Checks if the given user name is known.
   * @param username The name of the user to look for.
   * @return True, if the user exists.
   */
  public synchronized boolean hasUserName( final String username )
  {
    for( User user : this.users )
    {
      if( user.getUserName( ).equals( username ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Returns the user with the given name, or null if the user does not exist.
   * @param username The name of the user to return.
   * @return The corresponding user.
   */
  public synchronized User getUserFor( final String username )
  {
    for( User user : this.users )
    {
      if( user.getUserName( ).equals( username ) )
      {
        return user;
      }
    }
    return null;
  }
  
  /**
   * Adds a user to the list of all users. 
   * @param user The user to add to the list.
   */
  public synchronized void addUser( final User user )
  {
    if( ( user != null ) &&
        ( user.getPivots( ).isEmpty( ) ) &&
        !this.users.contains( user ) )
    {
      this.users.add( user );
    }
  }
  
  /**
   * Removes the given user from the user list.
   * @param user The user to remove.
   */
  public synchronized void removeUser( final User user )
  {
    if( ( user != null ) && this.users.contains( user ) )
    {
      this.users.remove( user );
    }
  }

  /**
   * Checks if the needed page needs remapping.
   * @param page The page to check.
   * @return True, if the page needs remapping.
   */
  public boolean isRemapNeeded( final VFCPage page ) 
  {
    for( User user : this.users )
    {
      if( user.isRemapNeeded( page ) )
      {
        return true;
      } 
    }
    return false;
  }
  
  /**
   * Remapps the given page.
   * @param page The page to remap.
   */
  public void remapAsNeeded( final VFCPage page ) 
  { 
    for( User user : this.users )
    {
      user.remapAsNeeded( page ); 
    }
  }
  
  /**
   * Returns an list of pivots for the given page, grouped by user.
   * @param page The page to return the pivot list.
   * @return An pivot list, grouped by user.
   */
  public Map<User, List<Pivot>> getPivotsFor( final VFCPage page ) 
  {  
    final Map< User, List<Pivot> > map = new TreeMap< User, List<Pivot> >( User.USER_NAME_COMPARATOR );
    
    for( User user : this.users )
    { 
      if( user.isPageKnown( page ) )
      {
        map.put( user, user.getPivotsFor( page ) );
      }
    }
    
    return Collections.unmodifiableMap( map );
  }

  /**
   * Returns if the page url is known (and if it is known for the given user or not).
   * @param url The page url to look for.
   * @param user The user that should known about the page.
   * @return An object with the collected information.
   */
  public PageState isPageKnown( final String url, final User user ) 
  { 
    boolean pageKnown = false;
    for( User currentUser : this.users )
    {
      if( currentUser.isPageKnown( url ) )
      {
        if ( User.USER_NAME_COMPARATOR.compare( user, currentUser ) == 0 )
        {
          return PageState.PAGE_KNOWN_FOR_USER;
        }
        else
        {
          pageKnown = true;
        }
      }
    }
    if( pageKnown )
    {
      return PageState.PAGE_KNOWN;
    }
    
    return PageState.PAGE_UNKNOWN;
  }
  
  /**
   * Returns the page with the given url. (Checking also if the given user known the page )
   * @param url The url of the page to return.
   * @param user The user that should known the page.
   * @return An tuple with information about the user containing the page, 
   * and the page with the given url.
   */
  public Pair<PageState, VFCPage> peekPageInformation( final String url, final User user ) 
  { 
    VFCPage page = null;
    VFCPage compPage = null;
    
    for( User currentUser : this.users )
    {
      compPage = currentUser.peekPageInformation( url );
      if( compPage != null )
      {
        if ( User.USER_NAME_COMPARATOR.compare( user, currentUser ) == 0 )
        {
          return new Pair<PageState, VFCPage>( PageState.PAGE_KNOWN_FOR_USER, compPage );
        }
        else
        {
          page = compPage;
        }
      }
    }
    if( page != null )
    {
      return new Pair<PageState, VFCPage>( PageState.PAGE_KNOWN, page );
    }
    return new Pair<PageState, VFCPage>( PageState.PAGE_UNKNOWN, null );
  }  
}
