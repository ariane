/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.standard;

import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * An proxy action/processor that implements an unifying action/processor
 * that handles the standard http caching protocol.
 * @author Fat Cat
 * @since 0.0.6
 */
public class StandardHTTPProxyAction extends ProxyAction
{
  
  /** Array to contain all the embedded actions/processors. */
  private ProxyAction[ ] proxyAction;
  
  /**
   * Creates a new transparent proxy.
   * @param successor The next proxy action processor.
   * @param config The configuration to use for this transparent proxy.
   */
  public StandardHTTPProxyAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( StandardHTTPProxyAction.class );
    this.proxyAction = new ProxyAction[ 4 ];
    this.proxyAction[ 0 ] = new StandardRequestProxyAction( null, config );
    this.proxyAction[ 1 ] = new RangeRequestProxyAction( null, config );
    this.proxyAction[ 2 ] = new NoCacheProxyAction( null, config );
    this.proxyAction[ 3 ] = new ConditionalCacheProxyAction( null, config );
  }
  
  /**
   * Checks if this processor can handle the given request.&nbsp;
   * This processor always returns true.
   * @param aRequest The request to check for.
   * @return Always true.
   * @throws IllegalArgumentException Never thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return true;
  }
  
  /**
   * This method handles a request, by asking the original server, for
   * a response and directly send it to the client AKA a fully transparent proxy.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {    
    for( ProxyAction httpaction : this.proxyAction )
    {      
      if( httpaction.canHandle( aRequest ) )
      {
        httpaction.handle( aRequest );
        return;
      }      
    }    
  }
  
  /**
   * Returns the id of this processor.
   * @return The processor id that is "Standard HTTP Proxy Action".
   */
  @Override( )
  public String getActionID( )
  {
    return "Standard HTTP Proxy Action";
  }
}
