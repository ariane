/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions;

import java.net.MalformedURLException;
import java.net.URL;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * An proxy action/processor that implements an fully transparent proxy without
 * touching or caching the request.
 * @author Fat Cat
 * @since 0.0.2
 */
public class TransparentProxyAction extends ProxyAction
{
  
  /**
   * Creates a new transparent proxy.
   * @param successor The next proxy action processor.
   * @param config The configuration to use for this transparent proxy.
   */
  public TransparentProxyAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( TransparentProxyAction.class );
  }
  
  /**
   * Checks if this processor can handle the given request.&nbsp;
   * This processor always returns true.
   * @param aRequest The request to check for.
   * @return Always true.
   * @throws IllegalArgumentException Never thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return true;
  }
  
  /**
   * This method handles a request, by asking the original server, for
   * a response and directly send it to the client AKA a fully transparent proxy.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    try
    {
      final Object[ ] args = { aRequest.getCurrentUUID( ), aRequest.getRequest( ).getUri( ), aRequest.getState( ).toString( ) };
      
      this.logger.info( "Received an request:\nID: {}\nURI: {}\nState: {}", args );
      
      //Check if this is the first round...
      if ( aRequest.getState( ).equals( ProxyRequestState.PROCESSING ) )
      {
        this.handleMoreInfoRequest( aRequest );
      }
      else if ( aRequest.getState( ).equals( ProxyRequestState.MORE_INFO_RECEIVED ) )
      {
        this.handleMoreInfoReceivedRequest( aRequest );
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Error in the transparent proxy action.", ex );
    }
  }
  
  /**
   * Method called whenever a response is received from a server.
   * @param aRequest The request with the server answer.
   */
  private void handleMoreInfoReceivedRequest( final ProxyPageElement aRequest )
  {
    //Place the received response as this request response and send it... 
    final ProxyResponseMessage response = aRequest.getTransitoryResponse( );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    response.setDockName( aRequest.getRequest( ).getDockName( ) );
    
    //Because:
    // 1) The server response content is always uncompressed and in one piece 
    //    (aka not chunked) at this point;
    // 2) Since the client may support compression, we will try to send it an 
    //    compressed response, even if the request was received uncompressed.
    if ( aRequest.getRequest( ).containsHeader( "Accept-Content" ) )
    {
      response.addHeader( "Accept-Content", aRequest.getRequest( ).getHeader( "Accept-Content" ) );
    }
    
    aRequest.setResponse( response );
    aRequest.setState( ProxyRequestState.PROCESSED );
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Method called whenever the request is new.
   * @param aRequest The request needing more information from the server.
   */
  private void handleMoreInfoRequest( final ProxyPageElement aRequest )
  {
    //If it is place the request as a server request and ask for more info...
    final ProxyRequestMessage request = aRequest.getRequest( );
    
    URL host;
    int port = JellyfishConstants.DEFAULT_HTTP_PORT;
    try
    {
      host = new URL( request.getCompleteUri( ) );
      port = host.getPort( );
      if ( port == -1 )
      {
        port = JellyfishConstants.DEFAULT_HTTP_PORT;
      }
    }
    catch( final MalformedURLException e )
    {
      //Do nothing
    }
    
    //Notice that since the server port can be different than the standard 80,
    //we will extract the port in order to use it in the HTTPServerSender channel.
    request.addExtra( "server-port", Integer.toString( port ) );
    aRequest.setTransitoryData( request );
    aRequest.setState( ProxyRequestState.MORE_INFO );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Returns the id of this processor.
   * @return The processor id that is "Transparent Proxy Action".
   */
  @Override( )
  public String getActionID( )
  {
    return "Transparent Proxy Action";
  }
}
