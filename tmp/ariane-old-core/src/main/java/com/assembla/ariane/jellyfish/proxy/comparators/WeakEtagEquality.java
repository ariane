/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.comparators;

/**
 * Compares two validators, using the weak etag validation. 
 * @author Fat Cat
 * @since 0.0.6
 * @version 1
 */
public final class WeakEtagEquality implements Equality< String >
{
  
  /** The singleton object. */
  private static final Equality< String > inst = new WeakEtagEquality();
  
  /**
   * Returns the last modified equality operator singleton.
   * @return The last modified operator singleton.
   */
  public static Equality< String > instance( )
  {
    return WeakEtagEquality.inst;
  }
  
  /**
   * Creates a new weak etag validator.
   */
  private WeakEtagEquality( )
  {
    //Does nothing...
  }
  
  /**
   * Checks if both validators are equal.
   * @param first The first validator.
   * @param second The second validator.
   * @return True, if they are.
   * @see com.assembla.ariane.jellyfish.proxy.comparators.Equality#equals(java.lang.Object, java.lang.Object)
   */
  @Override( )
  public boolean equals( final String first, final String second )
  {
    //Check if both validators are weak etags...
    if( this.isValidator( first ) && this.isValidator( second ) )
    {      
      
      //Trim the W/ or w/ substring from any of the validators, since it does not matter
      //for comparing weak e-tags.
      final String firstValTrimmed = ( first.startsWith( "W/" ) || first.startsWith( "w/" ) ) ? first.substring( 1 ) : first;
      final String secValTrimmed = ( second.startsWith( "W/" ) || second.startsWith( "w/" ) ) ? second.substring( 1 ) : second;
      
      //weak etags should be strings with the same size...
      if( firstValTrimmed.length( ) != secValTrimmed.length( ) )
      {
        return false;
      }
      else
      {        
        //Compare characters bit by bit...
        for( int i = 0; i < firstValTrimmed.length( ); i++ )
        {
          if( firstValTrimmed.charAt( i ) != secValTrimmed.charAt( i ) )
          {
            return false;
          }
        }        
        return true;
      }
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Checks if the given validator is a weak e-tag validator or a compatible one.
   * @param candidate The validator to check for.
   * @return True, if the validator is a weak e-tag or compatible validator.
   * @see com.assembla.ariane.jellyfish.proxy.comparators.Equality#isValidator(java.lang.Object)
   */
  @Override( )
  public boolean isValidator( final String candidate )
  {
    if ( candidate.matches( "([a-zA-Z]{3}),\\s([0-9]{2})\\s([a-zA-Z]{3})\\s([0-9]{4})\\s([0-9]{2}):([0-9]{2}):([0-9]{2})\\sGMT" ) )
    {
      //This is a date field...
      return false;
    }
    else 
    {
      //This is a weak or compatible e-tag, by exclusion...
      return true;
    }
  }
  
}
