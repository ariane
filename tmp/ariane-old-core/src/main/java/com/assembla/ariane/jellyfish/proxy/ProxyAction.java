/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy;

import java.util.Objects;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.perfectjpattern.core.behavioral.chainofresponsibility.AbstractHandler;
import org.slf4j.Logger;

/**
 * Class that defines an default proxy action, that receives an page element
 * and transforms it.
 * 
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 */
public abstract class ProxyAction extends AbstractHandler< ProxyPageElement >
{
  /** The processor configuration. */
  protected HierarchicalConfiguration config;
  
  /** The logger object. */
  protected Logger logger;
  
  /**
   * Creates a new processor.
   * @param successor The next processor in the chain.
   * @param config The configuration to use for this processor.
   */
  public ProxyAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    if ( successor != null )
    {
      this.setSuccessor( successor );
    }
    this.config = config;
  }
  
  /**
   * Checks if the processor can handle the given request.
   * @param aRequest The request to check.
   * @return True, if the request can be handled.
   * @throws IllegalArgumentException If some error happens.
   * @see org.perfectjpattern.core.behavioral.chainofresponsibility.AbstractHandler#canHandle(java.lang.Object)
   */
  @Override( )
  public abstract boolean canHandle( ProxyPageElement aRequest ) throws IllegalArgumentException;
  
  /**
   * Handles the given request.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens. 
   * @see org.perfectjpattern.core.behavioral.chainofresponsibility.AbstractHandler#handle(java.lang.Object)
   */
  @Override( )
  public abstract void handle( ProxyPageElement aRequest ) throws IllegalArgumentException;
  
  /**
   * Checks if two processors are equal.
   * @param action The other processor.
   * @return True, if they are.
   */
  public boolean equals( final ProxyAction action )
  {
    return this.getActionID( ).equals( action.getActionID( ) );
  }
  
  /**
   * Checks if the objects are equal.
   * @param obj The other object.
   * @return True if they are. 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  {
    if ( obj instanceof ProxyAction ) 
    {
      return this.equals( ( ProxyAction ) obj );
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Calculates an hash code for this processor.
   * @return The processor hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this );
  }
  
  /**
   * Returns the unique identifier of this processor.
   * @return The processor id.
   */
  public abstract String getActionID( );
}
