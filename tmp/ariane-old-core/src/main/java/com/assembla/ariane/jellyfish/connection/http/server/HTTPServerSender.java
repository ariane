/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.server;

import io.netty.bootstrap.ClientBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPipelineFactory;
import io.netty.channel.Channels;
import io.netty.channel.socket.nio.NioClientSocketChannelFactory;
import io.netty.handler.codec.http.HttpChunkAggregator;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpContentDecompressor;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.connection.http.messages.requests.HTTPServerRequest;
import com.assembla.ariane.jellyfish.util.collections.ProxyMessageHolder;
import com.assembla.ariane.jellyfish.util.collections.server.HTTPSentIdHolder;
import com.assembla.ariane.jellyfish.util.collections.server.ServerMessageQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;

/**
 * This class implements an server request sender.
 * @author Fat Cat
 * @version 3
 * @since 0.0.2
 */
public class HTTPServerSender extends AbstractModule implements ChannelPipelineFactory, Callable< Void >
{
  
  private static final int DEFAULT_SLEEP_TIME = 120;

  /** The current port. */
  private final int currentPort;
  
  /** The name of the associated dock. */
  private String dockName;
  
  /** The bootstrap factory object to use. */
  private ClientBootstrap bootstrap;
  
  /**
   * Creates a new http server sender.
   * @param config The configuration of the server sender.
   * @param dockName The name of the associated dock.
   */
  public HTTPServerSender( final HierarchicalConfiguration config, final String dockName )
  {
    super( config );
    this.logger = LoggerFactory.getLogger( HTTPServerSender.class );
    this.currentPort = config.getInt( "port-number", 0 );
    this.logger.info( "The server sender port is {}", this.currentPort );
    this.dockName = dockName;
  }
  
  /**
   * Stops the http server sender.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override( )
  public void stop( )
  {
    this.logger.info( "Trying to stop the server request sender." );
    this.setRunning( false );
  }
  
  /**
   * Starts the http server sender.
   * @return Nothing.
   * @throws Exception If some error happens.
   * @see java.util.concurrent.Callable#call()
   */
  @Override( )
  public Void call( ) throws Exception
  {
    try
    {
      ProxyRequestMessage request = null;
      HTTPServerRequest httpreq = null;
      this.setRunning( true );
      this.logger.info( "Server request sender is started." );
      
      // Configure the bootstrap object
      this.bootstrap = new ClientBootstrap( new NioClientSocketChannelFactory( ) );
      this.bootstrap.setPipelineFactory( this );
      this.logger.debug( "Server request sender is configured." );
      
      while( true )
      {
        request = null;
        
        //Check if the server request queue has any requests
        this.logger.debug( "Checking for server requests." );
        try
        {
          //Select a request...
          while( request == null )
          {  
            request = ServerMessageQueue.instance( ).getRequests( ).poll( 
                JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS );

            if ( terminateInstance() ) { break; }
          }           
        }
        catch( final InterruptedException ex )
        {
          this.logger.debug( "The server receptor is interrupted." );
        }
        
        if ( request != null )
        {
          this.logger.info( "Found a request with id {}", request.getMessageId( ) );
          this.logger.info( "URI is {}", request.getUri( ) );
          httpreq = HTTPServerRequest.addRemainingHeaders( HTTPServerRequest.messageConverter( request ) );
          this.logger.info( "URI of converted request is {}", httpreq.getUri( ) );
          
          try
          {
            final InetAddress addr = InetAddress.getByName( httpreq.getHeader( "Host" ) );
            addr.getHostAddress( ); //In order to trigger an DNS request.
            
            //Notice that since the server port can be different than the standard 80,
            //we will need to use it in the remote socket address, for the connection
            //channel.
            final InetSocketAddress socketAdd = new InetSocketAddress( addr, Integer.parseInt( httpreq.getExtra( "server-port" ) ) );
            
            this.logger.info( "Starting a connection with {}", socketAdd.toString( ) );
            
            //...Add it to the mapping...
            HTTPSentIdHolder.instance( ).add( socketAdd.toString( ), request.getMessageId( ) );
            
            //...Add the request time...
            ProxyMessageHolder.instance( ).setRequestTime( request.getMessageId( ), DateTime.now( ) );

            if ( terminateInstance() ) { break; }
            
            //...Send the request to the server
            final Channel channel = this.bootstrap.connect( socketAdd ).awaitUninterruptibly( ).getChannel( );
            channel.write( httpreq.getNativeRequest( ) );
            while( channel.isConnected( ) )
            {
              Thread.sleep( HTTPServerSender.DEFAULT_SLEEP_TIME );
            }
            channel.close( );

            if ( terminateInstance() ) { break; }

            this.logger.info( "Request written" );
          }
          catch( final UnknownHostException ex )
          {
            this.logger.error( "Couldn't find the local address", ex );
            final ProxyResponseMessage response = new ProxyResponseMessage( request.getMessageId( ) );
            response.addHeader( "Date", DateTime.now( ).toString( JellyfishConstants.HTTP_FORMATTER ) );
            response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
            response.addHeader( "Cache-Control", "no-cache" );
            response.addHeader( "Pragma", "no-cache" );
            response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
            response.setResponseCode( ProxyResponseCode.create(
              JellyfishConstants.HTTPErrorCodes.BAD_GATEWAY.errorcode,
                "Bad Gateway", ex ) );
            
            final StringBuilder sb = new StringBuilder( );
            sb.append( "The specified address does not exit.\n" );
            sb.append( "Please check the address of the website you want." );
            
            response.setContentString( response.getNativeResponseCode( ).getHtmlPage( sb.toString( ) ) );

            if ( terminateInstance() ) { break; }

            ServerMessageQueue.instance( ).getResponses( ).put( response );
          }
        }
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Exception in ResponseDistributorModule", ex );
    }
    return null;
  }

  /**
   * Checks if this server sender needs to stop and stops it.
   * @return True, if the server is stopped, false otherwise.
   */
  private boolean terminateInstance()
  {
    if ( !this.isRunning( ) )
    {
      this.logger.info( "Stopping the server request sender." );
      //Stop the server sender.
      HTTPSentIdHolder.instance().clear( );
      HTTPSentIdHolder.instance().getChannels( ).close( );
      ServerMessageQueue.instance().getRequests( ).clear( );
      if ( this.bootstrap != null )
      {
        this.bootstrap.releaseExternalResources( );
      }
      return true;
    }
    else return false;
  }

  /**
   * Does nothing, since this uses an callable...
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override( )
  public void run( )
  {
  }
  
  /**
   * Returns an new pipeline.
   * @return The configured pipeline.
   * @throws Exception If some error happens.
   * @see io.netty.channel.ChannelPipelineFactory#getPipeline()
   */
  public ChannelPipeline getPipeline( ) throws Exception
  {
    final ChannelPipeline pipeline = Channels.pipeline( );
    
    pipeline.addLast( "HttpClient", new HttpClientCodec( 
        this.config.getInt( 
            "max-header-line-size", JellyfishConstants.DEFAULT_MAX_HEADER_LINE_SIZE ), 
        this.config.getInt( 
            "max-header-size", JellyfishConstants.DEFAULT_MAX_HEADER_SIZE ), 
        this.config.getInt( 
            "max-content-size", JellyfishConstants.DEFAULT_MAX_CONTENT_SIZE ), 
        false ) );
    
    pipeline.addLast( "HttpDecompressor", new HttpContentDecompressor( ) );
    
    pipeline.addLast( "HttpChunkAggregator", new HttpChunkAggregator( 
        this.config.getInt( 
            "max-message-size", JellyfishConstants.DEFAULT_MAX_MESSAGE_SIZE ) ) );
    
    pipeline.addLast( "com.assembla.ariane.jellyfish-server", new HTTPServerChannelHandler( this.dockName ) );
    return pipeline;
  }
}
