/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages.requests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.UUID;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyMessage;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import org.joda.time.DateTime;

/**
 * This class represents an generic proxy request message.
 * @author Fat Cat
 * @version 3
 * @since 0.0.1
 */
public class ProxyRequestMessage extends ProxyMessage implements Serializable, GenericRequest
{
  
  /** The version of this request message. */
  private static final long serialVersionUID = -9085298100215615495L;
  
  /**
   * Creates a new empty proxy message given the requestId.
   * @param requestId the id of the proxy message.
   */
  public ProxyRequestMessage( final UUID requestId )
  {
    super( requestId );
  }
  
  // Method
  
  /**
   * Returns the method used by the request.
   * @return The request method.
   * @see com.assembla.ariane.jellyfish.proxy.messages.requests.GenericRequest#getMethod()
   */
  @Override( )
  public String getMethod( )
  {
    if ( this.extra.containsKey( "Method" ) )
    {
      return this.extra.get( "Method" );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }
  
  /**
   * Sets the request method.
   * @param method The new request method.
   * @see com.assembla.ariane.jellyfish.proxy.messages.requests.GenericRequest#setMethod(java.lang.String)
   */
  @Override( )
  public void setMethod( final String method )
  {
    if ( method != null )
    {
      this.extra.put( "Method", method );
    }
  }

  // URI
  
  /**
   * Returns the URI of the request.
   * @return The request URI.
   * @see com.assembla.ariane.jellyfish.proxy.messages.requests.GenericRequest#getUri()
   */
  @Override( )
  public String getUri( )
  {
    if ( this.extra.containsKey( "URI" ) )
    {
      return this.extra.get( "URI" );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }
  
  /**
   * Sets the request uri.
   * @param uri The new request uri.
   * @see com.assembla.ariane.jellyfish.proxy.messages.requests.GenericRequest#setUri(java.lang.String)
   */
  @Override( )
  public void setUri( final String uri )
  {
    if ( uri != null )
    {
      this.extra.put( "URI", uri );
    }
  }
  
  // Complete URI
  
  /**
   * Returns the complete URI of the request.
   * @return The request complete URI.
   */
  public String getCompleteUri( )
  {
    if ( this.extra.containsKey( "CompleteURI" ) )
    {
      return this.extra.get( "CompleteURI" );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }
  
  /**
   * Sets the complete request uri.
   * @param uri The new complete request uri.
   */
  public void setCompleteUri( final String uri )
  {
    if ( uri != null )
    {
      this.extra.put( "CompleteURI", uri );
    }
  }
  
  //Request Time
  
  /**
   * Returns the date/time when this request was sent.
   * @return The date/time when this request was sent.
   */
  public DateTime getRequestTime( )
  {
    if ( this.extra.containsKey( "RequestTime" ) )
    {
      try
      {
        return DateTime.parse( this.extra.get( "RequestTime" ) );
      }
      catch( final IllegalArgumentException ex )
      {
        return DateTime.now( );
      }
    }
    else
    {
      return DateTime.now( );
    }
  }
  
  /**
   * Changes the request date/time.
   * @param datetime The new datetime.
   */
  public void setRequestTime( final DateTime datetime )
  {
    if ( datetime != null )
    {
      this.extra.put( "RequestTime", datetime.toString( ) );
    }
  }
  
  /**
   * Checks if this request has an RequestTime value.
   * @return True, if it has.
   */
  public boolean hasRequestTime( )
  {
    return this.extra.containsKey( "RequestTime" );
  }
  
  // Utilities
  
  /**
   * Clones this proxy request message.
   * @return A new identical proxy request message.
   */
  public ProxyRequestMessage copy()
  {    
    try //main clone method uses serialization
    {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      final ObjectOutputStream out = new ObjectOutputStream( baos );
      out.writeObject( this );
      
      final ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray( ) );
      final ObjectInputStream ois = new ObjectInputStream( bais );
      return ( ProxyRequestMessage ) ois.readObject( );
    }
    catch( final IOException | ClassNotFoundException e ) //An alternate clone method, just in case...
    {
      final ProxyRequestMessage request = new ProxyRequestMessage( this.messageUUID );
      
      request.setContent( this.getContent( ).copy( ) );
      request.setDockName( this.getDockName( ) );
      request.setMethod( this.getMethod() );
      request.setProtocol( ProxyProtocol.create( this.proxyProtocol.getMajor( ), this.proxyProtocol.getMinor( ), this.proxyProtocol.getProtocolName( ) ) );
      request.setRequestTime( DateTime.parse( this.getRequestTime( ).toString( ) ) );
      request.setUri( this.getUri( ) );
      
      //Add the headers
      for( String header : this.getHeaderNames( ) )
      { 
        for( String value : this.getHeaders( header ) )
        { 
          request.addHeader( header, value );
        }
      }
      
      //Add the extra
      for( String extraname : this.getExtraValueNames( ) )
      {
        request.addExtra( extraname, this.getExtra( extraname ) );
      }
      
      //Add the ranges
      for( Pair<Integer, Integer> range : this.getRanges( ) )
      {
        request.addRange( range );
      }
      
      return request;
    }
  }
  
  /**
   * Removes all hop-to-hop headers.
   */
  public void removeHopToHopHeaders( )
  {
    for( String headerName : this.hopHeaders )
    {
      this.removeHeader( headerName );
    }    
  }
  
}
