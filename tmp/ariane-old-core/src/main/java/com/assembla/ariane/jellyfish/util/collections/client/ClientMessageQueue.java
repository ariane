/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.client;

import com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.slf4j.LoggerFactory;

/**
 * This class implements an queue of http client requests.
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 */
public final class ClientMessageQueue extends JellyfishQueue< ProxyRequestMessage, ProxyResponseMessage >
{
  
  /** The singleton. */
  private static final ClientMessageQueue inst = new ClientMessageQueue( );
  
  /**
   * Method to return this singleton.
   * @return The singleton.
   */
  public static ClientMessageQueue instance( )
  {
    return ClientMessageQueue.inst;
  }
  
  /**
   * Private constructor to create a new queue.
   */
  private ClientMessageQueue( )
  {
    super( );
    this.logger = LoggerFactory.getLogger( ClientMessageQueue.class );
  }
  
  /**
   * Method that returns the size of the request queue.
   * @return The size of the request queue (1024).
   * @see com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue#sendSize()
   */
  @Override( )
  protected int sendSize( )
  {
    return JellyfishConstants.DEFAULT_QUEUE_SIZE;
  }
  
  /**
   * Method that returns the size of the response queue.
   * @return The size of the response queue (1024).
   * @see com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue#receiveSize()
   */
  @Override( )
  protected int receiveSize( )
  {
    return JellyfishConstants.DEFAULT_QUEUE_SIZE;
  }
}
