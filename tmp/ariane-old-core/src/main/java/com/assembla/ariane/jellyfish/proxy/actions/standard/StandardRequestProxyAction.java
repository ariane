/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.standard;

import io.netty.buffer.ChannelBuffers;
import java.net.MalformedURLException;
import java.net.URL;
import com.assembla.ariane.jellyfish.util.collections.cache.CacheMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.RequestType;
import com.assembla.ariane.jellyfish.proxy.exceptions.ArgumentException;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.WarningValue;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQueryOperation;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponse;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * An proxy action processor that implements an fully transparent proxy without
 * touching or caching the request.
 * @author Fat Cat, Alex
 * @since 0.0.6
 * @version 2
 */
public class StandardRequestProxyAction extends ProxyAction
{
  
    /**
   * Creates a new transparent proxy.
   * @param successor The next proxy action processor.
   * @param config The configuration to use for this transparent proxy.
   */
  public StandardRequestProxyAction( final IHandler< ProxyPageElement > successor,
                                     final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( StandardRequestProxyAction.class );
  }
  
  /**
   * Checks if this processor can handle the given request.<br/>
   * Only returns true if:
   * <ol>
   *   <li>The request is not conditional.</li>
   *   <li>The request is not an range request.</li>
   *   <li>The request is cacheable</li>
   *   <li>The request is a get or head request</li>
   * </ol>
   * The two conditions above are handled somewhere else, while
   * the other two conditions are handled using an transparent proxy mode.
   * @param aRequest The request to check for.
   * @return True, if the conditions above are satisfied.
   * @throws IllegalArgumentException Never thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return RequestType.isCacheableRequest( aRequest ) &&
           RequestType.isGetOrHeadRequest( aRequest ) &&
           !RequestType.isConditionalRequest( aRequest ) &&
           !RequestType.isRangeRequest( aRequest );
  }
  
  /**
   * This method handles a request, by asking the original server, for
   * a response and directly send it to the client AKA a fully transparent proxy.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    try
    {
      final Object[ ] args = { aRequest.getCurrentUUID( ), aRequest.getRequest( ).getUri( ), aRequest.getState( ).toString( ) };
      
      this.logger.info( "Received an request: { ID: {}, URI: {}, State: {}",
                        args );
      
      //When a request is new, we:
      if ( aRequest.getState( ).equals( ProxyRequestState.PROCESSING ) )
      {
        this.logger.debug( "The request {} is new.", aRequest.getRequest( ).getMessageId( ) );
        processNewRequest( aRequest );
      }
      else if ( aRequest.getState( ).equals( ProxyRequestState.MORE_INFO_RECEIVED ) )
      {
        this.logger.debug( "The request {} is from the server.", aRequest.getRequest( ).getMessageId( ) );
        //When the request is received from a server, we:
        processReceivedResponse( aRequest );
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Error in the standard request proxy action.", ex );
    }
  }
  
  /**
   * Method to handle a new request.
   * @param aRequest The request to handle.
   */
  private void processNewRequest( final ProxyPageElement aRequest )
  {
    //remove any hop-by-hop headers,
    this.logger.debug( "Removing hop-to-hop headers from request {}", aRequest.getRequest( ).getMessageId( ) );
    aRequest.removeHopToHopHeaders( true );
    
    //check if the request is in cache,
    final CacheResponse cachedResponse = getCachedPage( aRequest );
    
    if( cachedResponse != null )
    {
      this.logger.debug( "Cache response code is {}", cachedResponse.getResponse( ).toString( ) );
    }
    
    //if the request is in cache
    if ( ( cachedResponse != null ) && ( cachedResponse.getResponse( ) == com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponseOperation.OK ) )
    {
      this.logger.info( "There is an valid cached response for {}", aRequest.getRequest( ).getMessageId( ) );
      
      final ProxyPageElement cachedResponseElement = cachedResponse.getResponseElement( );
      
      if( this.logger.isDebugEnabled( ) )
      {
        StringBuilder tmp = new StringBuilder( );
        tmp.append( "Information for the cached response is:\n" );
        
        if( cachedResponseElement.hasRequest( ) )
        {
          tmp.append( String.format( "Server Request Time: %s\n", cachedResponseElement.getRequest( ).getRequestTime( ).toString( JellyfishConstants.DEBUG_FORMATTER ) ) );
        }
        
        if( cachedResponseElement.hasResponse( ) )
        {
          tmp.append( String.format( "Server Response Time: %s\n", cachedResponseElement.getResponse( ).getResponseTime( ).toString( JellyfishConstants.DEBUG_FORMATTER ) ) );
          tmp.append( String.format( "Corrected Received Age: %d\n", cachedResponseElement.getResponse( ).getCorrectedReceivedAge( ) ) );
        }
        
        tmp.append( String.format( "Corrected Initial Age: %d\n", cachedResponseElement.calculateCorrectedInitialAge( ) ) );
        tmp.append( String.format( "Resident Time: %d\n", cachedResponseElement.calculateResidentTime( ) ) );
        tmp.append( String.format( "Page Current Age: %d\n", cachedResponseElement.calculateCurrentAge( ) ) );
        tmp.append( String.format( "Page Freshness Lifetime: %d\n", cachedResponseElement.calculateFreshnessLifetime( ) ) );
        tmp.append( String.format( "Page Expiration Lifetime: %d\n", cachedResponseElement.calculateExpirationTime( ) ) );
        this.logger.debug( tmp.toString( ) );
        tmp.setLength(0);
      }
      
      //we see if it is fresh and if it is
      if ( cachedResponseElement.calculateFreshnessLifetime( ) > cachedResponseElement.calculateCurrentAge( ) )
      {
        this.logger.info( "Page in cache is fresh for request {}",  aRequest.getRequest( ).getMessageId( ) );
        aRequest.addExtra("cached", "true");
        sendCachedResponse( aRequest, cachedResponseElement );
      }
      else //else,
      {   
        this.logger.info( "Page in cache is NOT fresh for request {}",  aRequest.getRequest( ).getMessageId( ) );
        
        //If the option Only-If-Cached is present
        if ( aRequest.getRequest( ).containsExtra( "only-if-cached" ) )
        {
          //we must return the result to the user...
          this.logger.info( "Request {} must be satisfied by the cache, because of cache-control", aRequest.getRequest( ).getMessageId( ) );
          aRequest.addExtra("cached", "true");
          sendCachedResponse( aRequest, cachedResponseElement );
        }
        else //otherwise, we send an conditional request to the server
        {
          this.logger.info( "Request {} is being sent to the server", aRequest.getRequest( ).getMessageId( ) );
          
          final ProxyRequestMessage request = aRequest.getRequest( ).copy( );
          aRequest.addExtra("in-cache", "true");
          if ( cachedResponseElement.getResponse( ).containsHeader( "ETag" ) )
          {
            this.logger.debug( "Request {} etag is {}", aRequest.getRequest( ).getMessageId( ), cachedResponseElement.getResponse( ).getHeader( "ETag" ) );
            request.addHeader( "If-None-Match", cachedResponseElement.getResponse( ).getHeader( "ETag" ) );
          }
          
          if ( cachedResponseElement.getResponse( ).containsHeader( "Last-Modified" ) )
          {
            this.logger.debug( "Request {} last-modified is {}", aRequest.getRequest( ).getMessageId( ), cachedResponseElement.getResponse( ).getHeader( "Last-Modified" ) );
            request.addHeader( "If-Modified-Since", cachedResponseElement.getResponse( ).getHeader( "Last-Modified" ) );
          }
          
          sendToServer( aRequest, request );
        }
      }
      
    }
    else
    {
      this.logger.info( "There is NO valid cached response for {}", aRequest.getRequest( ).getMessageId( ) );
      
      if ( aRequest.getRequest( ).containsExtra( "only-if-cached" ) )
      {
        this.logger.info( "Request {} must be satisfied by the cache, because of cache-control", aRequest.getRequest( ).getMessageId( ) );
        this.logger.info( "Sending an error response for {}", aRequest.getRequest( ).getMessageId( ) );
        
        //We return an error message
        final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
        response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
        response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
        response.setResponseCode( ProxyResponseCode.create( 
            JellyfishConstants.HTTPErrorCodes.GATEWAY_TIMEOUT.errorcode, "Gateway Timeout" ) );
        
        response.setContentString( response.getNativeResponseCode( ).getHtmlPage( null ) );
        aRequest.setResponse( response );
        aRequest.setState( ProxyRequestState.PROCESSED );
        
        //Because a request can come from any dock, than an response must be 
        //associated to that dock so that it can be sent by the sender associated
        //with the dock.
        this.logger.debug( "Request {} dock is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getDockName( ) );
        aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
        
        try
        {
          ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
          this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
        }
        catch( final InterruptedException e )
        {
          //Do nothing
        }        
      }
      else //else, send a request to the server
      {
        this.logger.info( "Request {} is being sent to the server", aRequest.getRequest( ).getMessageId( ) );
        this.sendToServer( aRequest, aRequest.getRequest( ) );
      }
    }
  }

  /**
   * Sends an cached response to the client.
   * @param aRequest The request to send.
   * @param cachedResponseElement The cached page.
   */
  private void sendCachedResponse( final ProxyPageElement aRequest, final ProxyPageElement cachedResponseElement )
  {
    this.logger.debug( "Request {} is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getMethod( ) );
    
    //If the method is get
    if( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {          
      //we return the full cached page to the user.
      aRequest.setResponse( cachedResponseElement.getResponse( ) );        
    }
    else //else if the method is head. (remember that other methods aren't cacheable)
    {
      //we return only the headers...
      final ProxyResponseMessage headerResponse = cachedResponseElement.getResponse( ).copy( );
      headerResponse.setContent( ChannelBuffers.EMPTY_BUFFER );
      aRequest.setResponse( headerResponse );          
    }
    
    //Add an age header
    if( aRequest.getResponse( ).containsHeader( "Age" ) )
    {
      aRequest.getResponse( ).removeHeader( "Age" );
    }
    aRequest.getResponse( ).addHeader( "Age", Long.toString( cachedResponseElement.calculateCurrentAge( ) ) );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    this.logger.debug( "Request {} dock is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getDockName( ) );
    aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
    aRequest.getResponse( ).setMessageId( aRequest.getRequest( ).getMessageId( ) );
    
    aRequest.setState( ProxyRequestState.PROCESSED );
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }

  /**
   * Sends an request to the server asking for more information.
   * @param aRequest The request page to use. 
   * @param request The request to send.
   */
  private void sendToServer( final ProxyPageElement aRequest, final ProxyRequestMessage request )
  {
    this.logger.info( "Preparing request {} to be sent to the server", aRequest.getRequest( ).getMessageId( ) );
    
    URL host;
    int port = JellyfishConstants.DEFAULT_HTTP_PORT;
    try
    {
      host = new URL( aRequest.getRequest( ).getCompleteUri( ) );
      port = host.getPort( );
      if ( port == -1 )
      {
        port = JellyfishConstants.DEFAULT_HTTP_PORT;
      }
    }
    catch( final MalformedURLException e )
    {
      //Do nothing
    }
    
    this.logger.debug( "Server port for request {} is {}", aRequest.getRequest( ).getMessageId( ), port );
    
    //Notice that since the server port can be different than the standard 80,
    //we will extract the port in order to use it in the HTTPServerSender channel.
    request.addExtra( "server-port", Integer.toString( port ) );
    aRequest.setTransitoryData( request );
    
    aRequest.setState( ProxyRequestState.MORE_INFO );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} was sent to server", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }

  /**
   * Method to process an server response, to a processor request.
   * @param aRequest The request to use.
   */
  private void processReceivedResponse( final ProxyPageElement aRequest )
  {
    //Check if this is a conditional response answer (or bad gateway meaning no connection)
    final int responseCode = aRequest.getTransitoryResponse( ).getResponseCodeAsInteger( );
    
    this.logger.debug( "The response code of request {} is {}", aRequest.getRequest( ).getMessageIdAsString( ), responseCode );
    
    if ( ( responseCode == JellyfishConstants.HTTPErrorCodes.NOT_MODIFIED.errorcode ) && 
         ( responseCode == JellyfishConstants.HTTPErrorCodes.BAD_GATEWAY.errorcode ) ) //if, it is then,
    {
      //we should retrieve the page in cache,
      final CacheResponse cachedresponse = getCachedPage( aRequest );
      
      if ( cachedresponse != null )
      {
        this.logger.debug( "The cache result is {}", cachedresponse.getResponse( ).toString( ) );
        
        if ( responseCode == JellyfishConstants.HTTPErrorCodes.NOT_MODIFIED.errorcode )
        {
          this.logger.info( "Received an 304 response for request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
          
          //remove any 1xx warning fields,
          final ProxyResponseMessage response = cachedresponse.getResponseElement( ).getResponse( ).copy( );
          
          this.logger.info( "Cleaning temporary warning fields from request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
          response.clearTemporaryWarnings( );
          
          //combine the headers of both page responses, meaning that headers present,
          // in the new response replace the ones in the old one.
          this.logger.info( "Combining the received headers from request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
          response.mergeHeaders( aRequest.getTransitoryResponse( ) );
          response.replaceWarnings( aRequest.getTransitoryResponse( ).getWarnings( ) );
          
          //remove any hop-to-hop headers,
          this.logger.info( "Removing hop headers from request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
          response.removeHopToHopHeaders( );
          
          this.logger.debug( "Request {} is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getMethod( ) );
          
          //If the method is get
          if( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
          {
            this.logger.info( "Recaching request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
            
            //recache the obtained result,
            aRequest.setResponse( response );
            aRequest.emptyTransitoryData( );
            
            //empty the request message, so that we don't cache it.
            aRequest.getRequest( ).setContentString( "" );
            aRequest.setState( ProxyRequestState.ARCHIVED );
            
            this.changeCachedPage( aRequest );
          }          
          
          sendClientResponse( aRequest, response );
        }
        else //There is no connection to the server...
        {
          this.logger.info( "There is no server connection for request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
          
          final ProxyResponseMessage message = cachedresponse.getResponseElement( ).getResponse( ).copy( );
          message.addWarning( WarningValue.create( JellyfishConstants.REVALIDATION_FAILED, 
              "Jellyfish", "Revalidation failed" ) );
                   
          sendClientResponse( aRequest, message );
        }
        
      }
      else //send an error response.
      {
        this.logger.warn( "Cached result of request {} was removed!", aRequest.getRequest( ).getMessageIdAsString( ) );
        
        final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
        response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
        response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
        response.setResponseCode( ProxyResponseCode.create( 
            JellyfishConstants.HTTPErrorCodes.GATEWAY_TIMEOUT.errorcode, 
            "Gateway Timeout" ) );
        
        final StringBuilder sb = new StringBuilder( );
        sb.append( "There was an unknown error in the proxy.\n" );
        sb.append( "Please try again after some seconds." );
        
        response.setContentString( response.getNativeResponseCode( ).getHtmlPage( sb.toString( ) ) );
        aRequest.setResponse( response );
        aRequest.setState( ProxyRequestState.PROCESSED );
        
        //Because a request can come from any dock, than an response must be 
        //associated to that dock so that it can be sent by the sender associated
        //with the dock.
        this.logger.debug( "Request {} dock is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getDockName( ) );
        aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
        
        try
        {
          ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
          this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
        }
        catch( final InterruptedException e )
        {
          //do nothing
        }
      }
      
    }
    else //else,
    {
      this.logger.info( "Received a new response for request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
      
      //remove any hop-to-hop headers,
      final ProxyResponseMessage serverResponse = aRequest.getTransitoryResponse( );
      
      this.logger.info( "Removing hop headers from request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
      serverResponse.removeHopToHopHeaders( );
      
      this.logger.debug( "Request {} is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getMethod( ) );
      
      //if the method is get
      if( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) && RequestType.isCacheable( aRequest ) )
      {
        this.logger.info( "Caching new request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
        //cache the new page,
        aRequest.setResponse( serverResponse.copy( ) );
        aRequest.setState( ProxyRequestState.ARCHIVED );
        aRequest.emptyTransitoryData( );
        
        //empty the request message, so that we don't cache it.
        aRequest.getRequest( ).setContentString( "" );
        aRequest.getRequest( ).removeHeader( "Content-Transfer" );
        aRequest.getResponse( ).removeHeader( "Content-Transfer" );
        
        this.addCachedPage( aRequest );
      }
      
      sendClientResponse( aRequest, serverResponse );
    }
  }

  /**
   * Sends the given response message to the client.
   * @param aRequest The page request.
   * @param message The response message.
   */
  private void sendClientResponse( final ProxyPageElement aRequest, final ProxyResponseMessage message )
  {
    this.logger.debug( "Request {} is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getMethod( ) );
    
    //if the method is get
    if( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      //we return the full cached page to the user.
      aRequest.setResponse( message );
    }
    else //else, if the method is head. (remember that other methods aren't cacheable) 
    {
      //we return only the headers...        
      message.setContent( ChannelBuffers.EMPTY_BUFFER );
      aRequest.emptyTransitoryData( ); //cleaning the request object
      aRequest.getRequest( ).setContentString( "" );
      aRequest.setResponse( message );
    }
          
    //send the result to the client.
    aRequest.setState( ProxyRequestState.PROCESSED );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    this.logger.debug( "Request {} dock is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getDockName( ) );
    aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
        
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Returns the page in cache, if existing.
   * @param aRequest The request to use for the query.
   * @return The page in cache or null, if something happens.
   */
  private CacheResponse getCachedPage( final ProxyPageElement aRequest )
  {
    CacheResponse response = null;
    final CacheQuery query = new CacheQuery(CacheQueryOperation.QUERY,
      aRequest.getCurrentUUID(), aRequest.getRequest().getMethod(),
      aRequest.getRequest().getCompleteUri(), "http");
    try
    {
      CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( "The standard cache consistency was interrupted.", ex );
    }
    
    try
    {
      response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( "The standard cache consistency was interrupted.", ex );
    }
    
    return response;
  }
  
  /**
   * Changes the page in cache, to the given one.
   * @param aRequest The request to use for the query.
   */
  private void changeCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.CHANGE, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return;
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
    }
  }
  
  /**
   * Adds the page to the cache.
   * @param aRequest The request to use for the query.
   */
  private void addCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.ADD, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return;
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        CacheMessageProcessingQueue.instance( ).getResponses( ).take( );        
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
    }
  }
  
  /**
   * Returns the id of this proxy, that is "Standard Request Proxy Action".
   * @return The id of this proxy.
   */
  @Override( )
  public String getActionID( )
  {
    return "Standard Request Proxy Action";
  }
}
