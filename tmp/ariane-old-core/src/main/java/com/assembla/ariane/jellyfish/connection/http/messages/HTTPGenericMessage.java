/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.messages;

import io.netty.buffer.ChannelBuffer;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMessage;
import io.netty.handler.codec.http.HttpVersion;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import org.slf4j.Logger;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.GenericMessage;

/**
 * This class is a generic http message.
 * @author Fat Cat
 * @version 2
 * @since 0.0.6
 */
public class HTTPGenericMessage implements GenericMessage
{
  
  /** The extra. */
  protected final HashMap< String, String > extra;
  
  /** The request. */
  protected final HttpMessage message;
  
  /** The logger. */
  protected Logger logger;
  
  /** The message id. */
  protected UUID messageId;
  
  /** The range. */
  protected List< Pair< Integer, Integer >> range;
  
  /** The hop headers. */
  protected SortedSet< String > hopHeaders;

  /**
   * Copy constructor that initializes this generic message with the given message.
   * @param message The inner message to use.
   * @param messageId The message id.
   */
  public HTTPGenericMessage( final HttpMessage message, final UUID messageId )
  {
    this.extra = new HashMap< String, String >( );
    this.message = message;
    this.messageId = messageId;
    this.range = new ArrayList< Pair< Integer, Integer >>( JellyfishConstants.DEFAULT_ARRAY_LIST_SIZE );
    this.hopHeaders = new TreeSet< String >( );
  }
  
  // Dock name stuff
  
  /**
   * Sets the dockname associated with this message.
   * @param dockName The name of the dock.
   */
  @Override( )
  public void setDockName( final String dockName )
  {
    this.extra.put( "dock-name", dockName );
  }

  /**
   * Returns the dockname associated with this message.
   * @return The name of the associated dock, or an empty 
   * string if it does not exist.
   */
  @Override( )
  public String getDockName( )
  {
    if ( this.extra.containsKey( "dock-name" ) )
    {
      return this.extra.get( "dock-name" );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }

  // Header stuff
  
  /**
   * Adds the specified header to the header list, if it does not exist.
   * @param name The header name.
   * @param value The header value.
   */
  @Override( )
  public void addHeader( final String name, final String value )
  {
    if ( !this.message.containsHeader( name ) )
    {
      this.message.addHeader( name, value );
    }
  }

  /**
   * Checks if the given header exists.
   * @param name The header name.
   * @return True, if it does.
   */
  @Override( )
  public boolean containsHeader( final String name )
  {
    return this.message.containsHeader( name );
  }

  /**
   * Returns the value of the header with the specified name.<br/>
   * If the given header name does not exist, empty string is returned.
   * @param name The name of the header.
   * @return The value of the header.
   */
  @Override( )
  public String getHeader( final String name )
  {
    if ( this.containsHeader( name ) )
    {
      return this.message.getHeader( name );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }

  /**
   * Returns the count of all available headers.
   * @return The number of available headers.
   */
  @Override( )
  public int getHeaderCount( )
  {
    return this.message.getHeaderNames( ).size( );
  }

  /**
   * Returns an set containing the names of all known headers.<br/> 
   * There might be headers with one name and more than one value, 
   * use getMessage() to obtain the inner message and get them all.
   * @return The names of all known headers.
   */
  @Override( )
  public Set< String > getHeaderNames( )
  {
    return Collections.unmodifiableSet( this.message.getHeaderNames( ) );
  }

  /**
   * Returns an list of values for the given header.
   * @param name The header name.
   * @return The header values.
   */
  @Override( )
  @SuppressWarnings( "unchecked" )
  public List< String > getHeaders( final String name )
  {
    if ( this.containsHeader( name ) )
    {
      return Collections.unmodifiableList( this.message.getHeaders( name ) );
    }
    else
    {
      return Collections.unmodifiableList( ( List< String > ) Collections.EMPTY_LIST );
    }
  }

  /**
   * Clears the header list.
   */
  @Override( )
  public void clearHeaders( )
  {
    this.message.clearHeaders( );
  }

  /**
   * Changes the given header, to the given value.<br/>
   * If the header contains multiple values, then those values are
   * first deleted and then the new one added.<br/>
   * If the header does not exist, then nothing is changed.
   * @param name The name of the header.
   * @param value The new value of the header.
   */
  @Override( )
  public void changeHeader( final String name, final String value )
  {
    if ( this.containsHeader( name ) )
    {
      this.removeHeader( name );
      this.message.addHeader( name, value );
    }
  }

  /**
   * Removes the header with the given values, if it exists,
   * and all of it's values.
   * @param name The header name.
   */
  @Override( )
  public void removeHeader( final String name )
  {
    if ( this.containsHeader( name ) )
    {
      this.message.removeHeader( name );
    }
  }
  
  // Extra stuff
  
  /**
   * Adds a new extra value, if the value does not exist yet.
   * @param name The name of the extra value.
   * @param value The value of the extra value.
   */
  @Override( )
  public void addExtra( final String name, final String value )
  {
    if ( !this.containsExtra( name ) )
    {
      this.extra.put( name, value );
    }
  }

  /**
   * Checks if the given extra value exists.
   * @param name The extra value name.
   * @return True if it exists, false otherwise.
   */
  @Override( )
  public boolean containsExtra( final String name )
  {
    return this.extra.containsKey( name );
  }

  /**
   * Returns the extra value corresponding to the given name.
   * @param name The extra value name.
   * @return The extra value or an empty string if the name does not exist.
   */
  @Override( )
  public String getExtra( final String name )
  {
    if ( this.containsExtra( name ) )
    {
      return this.extra.get( name );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING; 
    }
  }

  /**
   * Returns the number of available extra values.
   * @return The number of available extra values.
   */
  @Override( )
  public int getExtraValueCount( )
  {
    return this.extra.size( );
  }

  /**
   * Returns an unmodifiable set with all the names of the extra values.
   * @return The names of the extra values.
   */
  @Override( )
  public Set< String > getExtraValueNames( )
  {
    return Collections.unmodifiableSet( this.extra.keySet( ) );
  }

  /**
   * Clears the extra fields.
   */
  @Override( )
  public void clearExtra( )
  {
    this.extra.clear( );
  }

  /**
   * Removes the given key/value from the extra fields.
   * @param name The key of the value to remove.
   */
  @Override( )
  public void removeExtra( final String name )
  {
    if ( this.extra.containsKey( name ) )
    {
      this.extra.remove( name );
    }
  }

  /**
   * Changes the given extra field.
   * @param name The name of the new extra field
   * @param value The value of the new extra field
   */
  @Override( )
  public void changeExtra( final String name, final String value )
  {
    if ( this.extra.containsKey( name ) )
    {
      this.removeExtra( name );
      this.extra.put( name, value );
    }
  }
  
  /**
   * Checks if this message has extra values.
   * @return True, if it has.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#hasExtraValues()
   */
  @Override( )
  public boolean hasExtraValues( )
  {
    return !this.extra.isEmpty( );
  }

  // Content stuff

  /**
   * Gets the request content.
   * @return The request content.
   */
  @Override( )
  public ChannelBuffer getContent( )
  {
    return this.message.getContent( );
  }

  /**
   * Returns the size of the available content.
   * @return The size of the content, or -1 if there is no content.
   */
  @Override( )
  public long getContentLength( )
  {
    return HttpHeaders.getContentLength( this.message, -1 );
  }

  /**
   * Changes the content of this request.
   * @param content The new request content.
   */
  @Override( )
  public void setContent( final ChannelBuffer content )
  {
    this.message.setContent( content );
  }

  // Range stuff
  
  /**
   * Checks if the given range is in the range list.
   * @param range The range to check.
   * @return True, if the range is in the list.
   */
  @Override( )
  public boolean containsRange( final Pair< Integer, Integer > range )
  {
    for ( Pair< Integer, Integer > crange : this.range )
    {
      if ( crange.equals( range ) ) 
      { 
        return true; 
      }
    }
    return false;
  }

  /**
   * Checks if the given range is in the list.
   * @param min The lower bound of the range.
   * @param max The upper bound of the range.
   * @return True, if the range is in the list.
   */
  @Override( )
  public boolean containsRange( final Integer min, final Integer max )
  {
    return this.containsRange( new Pair< Integer, Integer >( min, max ) );
  }

  /**
   * Returns an unmodifiable list containing all ranges.
   * @return An range list.
   */
  @Override( )
  public List< Pair< Integer, Integer >> getRanges( )
  {
    return Collections.unmodifiableList( this.range );
  }

  /**
   * Adds a new range that does not have a lower bound.
   * @param max The upper bound of the range.
   */
  @Override( )
  public void addRangeMax( final int max )
  {
    if ( !this.containsRange( -1, max ) )
    {
      this.range.add( new Pair< Integer, Integer >( -1, max ) );
    }
  }

  /**
   * Adds a new range that does not have a upper bound.
   * @param min The lower bound of the range.
   */
  @Override( )
  public void addRange( final int min )
  {
    if ( !this.containsRange( min, -1 ) )
    {
      this.range.add( new Pair< Integer, Integer >( min, -1 ) );
    }
  }

  /**
   * Adds a new range.
   * @param min The lower bound of the range.
   * @param max The upper bound of the range.
   */
  @Override( )
  public void addRange( final int min, final int max )
  {
    if ( !this.containsRange( min, max ) )
    {
      this.range.add( new Pair< Integer, Integer >( min, max ) );
    }
  }

  /**
   * Adds a new range.
   * @param range A pair representing the new range lower and upper bounds.
   */
  @Override( )
  public void addRange( final Pair< Integer, Integer > range )
  {
    if ( !this.containsRange( range ) )
    {
      this.range.add( range );
    }
  }

  /**
   * Adds the given list to this range list.
   * @param ranges The list of ranges to add.
   */
  @Override( )
  public void addAllRanges( final Collection< Pair< Integer, Integer >> ranges )
  {
    for ( Pair< Integer, Integer > range : ranges )
    {
      this.addRange( range );
    }
  }

  /**
   * Clears the range list.
   */
  @Override( )
  public void clearRanges( )
  {
    this.range.clear( );
  }

  /**
   * Returns the number of available ranges.
   * @return The number of ranges.
   */
  @Override( )
  public int getRangeNumber( )
  {
    return this.range.size( );
  }

  /**
   * Checks if there are ranges in this request.
   * @return True, if they are.
   */
  @Override( )
  public boolean hasRanges( )
  {
    return !this.range.isEmpty( );
  }

  // Keep alive stuff
  
  /**
   * Changes this request so that it is a keep alive request.
   */
  @Override( )
  public void setKeepAlive( )
  {
    HttpHeaders.setKeepAlive( this.message, true );
  }

  /**
   * Changes this request so that it is NOT a keep alive request.
   */
  @Override( )
  public void unsetKeepAlive( )
  {
    HttpHeaders.setKeepAlive( this.message, false );
  }

  /**
   * Checks if this request is keep-alive.
   * @return True, if it is.
   */
  @Override( )
  public boolean isKeepAlive( )
  {
    return HttpHeaders.isKeepAlive( this.message );
  }

  // Protocol version stuff
  
  /**
   * Returns the protocol version of this request.
   * @return The protocol version as a netty HttpVersion object.
   */
  public HttpVersion getNativeProtocolVersion( )
  {
    return this.message.getProtocolVersion( );
  }

  /**
   * Returns the protocol version of this request as a string.
   * @return The protocol version as a string
   */
  @Override( )
  public String getProtocolVersion( )
  {
    return this.message.getProtocolVersion( ).toString( );
  }

  /**
   * Changes the protocol version of this request.
   * @param version The new protocol version.
   */
  public void setProtocolVersion( final HttpVersion version )
  {
    if ( ( version != null ) && ( !this.message.getProtocolVersion( ).equals( version ) ) )
    {
      this.message.setProtocolVersion( version );
    }
  }

  /**
   * Changes the protocol version of this request.
   * @param version The new protocol version.
   */
  @Override( )
  public void setProtocolVersion( final String version )
  {
    this.setProtocolVersion( HttpVersion.valueOf( version ) );
  }

  // Message id stuff
  
  /**
   * Returns the id of this request.
   * @return The id of this request.
   */
  @Override( )
  public synchronized UUID getMessageId( )
  {
    return this.messageId;
  }

  /**
   * Returns the request id as a string.
   * @return The request id.
   */
  @Override( )
  public String getMessageIdAsString( )
  {
    return this.messageId.toString( );
  }
    
  // Message stuff
  
  /**
   * Returns the inner netty HttpMessage object.
   * @return The inner message.
   */
  public HttpMessage getMessage( )
  {
    return this.message;
  }

  //Hop headers stuff
  
  /**
   * Checks if this header is a hop header.
   * @param header The header to check for.
   * @return True, if it is.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#containsHopHeader(java.lang.String)
   */
  @Override( )
  public boolean containsHopHeader( final String header )
  {
    return this.hopHeaders.contains( header );
  }

  /**
   * Adds the given hop header.<br/>
   * If the header exists, nothing is added.
   * @param header The hop header to add.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addHopHeader(java.lang.String)
   */
  @Override( )
  public void addHopHeader( final String header )
  {
    this.hopHeaders.add( header );
  }

  /**
   * Returns an read-only set with all hop headers.
   * @return An sorted set with all hop headers.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getHopHeaders()
   */
  @Override( )
  public SortedSet< String > getHopHeaders( )
  {
    return Collections.unmodifiableSortedSet( this.hopHeaders );
  }

  /**
   * Returns the number of hop headers.
   * @return The number of hop headers.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getNumberOfHopHeaders()
   */
  @Override( )
  public int getNumberOfHopHeaders( )
  {
    return this.hopHeaders.size( );
  }

  /**
   * Removes the given hop header.
   * @param header The hop header to remove.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#removeHopHeader(java.lang.String)
   */
  @Override( )
  public void removeHopHeader( final String header )
  {
    this.hopHeaders.remove( header );
  }

  /**
   * Clears all of the hop headers.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#clearHopHeaders()
   */
  @Override( )
  public void clearHopHeaders( )
  {
    this.hopHeaders.clear( );
  }
  
  /**
   * Checks if there are any hop headers.
   * @return True, if there are.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#hasHopHeaders()
   */
  @Override( )
  public boolean hasHopHeaders( )
  {
    return !this.hopHeaders.isEmpty( );
  }
  
}
