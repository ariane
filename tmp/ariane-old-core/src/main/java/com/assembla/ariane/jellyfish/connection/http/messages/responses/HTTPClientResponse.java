/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.messages.responses;

import io.netty.buffer.ChannelBuffers;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.UUID;
import org.slf4j.LoggerFactory;
import com.assembla.ariane.jellyfish.connection.http.messages.requests.HTTPClientRequest;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.WarningValue;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;

/**
 * This represents an http client response.
 * @author Fat Cat
 * @since 0.0.1
 * @version 5
 */
public class HTTPClientResponse extends HTTPGenericResponse
{
  
  /**
   * Converts an http response to a generic proxy response.
   * @param httpresp The http response to convert.
   * @return An corresponding generic proxy response.
   */
  public static ProxyResponseMessage messageConverter( final HTTPClientResponse httpresp )
  {
    final ProxyResponseMessage response = new ProxyResponseMessage( httpresp.getMessageId( ) );
    
    //Response code
    response.setResponseCode( ProxyResponseCode.create( httpresp.getNativeResponseCode( ).getCode( ), 
                                                        httpresp.getNativeResponseCode( ).getReasonPhrase( ) ) );
    
    //Protocol
    response.setProtocol( ProxyProtocol.create( httpresp.getNativeProtocolVersion( ).getMajorVersion( ), 
                                                httpresp.getNativeProtocolVersion( ).getMinorVersion( ), 
                                                httpresp.getNativeProtocolVersion( ).getProtocolName( ) ) );
    
    //Content
    response.setContent( httpresp.getContent( ) );
    
    //Dock Name
    response.setDockName( httpresp.getDockName( ) );
    
    //Keep Alive
    if ( httpresp.isKeepAlive( ) )
    {
      response.setKeepAlive( );
    }
    
    //Headers
    for ( final String headerName : httpresp.getHeaderNames( ) )
    {
      response.addHeader( headerName, httpresp.getHeader( headerName ) );
    }
    
    //Extra Values
    for ( final String extraName : httpresp.getExtraValueNames( ) )
    {
      response.addExtra( extraName, httpresp.getExtra( extraName ) );
    }
    
    //Ranges
    for ( final Pair< Integer, Integer > pair : httpresp.getRanges( ) )
    {
      response.addRange( pair );
    }
    
    //Hop Headers
    for ( String hopHeader : httpresp.getHopHeaders( ) )
    {
      response.addHopHeader( hopHeader );
    }
    
    //Warnings
    for ( WarningValue warning : httpresp.getWarnings( ) )
    {
      response.addWarning( warning );
    }
    
    return response;
  }
  
  /**
   * Converts an generic proxy response to a http response.
   * @param proxyresp The generic proxy response.
   * @return The corresponding http response.
   */
  public static HTTPClientResponse messageConverter( final ProxyResponseMessage proxyresp )
  {
    final HTTPClientResponse response = new HTTPClientResponse( proxyresp.getMessageId( ) );
    
    response.setContent( proxyresp.getContent( ) );
    response.setDockName( proxyresp.getDockName( ) );
    response.setResponseCode( HttpResponseStatus.valueOf( proxyresp.getNativeResponseCode( ).getResponseCode( ) ) );
    response.setProtocolVersion( HttpVersion.valueOf( proxyresp.getProtocolVersion( ) ) );
    
    if ( proxyresp.isKeepAlive( ) )
    {
      response.setKeepAlive( );
    }
    
    //extra
    for ( final String extraName : proxyresp.getExtraValueNames( ) )
    {
      response.addExtra( extraName, proxyresp.getExtra( extraName ) );
    }
    
    //headers
    for ( final String headerName : proxyresp.getHeaderNames( ) )
    {
      response.addHeader( headerName, proxyresp.getHeader( headerName ) );
    }
    
    //ranges
    for ( final Pair< Integer, Integer > pair : proxyresp.getRanges( ) )
    {
      response.addRange( pair );
    }
    
    //warnings
    for ( WarningValue warning : proxyresp.getWarnings( ) )
    {
      response.addWarning( warning );
    }
    
    //hop headers
    for ( String hopHeader : proxyresp.getHopHeaders( ) )
    {
      response.addHopHeader( hopHeader );
    }
    
    return response;
  }

  /**
   * Adds the remaining headers to the response.
   * @param response The response to add the headers.
   * @return The response object with the added headers.
   */
  public static HTTPClientResponse addRemainingHeaders( final HTTPClientResponse response )
  {
    //Calculates content length header
    if( !response.containsHeader( "Content_Length" ) && ( !response.getContent( ).equals( ChannelBuffers.EMPTY_BUFFER ) ) )
    {
      HttpHeaders.setContentLength( response.getResponse( ), response.getContent( ).toString( CharsetUtil.US_ASCII ).length( ) );
    }
    
    StringBuffer sb = new StringBuffer( );
    
    //Adds the range header
    if ( !response.getRanges( ).isEmpty( ) )
    { 
      sb.append( "bytes " );
      for ( final Pair< Integer, Integer > pair : response.getRanges( ) )
      {
        if ( ( pair.getFirst( ) == -1 ) && ( pair.getSecond( ) != -1 ) ) // max only
        {
          sb.append( String.format( "-%d", pair.getSecond( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) == -1 ) ) // min only
        {
          sb.append( String.format( "%d-", pair.getFirst( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) != -1 ) ) // both
        {
          sb.append( String.format( "%d-%d", pair.getFirst( ), pair.getSecond( ) ) );
        }
      }
      if ( response.containsExtra( "max-range-content" ) )
      {
        sb.append( String.format( "/%s", response.getExtra( "max-range-content" ) ) );
      }
      else
      {
        sb.append( "/*" );
      }      
      response.addHeader( "Content-Range", sb.toString( ) );
    }
    
    //Adds the warning header
    if( response.hasAnyWarning( ) )
    {
      sb = new StringBuffer( );      
      final List< WarningValue > tmplist = response.getWarnings( );
      
      for( int i = 0; i < response.getNumberOfWarnings( ); i++ )
      {
        sb.append( tmplist.get( i ).toString( ) );
        if( i < response.getNumberOfWarnings( ) - 1 )
        {
          sb.append( "," );
        }
      }
      response.addHeader( "Warning", sb.toString( ) );      
    }
    
    return response;
  }
  
  /**
   * Creates a new client response.
   * @param requestId The id of this response.
   */
  public HTTPClientResponse( final UUID requestId )
  {
    super( requestId );
    this.logger = LoggerFactory.getLogger( HTTPClientRequest.class );
  }
  
  /**
   * Creates a new client response, based on a HttpResponse object.
   * @param requestId The requestId of the response.
   * @param response The other HttpResponse.
   */
  public HTTPClientResponse( final UUID requestId, final HttpResponse response )
  {
    super( requestId, response );
    this.logger = LoggerFactory.getLogger( HTTPClientRequest.class );
  }
  
  //Utilities
  
  /**
   * Clones this http client response message.
   * @return A new identical http client response message.
   */
  public HTTPClientResponse copy( )
  {
    try //main clone method uses serialization
    {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream( );
      final ObjectOutputStream out = new ObjectOutputStream( baos );
      out.writeObject( this );
      
      final ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray( ) );
      final ObjectInputStream ois = new ObjectInputStream( bais );
      return ( HTTPClientResponse ) ois.readObject( );
    }
    catch( final IOException | ClassNotFoundException e ) //An alternate clone method, just in case...
    {
      final HTTPClientResponse response = new HTTPClientResponse( this.messageId );
      
      response.setContent( this.getContent( ).copy( ) );
      response.setDockName( this.getDockName( ) );
      response.setProtocolVersion( HttpVersion.valueOf( this.getProtocolVersion( ) ) );
      response.setResponseCode( HttpResponseStatus.valueOf( this.getResponseCodeAsInteger( ) ) );
      
      if ( response.isKeepAlive( ) )
      {
        response.setKeepAlive( );
      }
      
      //Add the headers
      for ( String header : this.getHeaderNames( ) )
      {
        for ( String value : this.getResponse( ).getHeaders( header ) )
        {
          response.getResponse( ).addHeader( header, value );
        }
      }
      
      //Add the extra
      for ( String extraname : this.getExtraValueNames( ) )
      {
        response.addExtra( extraname, this.getExtra( extraname ) );
      }
      
      //Add the ranges
      for ( Pair< Integer, Integer > range : this.getRanges( ) )
      {
        response.addRange( range );
      }
      
      //Add the hop headers
      for ( String header : this.hopHeaders )
      {
        response.addHopHeader( header );
      }
      
      //Add the warning header
      for ( WarningValue warning : this.warnings )
      {
        if( warning.hasDate( ) )
        {
          response.addWarning( WarningValue.create( warning.getCode( ), warning.getAgent( ), warning.getText( ), warning.getDate( ) ) );
        }
        else
        {
          response.addWarning( WarningValue.create( warning.getCode( ), warning.getAgent( ), warning.getText( ) ) );
        }
      }
      
      return response;
    }
  }
}
