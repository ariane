/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.docks;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.assembla.ariane.jellyfish.connection.http.client.HTTPClientReceptor;
import com.assembla.ariane.jellyfish.connection.http.client.HTTPClientSender;
import com.assembla.ariane.jellyfish.connection.http.server.HTTPServerSender;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.LoggerFactory;

/**
 * An dock that uses the http protocol.
 * @author Fat Cat
 * @since 0.0.1
 * @version 3
 */
public class HttpDock extends Dock
{
  
  /** The dock thread pool. */
  private final ExecutorService thread_pool = Executors.newFixedThreadPool( 3 );
  
  /**
   * Constructor that creates a new HTTPDock.
   * @param configuration The http dock configuration.
   */
  public HttpDock( final HierarchicalConfiguration configuration )
  {    
    super( configuration );
    this.logger = LoggerFactory.getLogger( HttpDock.class );
    this.logger.info( "Adding a new http dock" );
    for( final HierarchicalConfiguration socketInfo : configuration.configurationsAt( "entry" ) )
    {      
      if( socketInfo.getString( "[@type]", "" ).equals( "client" ) )
      {
        this.client[ 0 ] = new HTTPClientReceptor( socketInfo, this.getName( ) );
        this.client[ 1 ] = new HTTPClientSender( socketInfo, this.getName( ) );
      }
      else if( socketInfo.getString( "[@type]", "" ).equals( "server" ) )
      {
        this.server[ 0 ] = new HTTPServerSender( socketInfo, this.getName( ) );
      }
    }
  }

  /**
   * Starts this dock.
   * @param startClient Whether or not to start the client.
   * @param startServer Whether or not to start the server.
   * @throws Exception If some error happens.
   */
  @SuppressWarnings( "unchecked" )
  @Override()
  public void start( final boolean startClient, final boolean startServer ) throws Exception
  {
    this.logger.info( "Starting the http dock" );
    if( startClient )
    {
      this.thread_pool.submit( this.client[ 0 ] ); //receptor
      this.thread_pool.submit( this.client[ 1 ] ); //sender
      this.logger.info( "Starting the client http dock" );      
    }
    if( startServer )
    {
      this.thread_pool.submit( ( Callable<Void> ) this.server[ 0 ] ); //sender
      this.logger.info( "Starting the server http dock" );
    }
    this.logger.info( "Started the http dock" );
  }

  /**
   * Stops this dock.
   * @param stopClient Whether or not to stop the client.
   * @param stopServer Whether or not to stop the server.
   * @throws Exception If some error happens.
   */
  @Override( )
  public void stop( final boolean stopClient, final boolean stopServer ) throws Exception
  {
    if( stopClient )
    {
      this.logger.info( "Stopping the client http module" );
      this.client[ 0 ].stop( ); //receptor
      this.client[ 1 ].stop( ); //transmitter
      this.logger.info( "Stopped the client http module" );
    }
    if( stopServer )
    {
      this.logger.info( "Stopping the server http module" );
      this.server[ 0 ].stop( ); //sender
      this.logger.info( "Stopped the server http module" );
    }
    if( stopClient && stopServer )
    {
      this.logger.info( "Stopping the http dock" );
      this.thread_pool.shutdown();
      
      try
      {
        if( !this.thread_pool.awaitTermination( JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS ) )
        {
          this.thread_pool.shutdownNow( );
        }
      }
      catch( final InterruptedException ex ) 
      { 
        Thread.currentThread( ).interrupt( ); 
      }
      
      this.logger.info( "Stopped the http dock" );
    }
    
  }
  
}
