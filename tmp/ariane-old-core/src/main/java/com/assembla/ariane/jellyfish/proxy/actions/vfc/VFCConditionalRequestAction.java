/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.vfc;

import io.netty.buffer.ChannelBuffers;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponseOperation;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.htmlparser.util.ParserException;
import org.joda.time.DateTime;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;
import com.assembla.ariane.jellyfish.util.collections.cache.CacheMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.vfc.VFCPageList;
import com.assembla.ariane.jellyfish.util.html.visitors.StandardVFCLinkVisitor;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.RequestType;
import com.assembla.ariane.jellyfish.proxy.comparators.LastModifiedEquality;
import com.assembla.ariane.jellyfish.proxy.comparators.StrongEtagEquality;
import com.assembla.ariane.jellyfish.proxy.comparators.WeakEtagEquality;
import com.assembla.ariane.jellyfish.proxy.exceptions.ArgumentException;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.WarningValue;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQueryOperation;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponse;
import com.assembla.ariane.jellyfish.proxy.vfc.User;
import com.assembla.ariane.jellyfish.proxy.vfc.Users;
import com.assembla.ariane.jellyfish.proxy.vfc.VFCPage;

/**
 * This action handles an conditional request using VFC.<br/>
 * That is an request that is not to this server and is conditional
 * is not a range request and is virtually cacheable.<br/>
 * An request is virtually cacheable when it is not an upgrade request,
 * does not have trailers, is not an 100-continue expect message.<br/>
 * Since VFC does not cares about cache-control or pragma, requests/responses
 * that wouldn't be cacheable in other other servers, will be cached by
 * this scheme.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public final class VFCConditionalRequestAction extends ProxyAction
{
  
  /**
   * Creates a new VFC conditional request action.
   * @param successor The next action in the list.
   * @param config The configuration of this action.
   */
  public VFCConditionalRequestAction( 
        final IHandler< ProxyPageElement > successor, 
        final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( VFCConditionalRequestAction.class );
  }
  
  /**
   * Checks if the given request can be processed by this action.<br/>
   * As told in the class documentation, this action can be processed, if
   * it is not for this server, it is conditional, it is not a range 
   * request, is virtually cacheable and is a get or head request. 
   * @param aRequest The request to look for.
   * @return True, if the request can be handled by this action.
   * @throws IllegalArgumentException Never Thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return !RequestType.isClientOperation( aRequest ) && 
           !RequestType.isRangeRequest( aRequest ) &&  
            RequestType.isConditionalRequest( aRequest ) && 
            RequestType.isVirtuallyCacheableRequest( aRequest ) && 
            RequestType.isGetOrHeadRequest( aRequest );
  }
  
  /**
   * Handles this conditional request, using the VFC approach.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    final Object[ ] args = { aRequest.getCurrentUUID( ), aRequest.getRequest( ).getUri( ), aRequest.getState( ).toString( ) };
    
    this.logger.info( "Received an request:\nID: {}\nURI: {}\nState: {}", args );
    
    //if request is new
    if ( aRequest.getState( ) == ProxyRequestState.PROCESSING )
    {
      this.logger.info( "Request with id {} is a new request", aRequest.getCurrentUUID( ) );
      
      processClientRequest( aRequest );
    }
    else if ( aRequest.getState( ) == ProxyRequestState.MORE_INFO_RECEIVED )
    {
      //else (if request is from the server)
      processServerResponse( aRequest );
    }
  }

  /**
   * Processes a new request from the client.
   * @param aRequest The proxy page withe the request.
   */
  private void processClientRequest( final ProxyPageElement aRequest )
  {
    //remove any hop-by-hop headers,
    this.logger.debug( "Removing hop-to-hop headers from request {}", aRequest.getRequest( ).getMessageId( ) );
    aRequest.removeHopToHopHeaders( true );
    
    final ProxyRequestMessage request = aRequest.getRequest( );
    
    //(check) if the page is known for the given user (get the page) (from all users)
    //user name is in header X-USERNAME
    VFCPage page = null;
    User user = null;
    if ( request.containsHeader( "X-USERNAME" ) && 
        Users.instance( ).hasUserName( request.getHeader( "X-USERNAME" ) ) )
    {
      final String username = request.getHeader( "X-USERNAME" );
      user = Users.instance( ).getUserFor( username );
      page = user.peekPageInformation( aRequest.getRequest( ).getCompleteUri( ) );
      
    }
    
    if ( page != null )
    {
      //(check) if the page is already cached (isCached)
      if ( page.isCached( ) )
      {
        handleRequestCachedPage( aRequest, request, page, user );
      }
      else //else (if the page is not cached)
      {
        //create an unconditional server request
        sendUnconditionalRequestToServer( aRequest, request );
      }
    }   
    else //else (the page is not known)
    {
      //set an extra-value (direct=true)
      aRequest.addExtra( "direct", Boolean.toString( Boolean.TRUE ) );
      
      //create an unconditional server request
      this.sendToServer( aRequest, aRequest.getRequest( ) );
    }
  }

  /**
   * Handles an request that has an cached response.
   * @param aRequest The proxy page.
   * @param request The user request.
   * @param page The VFCPage corresponding to the cached response.
   * @param user The VFC user name.
   */
  private void handleRequestCachedPage( final ProxyPageElement aRequest, 
      final ProxyRequestMessage request, final VFCPage page, final User user )
  {
    //get the cached page
    final CacheResponse cacheResponse = this.getCachedPage( aRequest );
    if ( cacheResponse.getResponse( ) == CacheResponseOperation.OK )
    {
      
      //(check) if the etags match
      if( this.checkCondition( aRequest, 
          cacheResponse.getResponseElement( ).getResponse( ) ) )
      {
        handleRequestMatchingEtags( aRequest, request, page, user );
      }
      else //else (if the etags do not match)
      {
        //create an unconditional server request
        sendUnconditionalRequestToServer( aRequest, request );
      }
    }
    else //else (there is no cached page)
    {
      //unset the cached attribute
      page.unsetCached( );
      
      if ( VFCPageList.instance( ).change( page.getUrl( ), page ) == null )
      {
        VFCPageList.instance( ).add( page );
      }
      
      //create an unconditional server request
      sendUnconditionalRequestToServer( aRequest, request );
    }
  }

  /**
   * Handles an request whose cached response matchs the request etags.
   * @param aRequest The proxy page.
   * @param request The client request.
   * @param page The VFCPage of the request.
   * @param user The VFC user name.
   */
  private void handleRequestMatchingEtags( final ProxyPageElement aRequest, 
      final ProxyRequestMessage request, final VFCPage page, final User user )
  {
    //(check) if the page is fresh (page vfc-vector < consistency vfc-vector)
    if ( user.isPageFresh( page ) )
    {
      //add one to the frequency counter
      page.incrementFrequency( );
    
      //add one to the recency of all pages
      VFCPageList.instance( ).incrementRecency( );
      
      if ( VFCPageList.instance( ).change( page.getUrl( ), page ) == null )
      {
        VFCPageList.instance( ).add( page );
      }
      
      //check (if the page needs to he remaped) (remap all users) (the page has not change)
      Users.instance( ).remapAsNeeded( page );
                      
      //return response 304 to the user
      sendNotModifiedResponse( aRequest, false );
      
    }
    else //else (page is not fresh)
    {
      //create an unconditional server request
      sendUnconditionalRequestToServer( aRequest, request );
    }
  }

  /**
   * Processes an response from the server.
   * @param aRequest The proxy page with the response.
   */
  private void processServerResponse( final ProxyPageElement aRequest )
  {
    //if direct is not set
    if ( !aRequest.containsExtra( "direct" ) )
    {
      
      //get the server response
      final ProxyResponseMessage serverResponse = aRequest.
          getTransitoryResponse( ).copy( );
      
      //get the corresponding page (from consistency map) (from all users)
      final VFCPage correspondingPage = VFCPageList.instance( ).get( 
          aRequest.getRequest( ).getCompleteUri( ) );
      
      //(check) if there is an VFCPage
      if ( correspondingPage != null )
      {
        handleValidVFCPage( aRequest, serverResponse, correspondingPage );
      }
      else //else (there isn't an VFCPage)
      {
        //return the received page
        this.sendClienResponse( aRequest, serverResponse );
      }
    }
    else //else (direct is set)
    {
      //return the received page
      this.sendClienResponse( aRequest, aRequest.getTransitoryResponse( ) );
    }
  }

  /**
   * Processes an response that corresponds to an existing VFCPage.
   * @param aRequest The proxy page.
   * @param serverResponse The server response.
   * @param correspondingPage The page corresponding go the response.
   */
  private void handleValidVFCPage( final ProxyPageElement aRequest, 
                                   final ProxyResponseMessage serverResponse, 
                                   final VFCPage correspondingPage )
  {
    //(check) if the cached attribute is invalid
    final CacheResponse cacheResponse = this.getCachedPage( aRequest );
    if( ( cacheResponse.getResponse( ) != CacheResponseOperation.OK ) &&
        ( correspondingPage.isCached( ) ) )
    {
      //unset the cached attribute
      correspondingPage.unsetCached( );
      
      //Reupdate the changed page.
      if ( VFCPageList.instance( ).change( correspondingPage.getUrl( ), correspondingPage ) == null )
      {
        VFCPageList.instance( ).add( correspondingPage );
      }            
    }
    
    //check if the etags match = store
    final boolean match = this.checkCondition( aRequest, serverResponse );
    
    //(check) if the response is not an error
    if ( 
        ( serverResponse.getResponseCodeAsInteger( ) >= 
          JellyfishConstants.HTTPErrorCodes.OK.errorcode ) && 
        ( serverResponse.getResponseCodeAsInteger( ) < 
            JellyfishConstants.HTTPErrorCodes.MULTIPLE_CHOICES.errorcode ) )
    {
      handleNonErrorResponse( aRequest, serverResponse, correspondingPage, match );
    }
    else //else (if the response is an error)
    { 
      handleErrorResponse( aRequest, serverResponse, correspondingPage ); 
    }
  }

  /**
   * Handles an error response from the server.
   * @param aRequest The proxy page.
   * @param serverResponse The server response.
   * @param correspondingPage The VFCPage corresponding to the server response.
   */
  private void handleErrorResponse( final ProxyPageElement aRequest, 
      final ProxyResponseMessage serverResponse, final VFCPage correspondingPage )
  {
    //(check) if the response code is 504 (invalid gateway)
    if( serverResponse.getResponseCodeAsInteger( ) == 
        JellyfishConstants.HTTPErrorCodes.GATEWAY_TIMEOUT.errorcode )
    {
      //TODO Special option unmodified-on-disconnected

      //(check) if the page is listed as cached
      // and there is special behavior
      if( correspondingPage.isCached( ) && 
          this.config.getBoolean( "unmodified-on-disconnected",
              false ) )
      {
        //return response 304 to the user (with an warning)
        sendNotModifiedResponse( aRequest, true );
      }
      else //else (page is not listed as cached and/or there is no special behavior) 
      {
        //return the error page
        this.sendClienResponse( aRequest, serverResponse );
      }
    }
    else //else (response code is not 504)
    {
      //return the received page
      this.sendClienResponse( aRequest, serverResponse );
    }
  }

  /**
   * Handles an non error response.
   * @param aRequest The proxy page.
   * @param serverResponse The server response.
   * @param correspondingPage The VFCPage corresponding to the server response.
   * @param match If the response matches or not the request conditions.
   */
  private void handleNonErrorResponse( final ProxyPageElement aRequest, 
      final ProxyResponseMessage serverResponse, 
      final VFCPage correspondingPage, final boolean match )
  {
    try
    {            
      //(check) if the page is listed as cached
      if ( correspondingPage.isCached( ) )
      {
        this.parseChangedPage( aRequest, serverResponse, correspondingPage );                
      }
      else //else (the page is listed as not cached)
      { 
        this.parseNewPage( aRequest, serverResponse, correspondingPage );
      }
    }
    catch( final ParserException ex )
    {
      this.logger.warn( "There was an parser exception on request {}", 
          aRequest.getRequest( ).getMessageIdAsString( ) );
      this.logger.warn( "Exception: ", ex );
    }
      
    //if the etags match
    if( match )
    {
      //return response 304 to the user
      sendNotModifiedResponse( aRequest, false );
    }
    else //else (if the etags do not match)
    {
      //return the received response
      this.sendClienResponse( aRequest, 
          aRequest.getTransitoryResponse( ) );
    }
  }

  /**
   * Parses the given new server response.<br/>
   * This method checks if the page is an html page, then if it is
   * parses the html page, extracts the links, images, scripts and stylesheet
   * urls and adds them to the VFCPage as child pages and to the pivots
   * containing the parent page.
   * @param aRequest The proxy request page.
   * @param serverResponse The response from the server.
   * @param correspondingPage The VFC page corresponding to the server response.
   * @throws ParserException If there is an error parsing the html.
   */
  private void parseNewPage( final ProxyPageElement aRequest, 
                             final ProxyResponseMessage serverResponse, 
                             final VFCPage correspondingPage ) throws ParserException
  {
    //(check) if the page is an html page
    if ( VFCUtilityFunctions.isHtmlPage( serverResponse ) )
    {
      //parse the page
      final StandardVFCLinkVisitor visitor = 
          VFCUtilityFunctions.parsePage( serverResponse );
      
      //add the parsed pages in the respective consistency zones
      VFCUtilityFunctions.addNewParsedPages( correspondingPage, visitor );
    }
    
    //set the vfc-vector as (current_distance, 0, current_frequency + 1)
    correspondingPage.incrementFrequency( );
    correspondingPage.resetRecency( );
    
    //add one to the recency of all pages (remember that this is an lazy algorithm)
    VFCPageList.instance( ).incrementRecency( );
    
    //remap the page in the consistency zones (of all users)
    Users.instance( ).remapAsNeeded( correspondingPage );
    
    //cache the new page
    final ProxyPageElement cachedPage = new ProxyPageElement( );
    cachedPage.setRequest( aRequest.getRequest( ).copy( ) );
    cachedPage.setResponse( serverResponse );
    cachedPage.setState( ProxyRequestState.ARCHIVED );
    cachedPage.emptyTransitoryData( );
    
    if ( this.addCachedPage( cachedPage ).getResponse( ) == 
        CacheResponseOperation.OK )
    {
      correspondingPage.setCached( );
      correspondingPage.changePageId( 
          aRequest.getRequest( ).getMessageId( ) );
    }
    
    //Reupdate the changed page.
    if ( VFCPageList.instance( ).change( 
        correspondingPage.getUrl( ), correspondingPage ) == null )
    {
      VFCPageList.instance( ).add( correspondingPage );
    }
  }

  /**
   * This method parses an changed server response.<br/>
   * This method checks if the page is an html page, then if it is
   * parses the html page, extracts the links, images, scripts and stylesheet
   * urls and merges them to the parent page child pages and to the pivots
   * containing the parent page.
   * @param aRequest The proxy request page.
   * @param serverResponse The response from the server.
   * @param correspondingPage The VFC page corresponding to the server response.
   * @throws ParserException If there is an error parsing the html.
   */
  private void parseChangedPage( final ProxyPageElement aRequest, 
                                 final ProxyResponseMessage serverResponse, 
                                 final VFCPage correspondingPage ) throws ParserException
  {
    //(check) if the page is an html page
    if ( VFCUtilityFunctions.isHtmlPage( serverResponse ) )
    {
      //parse the page
      final StandardVFCLinkVisitor visitor = VFCUtilityFunctions.parsePage( serverResponse );
      
      //merge the new associated pages in the users consistency zones 
      //and delete the non-existing ones
      VFCUtilityFunctions.mergeParsedPages( correspondingPage, visitor );
      
    }
    
    //set the vfc-vectors as (current_distance, 0, current_frequency + 1)
    correspondingPage.incrementFrequency( );
    correspondingPage.resetRecency( );
    
    //add one to the recency of all pages 
    // (remember that this is an lazy algorithm, so we don't need to check if
    //  pages are well mapped, into the consistency zones, we will do that
    //  when there is a request for a page and if the page is not fresh)
    VFCPageList.instance( ).incrementRecency( );
    
    //remap the page in the consistency zones (of all users)
    Users.instance( ).remapAsNeeded( correspondingPage );
    
    //cache the new page
    final ProxyPageElement cachedPage = new ProxyPageElement( );
    cachedPage.setRequest( aRequest.getRequest( ).copy( ) );
    cachedPage.setResponse( serverResponse );
    cachedPage.setState( ProxyRequestState.ARCHIVED );
    cachedPage.emptyTransitoryData( );
    
    if ( this.changeCachedPage( cachedPage ).getResponse( ) == CacheResponseOperation.OK )
    {
      correspondingPage.setCached( );
    }
    
    //Reupdate the changed page.
    if ( VFCPageList.instance( ).change( correspondingPage.getUrl( ), correspondingPage ) == null )
    {
      VFCPageList.instance( ).add( correspondingPage );
    }
  }
  
  /**
   * Sends an not modified response to the client.
   * @param aRequest The page request.
   * @param connectionError If there was an error connecting to the server. 
   */
  private void sendNotModifiedResponse( final ProxyPageElement aRequest, final boolean connectionError )
  {
    
    final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
    
    if( connectionError )
    {
      response.addWarning( WarningValue.create( JellyfishConstants.REVALIDATION_FAILED, 
          "Jellyfish", "Revalidation failed" ) );
    }
    
    
    response.addHeader( "Date", DateTime.now( ).toString( JellyfishConstants.HTTP_FORMATTER ) );
    response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
    response.setResponseCode( ProxyResponseCode.create( JellyfishConstants.HTTPErrorCodes.NOT_MODIFIED.errorcode, "Not Modified" ) );
    
    this.sendClienResponse( aRequest, response );
  }
  
  /**
   * Sends an response to the client.
   * @param aRequest The client request.
   * @param response The response for the request.
   */
  private void sendClienResponse( final ProxyPageElement aRequest, final ProxyResponseMessage response )
  {
    this.logger.debug( "Request {} is {}", 
        aRequest.getRequest( ).getMessageId( ), 
        aRequest.getRequest( ).getMethod( ) );
    
    //If the method is get
    if ( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      //we return the full cached page to the user.
      aRequest.setResponse( response );
    }
    else //else if the method is head. (remember that other methods aren't cacheable)
    {
      //we return only the headers...
      final ProxyResponseMessage headerResponse = response.copy( );
      headerResponse.setContent( ChannelBuffers.EMPTY_BUFFER );
      aRequest.setResponse( headerResponse );
    }
    
    //send the result to the client.
    aRequest.setState( ProxyRequestState.PROCESSED );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    this.logger.debug( "Request {} dock is {}", 
        aRequest.getRequest( ).getMessageId( ), 
        aRequest.getRequest( ).getDockName( ) );
    
    aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
    
    aRequest.getResponse( ).setMessageId( aRequest.getRequest( ).getMessageId( ) );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Sends an unconditional request to the server.
   * @param aRequest The request page to use.
   * @param request The request to send. 
   */
  private void sendUnconditionalRequestToServer( final ProxyPageElement aRequest, 
      final ProxyRequestMessage request )
  {
    final ProxyRequestMessage uncondRequest = request.copy( );
    
    uncondRequest.removeHeader( "If-Match" );
    uncondRequest.removeHeader( "If-None-Match" );
    uncondRequest.removeHeader( "If-Modified-Since" );
    uncondRequest.removeHeader( "If-Unmodified-Since" );
    
    this.sendToServer( aRequest, uncondRequest );
  }
  
  /**
   * Sends an request to the server asking for more information.
   * @param aRequest The request page to use. 
   * @param request The request to send.
   */
  private void sendToServer( final ProxyPageElement aRequest, final ProxyRequestMessage request )
  {
    this.logger.info( "Preparing request {} to be sent to the server", 
        aRequest.getRequest( ).getMessageId( ) );
    
    URL host;
    int port = JellyfishConstants.DEFAULT_HTTP_PORT;
    try
    {
      host = new URL( aRequest.getRequest( ).getCompleteUri( ) );
      port = host.getPort( );
      if ( port == -1 )
      {
        port = JellyfishConstants.DEFAULT_HTTP_PORT;
      }
    }
    catch( final MalformedURLException e )
    {
      //Do nothing
    }
    
    this.logger.debug( "Server port for request {} is {}", 
        aRequest.getRequest( ).getMessageId( ), port );
    
    //Notice that since the server port can be different than the standard 80,
    //we will extract the port in order to use it in the HTTPServerSender channel.
    request.addExtra( "server-port", Integer.toString( port ) );
    aRequest.setTransitoryData( request );
    
    aRequest.setState( ProxyRequestState.MORE_INFO );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} was sent to server", 
          aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Checks if any of the given etags matches this one.
   * @param header The etag values.
   * @param etag The etag to each of them to.
   * @return True, if any of them matches.
   */
  private boolean ifMatchVerifier( final String header, final String etag )
  {
    for ( String value : header.split( "," ) )
    {
      if ( value.equals( "*" ) ) 
      { 
        return true; 
      }
      
      if ( StrongEtagEquality.instance( ).isValidator( value.replace( "\"", "" ) ) )
      {
        if ( StrongEtagEquality.instance( ).equals( value.replace( "\"", "" ), etag.replace( "\"", "" ) ) ) 
        { 
          return true; 
        }
      }
      if ( WeakEtagEquality.instance( ).isValidator( value.replace( "\"", "" ) ) )
      {
        if ( WeakEtagEquality.instance( ).equals( value.replace( "\"", "" ), etag.replace( "\"", "" ) ) ) 
        { 
          return true; 
        }
      }
    }
    return false;
  }
  
  /**
   * Checks if the conditions in the request hold.
   * @param aRequest The page of the request.
   * @param responseMessage The response message to use in the verification.
   * @return True, if they hold.
   */
  private boolean checkCondition( final ProxyPageElement aRequest, final ProxyResponseMessage responseMessage )
  {
    boolean match1 = true;
    boolean match2 = true;
    boolean match3 = true;
    boolean match4 = true;
    
    //Check for an If-Match condition
    if ( aRequest.getRequest( ).containsHeader( "If-Match" ) )
    {
      this.logger.debug( "There is an If-Match conditional request." );
      //Get the existing etag
      String etag = "";
      if ( responseMessage.containsHeader( "ETag" ) )
      {
        this.logger.debug( "There is an E-tag in the request." );
        etag = responseMessage.getHeader( "Etag" );
      }
      
      //Check if the tag does not match
      match1 = !this.ifMatchVerifier( aRequest.getRequest( ).getHeader( "If-Match" ), etag );
    }

    //Check for an If-None-Match condition
    if ( aRequest.getRequest( ).containsHeader( "If-None-Match" ) )
    {
      this.logger.debug( "There is an If-None-Match conditional request." );
      //Get the existing etag
      String etag = "";
      if ( responseMessage.containsHeader( "ETag" ) )
      {
        this.logger.debug( "There is an E-tag in the request." );
        etag = responseMessage.getHeader( "ETag" );
      }
      
      //Check if the tag matchs
      match2 = this.ifMatchVerifier( aRequest.getRequest( ).getHeader( "If-None-Match" ), etag );
    }
    
    //Check for an If-Modified-Since condition
    if ( aRequest.getRequest( ).containsHeader( "If-Modified-Since" ) )
    {
      this.logger.debug( "There is an If-Modified-Since conditional request." );
      //Get the existing modified since
      String modifiedDate = "";
      if ( responseMessage.containsHeader( "Last-Modified" ) )
      {
        this.logger.debug( "There is an Last Modified in the request." );
        modifiedDate = responseMessage.getHeader( "Last-Modified" );
      }
      
      //Check if the date does not match
      match3 = LastModifiedEquality.instance( ).equals( aRequest.getRequest( ).getHeader( "If-Modified-Since" ), modifiedDate );
    }
    
    //Check for an If-Unmodified-Since condition
    if ( aRequest.getRequest( ).containsHeader( "If-Unmodified-Since" ) )
    {
      this.logger.debug( "There is an If-Unmodified-Since conditional request." );
      
      //Get the existing modified since
      String modifiedDate = "";
      if ( responseMessage.containsHeader( "Last-Modified-Since" ) )
      {
        this.logger.debug( "There is an Last Modified in the request." );
        modifiedDate = responseMessage.getHeader( "Last-Modified-Since" );
      }
      
      //Check if the date matches
      match4 = !LastModifiedEquality.instance( ).equals( aRequest.getRequest( ).getHeader( "If-Unmodified-Since" ), modifiedDate );
    }
    
    return match1 && match2 && match3 && match4; 
  }
  
  /**
   * Returns the page in cache, if existing.
   * @param aRequest The request to use for the query.
   * @return The page in cache or null, if something happens.
   */
  private CacheResponse getCachedPage( final ProxyPageElement aRequest )
  {
    CacheResponse response = null;
    final CacheQuery query = new CacheQuery(CacheQueryOperation.QUERY,
      aRequest.getCurrentUUID(), aRequest.getRequest().getMethod(),
      aRequest.getRequest().getCompleteUri(), "http");
    try
    {
      CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( 
          "The VFC conditional cache consistency was interrupted.", ex );
    }
    
    try
    {
      response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( 
          "The VFC conditional cache consistency was interrupted.", ex );
    }
    
    return response;
  }
  
  /**
   * Adds the page to the cache.
   * @param aRequest The request to use for the query.
   * @return The response of the cache.
   */
  private CacheResponse addCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isVirtuallyCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      CacheResponse response = null;
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.ADD, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return new CacheResponse( CacheQueryOperation.ADD, UUID.randomUUID( ),
           CacheResponseOperation.ERROR );
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The VFC standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The VFC standard cache consistency was interrupted.", ex );
      }
      
      return response;
    }
    return new CacheResponse( CacheQueryOperation.ADD, UUID.randomUUID( ), 
        CacheResponseOperation.ERROR );
  }
  
  /**
   * Changes the page in cache, to the given one.
   * @param aRequest The request to use for the query.
   * @return The response of the cache.
   */
  private CacheResponse changeCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isVirtuallyCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      CacheResponse response = null;
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.CHANGE, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return new CacheResponse( CacheQueryOperation.CHANGE, UUID.randomUUID( ),
          CacheResponseOperation.ERROR );
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      return response;
    }
    return new CacheResponse( CacheQueryOperation.CHANGE, UUID.randomUUID( ),
      CacheResponseOperation.ERROR );
  }
    
  /**
   * Returns the string containing the action id of this proxy.<br/>
   * The string is "VFC Conditional Request Action".
   * @return The id of this action.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#getActionID()
   */
  @Override( )
  public String getActionID( )
  {
    return "VFC Conditional Request Action";
  }
  
}
