/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy;

import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.perfectjpattern.core.behavioral.chainofresponsibility.OnlyOneHandleStrategy;

/**
 * This class represents an null action/processor, that can handle everything.
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 */
public final class NullProxyActionImpl extends ProxyAction
{
  
  /**
   * Constructor for a null action.
   * @param successor The successor of this action.
   * @param config The configuration of this action.
   */
  public NullProxyActionImpl( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.setChainStrategy( OnlyOneHandleStrategy.getInstance( ) );
  }
  
  /**
   * Checks if this processor can handle the given request.
   * @param aRequest The request to check.
   * @return Always true.
   * @throws IllegalArgumentException Never thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return true;
  }
  
  /**
   * Handles the given request.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException Never thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
  }
  
  /**
   * Returns the identifier of this processor.
   * @return An string containing NullAction, that is the processor identifier.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#getActionID()
   */
  @Override( )
  public String getActionID( )
  {
    return "NullAction";
  }
  
}
