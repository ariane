/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.html.visitors;

import java.io.Serializable;
import java.util.Comparator;
import java.util.UUID;
import com.assembla.ariane.jellyfish.util.collections.vfc.VFCPageList;
import com.assembla.ariane.jellyfish.proxy.vfc.VFCPage;
import com.google.common.base.Objects;

/**
 * This is a simple class to hold information about a visited link.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public final class VFCLinkInformation implements Serializable
{
  
  /** This class version. */
  private static final long serialVersionUID = 6188085385452745532L;

  /** The link url. */
  private String urlName;
  
  /** The type of link. */
  private VisitedLinkType linkType;
  
  /** An comparator that takes in attention only the page url. */
  public static final VFCLinkUrlComparator URLComparator = new VFCLinkUrlComparator( );
    
  /**
   * Constructor that creates a new information holder. 
   * @param urlName The name of the link url.
   * @param linkType The link type.
   */
  public VFCLinkInformation( final String urlName, final VisitedLinkType linkType )
  {
    if( urlName == null )
    {
      throw new NullPointerException( "The url of the link is null" );
    }
    
    this.urlName = urlName;
    this.linkType = ( linkType == null ) ? VisitedLinkType.UNKNOWN : linkType;
    
  }

  /**
   * Returns the url of the link represented by this object.
   * @return The link url.
   */
  public synchronized String getUrlName( )
  {
    return this.urlName;
  }

  /**
   * Returns the type of link represented by this object.
   * @return The link type.
   */
  public synchronized VisitedLinkType getLinkType( )
  {
    return this.linkType;
  }

  /**
   * Checks if the given objects are equal.
   * @param other The other link to compare.
   * @return True, if both links are equal.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object other )
  {
    if( other instanceof VFCLinkInformation )
    {
      final VFCLinkInformation info = ( VFCLinkInformation ) other;
      
      return this.urlName.equals( info.getUrlName( ) ) &&
             this.linkType.equals( info.getLinkType( ) );
    }
    return false;
  }
  
  /**
   * Returns an VFCPage corresponding to this url.<br/>
   * Note that a new page may be created and inserted in the
   * list of VFC Pages.
   * @param parentPage The parent of this page.
   * @return The page corresponding to this url.
   */
  public VFCPage getPage( final VFCPage parentPage )
  {
    //(check) if there is an page with the given url
    VFCPage page = VFCPageList.instance( ).get( this.urlName );
    
    if( page != null )
    {
      return page;
    }
    else //else (there is no page with the given url)
    {
      //create a new page using createUsingParent( parent_page, visitor);
      page = VFCPage.createUsingParent( parentPage, this.linkType, this.urlName, 
          UUID.randomUUID( ) );
      
      VFCPageList.instance( ).add( page );
      return page;
    }
    
  }

  /**
   * Calculates and returns an hash code.
   * @return The calculated hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this.linkType, this.urlName );
  }

  /**
   * Returns an string representation of this link information.
   * @return An string with information about this link.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    return String.format( "(%s, %s)", this.linkType.toString( ), this.urlName );
  }
  
  /**
   * This class compares an link information object using their urls.
   * @author Fat Cat
   * @since 0.0.7
   * @version 1
   */
  public static final class VFCLinkUrlComparator implements Comparator< 
      VFCLinkInformation >, Serializable
  {

    /** The class version. */
    private static final long serialVersionUID = 5152547482915388984L;

    /** Creates a new comparator. */
    public VFCLinkUrlComparator( )
    {      
    }
    
    /**
     * Compares the links using only the url.
     * @param left The left link to compare.
     * @param right The right link to compare.
     * @return The value 0 if the right page url is equal to the left page url; 
     * a value less than 0 if the left page url is lexicographically less than 
     * the right page url; and a value greater than 0 if the left page url is 
     * lexicographically greater than the right page url.
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override( )
    public int compare( final VFCLinkInformation left, final VFCLinkInformation right )
    {
      return left.getUrlName( ).compareTo( right.getUrlName( ) );
    }
    
  }
  
}
