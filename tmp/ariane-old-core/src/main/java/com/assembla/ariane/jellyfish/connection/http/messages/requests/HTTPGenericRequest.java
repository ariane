/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.messages.requests;

import io.netty.handler.codec.http.DefaultHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpVersion;
import java.util.UUID;
import com.assembla.ariane.jellyfish.connection.http.messages.HTTPGenericMessage;
import com.assembla.ariane.jellyfish.proxy.messages.requests.GenericRequest;

/**
 * This class holds information about a generic request.
 * @author Fat Cat
 * @version 2
 * @since 0.0.6
 */
public class HTTPGenericRequest extends HTTPGenericMessage implements GenericRequest
{
  
  /** The request. */
  protected final HttpRequest request;
  
  /**
   * Copy constructor that initializes this generic request with the given request.
   * @param message The inner message to use.
   * @param requestId The request id.
   */
  public HTTPGenericRequest( final HttpRequest message, final UUID requestId )
  {
    super( message, requestId );
    this.request = message;
  }
  
  /**
   * Creates a new empty generic request.
   * @param requestId The request id.
   */
  public HTTPGenericRequest( final UUID requestId )
  {
    this( new DefaultHttpRequest( HttpVersion.HTTP_1_1, HttpMethod.GET, "" ), requestId );
  }
  
  // Http method stuff
  
  /**
   * Returns the native http method.
   * @return An netty HttpMethod.
   */
  public HttpMethod getNativeMethod( )
  {
    return this.request.getMethod( );
  }
  
  /**
   * Returns the http method as a string.
   * @return An string representing the http method.
   */
  @Override( )
  public String getMethod( )
  {
    return this.request.getMethod( ).toString( );
  }
  
  /**
   * Sets the http method.
   * @param method The http method as a HttpMethod object.
   */
  public void setMethod( final HttpMethod method )
  {
    if ( ( method != null ) && ( !this.request.getMethod( ).equals( method ) ) )
    {
      this.request.setMethod( method );
    }
  }
  
  /**
   * Sets the http method.
   * @param method The http method as a string.
   */
  @Override( )
  public void setMethod( final String method )
  {
    this.setMethod( HttpMethod.valueOf( method ) );
  }

  // Uri stuff

  /**
   * Returns the uri of this request.
   * @return The uri of this request.
   */
  @Override( )
  public String getUri( )
  {
    return this.request.getUri( );
  }

  /**
   * Changes the uri of this request.
   * @param uri The new uri of this request.
   */
  @Override( )
  public void setUri( final String uri )
  {
    if ( ( uri != null ) && ( !this.request.getUri( ).equals( uri ) ) )
    {
      this.request.setUri( uri );
    }
  }

  // Inner http request
  
  /**
   * Returns the native netty HTTPRequest object.
   * @return The inner HTTPRequest.
   */
  public HttpRequest getNativeRequest( )
  {
    return this.request;
  }
  
}
