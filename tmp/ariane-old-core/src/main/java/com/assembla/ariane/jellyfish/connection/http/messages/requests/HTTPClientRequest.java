/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.messages.requests;

import io.netty.handler.codec.http.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.util.*;
import org.slf4j.LoggerFactory;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;

/**
 * This class implements an Client Request with fields used by com.assembla.ariane.jellyfish.
 * @author Fat Cat
 * @serial 0.0.1
 * @version 4
 */
public class HTTPClientRequest extends HTTPGenericRequest
{
  
  /**
   * The size of the string "bytes=".
   */
  private static final int RANGE_HEADER_BYTE_START = 6;

  /**
   * Converts an http request to a generic proxy request.
   * @param http_request The http request to convert.
   * @return An corresponding generic proxy request.
   */
  public static ProxyRequestMessage messageConverter(
    HTTPClientRequest http_request )
  {
    final ProxyRequestMessage request = new ProxyRequestMessage( http_request.getMessageId( ) );
    
    request.setMethod( http_request.getMethod() );
    request.setProtocol( ProxyProtocol.create( http_request.getNativeProtocolVersion( ).getMajorVersion( ),
                                               http_request.getNativeProtocolVersion( ).getMinorVersion( ),
                                               http_request.getNativeProtocolVersion( ).getProtocolName( ) ) );
    
    request.setContent( http_request.getContent( ) );
    
    //Complete URI
    StringBuffer sb = new StringBuffer( );
    if ( !http_request.getUri( ).contains( http_request.getHeader( "Host" ) ) )
    {      
      sb.append( http_request.getHeader( "Host" ) );
    }
    sb.append( http_request.getUri( ) );
    
    request.setCompleteUri( sb.toString( ) );
    
    //Relative URI
    sb = new StringBuffer( );
    
    String req = http_request.getUri( );
    if ( http_request.getUri( ).contains( http_request.getHeader( "Host" ) ) )
    {
      if ( !http_request.getHeader( "Host" ).startsWith( "http://" ) )
      {
        sb.append( "http://" );
      }
      sb.append( http_request.getHeader( "Host" ) );
      req = http_request.getUri( ).replaceFirst( sb.toString( ), "" );
    }
    
    request.setUri( req );
    
    if ( http_request.isKeepAlive( ) )
    {
      request.setKeepAlive( );
    }
    
    request.setDockName( http_request.getDockName( ) );
    
    //Headers
    for ( String headerName : http_request.getHeaderNames( ) )
    {
      request.addHeader( headerName, http_request.getHeader( headerName ) );
    }
    
    //Extra values
    for ( String extraName : http_request.getExtraValueNames( ) )
    {
      request.addExtra( extraName, http_request.getExtra( extraName ) );
    }
    
    //Ranges
    for ( Pair< Integer, Integer > pair : http_request.getRanges() )
    {
      request.addRange( pair );
    }
    
    //Hop Headers
    for ( String hopHeader : http_request.getHopHeaders( )  )
    {
      request.addHopHeader( hopHeader );
    }
    
    return request;
  }
  
  /**
   * Converts an generic proxy request to a http request.
   * @param proxy_request The generic proxy request.
   * @return The corresponding http request.
   */
  public static HTTPClientRequest messageConverter(
    ProxyRequestMessage proxy_request )
  {
    final HTTPClientRequest request = new HTTPClientRequest( proxy_request.getMessageId( ) );
    
    request.setContent( proxy_request.getContent( ) );
    request.setDockName( proxy_request.getDockName( ) );
    request.setProtocolVersion( HttpVersion.valueOf( proxy_request.getProtocolVersion( ) ) );
    request.setMethod( HttpMethod.valueOf( proxy_request.getMethod( ) ) );
    request.setUri( proxy_request.getUri( ) );
    
    if ( proxy_request.isKeepAlive( ) )
    {
      request.setKeepAlive( );
    }
    
    //extra values
    for ( String extraName : proxy_request.getExtraValueNames( ) )
    {
      request.addExtra( extraName, proxy_request.getExtra( extraName ) );
    }
    
    //header names
    for ( String headerName : proxy_request.getHeaderNames( ) )
    {
      request.addHeader( headerName, proxy_request.getHeader( headerName ) );
    }
            
    //ranges
    for ( Pair< Integer, Integer > pair : proxy_request.getRanges( ) )
    {
      request.addRange( pair );
    }
    
    //hop headers
    for ( String hopHeader : proxy_request.getHopHeaders( ) )
    {
      request.addHopHeader( hopHeader );
    }
    
    return request;
  }

  /**
   * Adds the remaining headers to the request. 
   * @param request The request to add the headers.
   * @return The request with all the headers.
   */
  public static HTTPClientRequest addRemainingHeaders( final HTTPClientRequest request )
  {
    if( request.hasRanges( ) )
    { 
      final StringBuilder sb = new StringBuilder( );
      sb.append( "bytes=" );
      for ( Pair< Integer, Integer > pair : request.getRanges( ) )
      {
        if ( ( pair.getFirst( ) == -1 ) && ( pair.getSecond( ) != -1 ) ) // max only
        {
          sb.append( String.format( "-%d", pair.getSecond( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) == -1 ) ) // min only
        {
          sb.append( String.format( "%d-", pair.getFirst( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) != -1 ) ) // both
        {
          sb.append( String.format( "%d-%d", pair.getFirst( ), pair.getSecond( ) ) );
        }
      }
      request.addHeader( "Range", sb.toString( ) );    
    }    
    return request;
  }
  
  /**
   * Copy constructor that initializes this client request with the given request.
   * @param message The inner message to use.
   * @param requestId The request id.
   */
  public HTTPClientRequest( final HttpRequest message, final UUID requestId )
  {
    super( message, requestId );
    this.logger = LoggerFactory.getLogger( HTTPClientRequest.class );
  }
  
  /**
   * Creates a new empty client request.
   * @param requestId The request id.
   */
  public HTTPClientRequest( final UUID requestId )
  {
    super( requestId );
    this.logger = LoggerFactory.getLogger( HTTPClientRequest.class );
  }
  
  // Utilities
  
  /**
   * Checks if the given request is equal to this one.
   * @param obj The request to compare to.
   * @return True if it is.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals( final Object obj )
  {
    return obj instanceof HTTPClientRequest &&
      this.messageId.equals( ( ( HTTPClientRequest ) obj ).getMessageId() );
  }
  
  /**
   * Calculates an hash code for this object.
   * @return The hash code of this object
   */
  public int hashCode( )
  {
    return this.messageId.hashCode( );
  }
  
  // Check
  
  /**
   * Checks this request for consistency.
   */
  public void check( )
  {
    this.logger.debug( "Checking the received message {} with ID {}",
      this.request.getUri( ), this.messageId );
    
    if ( !handleOptionTrace( ) ) { return; }
    
    if ( ( this.getNativeMethod( ) != HttpMethod.GET ) &&
         ( this.getNativeMethod( ) != HttpMethod.HEAD ) &&
           ( !this.containsHeader( HttpHeaders.Names.CONTENT_LENGTH ) ) )
    {
      this.logger.warn( "{}: The request requires the content length header", this.messageId );
      this.extra.put( "error-number", "411" );
      this.extra.put( "error-message", "Length Required" );
      return;
    }
    
    this.parseCacheControl( );
    
    if ( !this.parseHostHeader( ) ) 
    { 
      return; 
    }
    
    if ( this.containsHeader( "Date" ) )
    {
      try
      {
        HttpHeaders.getDate( this.request );
      }
      catch( final ParseException e )
      {
        this.logger.warn( "{}: The date header is invalid", this.messageId );
        this.extra.put( "error-number", Integer.toString(
          JellyfishConstants.HTTPErrorCodes.BAD_REQUEST.errorcode ) );
        this.extra.put( "error-message",
          "Bad Request: The date header is invalid" );
        return;
      }
    }
    
    if ( HttpHeaders.is100ContinueExpected( this.request ) &&
       ( this.request.getProtocolVersion( ) == HttpVersion.HTTP_1_0 ) )
    {
      this.extra.put( "continue", "true" );
    }
    
    if ( HttpHeaders.is100ContinueExpected( this.request ) )
    {
      this.addExtra( "virtual-no-cache", "true" );
    }
    
    if ( this.containsHeader( HttpHeaders.Names.PRAGMA ) &&
       ( !this.containsHeader( HttpHeaders.Names.CACHE_CONTROL ) ) )
    {
      this.logger.warn( "{}: Parsing pragma header!", this.messageId );
      this.addExtra( "no-cache", "true" );      
    }
    
    if ( this.containsHeader( HttpHeaders.Names.UPGRADE ) )
    {
      this.logger.warn( "{}: Upgrade header is present...", this.messageId );
      this.extra.put( "error-number", "501" );
      this.extra.put( "error-message", "Not Implemented: The upgrade header is not supported" );
    }
    
    this.parseRangeHeader( );
    
    this.checkConnection( );
  }
  
  /**
   * Parses the connection header if available.
   */
  public void checkConnection( )
  {
    for ( String connection : this.getHeaders( "Connection" ) )
    {
      for ( String value : connection.split( "," ) )
      {
        if ( ( !value.toLowerCase( ).equals( "close" ) ) && ( !value.toLowerCase( ).equals( "keep-alive" ) ) )
        {
          this.hopHeaders.add( value );
        }
      }
    }
    
    //Standard hop headers
    this.hopHeaders.add( "Connection" );
    this.hopHeaders.add( "Proxy-Authorization" );
    this.hopHeaders.add( "TE" );
    this.hopHeaders.add( "Upgrade" ); 
  }
  
  /**
   * Handles an trace request.
   * @return True, if there is no error, or no request to answer.
   */
  private boolean handleOptionTrace( )
  {
    if ( ( this.request.getMethod( ) == HttpMethod.TRACE ) ||
         ( this.request.getMethod( ) == HttpMethod.OPTIONS ) )
    {
      if ( this.containsHeader( HttpHeaders.Names.MAX_FORWARDS ) )
      {
        this.logger.warn( "{}: Header max-forwards present", this.messageId );
        try
        {
          final int hops = Integer.parseInt( this.getHeader( HttpHeaders.Names.MAX_FORWARDS ) );
          if ( hops <= 0 )
          {
            this.logger.warn( "{}: Answering the trace or option message", this.messageId );
            this.extra.put( "error-number", "200" );
            this.extra.put( "error-message", "OK" );
            return false;
          }
          else
          {
            this.extra.put( "hops", Integer.toString( hops - 1 ) );
          }
        }
        catch( final NumberFormatException ex )
        {
          this.logger.warn( "{}: max-forwards header is invalid", this.messageId );
          this.extra.put( "error-number", Integer.toString( JellyfishConstants.HTTPErrorCodes.BAD_REQUEST.errorcode ) );
          this.extra.put( "error-message", "Bad Request: Max-forward does not represent an number" );
          return false;
        }
      }
      
      if ( this.containsHeader( HttpHeaders.Names.VIA ) )
      {
        this.logger.info( "{}: Parsing Via header", this.messageId );
        final StringBuilder sb = new StringBuilder( );
        sb.append( this.getHeader( HttpHeaders.Names.VIA ) );
        sb.append( this.request.getProtocolVersion( ).getMajorVersion( ) );
        sb.append( "." );
        sb.append( this.request.getProtocolVersion( ).getMinorVersion( ) );
        sb.append( " " );
        sb.append( "Jellyfish" );
        this.extra.put( HttpHeaders.Names.VIA, sb.toString( ) );
      }
    }
    return true;
  }
  
  /**
   * Parses the range header.
   */
  private void parseRangeHeader( )
  {
    if ( this.containsHeader( HttpHeaders.Names.RANGE ) )
    {
      this.logger.info( "{}: Parsing range header", this.messageId );
      
      final String header_value = this.getHeader( HttpHeaders.Names.RANGE );
      
      if ( header_value.startsWith( "bytes=" ) )
      {
        final List< String > ranges = Arrays.asList(
          header_value.substring(
            HTTPClientRequest.RANGE_HEADER_BYTE_START ).trim().split( "," ) );
        
        // Get a list of ranges.
        int max, min;
        for ( final String current_range : ranges )
        {
          try
          {
            if ( current_range.startsWith( "-" ) )
            {
              max = Integer.parseInt( current_range.substring( 1 ) );
              this.addRangeMax( max );
            }
            else if ( current_range.endsWith( "-" ) )
            {
              min = Integer.parseInt( current_range.substring( 0,
                current_range.length( ) - 1 ) );
              this.addRange( min );
            }
            else
            {
              min = Integer.parseInt( current_range.split( "-" )[ 0 ] );
              max = Integer.parseInt( current_range.split( "-" )[ 1 ] );
              this.addRange( min, max );
            }
          }
          catch( final NumberFormatException | IndexOutOfBoundsException ex )
          {
            this.logger.debug( "There was an error parsing an range in request {}",
              this.messageId != null ? this.messageId : "" );
          }
        }
      }
    }
    request.removeHeader( "Range" );
  }
  
  /**
   * Method to parse the cache control header.
   */
  private void parseCacheControl( )
  {
    if ( this.containsHeader( "Cache-Control" ) )
    {
      this.logger.debug( "{}: Parsing cache-control", this.messageId );
      final List< String > list = Arrays.asList( this.getHeader( "Cache-Control" ).split( "," ) );
      
      simpleCacheControl( list );
      
      complexCacheControl( list );
    }
  }
  
  /**
   * Method to parse an cache control value with an option.
   * @param list The list of cache control values.
   */
  private void complexCacheControl( final List< String > list )
  {
    for ( final String directive : list )
    {
      if ( directive.startsWith( "max-age" ) )
      {
        this.logger.debug( "{}: Max-age specified...", this.messageId );
        if ( directive.split( "=" ).length > 1 )
        {
          try
          {
            final int max_age = Integer.parseInt( directive.split( "=" )[ 1 ].trim( ) );
            this.extra.put( "max-age", Integer.toString( max_age ) );
          }
          catch( final NumberFormatException ex )
          {
            this.logger.warn( "{}: Max-age is not parseable!", this.messageId );
          }
        }
      }
      else if ( directive.startsWith( "max-stale" ) )
      {
        this.logger.debug( "{}: Max-stale specified...", this.messageId );
        if ( directive.split( "=" ).length > 1 )
        {
          try
          {
            final int max_age = Integer.parseInt( directive.split( "=" )[ 1 ].trim( ) );
            this.extra.put( "max-stale", Integer.toString( max_age ) );
          }
          catch( final NumberFormatException ex )
          {
            this.logger.warn( "{}: Max-stale is not parseable!", this.messageId );
          }
        }
      }
      else if ( directive.startsWith( "max-fresh" ) )
      {
        this.logger.debug( "{}: Max-fresh specified...", this.messageId );
        if ( directive.split( "=" ).length > 1 )
        {
          try
          {
            final int max_age = Integer.parseInt( directive.split( "=" )[ 1 ].trim( ) );
            this.extra.put( "max-fresh", Integer.toString( max_age ) );
          }
          catch( final NumberFormatException ex )
          {
            this.logger.warn( "{}: Max-fresh is not parseable!", this.messageId );
          }
        }
      }
    }
  }
  
  /**
   * Parses the cache control values without an attached value.
   * @param list The list of cache control values.
   */
  private void simpleCacheControl( final List< String > list )
  {
    if ( list.contains( "no-cache" ) )
    {
      this.logger.debug( "{}: No-cache specified...", this.messageId );
      this.extra.put( "no-cache", "true" );
    }
    if ( list.contains( "no-store" ) )
    {
      this.logger.debug( "{}: No-store specified...", this.messageId );
      this.extra.put( "no-cache", "true" );
      this.extra.put( "no-store", "true" );
    }
    if ( list.contains( "no-transform" ) )
    {
      this.logger.debug( "{}: No-transform specified...", this.messageId );
      this.extra.put( "no-transform", "true" );
    }
    if ( list.contains( "only-if-cached" ) )
    {
      this.logger.debug( "{}: Only-if-cached specified...", this.messageId );
      this.extra.put( "only-if-cached", "true" );
    }
  }
  
  /**
   * Parses the host header.
   * @return true, if successful
   */
  private boolean parseHostHeader( )
  {
    if ( !this.containsHeader( HttpHeaders.Names.HOST ) )
    {
      this.logger.warn( "{}: Host header is not present.", this.messageId );
      this.extra.put( "error-number", Integer.toString( JellyfishConstants.HTTPErrorCodes.BAD_REQUEST.errorcode ) );
      this.extra.put( "error-message", "Bad Request: The host header is absent." );
      return false;
    }
    else
    {
      return true;
    }
  }
  
  //Utilities
  
  /**
   * Clones this http client response message.
   * @return A new identical http client response message.
   */
  public HTTPClientRequest copy( )
  {
    try
    //main clone method uses serialization
    {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream( );
      final ObjectOutputStream out = new ObjectOutputStream( baos );
      out.writeObject( this );
      
      final ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray( ) );
      final ObjectInputStream ois = new ObjectInputStream( bais );
      return ( HTTPClientRequest ) ois.readObject( );
    }
    catch( final IOException | ClassNotFoundException e ) //An alternate clone method, just in case...
    {
      final HTTPClientRequest request = new HTTPClientRequest( this.messageId );
      
      request.setContent( this.getContent( ).copy( ) );
      request.setDockName( this.getDockName( ) );
      request.setProtocolVersion( HttpVersion.valueOf( this.getProtocolVersion() ) );
      request.setMethod( HttpMethod.valueOf( this.getMethod() ) );
      request.setUri( this.getUri( ) );
      
      if ( request.isKeepAlive( ) )
      {
        request.setKeepAlive( );
      }
      
      //Add the headers
      for ( String header : this.getHeaderNames( ) )
      {
        for ( String value : this.getNativeRequest( ).getHeaders( header ) )
        {
          request.getNativeRequest( ).addHeader( header, value );
        }
      }
      
      //Add the extra
      for ( String extra_names : this.getExtraValueNames( ) )
      {
        request.addExtra( extra_names, this.getExtra( extra_names ) );
      }
      
      //Add the ranges
      for ( Pair< Integer, Integer > range : this.getRanges( ) )
      {
        request.addRange( range );
      }
      
      //Add the hop headers
      for ( String hopHeader : this.getHopHeaders( ) )
      {
        request.addHopHeader( hopHeader );
      }
      
      return request;
    }
  }
}
