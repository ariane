/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.vfc;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import com.google.common.base.Objects;

/**
 * This class represents an consistency zone (or an vector-field consistency zone).
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public final class ConsistencyZone implements Serializable
{
  
  /** This class version. */
  private static final long serialVersionUID = 4469316955776283080L;
  
  private final String pivotUrl;
  
  /** The max accepted vfc vector.*/
  private VFCVector maxVFC;
  
  /** An list with all the pages in this zone.*/
  private List< VFCPage > pages;
  
  /** An comparator that uses the consistency zone maximum vfc value.*/
  public static final VFCComparator MaximumVFCComparator = new VFCComparator( );
  
  /**
   * Creates a new consistency zone.
   * @param maxVFC The maximum VFCVector of this consistency zone.
   * @param pivotUrl The url of the pivot containing this consistency zone.
   */
  public ConsistencyZone( final VFCVector maxVFC, final String pivotUrl )
  {
    this.maxVFC = maxVFC;
    this.pages = Collections.synchronizedList( new LinkedList< VFCPage >( ) );
    this.pivotUrl = pivotUrl;
  }
  
  /**
   * Returns the maximum VFC-Vector that will be in this consistency zone.
   * @return The maximum VFC-Vector of this zone.
   */
  public synchronized VFCVector getMaxVFC( )
  {
    return this.maxVFC;
  }
  
  /**
   * Returns the url of the pivot containing this consistency zone.
   * @return The pivot url.
   */
  public synchronized String getPivotUrl( )
  {
    return this.pivotUrl;
  }

  /**
   * Returns an read-only list with all the pages in this consistency zone.
   * @return An list of all contained pages.
   */
  public synchronized List< VFCPage > getPages( )
  {
    return Collections.unmodifiableList( this.pages );
  }
  
  /**
   * Adds a new page to this consistency zone.<br/>
   * Note that shouldBelongHere() is used before adding a page.
   * @param page The new page to add.
   */
  public synchronized void addPage( final VFCPage page )
  {
    if ( this.shouldBelongHere( page ) )
    {
      this.pages.add( page );
    }
  }
  
  /**
   * Removes the given page.<br/>
   * Note that if the page does not exist, than nothing is done.
   * @param page The page to remove.
   */
  public synchronized void removePage( final VFCPage page )
  {
    this.pages.remove( page );
  }
  
  /**
   * Return on first true page in collection.<br/> 
   * This operation returns on first match.
   * @param url The url to look for.
   * @return True, if there is an page with the given url. 
   */
  public boolean isPageKnown( final String url )
  {
    for ( VFCPage page : this.pages )
    {
      if ( page.getUrl( ).equals( url ) ) 
      { 
        return true; 
      }
    }
    return false;
  }
  
  /**
   * Return on first true page in collection.<br/>
   * This operation returns on first match and uses page.getVector() &lt;= page 
   * to filter pages, before iteration.
   * @param page The page to check for existence.
   * @return True, if the page exists.
   */
  public boolean isPageKnown( final VFCPage page )
  {
    if ( VFCVector.CONSISTENCY_RULES_COMPARATOR.compare( 
        this.maxVFC, page.getVector( this.pivotUrl ) ) <= 0 )
    {
      for ( VFCPage child : this.pages )
      {
        if ( child.equals( page ) ) 
        { 
          return true; 
        }
      }
    }
    return false;
  }
  
  /**
   * Returns the page corresponding to the given url or null 
   * if the page is unknown.
   * @param url The url to look for.
   * @return The page corresponding to the given url.
   */
  public VFCPage peekPageInformation( final String url )
  {
    for ( VFCPage page : this.pages )
    {
      if ( page.getUrl( ).equals( url ) ) 
      { 
        return page;
      }
    }
    return null;
  }
  
  /**
   * Checks if the given page is fresh.<br/>
   * This method locate the page and then uses the following operation, to 
   * check for freshness:<br/>
   * <code>page.getVector().recency &lt; this.vector.recency && 
   * page.getVector().frequency &lt; this.vector.frequency</code>
   * @param page The page to check for freshness.
   * @return True, if the page IS FOUND AND IS FRESH.
   */
  public boolean isPageFresh( final VFCPage page )
  {
    if ( VFCVector.CONSISTENCY_RULES_2D_COMPARATOR.compare( this.maxVFC, 
        page.getVector( this.pivotUrl ) ) <= 0 )
    {
      for ( VFCPage child : this.pages )
      {
        if ( child.equals( page ) )
        { 
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * Checks if the given page should belong in this consistency zone.<br/>
   * If the page is not inside and <code>page.getVector().recency &lt; this.vector.recency 
   * && page.getVector().frequency &lt; this.vector.frequency</code>, then return true.
   * @param page The page to check.
   * @return True, if the page should belong here.
   */
  public boolean shouldBelongHere( final VFCPage page )
  {
    if ( !this.isPageKnown( page ) )
    {
      return VFCVector.CONSISTENCY_RULES_COMPARATOR.compare( 
          this.maxVFC, page.getVector( this.pivotUrl ) ) <= 0;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Checks if the given object is equal to this one.
   * @param obj The object to check.
   * @return True, if it is.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  {
    if ( ( obj instanceof ConsistencyZone ) && ( obj != null ) )
    {
      final ConsistencyZone other = ( ConsistencyZone ) obj;
      
      if ( other.getMaxVFC( ).equals( this.maxVFC ) &&
           other.getPivotUrl( ).equals( this.pivotUrl ) )
      {
        for ( VFCPage page : other.getPages( ) )
        {
          return this.isPageKnown( page );
        }
      }
    }
    return false;
  }
  
  /**
   * Calculates and returns an hash code for this consistency zone.
   * @return The calculated hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this.maxVFC, this.pages, this.pivotUrl );
  }
  
  /**
   * Returns an string representation of this consistency zone.
   * @return An string representing this zone.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    final StringBuffer sb = new StringBuffer( );
    sb.append( String.format( "Maximum VFC: %s\n", this.maxVFC.toString( ) ) );
    sb.append( String.format( "Page Number: %d\n", this.pages.size( ) ) );
    sb.append( String.format( "Pivot Url: %s\n", this.pivotUrl ) );
    return sb.toString( );
  }
  
  /**
   * This is a comparator that compares two consistency zone according to their
   * maximum VFC vector.
   * @author Fat Cat
   * @since 0.0.7
   * @version 1
   */
  public static final class VFCComparator implements Comparator< ConsistencyZone >, Serializable
  {
    
    /** The comparator version. */
    private static final long serialVersionUID = -6663660413070950457L;

    /**
     * Creates a new comparator.
     */
    public VFCComparator( )
    {
      
    }

    /**
     * Compares the given consistency zones, using their maximum vfc value.
     * @param left The left consistency zone.
     * @param right The right consistency zone.
     * @return A negative integer, zero, or a positive integer as the left consistency 
     * zone maximum vector is less than, equal to, or greater than the 
     * right consistency zone maximum vector
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override( )
    public int compare( final ConsistencyZone left, final ConsistencyZone right )
    {
      return VFCVector.CONSISTENCY_RULES_COMPARATOR.compare( left.getMaxVFC( ), right.getMaxVFC( ) );
    }
  }
  
}
