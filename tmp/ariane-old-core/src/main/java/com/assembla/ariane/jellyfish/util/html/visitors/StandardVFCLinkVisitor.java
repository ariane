/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.html.visitors;

import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import org.htmlparser.Attribute;
import org.htmlparser.Tag;
import org.htmlparser.tags.FrameTag;
import org.htmlparser.tags.ImageTag;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.tags.ScriptTag;
import org.slf4j.LoggerFactory;

/**
 * This visitor visits all the links of a page and classifies
 * them according to the VFC distance algorithm.<br/>
 * So according to the algorithm:
 * <ul>
 *   <li>An img tag receives an distance of zero relative to the parent.</li>
 *   <li>An a (link) tag receives an distance of one relative to the parent.</li>
 *   <li>An link tag receives an distance of zero relative to the parent.</li>
 *   <li>An frame tag receives an distance of zero relative to the parent.</li>
 *   <li>An style tag receives an distance of zero relative to the parent.</li>
 * </ul>
 * The only exceptions are in the case of an pivot, in which all links except the frame tag 
 * receive and extra increment of one in the distance.<br/>
 * Because an pivot is supposed to be the only element with distance zero, ie.,
 * it lives isolated in an zone of its own, except in the case of a frame page,
 * since an frame page is supposed to be rendered as a whole.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public final class StandardVFCLinkVisitor extends BaseVisitor
{
  
  private SortedSet< VFCLinkInformation > linkList;
  
  /**
   * Creates a new VFC Link visitor, and
   * initializes all fields.
   */
  public StandardVFCLinkVisitor( )
  {
    this.linkList = Collections.synchronizedSortedSet( 
        new TreeSet< VFCLinkInformation >( VFCLinkInformation.URLComparator ) );
    this.logger = LoggerFactory.getLogger( StandardVFCLinkVisitor.class );
  }
  
  /**
   * Checks if the set of links, contains the given link.
   * @param link The link to check.
   * @return True, if the set contains the link, false otherwise.
   */
  public boolean containsUrl( final String link )
  {
    for( VFCLinkInformation info : this.linkList )
    {
      if( info.getUrlName( ).equals( link ) )
      {
        return true;
      }
    }
    return false;
  }
    
  /**
   * Returns an read-only set with all the parsed links and their distances.
   * @return An set of links and their distances.
   */
  public synchronized SortedSet< VFCLinkInformation > getLinkSet( )
  {
    return Collections.unmodifiableSortedSet( this.linkList );
  }
  
  /**
   * Clears the set of links.
   */
  public synchronized void clear( )
  {
    this.linkList.clear( );
  }
  
  /**
   * Checks if the link set is empty.
   * @return True, if it is, false otherwise.
   */
  public synchronized boolean isEmpty( )
  {
    return this.linkList.isEmpty( );
  }
  
  /**
   * Returns the number of elements in the link set.
   * @return The number of links in this set.
   */
  public synchronized int getSize( )
  {
    return this.linkList.size( );
  }

  /**
   * This method visits an image tag<br/> 
   * And adds it to the list of visited links, with an distance of zero,
   * since the pivots are handled somewhere else.
   * @param tag The visited image tag.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitImageTag(org.htmlparser.tags.ImageTag)
   */
  @Override( )
  protected void visitImageTag( final ImageTag tag )
  {
    super.visitImageTag( tag );
    if( ( tag.getImageURL( ) != null ) && 
        ( !this.containsUrl( 
            tag.getPage( ).getAbsoluteURL( tag.getImageURL( ) ) ) ) )
    {
      this.linkList.add( new VFCLinkInformation( 
          tag.getPage( ).getAbsoluteURL( 
              tag.getImageURL( ) ), VisitedLinkType.IMAGE ) );
    }
  }

  /**
   * This method visits an link tag<br/>
   * And adds it to the list of visited links, with an distance of one.
   * @param tag The visited link tag.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitLinkTag(org.htmlparser.tags.LinkTag)
   */
  @Override( )
  protected void visitLinkTag( final LinkTag tag )
  {
    super.visitLinkTag( tag );
    if( ( tag.getLink( ) != null ) && 
        ( tag.isHTTPLink( ) ) && 
        ( !this.containsUrl( 
            tag.getPage( ).getAbsoluteURL( tag.getLink( ) ) ) ) )
    {
      this.linkList.add( new VFCLinkInformation( tag.getPage( ).getAbsoluteURL( 
              tag.getLink( ) ), VisitedLinkType.ALINK ) );
    }
  }

  /**
   * This method visits an script tag.<br/>
   * And adds it to the list of links with an distance of zero.
   * @param tag The visited script tag.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitScriptTag(org.htmlparser.tags.ScriptTag)
   */
  @Override( )
  protected void visitScriptTag( final ScriptTag tag )
  {
    super.visitScriptTag( tag );
    final Attribute att = tag.getAttributeEx( "src" );
    
    if( ( att != null ) && att.isValued( ) )
    {
     
      final String absoluteUrl = tag.getPage( ).getAbsoluteURL( att.getValue( ) );
      
      if( ( absoluteUrl != null ) &&
          ( !this.containsUrl( absoluteUrl ) ) )
      {
        this.linkList.add( new VFCLinkInformation( absoluteUrl, 
            VisitedLinkType.SCRIPT ) );
      }
    }
  }

  /**
   * This method visits an image tag<br/> 
   * And adds it to the list of visited links, with and distance of zero,
   * since the pivots are handled somewhere else.
   * @param tag The visited image tag. 
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitImageEndTag(org.htmlparser.tags.ImageTag)
   */
  @Override( )
  protected void visitImageEndTag( final ImageTag tag )
  {
    super.visitImageEndTag( tag );
    if( ( tag.getImageURL( ) != null ) && 
        ( !this.containsUrl( 
            tag.getPage( ).getAbsoluteURL( tag.getImageURL( ) ) ) ) )
    {
      this.linkList.add( new VFCLinkInformation( 
          tag.getPage( ).getAbsoluteURL( 
              tag.getImageURL( ) ), VisitedLinkType.IMAGE ) );
    }
  }

  /**
   * This method visits an link tag<br/>
   * And adds it to the list of visited links, with an distance of one.
   * @param tag The visited link tag. 
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitLinkEndTag(org.htmlparser.tags.LinkTag)
   */
  @Override( )
  protected void visitLinkEndTag( final LinkTag tag )
  {
    super.visitLinkEndTag( tag );
    if( ( tag.getLink( ) != null ) && 
        ( tag.isHTTPLink( ) ) && 
        ( !this.containsUrl( 
            tag.getPage( ).getAbsoluteURL( tag.getLink( ) ) ) ) )
    {
      this.linkList.add( new VFCLinkInformation( tag.getPage( ).getAbsoluteURL( 
          tag.getLink( ) ), VisitedLinkType.ALINK ) );
    }
  }

  /**
   * This method visits an script tag.<br/>
   * And adds it to the list of links with an distance of zero.
   * @param tag The visited script tag. 
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitScriptEndTag(org.htmlparser.tags.ScriptTag)
   */
  @Override( )
  protected void visitScriptEndTag( final ScriptTag tag )
  {
    super.visitScriptEndTag( tag );
    final Attribute att = tag.getAttributeEx( "src" );
    
    if( ( att != null ) && att.isValued( ) )
    {
     
      final String absoluteUrl = tag.getPage( ).getAbsoluteURL( att.getValue( ) );
      
      if( ( absoluteUrl != null ) &&
          ( !this.containsUrl( absoluteUrl ) ) )
      {
        this.linkList.add( new VFCLinkInformation( absoluteUrl, 
            VisitedLinkType.SCRIPT ) );
      }
    }
  }
  
  /**
   * Method to visit an frame tag.<br/>
   * And add it to the list of links with an distance of one.
   * @param tag The visited frame tag.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitFrameTag(org.htmlparser.tags.FrameTag)
   */
  @Override( )
  protected void visitFrameTag( final FrameTag tag )
  {
    super.visitFrameTag( tag );
    if( ( tag.getFrameLocation( ) != null ) &&
        ( !this.containsUrl( tag.getFrameLocation( ) ) ) )
    {
      this.linkList.add( new VFCLinkInformation( tag.getFrameLocation( ), 
          VisitedLinkType.FRAME ) );
    }
  }

  /**
   * Method to visit an frame tag.<br/>
   * And add it to the list of links with an distance of one.
   * @param tag The visited frame tag. 
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitFrameEndTag(org.htmlparser.tags.FrameTag)
   */
  @Override( )
  protected void visitFrameEndTag( final FrameTag tag )
  {
    super.visitFrameEndTag( tag );
    if( ( tag.getFrameLocation( ) != null ) &&
        ( !this.containsUrl( tag.getFrameLocation( ) ) ) )
    {
      this.linkList.add( new VFCLinkInformation( tag.getFrameLocation( ), 
          VisitedLinkType.FRAME ) );
    }
  }

  /**
   * Method that visits an link tag.<br/>
   * And adds it to the list of links with an distance of zero. 
   * @param tag The possible visited link tag. 
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitGenericTag(org.htmlparser.Tag)
   */
  @Override( )
  protected void visitGenericTag( final Tag tag )
  {
    super.visitGenericTag( tag );    
    if( tag.getTagName( ).equalsIgnoreCase( "link" ) )
    {
      
      final Attribute att = tag.getAttributeEx( "href" ); 
      
      if( ( att != null ) && att.isValued( ) )
      {
        final String absoluteUrl = tag.getPage( ).getAbsoluteURL( att.getValue( ) );
        
        if( ( absoluteUrl != null ) &&
            ( !this.containsUrl( absoluteUrl ) ) )
        {
          this.linkList.add( new VFCLinkInformation( absoluteUrl, 
              VisitedLinkType.LINK ) );
        }
      }
    }
  }

  /**
   * Method that visits an link tag.<br/>
   * And adds it to the list of links with an distance of zero. 
   * @param tag The possible visited link tag.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#visitGenericEndTag(org.htmlparser.Tag)
   */
  @Override( )
  protected void visitGenericEndTag( final Tag tag )
  {
    super.visitGenericEndTag( tag );
    if( tag.getTagName( ).equalsIgnoreCase( "link" ) )
    {
      
      final Attribute att = tag.getAttributeEx( "href" ); 
      
      if( ( att != null ) && att.isValued( ) )
      {
        final String absoluteUrl = tag.getPage( ).getAbsoluteURL( att.getValue( ) );
        
        if( ( absoluteUrl != null ) &&
            ( !this.containsUrl( absoluteUrl ) ) )
        {
          this.linkList.add( new VFCLinkInformation( absoluteUrl, 
              VisitedLinkType.LINK ) );
        }
      }
    }
  }

  /**
   * Returns an unique hash code for this VFCLinkVisitor, that is 5000.
   * @return An hash code for this VFCLinkVisitor, that is 5000.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return 5000;
  }

  /**
   * Returns true if the other object is another VFCLinkVisitor.
   * @param obj The other object to compare.
   * @return True, if both objects are instances of VFCLinkVisitor.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  {
    return obj instanceof StandardVFCLinkVisitor;
  }
  
  /**
   * Returns an string representing this visitor.
   * @return An string representing this visitor.
   * @see com.assembla.ariane.jellyfish.util.html.visitors.BaseVisitor#toString()
   */
  @Override( )
  public String toString( )
  {
    return "VFC Link Visitor.";
  }
  
}
