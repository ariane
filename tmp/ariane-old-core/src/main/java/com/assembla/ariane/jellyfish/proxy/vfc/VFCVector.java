/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.vfc;

import java.io.Serializable;
import java.util.Comparator;
import com.google.common.base.Objects;

/**
 * This class represents an VFCVector, with all the used dimensions.<br/>
 * 
 * There are three dimensions on this VFCVector:<br/>
 * <ol>
 *  <li>Distance &rArr; This is the distance from the nearest pivot (or bookmarks).</li>
 *  <li>Recency &rArr; This is the global number of cache requests since the 
 *      caching of this entry.</li>
 *  <li>Frequency &rArr; This is the number of hits that this specific entry
 *      had since its caching.</li>
 * </ol>
 * @author Fat Cat
 * @version 1
 * @since 0.0.7
 */
public final class VFCVector implements Serializable
{

  /** The class version. */
  private static final long serialVersionUID = 2334297482301925022L;

  /** This is the distance to the nearest pivot. */
  private int distance;
  
  /** This is the number of total requests since this entry is cached. */
  private int recency;
  
  /** Comparator that uses the distance, recency and frequency in this order to compare two vectors. */
  public static final VFCVectorConsistencyComparator CONSISTENCY_RULES_COMPARATOR = new VFCVectorConsistencyComparator( );
  
  /** Comparator that uses the recency and frequency in this order to compare two vectors. */
  public static final VFCVector2DConsistencyComparator CONSISTENCY_RULES_2D_COMPARATOR = new VFCVector2DConsistencyComparator( );
  
  /** 
   * This is the number of requests to this entry since its cache.
   * 
   *  This is different from the above, since in the recency is the number
   *  of requests made to the proxy, since the cache of this entry, and the
   *  request page does not matter.
   *  
   *  On the other hand, frequency means the number of requests to this specific
   *  entry since the cache of this entry.
   */
  private int frequency;
    
  /**
   * Creates a new VFC-Vector.
   * @param distance The distance to the nearest pivot.
   * @param recency The recency (or oldness) value.
   * @param frequency The frequency (or hit) count.
   */
  public VFCVector( final int distance, final int recency, final int frequency )
  {
    super( );
    if( ( distance < 0 ) || ( recency < 0 ) || ( frequency < 0 ) )
    {
      throw new java.lang.NumberFormatException( "Some of the given dimensions are invalid." );
    }
    else
    {
      this.distance = distance;
      this.recency = recency;
      this.frequency = frequency;
    }
  }



  /**
   * Returns the recency value of this vector.
   * @return The recency or oldness of this vector.
   */
  public synchronized int getRecency( )
  {
    return this.recency;
  }

  /**
   * Changes the recency value of this vector.
   * @param recency The new recency value.
   */
  public synchronized void setRecency( final int recency )
  { 
    if( recency > 0 )
    {
      this.recency = recency;
    }
  }
  
  /**
   * Increments the recency value by one.<br/>
   * This is an utility method that is equivalent to:<br/>
   * <code>this.setRecency( this.getRecency( ) + 1 );</code>
   */
  public synchronized void incrementRecency( )
  {
    this.recency += 1;
  }
  
  /**
   * Resets the recency, ie. puts the counter back to zero.<br/>
   * This is an utility method that is equivalent to:<br/>
   * <code>this.setRecency( 0 );</code>
   */
  public synchronized void resetRecency( )
  {
    this.recency = 0;
  }

  /**
   * Returns the frequency count.
   * @return The frequency or hit count.
   */
  public synchronized int getFrequency( )
  {
    return this.frequency;
  }

  /**
   * Changes the frequency count. 
   * @param frequency The new frequency value.
   */
  public synchronized void setFrequency( final int frequency )
  {
    if( frequency > 0 )
    {
      this.frequency = frequency;
    }
  }

  /**
   * Increments the frequency value by one.<br/>
   * This is an utility method that is equivalent to:<br/>
   * <code>this.setFrequency( this.getFrequency( ) + 1 );</code>
   */
  public synchronized void incrementFrequency( )
  {
    this.frequency += 1;
  }

  /**
   * Returns the distance to the nearest pivot. 
   * @return The current distance value.
   */
  public synchronized int getDistance( )
  {
    return this.distance;
  }

  /**
   * Checks if this VFC-Vector is equal to the one received in the argument.
   * @param obj The other VFC-Vector.
   * @return True, if they are.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  {
    if( ( obj == null ) || !( obj instanceof VFCVector ) )
    {
      return false;
    }
    else
    {
      final VFCVector vector = ( VFCVector ) obj;
      if( ( this.distance == vector.getDistance( ) ) &&
          ( this.recency == vector.getRecency( ) ) &&
          ( this.frequency == vector.getFrequency( ) ) )
      {
        return true;
      }
      else
      {
        return false;
      }
    }      
  }

  /**
   * Calculates and returns an hash code for this vector.
   * @return The calculates hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this.distance, this.frequency, this.recency );
  }

  /**
   * Returns an string representation of this vector.
   * The format of the string is:<br/>
   * <code>( distance, recency, frequency )</code>
   * @return An string representation of this vector.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    final StringBuffer sb = new StringBuffer( );
    sb.append( "( " );
    sb.append( this.distance );
    sb.append( ", " );
    sb.append( this.recency );
    sb.append( ", " );
    sb.append( this.frequency );
    sb.append( " )" );
    return sb.toString( );
  }
  
  /**
   * This is an comparator that uses the recency and then the frequency in order
   * to compare or order the two given vectors.
   * @author Fat Cat
   * @since 0.0.7
   * @version 1
   */
  public static final class VFCVector2DConsistencyComparator implements Comparator< VFCVector >, Serializable
  {
    
    /** The comparator version. */
    private static final long serialVersionUID = -7823601724732350041L;

    /**
     * Creates a new comparator.
     */
    public VFCVector2DConsistencyComparator( )
    {
      
    }
    
    /**
     * Compares the given vfc vector, using consistency rules.
     * @param left The left VFC vector.
     * @param right The right VFC vector.
     * @return A negative integer, zero, or a positive integer as the VFC Vector 
     * is less than, equal to, or greater than the 
     * right VFC Vector. (using distance, recency and frequency in this order to compare)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override( )
    public int compare( final VFCVector left, final VFCVector right )
    {
      if ( left.getRecency( ) != right.getRecency( ) )
      {
        if ( left.getRecency( ) < right.getRecency( ) )
        {
          return -1;
        }
        else
        {
          return 1;
        }
      }
      else
      {
        if ( left.getFrequency( ) != right.getFrequency( ) )
        {
          if ( left.getFrequency( ) < right.getFrequency( ) )
          {
            return -1;
          }
          else
          {
            return 1;
          }
        }
        else
        {
          return 0;
        }
      }     
    }
  }
  
  /**
   * This is an comparator that uses the distance, then the recency and finally
   * the frequency in order to compare or order two vectors.
   * @author Fat Cat
   * @since 0.0.7
   * @version 1
   */
  public static final class VFCVectorConsistencyComparator implements Comparator< VFCVector >, Serializable
  {
    
    /** The comparator version. */
    private static final long serialVersionUID = -1139104081236007031L;

    /**
     * Creates a new comparator.
     */
    public VFCVectorConsistencyComparator( )
    {
      
    }
    
    /**
     * Compares the given vfc vector, using consistency rules.
     * @param left The left VFC vector.
     * @param right The right VFC vector.
     * @return A negative integer, zero, or a positive integer as the VFC Vector 
     * is less than, equal to, or greater than the 
     * right VFC Vector. (using distance, recency and frequency in this order to compare)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override( )
    public int compare( final VFCVector left, final VFCVector right )
    {
      if ( left.getDistance( ) != right.getDistance( ) )
      {
        if ( left.getDistance( ) < right.getDistance( ) )
        {
          return -1;
        }
        else
        {
          return 1;
        }
      }
      else
      {
        if ( left.getRecency( ) != right.getRecency( ) )
        {
          if ( left.getRecency( ) < right.getRecency( ) )
          {
            return -1;
          }
          else
          {
            return 1;
          }
        }
        else
        {
          if ( left.getFrequency( ) != right.getFrequency( ) )
          {
            if ( left.getFrequency( ) < right.getFrequency( ) )
            {
              return -1;
            }
            else
            {
              return 1;
            }
          }
          else
          {
            return 0;
          }
        }
      }
    }
  }
  
}
