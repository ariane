/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages;

import io.netty.buffer.ChannelBuffer;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;
import com.assembla.ariane.jellyfish.util.collections.Pair;

/**
 * This interface represents an generic message.
 * @author Fat Cat
 */
public interface GenericMessage
{
  
  //Dock Name stuff
  
  /**
   * Sets the dockname associated with this message.
   * @param dockName The name of the dock.
   */
  void setDockName( String dockName );
  
  /**
   * Returns the dockname associated with this message.
   * @return The name of the associated dock, or an empty 
   * string if it does not exist.
   */
  String getDockName( );
  
  //Header stuff
  
  /**
   * Adds the specified header to the header list, if it does not exist.
   * @param name The header name.
   * @param value The header value.
   */
  void addHeader( String name, String value );
  
  /**
   * Checks if the given header exists.
   * @param name The header name.
   * @return True, if it does.
   */
  boolean containsHeader( String name );
  
  /**
   * Returns the value of the header with the specified name.<br/>
   * If the given header name does not exist, empty string is returned.
   * @param name The name of the header.
   * @return The value of the header.
   */
  String getHeader( String name );
  
  /**
   * Returns the count of all available headers.
   * @return The number of available headers.
   */
  int getHeaderCount( );
  
  /**
   * Returns an set containing the names of all known headers.<br/> 
   * There might be headers with one name and more than one value, 
   * use getMessage() to obtain the inner message and get them all.
   * @return The names of all known headers.
   */
  Set< String > getHeaderNames( );
  
  /**
   * Returns an list of values for the given header.
   * @param name The header name.
   * @return The header values.
   */
  List< String > getHeaders( String name );
  
  /**
   * Clears the header list.
   */
  void clearHeaders( );
  
  /**
   * Changes the given header, to the given value.<br/>
   * If the header contains multiple values, then those values are
   * first deleted and then the new one added.<br/>
   * If the header does not exist, then nothing is changed.
   * @param name The name of the header.
   * @param value The new value of the header.
   */
  void changeHeader( String name, String value );
  
  /**
   * Removes the header with the given values, if it exists,
   * and all of it's values.
   * @param name The header name.
   */
  void removeHeader( String name );
  
  //Extra stuff
  
  /**
   * Adds a new extra value, if the value does not exist yet.
   * @param name The name of the extra value.
   * @param value The value of the extra value.
   */
  void addExtra( String name, String value );
  
  /**
   * Checks if the given extra value exists.
   * @param name The extra value name.
   * @return True if it exists, false otherwise.
   */
  boolean containsExtra( String name );
  
  /**
   * Returns the extra value corresponding to the given name.
   * @param name The extra value name.
   * @return The extra value or an empty string if the name does not exist.
   */
  String getExtra( String name );
  
  /**
   * Returns the number of available extra values.
   * @return The number of available extra values.
   */
  int getExtraValueCount( );
  
  /**
   * Returns an unmodifiable set with all the names of the extra values.
   * @return The names of the extra values.
   */
  Set< String > getExtraValueNames( );
  
  /**
   * Clears the extra fields.
   */
  void clearExtra( );
  
  /**
   * Removes the given key/value from the extra fields.
   * @param name The key of the value to remove.
   */
  void removeExtra( String name );
  
  /**
   * Changes the given extra field.
   * @param name The name of the new extra field
   * @param value The value of the new extra field
   */
  void changeExtra( String name, String value );
  
  /**
   * Checks if there are any extra values.
   * @return True, if there are extra values.
   */
  boolean hasExtraValues( );
  
  //Content stuff
  
  /**
   * Gets the request content.
   * @return The request content.
   */
  ChannelBuffer getContent( );
  
  /**
   * Returns the size of the available content.
   * @return The size of the content, or -1 if there is no content.
   */
  long getContentLength( );
  
  /**
   * Changes the content of this request.
   * @param content The new request content.
   */
  void setContent( ChannelBuffer content );
  
  //Range stuff
  
  /**
   * Checks if the given range is in the range list.
   * @param range The range to check.
   * @return True, if the range is in the list.
   */
  boolean containsRange( Pair< Integer, Integer > range );
  
  /**
   * Checks if the given range is in the list.
   * @param min The lower bound of the range.
   * @param max The upper bound of the range.
   * @return True, if the range is in the list.
   */
  boolean containsRange( Integer min, Integer max );
  
  /**
   * Returns an unmodifiable list containing all ranges.
   * @return An range list.
   */
  List< Pair< Integer, Integer >> getRanges( );
  
  /**
   * Adds a new range that does not have a lower bound.
   * @param max The upper bound of the range.
   */
  void addRangeMax( int max );
  
  /**
   * Adds a new range that does not have a upper bound.
   * @param min The lower bound of the range.
   */
  void addRange( int min );
  
  /**
   * Adds a new range.
   * @param min The lower bound of the range.
   * @param max The upper bound of the range.
   */
  void addRange( int min, int max );
  
  /**
   * Adds a new range.
   * @param range A pair representing the new range lower and upper bounds.
   */
  void addRange( Pair< Integer, Integer > range );
  
  /**
   * Adds the given list to this range list.
   * @param ranges The list of ranges to add.
   */
  void addAllRanges( Collection< Pair< Integer, Integer >> ranges );
  
  /**
   * Clears the range list.
   */
  void clearRanges( );
  
  /**
   * Returns the number of available ranges.
   * @return The number of ranges.
   */
  int getRangeNumber( );
  
  /**
   * Checks if there are ranges in this request.
   * @return True, if they are.
   */
  boolean hasRanges( );
  
  //Keep-alive stuff
  
  /**
   * Changes this request so that it is a keep alive request.
   */
  void setKeepAlive( );
  
  /**
   * Changes this request so that it is NOT a keep alive request.
   */
  void unsetKeepAlive( );
  
  /**
   * Checks if this request is keep-alive.
   * @return True, if it is.
   */
  boolean isKeepAlive( );
  
  //Protocol version stuff
  
  /**
   * Returns the protocol version of this request as a string.
   * @return The protocol version as a string
   */
  String getProtocolVersion( );
  
  /**
   * Changes the protocol version of this request.
   * @param version The new protocol version.
   */
  void setProtocolVersion( String version );
  
  //Message id stuff
  
  /**
   * Returns the id of this request.
   * @return The id of this request.
   */
  UUID getMessageId( );
  
  /**
   * Returns the request id as a string.
   * @return The request id.
   */
  String getMessageIdAsString( );
  
  // Hop Headers
  
  /**
   * Checks if the given hop header exists.
   * @param header The header name.
   * @return True, if it does.
   */
  boolean containsHopHeader( final String header );
  
  /**
   * Adds the given hop header if the header does not exist.
   * @param header The header name.
   */
  void addHopHeader( final String header );
  
  /**
   * Returns an sorted set with the hop headers.
   * @return An read-only sorted set with the hop headers.
   */
  SortedSet<String> getHopHeaders( );
  
  /**
   * Returns the number of hop headers.
   * @return The number of hop headers.
   */
  int getNumberOfHopHeaders( );
  
  /**
   * Removes the given hop header.
   * @param header The hop header to remove.
   */
  void removeHopHeader( final String header );
  
  /**
   * Empties the hop header list.
   */
  void clearHopHeaders( );
  
  /**
   * Checks if there are any hop headers.
   * @return True, if there are.
   */
  boolean hasHopHeaders( );
  
}
