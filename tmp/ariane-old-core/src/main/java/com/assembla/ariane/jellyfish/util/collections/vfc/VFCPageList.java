/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.vfc;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import com.assembla.ariane.jellyfish.proxy.vfc.VFCPage;

/**
 * This class implements an list that contains all pages of covered by VFC.<br/>
 * Note that, this is list is implemented as a list of weakly referenced objects,
 * and hence they may be collected at any time by the garbage collector,
 * so operations like add() may work, even if an contains() call returned true.
 * Similarly an remove() operation may fail, even if contains() returned true.<br/>
 * As an implementation detail, this list is implemented in a lazy way, which 
 * means that even if no removed object (or null) is returned, the check for an 
 * orphaned reference will only be made when an operation that uses an 
 * given item is done. 
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public final class VFCPageList
{
 
  /** An list of weakly referenced VFCPages. */
  private final List< WeakReference< VFCPage > > pageList;
  
  /** The singleton object. */
  private static final VFCPageList inst = new VFCPageList( );
  
  /**
   * Returns the instance of this singleton. 
   * @return The page list singleton.
   */
  public static VFCPageList instance( )
  {
    return VFCPageList.inst;
  }
  
  /**
   * Creates a new VFCPage list.
   */
  private VFCPageList( )
  {
    this.pageList = Collections.synchronizedList( new LinkedList< WeakReference< VFCPage > >( ) );
  }
  
  /**
   * Adds the given page to the list.
   * @param page The page to add
   */
  public void add( final VFCPage page )
  {    
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( ( currentPage != null ) && currentPage.equals( page ) )
      {
        return;
      }
      
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
    }
    
    this.pageList.add( new WeakReference<VFCPage>( page ) );
  }

  /**
   * Adds all the pages in the collection to this list.
   * @param pages The pages to add.
   */
  public void addAll( final Collection< VFCPage > pages )
  {
    for( VFCPage page : pages )
    {
      this.add( page );
    }
  }

  /**
   * Clears all the pages in this list.
   */
  public void clear( )
  {
    this.pageList.clear( );
  }

  /**
   * Checks if the given page exists in this list.
   * @param page The page to check.
   * @return True, if the page exists.
   */
  public boolean contains( final VFCPage page )
  {
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( ( currentPage != null ) && currentPage.equals( page ) )
      {
        return true;
      }
      
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
    }
    return false;
  }

  /**
   * Checks if all the pages in the collection exist.
   * @param pages The pages to check.
   * @return True, if they all exist.
   */
  public boolean containsAllPages( final Collection< VFCPage > pages )
  {
    for( VFCPage page : pages )
    {
      if( !this.contains( page ) )
      {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Checks if the given url exists in this list.
   * @param url The url to check.
   * @return True, if the url exists.
   */
  public boolean contains( final String url )
  {
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( ( currentPage != null ) && currentPage.getUrl( ).equals( url ) )
      {
        return true;
      }
      
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
    }
    return false;
  }

  /**
   * Checks if all the URL's in the collection exist.
   * @param urls The URL's to check.
   * @return True, if they all exist.
   */
  public boolean containsAllUrls( final Collection< String > urls )
  {
    for( String url : urls )
    {
      if( !this.contains( url ) )
      {
        return false;
      }
    }
    return true;
  }

  /**
   * Returns the page with the given url.
   * @param url The url of the page to return.
   * @return The page with the corresponding url, or null if no
   * such page exists.
   */
  public VFCPage get( final String url )
  {
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( ( currentPage != null ) && currentPage.getUrl( ).equals( url ) )
      {
        return currentPage;
      }
      
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
    }
    return null;
  }

  /**
   * Checks if this list may be empty.<br/>
   * Note that this method may not be accurate, since
   * it does not take into attention values, that 
   * may already be garbage collected.<br/>
   * You should call get() or a similar method if
   * you need a more accurate response.
   * @return True, if it may be.
   */
  public boolean mayBeEmpty( )
  {
    return this.pageList.isEmpty( );
  }

  /**
   * Removes the given page from this list.
   * @param page The page to remove.
   */
  public void remove( final VFCPage page )
  {
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( ( currentPage != null ) && currentPage.equals( page ) )
      {
        it.remove( );
      }
      
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
    }
  }
  
  /**
   * Removes the page with the given url from this list.
   * @param url The url of the page to remove.
   */
  public void remove( final String url )
  {
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( ( currentPage != null ) && currentPage.getUrl( ).equals( url ) )
      {
        it.remove( );
      }
      
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
    }
  }

  /**
   * Removes all of the pages in the given list.
   * @param pages The list of pages to remove.
   */
  public void removeAllPages( final Collection< VFCPage > pages )
  {
    for( VFCPage page : pages )
    {
      this.remove( page );
    }
  }
  
  /**
   * Removes all of the urls in the given list.
   * @param urls The list of urls to remove.
   */
  public void removeAllUrls( final Collection< String > urls )
  {
    for( String url : urls )
    {
      this.remove( url );
    }
  }

  /**
   * Changes the page in the given url.<br/>
   * Note that the url of the new page must be identical, to the given
   * one!
   * @param url The url of the page to replace.
   * @param page The new page.
   * @return The old page.
   */
  public VFCPage change( final String url, final VFCPage page )
  {
    if( !page.getUrl( ).equals( url ) )
    {
      return null;
    }
    
    final VFCPage oldPage = this.get( url );
    if( oldPage != null )
    {
      this.remove( url );
      this.add( page );
    }
    return oldPage;
  }

  /**
   * Returns an estimate of the size of this list.<br/>
   * This method does not take into attention pages
   * that have been garbage collected.
   * @return The estimated size of this list.
   */
  public int sizeEstimate( )
  {
    return this.pageList.size( );
  }
  
  //Utility methods
  
  /**
   * Removes the VFC vectors associated with the given pivot.
   * @param pivotUrl The url of the pivot to use.
   */
  public void removePivot( final String pivotUrl )
  {
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
      else
      {
        currentPage.removeVector( pivotUrl );
      }
    }
  }
  
  /**
   * Increments the recency of all the contained pages.
   */
  public void incrementRecency( )
  {
    final Iterator< WeakReference< VFCPage > > it = this.pageList.iterator( );
    while( it.hasNext( ) )
    {
      final VFCPage currentPage = it.next( ).get( );
      if( currentPage == null )
      {
        try
        {
          it.remove( );
        }
        catch( final UnsupportedOperationException | IllegalStateException e )
        {
          
        }
      }
      else
      {
        currentPage.incrementRecency( );
      }
    }
  }

  /**
   * Returns an string representation of this list.<br/>
   * Note that the underlying list toString() method is used.
   * @return An string representing this list.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    return this.pageList.toString( );
  }
  
}
