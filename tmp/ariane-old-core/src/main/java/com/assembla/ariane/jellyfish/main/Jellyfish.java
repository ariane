/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.main;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import com.assembla.ariane.jellyfish.config.JellyfishConfiguration;
import com.assembla.ariane.jellyfish.connection.docks.DockManager;
import com.assembla.ariane.jellyfish.proxy.ProxyActionFactory;
import com.assembla.ariane.jellyfish.proxy.ProxyProcessingModule;
import com.assembla.ariane.jellyfish.proxy.actions.TransparentProxyAction;
import com.assembla.ariane.jellyfish.proxy.actions.standard.ConditionalCacheProxyAction;
import com.assembla.ariane.jellyfish.proxy.actions.standard.NoCacheProxyAction;
import com.assembla.ariane.jellyfish.proxy.actions.standard.RangeRequestProxyAction;
import com.assembla.ariane.jellyfish.proxy.actions.standard.StandardRequestProxyAction;
import com.assembla.ariane.jellyfish.proxy.storage.cache.ProxyCacheManager;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.configuration.ConfigurationException;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

/**
 * This is the main class of jellyfish.
 * @author Fat Cat
 * @version 3
 * @since 0.0.1
 */
public final class Jellyfish
{
  
  private static final int ERROR_EXIT = 5;

  /** The processing module, that launches the proxy handlers. */
  private ProxyProcessingModule processingModule;
  
  /** The logger object. */
  Logger logger = ( Logger ) LoggerFactory.getLogger( Jellyfish.class );
  
  /** Jellyfish configuration. */
  private final JellyfishConfiguration configuration;
  
  /** Cache Manager. */
  private ProxyCacheManager proxyCacheManager;
  
  /** Thread pool. */
  private ExecutorService thread_pool = Executors.newCachedThreadPool( );
  
  /** The command line. */
  private CommandLine line;

  /**
   * Method called whenever the server is started by a user in the command line.
   * @param args the arguments.
   * @throws InvocationTargetException If some error happens.
   */
  public static void main( final String[ ] args ) throws InvocationTargetException
  {
    //Starts the program from the command line...
    final Jellyfish jellyfish = new Jellyfish( );
    jellyfish.configure( args );
    try
    {
      jellyfish.run( );
    }
    catch( final ConfigurationException e1 )
    {
      throw new InvocationTargetException( e1 );
    }
    
    Runtime.getRuntime( ).addShutdownHook( new Thread( )
    {
      public void run( )
      {
        try
        {
          jellyfish.stop( );
        }
        catch( final InvocationTargetException e )
        {
          //do nothing
        }
      }
    } );
  }
  
  /**
   * Instantiates a new jellyfish class.
   */
  public Jellyfish( )
  {
    this.configuration = new JellyfishConfiguration( );
  }
  
  /**
   * Configures the server using the command line arguments.
   * @param args The command line arguments.
   */
  public void configure( final String[ ] args )
  {
    final CommandLineParser parser = new PosixParser( );
    final Options jellyfishOpts = this.configureMainOptions( );
    
    try
    {
      this.line = parser.parse( jellyfishOpts, args );
    }
    catch( final ParseException e )
    {
      final HelpFormatter formatter = new HelpFormatter( );
      formatter.printHelp( "jellyfish", jellyfishOpts, true );
      System.exit( 0 );
    }
    
    if ( this.line.hasOption( "version" ) || this.line.hasOption( "v" ) )
    {
      final HelpFormatter formatter = new HelpFormatter( );
      formatter.printUsage( new PrintWriter( System.out, true ), formatter.getWidth( ),
                            JellyfishConstants.CACHE_NAME, jellyfishOpts );
    }
    else
    {
      this.readConfiguration( );
      
      if ( this.line.hasOption( "verbose" ) || this.line.hasOption( "V" ) )
      {
        final Logger log = ( Logger ) LoggerFactory.getLogger( org.slf4j.Logger.ROOT_LOGGER_NAME );
        log.setLevel( Level.ALL );
      }
      
      //if(!(this.line.hasOption("debug") && this.line.hasOption("d") )) 
      //this.configureLogging(); 
    }
  }
  
  /**
   * Configure logging options.
   */
  @SuppressWarnings( "unused" )
  private void configureLogging( )
  {
    final String logconfig = this.configuration.getConfig( ).getString( "logger[@file]" );
    final LoggerContext context = ( LoggerContext ) LoggerFactory.getILoggerFactory( );
    try
    {
      final JoranConfigurator configurator = new JoranConfigurator( );
      configurator.setContext( context );
      context.reset( );
      configurator.doConfigure( logconfig );
    }
    catch( final JoranException e )
    {
      //do nothing      
    }
  }
  
  /**
   * Configures the acceptable command line options.
   * @return The command options.
   */
  @SuppressWarnings( "static-access" )
  private Options configureMainOptions( )
  {
    final Options jellyfishOpts = new Options( );
    
    final Option configFile = OptionBuilder.withArgName( "configuration" ).
                 withLongOpt( "configuration" ).
                 hasArg( true ).
                 isRequired( false ).
                 withDescription( "The configuration file to use" ).
                 create( "c" );
    
    final Option verbose = OptionBuilder.withArgName( "verbose" ).
                 withLongOpt( "verbose" ).
                 hasArg( false ).
                 isRequired( false ).
                 withDescription( "Turns on verbose logging" ).
                 create( "V" );
    
    final Option version = OptionBuilder.withArgName( "version" ).
                 withLongOpt( "version" ).
                 hasArg( false ).
                 isRequired( false ).
                 withDescription( "Shows the jellyfish version and exits" ).
                 create( "v" );
    
    final Option debug = OptionBuilder.withArgName( "debug" ).
                 withLongOpt( "debug" ).
                 hasArg( false ).
                 isRequired( false ).
                 withDescription( "Activates the debug mode" ).
                 create( "d" );
    
    jellyfishOpts.addOption( configFile );
    jellyfishOpts.addOption( verbose );
    jellyfishOpts.addOption( version );
    jellyfishOpts.addOption( debug );
    return jellyfishOpts;
  }

  /**
   * Checks if the given file exists.
   * @param filename The filename to check.
   * @return True, if it exists.
   */
  private boolean fileExists( final String filename )
  {
    final File file = new File( filename );
    return file.exists( );
  }
  
  /**
   * Reads the configuration file.
   */
  private void readConfiguration( )
  {
    if ( ( this.line.hasOption( "configuration" ) && this.fileExists( this.line.getOptionValue( "configuration" ) ) ) || ( this.line.hasOption( "c" ) && this.fileExists( this.line.getOptionValue( "c" ) ) ) )
    {
      try
      {
        this.configuration.init( this.line.getOptionValue( "configuration" ) );
      }
      catch( final ConfigurationException | IOException e )
      {
        this.logger.error( "An error ocurred while parsing the configuration file.", e );
        System.exit( -Jellyfish.ERROR_EXIT );
      }
    }
    else
    {
      this.logger.error( "Configuration file does not exist, exiting..." );
      System.exit( -Jellyfish.ERROR_EXIT );
    }
  }
  
  /**
   * Runs the cache server (called by both main and start).
   * @throws InvocationTargetException If some error occurs. 
   * @throws ConfigurationException If some configuration is invalid.
   */
  public void run( ) throws InvocationTargetException, ConfigurationException
  {
    this.logger.info( "Starting all services." );
    
    this.logger.info( "Configuring the dock manager." );
    DockManager.instance( ).configure( this.configuration.getConfig( ) );
    
    this.logger.info( "Configuring the processing modules." );
    ProxyActionFactory.instance( ).add( "Transparent Proxy Action", TransparentProxyAction.class );
    ProxyActionFactory.instance( ).add( "Standard Request Proxy Action", StandardRequestProxyAction.class );
    ProxyActionFactory.instance( ).add( "Conditional Cache Proxy Action", ConditionalCacheProxyAction.class );
    ProxyActionFactory.instance( ).add( "Range Request Proxy Action", RangeRequestProxyAction.class );
    ProxyActionFactory.instance( ).add( "No-Cache Proxy Action", NoCacheProxyAction.class );
    
    this.processingModule = new ProxyProcessingModule( this.configuration.getConfig( ) );
    this.processingModule.configure( );
    
    this.proxyCacheManager = new ProxyCacheManager( this.configuration.getConfig( ) );
    
    try
    {
      DockManager.instance( ).start( );
      this.processingModule.run( );
      this.thread_pool.execute( this.proxyCacheManager );
      this.logger.info( "Started everything with success." );
    }
    catch( final Exception e )
    {
      this.logger.error( "An error occurred while starting the DockManager.", e );
      System.exit( -Jellyfish.ERROR_EXIT );
    }
  }

  /**
   * Stops this server and all associated services.
   * @throws InvocationTargetException If some error happens.
   */
  public void stop( ) throws InvocationTargetException
  {
    this.logger.info( "Stopping all services." );
    try
    {
      DockManager.instance( ).stop( );
    }
    catch( final Exception e )
    {
      this.logger.error( "An error occurred while stopping the DockManager.", e );
    }
    finally
    {
      this.processingModule.stop( );
      this.proxyCacheManager.stop( );
      try
      {
        this.thread_pool.awaitTermination( JellyfishConstants.DEFAULT_WAITING_TIME,
                                          TimeUnit.SECONDS );
      }
      catch( final InterruptedException e )
      {
        this.thread_pool.shutdown();
      }
      this.logger.info( "All services stopped." );
    }
  }
}
