/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.client;

import java.util.concurrent.TimeUnit;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.connection.http.messages.responses.HTTPClientResponse;
import com.assembla.ariane.jellyfish.connection.http.messages.responses.HTTPGenericResponse;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageQueue;
import com.assembla.ariane.jellyfish.util.collections.client.HTTPChannelHolder;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.LoggerFactory;

/**
 * This class transmits the responses in the response queue to the client.
 * @author Fat Cat
 * @since 0.0.1
 * @version 3
 */
public class HTTPClientSender extends AbstractModule implements Runnable
{
  
  private String dockName;
  
  /**
   * Constructor of the response sender.
   * @param config The configuration to use.
   * @param dockName The name of the dock where this sender belongs. 
   */
  public HTTPClientSender( final HierarchicalConfiguration config, final String dockName )
  {
    super( config );
    this.dockName = dockName;
    logger = LoggerFactory.getLogger( HTTPClientSender.class );
  }
  
  /**
   * Runs the response transmitter.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override()
  public void run( )
  {
    try
    {
      ProxyResponseMessage mess = null;
      HTTPGenericResponse response = null;
      this.setRunning( true );
      this.logger.info( "Client response sender is started." );
      
      while( true )
      {      
        mess = null;
        
        if( !isRunning( ) )
        {
          ClientMessageQueue.instance( ).getResponses().clear( );
          this.logger.info( "Client response sender is finish." );
          break;
        }
        
        this.logger.debug( "Client response sender is searching for responses." );
        try
        {
          while( mess == null )
          {  
            mess = ClientMessageQueue.instance( ).getResponses().poll( JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS );
            if( !isRunning( ) )
            {
              ClientMessageQueue.instance( ).getResponses().clear( );
              this.logger.info( "Client response sender is finish." );
              break;
            }
          }  
        }
        catch( final InterruptedException e ) 
        {
          this.logger.debug( "I was interruped" );
        }
        
        if( mess != null ) //There is an response available!
        {
          this.logger.info( "Sending response for request {}", mess.getMessageId() );
          if( !mess.getDockName( ).equals( this.dockName ) )
          {
            //this was not received from me...
            this.logger.info( "Response dock-name is {}", mess.getExtra( "dock-name" ) );
            if( !isRunning( ) )
            {
              ClientMessageQueue.instance( ).getResponses().clear( );
              this.logger.info( "Client response sender is finish." );
              break;
            }
            try
            {
              ClientMessageQueue.instance( ).getResponses( ).put( mess );
            }
            catch( final InterruptedException e )
            {
              this.logger.debug( "I was interruped" );
            }
          }
          else
          {
            response = HTTPClientResponse.addRemainingHeaders( HTTPClientResponse.messageConverter( mess ) );
            logger.info( "Sending response for request {}", response.getMessageId( ) );
            
            if( HTTPChannelHolder.instance( ).containsKey( response.getMessageId( ) ) )
            {
              if( !isRunning( ) )
              {
                ClientMessageQueue.instance( ).getResponses().clear( );
                this.logger.info( "Client response sender is finish." );
                break;
              }
              
              final Channel channel = HTTPChannelHolder.instance( ).get( response.getMessageId( ) );
              
              if( channel.isWritable( ) )
              {
                final ChannelFuture future = channel.write( response.getResponse( ) );          
                
                logger.info( "Closing the channel..." );
                future.addListener( ChannelFutureListener.CLOSE );
              }
              else if( channel.isOpen( ) )
              {
                channel.close( );
              }
            }
          }
        }
      }
    }
    catch( final Exception ex )
    {
      logger.error( "Exception in HTTPClientSender", ex );
    }
  }

  /**
   * Stops the response transmitter.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override()
  public void stop( )
  {
    logger.info( "Trying to stop the client sender." );
    this.setRunning( false );
  }  
}
