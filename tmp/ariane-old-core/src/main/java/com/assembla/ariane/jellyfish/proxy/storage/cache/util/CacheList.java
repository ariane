/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.storage.cache.util;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents an collection of caches, ordered by consistency name.
 * @author Fat Cat
 * @param <Key> The key type to use in order to map the values in the storage.
 * @param <Value> The value type of the items in storage.
 * @version 2
 * @since 0.0.5
 */
public class CacheList< Key extends Serializable, Value extends Serializable >
{
  private final CacheManager manager;
  
  private final Logger logger;
  
  private final Map< String, Cache > cacheMap;
  
  /**
   * Creates a new cache list object.
   * @param manager The configured ehcache manager object.
   */
  public CacheList( final CacheManager manager )
  {
    this.cacheMap = Collections.synchronizedMap( new HashMap< String, Cache >( ) );
    this.manager = manager;
    this.logger = LoggerFactory.getLogger( CacheList.class );
  }
  
  /**
   * Builds a cache list given a configuration object.
   * @param configuration The configuration object to use.
   */
  public void configure( final HierarchicalConfiguration configuration )
  {
    this.logger.info( "Configuring the cache" );
    String consistencyName;
    String cacheName;
    for ( final HierarchicalConfiguration cacheinfo : configuration.configurationsAt( "cache" ) )
    {
      consistencyName = cacheinfo.getString( "[@consistency]", null );
      cacheName = cacheinfo.getString( "[@name]", null );
      
      if ( !this.add( consistencyName, cacheName ) )
      {
        this.logger.warn( "There is no cache for consistency {}", consistencyName );
      }
    }
  }
  
  /**
   * Clears all the caches.
   * @throws CacheException
   */
  public void clearLists( )
  {
    for ( final String consistency : this.cacheMap.keySet( ) )
    {
      try
      {
        this.cacheMap.get( consistency ).removeAll( );
      }
      catch( final CacheException | IllegalStateException ex )
      {
        this.logger.error( "Error cleaning the cache list", ex );
      }
    }
  }
  
  /**
   * Checks if this storage map, has an storage region specific to the given consistency.
   * @param consistencyName The name of the consistency to use
   * @return True, if it has.
   */
  public boolean contains( final String consistencyName )
  {
    return this.cacheMap.containsKey( consistencyName );
  }
  
  /**
   * Maps the given region to the given consistency name.
   * @param consistencyName The name of the consistency.
   * @param cacheName The cache name.
   * @return True, on success.
   * @throws CacheException If some error happens.
   */
  public boolean add( final String consistencyName, final String cacheName ) throws CacheException
  {
    if ( !this.contains( consistencyName ) )
    {
      try
      {
        final Cache cache = this.manager.getCache( cacheName );
        if ( cache != null )
        {
          this.cacheMap.put( consistencyName, cache );
          return true;
        }
      }
      catch( CacheException | IllegalStateException ex)
      {
        logger.warn("There was an ehcache error while adding the cache for " +
                      "{}", consistencyName);
        return false;
      }
    }
    return false;
  }
  
  /**
   * Returns the cache associated with the given consistency name.
   * @param consistencyName The name of the consistency to use.
   * @return The cache at the given index or null if the cache does not exist.
   */
  public Cache get( final String consistencyName )
  {
    return this.cacheMap.get( consistencyName );
  }
  
  /**
   * Checks if the cache list is empty.
   * @return True, if it is.
   */
  public boolean isEmpty( )
  {
    return this.cacheMap.isEmpty( );
  }
  
  /**
   * Removes the given cache from the list.
   * @param consistencyName The associated consistency strategy.
   * @return The removed cache.
   */
  public Cache remove( final String consistencyName )
  {
    if ( !this.contains( consistencyName ) )
    {
      return null;
    }
    else
    {
      return this.cacheMap.remove( consistencyName );
    }
  }
  
  /**
   * Returns the number of caches.
   * @return The number of caches.
   */
  public int size( )
  {
    return this.cacheMap.size( );
  }
  
  /**
   * Returns the names of all consistencies.
   * @return The names of all consistencies.
   */
  public Set< String > getAllConsistencyNames( )
  {
    return Collections.unmodifiableSet( this.cacheMap.keySet( ) );
  }
  
  // Cache methods
  
  /**
   * Returns the number of elements in the given consistency cache.
   * @param consistencyName The name of the consistency to use.
   * @return The number of elements in the cache or -1 if the cache name is unknown.
   */
  public int size( final String consistencyName )
  {
    if ( this.contains( consistencyName ) )
    {
      return this.cacheMap.get( consistencyName ).getSize( );
    }
    else
    {
      return -1;
    }
  }

  /**
   * Adds the given key/value pair to the cache associated with the given consistency.
   * @param consistencyName The consistency name.
   * @param key The entry key.
   * @param value The entry value.
   */
  public boolean add( String consistencyName, Key key, Value value )
  {
    if(this.contains(consistencyName))
    {
      try
      {
        this.get(consistencyName).putIfAbsent(new Element(key, value));
        return true;
      }
      catch(NullPointerException ex)
      {
        return false;
      }
    }
    else return false;
  }

  /**
   * Tries to change the given key/value pair to the cache associated
   * with the given consistency.
   * @param consistencyName The cache type.
   * @param key The entry key.
   * @param value The entry value.
   * @return True, if the value was changed, false otherwise.
   */
  public boolean change( final String consistencyName, final Key key,
                         final Value value )
  {
    if ( this.contains( consistencyName ) )
    {
      try
      {
        final Element element = new Element( key, value );
        this.cacheMap.get(consistencyName).put( element );
        return true;
      }
      catch( IllegalStateException | IllegalArgumentException |
               CacheException e )
      {
        return false;
      }
    }
    else return false;
  }

  /**
   * Tries to remove the given key from the cache associated with the given consistency.
   * @param consistencyName The consistency name.
   * @param key The entry key.
   * @return True, on success, false otherwise
   */
  public boolean remove( String consistencyName, Key key )
  {
    if ( this.contains( consistencyName ) )
    {
      try
      {
        this.cacheMap.get(consistencyName).remove( key );
      }
      catch( IllegalStateException | CacheException e )
      {
        return false;
      }
      return true;
    }
    else return false;
  }

  public Value get(String consistencyName, Key key)
  {
    if ( this.contains( consistencyName ) )
    {
      try
      {
        return (Value) this.cacheMap.get(consistencyName).get(key).getValue();
      }
      catch( IllegalStateException | CacheException e )
      {
        return null;
      }
    }
    else return null;
  }
  
  /**
   * Destroys the list and shuts down all of the associated keys.
   */
  public void shutdown( )
  {
    this.manager.removalAll( );
    this.clearLists( );
    this.cacheMap.clear( );
  }
}
