/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is a singleton that holds all the existing client sockets.
 * @author Fat Cat
 * @version 3
 * @since 0.0.1
 */
public final class HTTPChannelHolder implements ChannelFutureListener
{
  
  /** The logger to use.  */
  private final Logger logger;
  
  /**
   * Returns the instance of this singleton.
   * @return The channel holder singleton.
   */
  public static HTTPChannelHolder instance( )
  {
    return HTTPChannelHolder.inst;
  }
  
  /** The all channels. */
  private final ChannelGroup allChannels;
  
  /** The channels. */
  private final Map< UUID, Channel > channels;
  
  /** The singleton. */
  private static final HTTPChannelHolder inst = new HTTPChannelHolder( );
  
  /**
   * Private constructor to create an channel holder.
   */
  private HTTPChannelHolder( )
  {
    this.channels = Collections.synchronizedMap( new HashMap< UUID, Channel >( ) );
    this.allChannels = new DefaultChannelGroup( );
    this.logger = LoggerFactory.getLogger( HTTPChannelHolder.class );
  }
  
  /**
   * Closes and removes all the existing channels.
   */
  public synchronized void clear( )
  {
    this.logger.debug( "Clearing the channel holder" );
    this.channels.clear( );
    this.allChannels.disconnect( ).awaitUninterruptibly( );
  }
  
  /**
   * Returns if the given key exists.
   * @param key The key to check for.
   * @return True if the key exists, false otherwise.
   */
  public synchronized boolean containsKey( final UUID key )
  {
    return this.channels.containsKey( key );
  }
  
  /**
   * Return the channel corresponding to the given key, null if the
   * key does not exist. 
   * @param key The key to return the channel for.
   * @return The channel corresponding to the key or null.
   */
  public Channel get( final UUID key )
  {
    if ( this.containsKey( key ) ) 
    {
      return this.channels.get( key );
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Checks if the holder is empty.
   * @return True if the holder is empty, false otherwise
   */
  public boolean isEmpty( )
  {
    return this.channels.isEmpty( );
  }
  
  /**
   * Returns an set containing all the available keys.
   * @return An read-only set with all known keys
   */
  public Set< UUID > keySet( )
  {
    return Collections.unmodifiableSet( this.channels.keySet( ) );
  }
  
  /**
   * Adds a new channel and key to the holder.&nbsp;
   * 
   * Note that unlike the standard behavior, if the key
   * already exists nothing is replaced.
   * 
   * @param key The new key for the channel.
   * @param value The channel to add.
   * @return The added channel if the insertion was sucessfull.
   */
  public synchronized Channel put( final UUID key, final Channel value )
  {
    this.logger.debug( "Adding a new channel" );
    if ( !this.containsKey( key ) )
    {
      //Whenever the request comes from an existing connection...
      if ( this.allChannels.contains( value ) )
      {
        this.logger.debug( "Channel is already known." );
        this.channels.put( key, value );
      }
      else
      {
        //if the connection is new...
        this.logger.debug( "Channel is new." );
        value.getCloseFuture( ).addListener( this );
        this.channels.put( key, value );        
        this.allChannels.add( value );
      }
      return value;
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Closes and removes the channel associated with
   * the given key, if the key is not found null is
   * returned.
   * @param key The key of the channel to remove.
   * @return The removed and closed channel.
   */
  public synchronized Channel remove( final UUID key )
  {
    this.logger.debug( "Removing an channel." );
    Channel channel = null;
    if ( this.containsKey( key ) )
    {
      channel = this.channels.get( key );
      this.logger.debug( "Trying to close the channel..." );
      this.channels.get( key ).close( ).awaitUninterruptibly( );
      this.logger.debug( "Channel closed!" );
      return channel;
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Returns the number of key/values in this holder.
   * @return The number of entries in this holder.
   */
  public int size( )
  {
    return this.channels.size( );
  }
  
  /**
   * Returns the inner map.
   * @return An read-only version of the inner map.
   */
  public synchronized Map<UUID, Channel> getChannelMap()
  {
    return Collections.unmodifiableMap( this.channels );
  }
  
  /**
   * Gets the all channels.
   * @return the allChannels
   */
  public synchronized ChannelGroup getAllChannels( )
  {
    return this.allChannels;
  }

  /**
   * Returns the key for the given channel.
   * @param channel The channel to look for.
   * @return The key for the channel, of null if the key does not exist.
   */
  private synchronized UUID getKeyFor( final Channel channel )
  {
    for( UUID key : this.channels.keySet( ) )
    {
      if( this.get( key ).equals( channel ) )
      {
        return key;
      }
    }
    return null;
  }  
  
  /**
   * Method called whenever an channel is closed.
   * @param channelFuture The caller object.
   * @throws Exception If some error happens.
   */
  @Override( )
  public synchronized void operationComplete( final ChannelFuture channelFuture ) throws Exception
  {
    this.logger.debug( "Removing a channel from it's conection ID" );
    
    final Channel channel = channelFuture.getChannel( );
    final UUID key = this.getKeyFor( channel );
    
    if( ( key != null ) && this.containsKey( key ) )
    {
      this.channels.remove( key );
      this.logger.debug( "Channel dissociated from {}", key );
    }
  }
}
