/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.vfc;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import com.google.common.base.Objects;

/**
 * This class represents an VFC Pivot.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public final class Pivot implements Serializable
{
  
  /** This class version. */
  private static final long serialVersionUID = 8624860935486113447L;

  /** An page representing this pivot. */
  private VFCPage page;
  
  /** The maximum VFCVector before an update to this pivot. */
  private VFCVector maxPivot;
  
  /** The list of consistency zones associated with this pivot. */
  private List<ConsistencyZone> consistency_zones;
  
  /** An comparator for the pivot taking into attention only the url. */
  public static final PivotUrlComparator PIVOT_URL_COMPARATOR = new PivotUrlComparator( ); 
  
  /**
   * Creates a new pivot.
   * @param page The page corresponding to this pivot.
   * @param maxPivot The maximum VFC-Vector before an update to this pivot.
   * @throws NullPointerException If any of the two arguments is null.
   */
  public Pivot( final VFCPage page, final VFCVector maxPivot ) throws NullPointerException 
  { 
    
    if( ( page == null ) || ( maxPivot == null ) )
    {
      throw new NullPointerException( "Either the page or the max pivot is null" );
    }
    
    this.page = page;
    
    if( maxPivot.getDistance( ) == 0 )
    {
      this.maxPivot = maxPivot;
    }
    else
    {
      this.maxPivot = new VFCVector( 0, maxPivot.getRecency( ), maxPivot.getFrequency( ) );
    }
    
    this.consistency_zones = Collections.synchronizedList( new LinkedList<ConsistencyZone>( ) );
  }
  
  /**
   * Returns the page represented by this pivot.
   * @return The pivot page.
   */
  public synchronized VFCPage getPage( )
  {
    return this.page;
  }

  /**
   * Returns the max vfc-vector of this pivot.
   * @return The pivot max vfc-vector.
   */
  public synchronized VFCVector getMaxPivotVFCVector( )
  {
    return this.maxPivot;
  }
  
  /**
   * Changes the maximum VFC Vector.
   * @param vector The new maximum VFC vector.
   */
  public synchronized void changeMaxPivotVFCVector( final VFCVector vector )
  {
    if( vector != null )
    {
      if( vector.getDistance( ) == 0 )
      {
        this.maxPivot = vector;
      }
      else
      {
        this.maxPivot = new VFCVector( 0, vector.getRecency( ), vector.getFrequency( ) );
      }
    }
  }

  /**
   * Returns an read-only list with all the consistency zones.
   * @return An list with all the consistency zones.
   */
  public synchronized List< ConsistencyZone > getConsistencyZones( )
  {
    return Collections.unmodifiableList( this.consistency_zones );
  }
  
  /**
   * Adds a new consistency zone to this pivot.<br/>
   * The new zone must be empty, in order to be add.
   * @param zone The new zone to add.
   */
  public synchronized void addConsistencyZone( final ConsistencyZone zone )
  {
    if( ( zone != null ) && ( zone.getPages( ).isEmpty( ) ) )
    {
      boolean hasConsistencyZone = false;
      for( ConsistencyZone existingZone : this.consistency_zones )
      {
        if( ConsistencyZone.MaximumVFCComparator.compare( existingZone, zone ) == 0 )
        {
          hasConsistencyZone = true;
          break;
        }
      }
      if( !hasConsistencyZone )
      {
        this.consistency_zones.add( zone );
        Collections.sort( this.consistency_zones, ConsistencyZone.MaximumVFCComparator );
      }
    }
  }
  
  /**
   * Removes a consistency zone to this pivot.<br/>
   * @param zone The zone to remove.
   */
  public synchronized void removeConsistencyZone( final ConsistencyZone zone )
  {
    if( zone != null )
    {
      boolean hasConsistencyZone = false;
      for( ConsistencyZone existingZone : this.consistency_zones )
      {
        if( ConsistencyZone.MaximumVFCComparator.compare( existingZone, zone ) == 0 )
        {
          hasConsistencyZone = true;
          break;
        }
      }
      if( hasConsistencyZone )
      {
        this.consistency_zones.remove( zone );
        Collections.sort( this.consistency_zones, ConsistencyZone.MaximumVFCComparator );
      }
    }
  }
  
  /**
   * Removes all of the consistency zones.
   */
  public synchronized void removeAllConsistencyZones( )
  {
    this.consistency_zones.clear( );
  }

  /**
   * Checks if the given url is known.<br/>
   * When iterating return on first true consistency.isPageKnown(url).
   * Return true if the url is equal to the one of this pivot.
   * @param url The url to check for.
   * @return True, if the url is known by this pivot.
   */
  public boolean isPageKnown( final String url ) 
  { 
    if( !this.page.getUrl( ).equals( url ) )
    {
      for( ConsistencyZone zone : this.consistency_zones )
      {
        if( zone.isPageKnown( url ) )
        {
          return true;
        }
      }
      return false;
    }
    else
    {
      return true;
    }
  }
  
  /**
   * Checks if the given url is known.<br/> 
   * When iterating return on first true consistency.isPageKnown(page).
   * Return true if the page is equal to the one of this pivot.
   * @param page The page to look for.
   * @return True, if the page is known by this pivot.
   */
  public boolean isPageKnown( final VFCPage page ) 
  {  
    if( !this.page.equals( page ) )
    {
      if( page.containsVector( this.page.getUrl( ) ) )
      {
        for( ConsistencyZone zone : this.consistency_zones )
        {
          if( zone.isPageKnown( page ) )
          {
            return true;
          }
        }
        
        //Something is wrong we do not have the given page...
        page.removeVector( this.page.getUrl( ) );
      }
      return false;
    }
    else
    {
      return true;
    }
  }
  
  /**
   * Returns the page corresponding to the given url.<br/>
   * If the url is unknown null is returned, if the page
   * is this pivot then this pivot page is returned.
   * @param url The url of the page to return.
   * @return The page associated with the given url, or null if the page 
   * does not exist.
   */
  public VFCPage peekPageInformation( final String url ) 
  { 
    if( !this.page.getUrl( ).equals( url ) )
    {
      for( ConsistencyZone zone : this.consistency_zones )
      {
        if( zone.isPageKnown( url ) )
        {
          return zone.peekPageInformation( url );
        }
      }
      return null;
    }
    else
    {
      return this.page;
    }
  }
  
  /**
   * Checks if this page is fresh<br/>
   * Return on first not fresh.
   * Check if the page is this pivot.
   * @param page The page to check for freshness.
   * @return True, if the page is fresh.
   */
  public boolean isPageFresh( final VFCPage page ) 
  { 
    if( !this.page.equals( page ) )
    {
      for( ConsistencyZone zone : this.consistency_zones )
      {
        if( zone.isPageKnown( page ) )
        {
          return zone.isPageFresh( page );
        }
      }
      return false;
    }
    else
    {
      return VFCVector.CONSISTENCY_RULES_2D_COMPARATOR.compare( this.maxPivot, 
          this.page.getVector( this.page.getUrl( ) ) ) < 0;
    }
  }
  
  /**
   * Checks if we need to remap the given page.<br/>
   * Return on first shouldBelongHere(page) in collection (do not iterate over everything!)
   * Return false if page is this pivot.
   * @param page The page to check for.
   * @return True, if we need to remap the page.
   */
  public boolean isRemapNeeded( final VFCPage page ) 
  { 
    if( !this.page.equals( page ) )
    {
      for( ConsistencyZone zone : this.consistency_zones )
      {
        if( zone.shouldBelongHere( page ) )
        {
          return true;
        }
      }
      return false;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Remaps the given page.<br/>
   * 
   * record first shouldBelongHere(page) in collection and stop issuing shouldBelongHere(page).
   * locate the current consistency zone. (stop iterating once this is found!)
   * remove the page from current consistency zone.
   * reinsert page in the new consistency zone.
   *
   * Return immediately is the page is this pivot.
   * @param page The page to remap.
   */
  public void remapAsNeeded( final VFCPage page ) 
  {  
    if( !this.page.equals( page ) )
    {
      boolean shouldBelongHere = false;
      ConsistencyZone newZone = null;
      ConsistencyZone oldZone = null;
      
      for( ConsistencyZone zone : this.consistency_zones )
      {
        if( !shouldBelongHere )
        {
          if( zone.shouldBelongHere( page ) )
          {
            shouldBelongHere = true;
            newZone = zone;
          }
        }
        
        if( zone.isPageKnown( page ) )
        {
          oldZone = zone;
        }
        
        if( ( newZone != null ) && ( oldZone != null ) )
        {
          break;
        }
      }
      
      if( ( newZone != null ) && 
          ( oldZone != null ) && 
          ( ConsistencyZone.MaximumVFCComparator.compare( newZone, oldZone ) != 0 )  )
      {
        oldZone.removePage( page );
        newZone.addPage( page );
      }
    }
  }
  
  /**
   * Adds a new page to this pivot.<br/>
   * Check if page url is not present first!
   * Use shouldBelongHere(page) to see where to add the new page.
   * @param page The page to add.
   */
  public void addNewPage( final VFCPage page ) 
  {  
    if( !this.isPageKnown( page ) )
    {
      for( ConsistencyZone zone : this.consistency_zones )
      {
        if( zone.shouldBelongHere( page ) )
        {
          zone.addPage( page );
          break;
        }
      }
    }
  }
  
  /**
   * Tries to remove the page of this pivot.<br/>
   * NOTHING is done if this pivot does NOT contain this page.<br/>
   * This method used the page url in comparations.
   * @param page The page to remove.
   */
  public void removePage( final VFCPage page )
  {
    if( this.isPageKnown( page ) )
    {
      for( ConsistencyZone zone : this.consistency_zones )
      {
        if( zone.isPageKnown( page.getUrl( ) ) )
        {
          zone.removePage( page );
          break;
        }
      }
    }
  }
  
  /**
   * Checks if the given object is equal to this one.
   * @param obj The object to compare.
   * @return True, if it is.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  { 
    if( ( obj instanceof Pivot ) && ( obj != null ) )
    { 
      final Pivot other = ( Pivot ) obj;
      
      if( other.getPage( ).equals( this.page ) &&
          other.getMaxPivotVFCVector( ).equals( this.maxPivot ) )
      {
        final List<ConsistencyZone> otherConsistencyList = other.getConsistencyZones( );
        if( otherConsistencyList.size( ) == this.consistency_zones.size( ) )
        { 
          return this.consistency_zones.containsAll( otherConsistencyList );
        }
      }
    }
    return false;
  }

  /**
   * Calculates and returns an hash code for this pivot.
   * @return The calculated hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this.page, this.maxPivot, this.consistency_zones );
  }

  /**
   * Returns an string representation of this pivot.
   * @return An string representing this pivot.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    final StringBuffer sb = new StringBuffer( );
    
    sb.append( String.format( "Pivot URL: %s\n", this.page.getUrl( ) ) );
    sb.append( String.format( "Pivot VFC-Vector: %s\n", 
        this.page.getVector( this.page.getUrl( ) ).toString( ) ) );
    sb.append( String.format( "Pivot Max VFC-Vector: %s\n", this.maxPivot.toString( ) ) );
    sb.append( String.format( "Pivot Consistency Zones: %d\n", this.consistency_zones.size( ) ) );
    
    return sb.toString( );
  }
  
  /**
   * This is an comparator that compares pivots using the url of their page.<br/>
   * The string are compared in lexicographic order.
   * @author Fat Cat
   * @since 0.0.7
   * @version 1
   */
  public static class PivotUrlComparator implements Comparator<Pivot>, Serializable
  {

    /** The class version. */
    private static final long serialVersionUID = -2654886732141208211L;

    /**
     * Creates a comparator.
     */
    public PivotUrlComparator( )
    {
      
    }
    
    /**
     * Compares the left and right pivots, according to their url. 
     * @param left The left pivot to compare.
     * @param right The right pivot to compare.
     * @return The value 0 if the right pivot url is equal to the left pivot url; 
     * a value less than 0 if the left pivot url is lexicographically less than the 
     * right pivot url; and a value greater than 0 if the left pivot url is 
     * lexicographically greater than the right pivot url.
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare( final Pivot left, final Pivot right )
    {
      return VFCPage.URLComparator.compare( left.getPage( ), right.getPage( ) );
    }
  }
  
}
