/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.vfc;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.joda.time.DateTime;
import org.json.*;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.vfc.VFCPageList;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.RequestType;
import com.assembla.ariane.jellyfish.proxy.exceptions.ArgumentException;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import com.assembla.ariane.jellyfish.proxy.vfc.*;

/**
 * This is an proxy action that processes all requests related to a user in
 * the VFC algorithm.<br/>
 * These requests are addressed to the own cache server (though the HOST header),
 * and follow a simple restful protocol, using json content when needed.<br/>
 * The defined rest methods are:
 * <ul>
 *   <li>registerUser( String username ) &rarr; Registers 
 *   the user in the cache, given its username (that is an hash of its name, 
 *   browser and ip).
 *   </li>
 *   <li>unregisterUser( username ); &rarr; Unregisters the user with the given 
 *   id.
 *   </li>
 *   <li>addBookmark( username, jsonContent ); &rarr; Adds an(some) bookmark(s) to 
 *   the given user.
 *   </li>
 *   <li>changeBookmark( username, jsonContent ); &rarr; Changes the bookmarks for
 *   the given user. (An empty json content, means remove all bookmarks)
 *   </li>
 *   <li>addConsistencyZone( username, jsonContent ); &rarr; Adds an consistency 
 *   zone mapping for the given user.
 *   </li>
 *   <li>changeConsistencyZone( username, jsonContent ); &rarr; Changes the 
 *   consistency zone mapping for the given user.
 *   </li>
 * </ul>
 * @author Fat Cat
 * @version 1
 * @since 0.0.7
 */
public final class VFCClientOperations extends ProxyAction
{
  
  /**
   * Creates a new VFC client operation action.
   * @param successor The next action.
   * @param config The configuration of this action.
   */
  public VFCClientOperations( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( VFCClientOperations.class );
  }
  
  /**
   * Determines if the given request can be handled by this action.<br/>
   * This is true when the host header is equal to the address of this 
   * proxy/cache server.<br/>
   * This means that one of the network interfaces that is connected to
   * this cache server must, be equal to the translated hostname.
   * @param aRequest The request to check.
   * @return True, if the request can be processed by this action.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return RequestType.isClientOperation( aRequest );
  }
  
  /**
   * Handles the VFC Client operation.<br/>
   * Note that unlike most of the other handles, this handler directly produces
   * and response to the user client directly, without a round trip to the 
   * server.
   * @param aRequest The client request to process.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    
    try
    {
      //(check) if the request is new
      if( aRequest.getState( ) == ProxyRequestState.PROCESSING )
      {
        
        if( !aRequest.hasRequest( ) )
        {
          this.logger.warn( "There is no request to process." );
          throw new IllegalArgumentException( "There is no request to process." );
        }
      
        //parse the complete url
        final URL url = new URL( aRequest.getRequest( ).getCompleteUri( ) );
        
        //(check) if the request is a user add
        if( ( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "put" ) ) && 
            url.getPath( ).endsWith( "users" ) )
        {
      
          final String username = this.getUserName( url );
          
          //if the user does not exist
          if( !Users.instance( ).hasUserName( username ) )
          {
            //create a new empty user
            final User user = new User( username );
      
            //add the user
            Users.instance( ).addUser( user );
          }
        }
        
        //(check) if the request is a user remove
        if( ( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "delete" ) ) && 
            url.getPath( ).endsWith( "users" ) )
        {
          
          final String username = this.getUserName( url );
      
          //if the user exists
          if( Users.instance( ).hasUserName( username ) )
          {
            
            //remove the vfc vectors corresponding to the user pivots
            for( Pivot pivot : Users.instance( ).getUserFor( username ).getPivots( ) )
            {
              //To force the page string to be a different string.
              final String page = pivot.getPage( ).getUrl( ).concat( "" );
              
              VFCPageList.instance( ).removePivot( page );
            }
            
            //remove the user
            Users.instance( ).removeUser( Users.instance( ).getUserFor( username ) );
          }
      
        }
        
        //(check) if the request is a bookmark change
        if( ( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "put" ) ) && 
            url.getPath( ).endsWith( "bookmarks" ) )
        {
      
          final String username = this.getUserName( url );
      
          //if the user exists
          if( Users.instance( ).hasUserName( username ) )
          {          
      
            final User user = Users.instance( ).getUserFor( username );
            if( user.hasDefinitions( ) )
            {
            
              //(check) if the user already contains pivots
              if( !user.getPivots( ).isEmpty( ) )
              {
                //merge the pivots
                this.mergeUserPivots( user, aRequest.getRequest( ).getContentString( ), aRequest );
              }
              else //else (if the user does not have pivots)
              {
                //add the new pivots (try to find if the page exists...)
                this.addPivots( user, aRequest.getRequest( ).getContentString( ), aRequest );
              }
              
            }
            else
            {
              throw new ArgumentException( 
                  "User does not have consistency zone definitions..." );
            }
          } 
        }
        
        //(check) if the request is a consistency zone change
        if( ( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "put" ) ) && 
            url.getPath( ).endsWith( "vfc" ) )
        {
      
          final String username = this.getUserName( url );
          
          //if the user exists
          if( Users.instance( ).hasUserName( username ) )
          {
            
            final User user = Users.instance( ).getUserFor( username );
            final ConsistencyZoneDefinitions definition = new ConsistencyZoneDefinitions( new JSONObject( new JSONTokener( aRequest.getRequest( ).getContentString( ) ) ) );
      
            //(check) if the user already contains a consistency zone
            if( user.hasDefinitions( ) )
            {
              //if the consistency zone is not equal
              if( !user.getDefinitions( ).equals( definition ) )
              {
                //merge the consistency zone
                this.mergeConsistencyZones( user, definition );
              }
            }
      
            //Update the user consistency zone definitions
            user.setDefinitions( definition );
            
          }
        }
      }
      
      final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
      
      response.unsetKeepAlive( );
      
      response.setResponseCode( 
          ProxyResponseCode.create( JellyfishConstants.HTTPErrorCodes.OK.errorcode, 
              "OK" ) );
      
      response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
      response.setResponseTime( DateTime.now( ) );
      response.addHeader( "Date", response.getResponseTime( ).toString( JellyfishConstants.HTTP_FORMATTER ) );
      response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
      response.addHeader( "Content-Type", "text/html; charset=utf-8" );
      
      final String content = response.getNativeResponseCode( ).getHtmlPage( "Operation sucessful" );
      response.addHeader( "Content-Lenght", Integer.toString( content.length( ) ) );
      response.setContentString( content );
      
      sendClienResponse( aRequest, response );
       
      
    }
    catch( final MalformedURLException e )
    {
      this.logger.error( "The complete uri is invalid for request {}:\n{}\n", aRequest.getRequest( ).getMessageIdAsString( ), e.getMessage( ) );
      final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
      
      response.unsetKeepAlive( );
      
      response.setResponseCode( 
          ProxyResponseCode.create( JellyfishConstants.HTTPErrorCodes.SERVER_ERROR.errorcode, 
              "Server Error", e ) );
      
      response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
      response.setResponseTime( DateTime.now( ) );
      response.addHeader( "Date", response.getResponseTime( ).toString( JellyfishConstants.HTTP_FORMATTER ) );
      response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
      response.addHeader( "Content-Type", "text/html; charset=utf-8" );
      
      final String content = response.getNativeResponseCode( ).getHtmlPage(
         "Operation unsucessful<br/> The complete uri is invalid for request." );
      response.addHeader( "Content-Lenght", Integer.toString( content.length( ) ) );
      response.setContentString( content );
      
      sendClienResponse( aRequest, response );
    }
    catch( final JSONException | ArgumentException e )
    {
      this.logger.error( "There is an error with the request {} content:\n{}\n", aRequest.getRequest( ).getMessageIdAsString( ), e.getMessage( ) );
      final ProxyResponseMessage response = new ProxyResponseMessage( aRequest.getRequest( ).getMessageId( ) );
      
      response.unsetKeepAlive( );
      
      response.setResponseCode( ProxyResponseCode.create( 
          JellyfishConstants.HTTPErrorCodes.SERVER_ERROR.errorcode, 
          "Server Error", e ) );
      
      response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
      response.setResponseTime( DateTime.now( ) );
      response.addHeader( "Date", response.getResponseTime( ).toString( JellyfishConstants.HTTP_FORMATTER ) );
      response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
      response.addHeader( "Content-Type", "text/html; charset=utf-8" );
      
      final String content = response.getNativeResponseCode( ).getHtmlPage( "Operation unsucessful<br/> There is an error with the request content." );
      response.addHeader( "Content-Lenght", Integer.toString( content.length( ) ) );
      response.setContentString( content );
      
      sendClienResponse( aRequest, response );
    }
  }

  /**
   * Sends an response to the client.
   * @param aRequest The client request.
   * @param response The response for the request.
   */
  private void sendClienResponse( final ProxyPageElement aRequest, final ProxyResponseMessage response )
  {
    this.logger.debug( "Request {} is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getMethod( ) );
    
    aRequest.setResponse( response );
          
    //send the result to the client.
    aRequest.setState( ProxyRequestState.PROCESSED );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    this.logger.debug( "Request {} dock is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getDockName( ) );
    aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
        
    aRequest.getResponse( ).setMessageId( aRequest.getRequest( ).getMessageId( ) );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }

  /**
   * Adds the pivots in the given json bookmark string. 
   * @param user The user to add the new pivots
   * @param contentString The string containing the bookmark json.
   * @param request The user request.
   * @throws ArgumentException If some error happens.
   */
  private void addPivots( final User user, final String contentString, final ProxyPageElement request ) throws ArgumentException
  {
    final List<String> bookmarks = this.parseBookmarkJson( contentString );
    
    Pivot newPivot;
    VFCPage page;
    
    for( String url : bookmarks )
    {      
      //Does the url exist somewhere?
      final Pair<PageState, VFCPage> currentPage = Users.instance( ).peekPageInformation( url, user );
      
      if( currentPage.getFirst( ) != PageState.PAGE_UNKNOWN )
      {
        
        //Add this pivot to the list of vfc vectors of this page.
        page = currentPage.getSecond( );
        page.addVector( url, JellyfishConstants.PIVOT_VFC_VECTOR );
        
        //Add the existing page as pivot
        newPivot = new Pivot( page, user.getDefinitions( ).getPivotVector( ) ); 
        
        user.addPivot( newPivot );
        
        //Add the child pages to the new pivot
        recursiveAdd( newPivot, currentPage.getSecond( ) );
      }
      else
      {
        //Add this pivot to the list of vfc vectors of this page.
        page = new VFCPage( url, request.getRequest( ).getMessageId( ) );
        page.addVector( url, JellyfishConstants.PIVOT_VFC_VECTOR );
        
        VFCPageList.instance( ).add( page );
        
        newPivot = new Pivot( page, user.getDefinitions( ).getPivotVector( ) ); 
        
        user.addPivot( newPivot );
      }
    }
    
  }
  
  /**
   * Creates a new pivot for the user, according to its VFC definitions.
   * @param user The user to create the pivot for.
   * @param pivotPage The page of the user pivot.
   * @throws ArgumentException If the user has no VFC definitions.
   */
  private void generatePivot( final User user, final VFCPage pivotPage ) throws ArgumentException
  {
    if( !user.hasDefinitions( ) )
    {
      throw new ArgumentException( "The user does not have any VFC definitions." );
    }
    
    final Pivot pivot = new Pivot( pivotPage, user.getDefinitions( ).getPivotVector( ) );
    
    for( VFCVector vector : user.getDefinitions( ).getConsistencyZoneVectors( ) )
    {
      pivot.addConsistencyZone( 
          new ConsistencyZone( vector, pivot.getPage( ).getUrl( ) ) );
    }
  }

  /**
   * This method merges the pivots given in the content json string, with
   * the current ones.  
   * @param user The user to merge the pivots.
   * @param contentString The new user pivots.
   * @param request The user request.
   * @throws ArgumentException If some error happens while parsing the contentString.
   */
  private void mergeUserPivots( final User user, final String contentString, final ProxyPageElement request ) throws ArgumentException
  {
    // T ODO Auto-generated method stub
    
    // parse the json file
    final List<String> bookmarks = this.parseBookmarkJson( contentString );
    List<Pivot> pivotsToRemove = new ArrayList<>( user.getPivots( ).size( ) );
    
    // iterate in the user pivot list
    for( Pivot pivot : user.getPivots( ) )
    {
      if( !bookmarks.contains( pivot.getPage( ).getUrl( ) ) )
      {
        pivotsToRemove.add( pivot );
      }
    }
    
    //Remove the non-existing pivots
    for( Pivot pivot : pivotsToRemove )
    {
      //Remove the pivot from the list of vfc vectors.
      //To force the page string to be a different string.
      final String page = pivot.getPage( ).getUrl( ).concat( "" );
        
      VFCPageList.instance( ).removePivot( page );
      
      user.removePivot( pivot );
    }
    
    pivotsToRemove.clear();
    
    // iterate in the bookmark list
    final List<String> pivotsToAdd = new ArrayList<>( bookmarks.size( ) );
    
    for( String url : bookmarks )
    {
      if( !user.isPageKnown( url ) )
      {
        pivotsToAdd.add( url ); 
      }
    }
    
    //Add the new "new" pivots
    Pivot newPivot = null;
    
    for( String url : pivotsToAdd )
    {      
      //Does the url exist somewhere?
      final VFCPage currentPage = VFCPageList.instance( ).get( url );
      
      if( currentPage != null )
      {
       
        //Add this pivot to the list of vfc vectors of this page.
        currentPage.addVector( url, JellyfishConstants.PIVOT_VFC_VECTOR );
        
        //Add the existing page as pivot
        newPivot = new Pivot( currentPage, user.getDefinitions( ).getPivotVector( ) );
        
        //Add the existing page as pivot
        this.generatePivot( user, currentPage );
        
        //Add the child pages to the new pivot
        recursiveAdd( newPivot, currentPage );
      }
      else
      {
        //Add this pivot to the list of vfc vectors of this page.
        final VFCPage page = new VFCPage( url, request.getRequest( ).getMessageId( ) );
        page.addVector( url, JellyfishConstants.PIVOT_VFC_VECTOR );
        
        VFCPageList.instance( ).add( page );
        
        //Add the pivot to the user
        this.generatePivot( user, page );
      }
    }
    
    pivotsToAdd.clear( );
    
    //Check if any of the new pivots is an already existing page, for the user.
    for( String url : bookmarks )
    {
      if( user.isPageKnown( url ) && ( !user.hasPivot( url ) ) )
      {
        pivotsToAdd.add( url );
      }
    }
    
    VFCPage page;
    
    for( String url : pivotsToAdd )
    {
      
      //get the page corresponding to the new pivot
      page = user.peekPageInformation( url );
      
      //Add this pivot to the list of vfc vectors of this page.
      page.addVector( url, JellyfishConstants.PIVOT_VFC_VECTOR );
      
      //add the new pivot
      this.generatePivot( user, page );
      
      //add the child pages to the new pivot zone
      recursiveAdd( newPivot, page );
      
    }
  }

  /**
   * Adds the given page to the pivot<br/>
   * This method also adds all of the linked pages to the
   * pivot. 
   * @param newPivot The pivot to use.
   * @param page The page to add.
   */
  private void recursiveAdd( final Pivot newPivot, final VFCPage page )
  {
    for( VFCPage child : page.getChildPages( ) )
    {
      
      newPivot.addNewPage( child );
      
      if( !child.getChildPages( ).isEmpty( ) )
      {
        this.recursiveAdd( newPivot, child );
      }
    }
  }
  
  /**
   * Parses the given json bookmark string.
   * @param contentString The json string to parse.
   * @return An list of bookmarks of the user.
   * @throws ArgumentException If there is an error with the string.
   */
  private List< String > parseBookmarkJson( final String contentString ) throws ArgumentException
  {
    try
    {
      //  {
      final JSONObject root = new JSONObject( new JSONTokener( contentString ) );
      
      //  bookmarks:
      final JSONObject bookmarks = root.optJSONObject( "bookmarks" );
      
      //  number:3,
      final int number = bookmarks.optInt( "number", -1 );
      final List<String> list = new ArrayList<>( number );
      
      if( number < 0 ) 
      {
        throw new ArgumentException( "The number of bookmarks is less than zero." );
      }
      
      //  bookmark:
      final JSONArray bookmarkArray = bookmarks.optJSONArray( "bookmark" );
      
      if( ( bookmarkArray != null ) && ( bookmarkArray.length( ) > 0 ) )
      {
      
        JSONObject bookmark;
        final String name = JellyfishConstants.EMPTY_STRING;
        String url;
        
        for( int i = 0; i < bookmarkArray.length( ); i++ )
        {
        
          // {
          bookmark = bookmarkArray.optJSONObject( i );
          
          //  url:'ESCAPED_URL'
          url = URLDecoder.decode( bookmark.optString( "url", JellyfishConstants.EMPTY_STRING ), StandardCharsets.UTF_8.name( ) );
          
          if( ( name.length( ) > 0 ) && ( url.length( ) > 0 ) )
          {
            list.add( url );
          }
        }      
      }
      return list;
    }
    catch( final ArgumentException | JSONException | UnsupportedEncodingException ex )
    {
      throw new ArgumentException( "There was an error with the given string", ex );
    }
  }

  /**
   * This method merges the given consistency zone definition with the consistency zones from the user.
   * @param user The user to adapt the consistency zone.
   * @param definition The definitions to be merged.
   */
  private void mergeConsistencyZones( final User user, final ConsistencyZoneDefinitions definition )
  {
    List<VFCPage> pages;
    
    user.setDefinitions( definition );
    
    for( Pivot pivot : user.getPivots( ) )
    {
      //get the pages from each consistency zone
      pages = new LinkedList<>( );
      
      for( ConsistencyZone zone : pivot.getConsistencyZones( ) )
      {
        pages.addAll( zone.getPages( ) );
      }
  
      //remove the existing consistency zones
      pivot.removeAllConsistencyZones( );
      
      //Change the pivot maximum VFC-Vector.
      pivot.changeMaxPivotVFCVector( user.getDefinitions( ).getPivotVector( ) );
  
      //add the new consistency zones
      for( VFCVector vector : user.getDefinitions( ).getConsistencyZoneVectors( ) )
      {
        pivot.addConsistencyZone( 
            new ConsistencyZone( vector, pivot.getPage( ).getUrl( ) ) );
      }      
  
      //add the saved pages.
      for( VFCPage page : pages )
      {
        pivot.addNewPage( page );
      }
    }    
  }

  /**
   * Returns the user name for the given url.
   * @param url The url that contains an query with an user name.
   * @return The name of the user.
   * @throws IllegalArgumentException If there is no user name or no query.
   */
  private String getUserName( final URL url ) throws IllegalArgumentException
  {
    if( ( url.getQuery( ) == null ) &&
        ( url.getQuery( ).split( "=" ).length == 2 ) &&
          url.getQuery( ).split( "=" )[0].equalsIgnoreCase( "username" ) ) 
    {
      this.logger.warn( "There is no username field." );
      throw new IllegalArgumentException( "There is no username field." );
    }

    return url.getQuery( ).split( "=" )[1];
  }
  
  /**
   * Returns an string containing the id of this operation.<br/>
   * The string is "VFC Client Operation Action".
   * @return The id of this action.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#getActionID()
   */
  @Override( )
  public String getActionID( )
  {
    return "VFC Client Operation Action";
  }
  
}
