/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.handling;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.util.collections.ProxyMessageHolder;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageQueue;
import com.assembla.ariane.jellyfish.util.collections.server.ServerMessageQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.LoggerFactory;

/**
 * This class represents an proxy response handling action.
 * That collects responses from the processors and places them in the 
 * response queue so that the responses can be sent or in the server
 * request queue, if more information is needed.
 * @author Fat Cat
 * @since 0.0.2
 * @version 3
 */
public class ProxyResponseHandlingAction extends AbstractModule
{

  private FileWriter output;

  /**
   * Creates a new proxy response handling action.
   * @param config The module configuration.
   */
  public ProxyResponseHandlingAction( final HierarchicalConfiguration config )
  {
    super( config );
    this.logger = LoggerFactory.getLogger( ProxyResponseHandlingAction.class );
    File data = new File("C:\\data.txt" );
    try
    {
      output = new FileWriter(data, true);
    }
    catch( IOException e )
    {
      this.logger.error("Cannot log to file.");
      output = null;
    }
  }
  
  /**
   * Stops the response handler.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override( )
  public void stop( )
  {
    this.logger.info( "Stopping the response receiver." );
    this.setRunning( false );
  }
  
  /**
   * Runs the response handler.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override( )
  public void run( )
  {
    try
    {
      this.setRunning( true );
      ProxyPageElement response = null;
      this.logger.info( "Starting the response receiver." );
      while( true )
      {
        response = null;
        
        if ( !this.isRunning( ) )
        {
          this.logger.info( "Response receiver stopped." );
          if(output != null)
          {
            try
            {
              this.output.flush();
              this.output.close();
            }
            catch( java.io.IOException ex)
            {

            }
          }
          break;
        }
        
        this.logger.debug( "Response receiver checking for responses." );
        try
        {
          while( response == null )
          {  
            response = ClientMessageProcessingQueue.instance( ).getResponses( ).poll( 
                JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS );
            if ( !this.isRunning( ) )
            {
              this.logger.info( "Response receiver stopped." );
              if(output != null)
              {
                try
                {
                  this.output.flush();
                  this.output.close();
                }
                catch( java.io.IOException ex)
                {

                }
              }
              break;
            }
          }           
        }
        catch( final InterruptedException e )
        {
        }
        if ( response != null )
        {
          this.logger.info( "Processing the response:\nID: {},\nState: {}", response.getResponse( ).getMessageId( ), response.getState( ).toString( ) );
          
          if ( !this.isRunning( ) )
          {
            this.logger.info( "Response receiver stopped." );
            if(output != null)
            {
              try
              {
                this.output.flush();
                this.output.close();
              }
              catch( java.io.IOException ex)
              {

              }
            }
            break;
          }
          
          if ( response.getState( ).equals( ProxyRequestState.MORE_INFO ) )
          {
            if(this.handleMoreInfoRequest( response )) break;
          }
          else if ( response.getState( ).equals( ProxyRequestState.PROCESSED ) )
          {

            if(this.output != null)
            {
              StringBuilder sb = new StringBuilder();
              sb.append(response.getResponse().getMessageIdAsString());
              sb.append(",");
              sb.append(response.getResponse().getResponseCodeAsInteger());
              sb.append(",");
              sb.append(response.containsExtra("cached"));
              sb.append(",");
              sb.append(response.containsExtra("in-cache"));
              sb.append(",");
              sb.append(response.getRequest().getCompleteUri());
              sb.append(",");
              sb.append(response.getRequest().getMethod());
              sb.append("\n");
              this.output.write(sb.toString());
              this.output.flush();
            }

            if(this.handleProcessedRequest( response )) break;
          }
          else
          {
            this.logger.error( "There is a message in a invalid state!" );
          }
          
        }
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Exception in ProxyResponseHandlingAction", ex );
    }
  }
  
  /**
   * Method that handles an request that was processed.
   * @param response An page that contains the request and it's response.
   */
  private boolean handleProcessedRequest( final ProxyPageElement response )
  {
    try
    {
      if ( !this.isRunning( ) )
      {
        this.logger.info( "Response receiver stopped." );
        if(output != null)
        {
          try
          {
            this.output.flush();
            this.output.close();
          }
          catch( java.io.IOException ex)
          {

          }
        }
        return true;
      }
      ClientMessageQueue.instance( ).getResponses( ).put( response.getResponse( ) );
      this.logger.info( "Placed the response in the response queue for sending." );
    }
    catch( final InterruptedException e )
    {
    }
    return false;
  }
  
  /**
   * Method that handles an page where it is needed more information from
   * the page original server.
   * @param moreinfo The page that needs more information.
   */
  private boolean handleMoreInfoRequest( final ProxyPageElement moreinfo )
  {
    //Since an response when received from the server isn't directly connected
    //to any page, we will need to store the page in a map, so that the 
    //response is correctly associated.
    ProxyMessageHolder.instance( ).add( moreinfo );
    try
    {
      final ProxyRequestMessage request = moreinfo.getTransitoryRequest( );
      if ( request != null )
      {
        if ( !this.isRunning( ) )
        {
          this.logger.info( "Response receiver stopped." );
          try
          {
            this.output.flush();
            this.output.close();
          }
          catch( java.io.IOException ex)
          {

          }
          return true;
        }
        ServerMessageQueue.instance( ).getRequests( ).put( request );
        this.logger.info( "Placed the response in the request queue for server request sending." );
      }
      else
      {
        this.logger.warn( "There is an message with more info but no request!" );
      }
    }
    catch( final InterruptedException e )
    {
    }
    return false;
  }
}
