/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.docks;

import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.Logger;

/**
 * This class represents an abstract dock.
 * @author Fat Cat
 * @version 0.0.2
 * @since 1
 */
public abstract class Dock
{
  /** The dock name. */
  protected String name;
  
  /** The client modules. */
  protected AbstractModule[] client = new AbstractModule[2];
  
  /** The server modules. */
  protected AbstractModule[] server = new AbstractModule[2];
  
  /** The dock type. */
  protected int dockType;
  
  /** Used to check if the server part is started. */
  protected boolean isServerStarted;
  
  /** Used to check if the client part is started. */
  protected boolean isClientStarted;
  
  /** The logger. */
  protected Logger logger;
    
  /**
   * Creates a new dock.
   * @param configuration The dock configuration.
   */
  public Dock( final HierarchicalConfiguration configuration )
  {    
    if( configuration != null )
    {
      this.name = configuration.getString( "[@name]", "NULL" );
    }    
    this.isClientStarted = false;
    this.isServerStarted = false;
  }
  
  /**
   * Starts this dock.
   * @param startClient Whether to start the client connection.
   * @param startServer Whether to start the server connection.
   * @throws Exception If some error happens.
   */
  public abstract void start( boolean startClient, boolean startServer ) throws Exception;
  
  /**
   * Stops this dock.
   * @param stopClient Whether to stop the client connection.
   * @param stopServer Whether to stop the server connection.
   * @throws Exception If some error happens.
   */
  public abstract void stop( boolean stopClient, boolean stopServer ) throws Exception;
  
  /**
   * Returns the dock name.
   * @return The dock name
   */
  public String getName( )
  {
    return this.name;
  }

  /**
   * Returns the type of the dock.
   * @return The dock type.
   */
  public int getDockType( )
  {
    return this.dockType;
  }

  /**
   * Checks if the server part is started.
   * @return True, if it is.
   */
  public boolean IsServerStarted( )
  {
    return this.isServerStarted;
  }
  
  /**
   * Checks if the client part is started.
   * @return True, if it is.
   */
  public boolean IsClientStarted( )
  {
    return this.isClientStarted;
  }
}
