/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.storage.cache.query;

import com.google.common.base.Objects;
import com.assembla.ariane.jellyfish.proxy.storage.cache.util.PageKey;
import java.util.UUID;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.fasterxml.uuid.Generators;
import com.assembla.ariane.jellyfish.proxy.exceptions.ArgumentException;
import java.io.Serializable;

import static java.lang.String.format;

/**
 * This is a object representing a cache query.<br/>
 * This object uses the builder pattern to help creating a new query.
 * @author Fat Cat
 * @version 3
 * @since 0.0.5
 */
public final class CacheQuery implements Serializable
{

  /** Serial version. */
  private static final long serialVersionUID = -7509333900879135913L;

  /** The type of operation. */
  private final CacheQueryOperation operation;
  
  /** The id of the query, for pipelining purposes. */
  private final UUID queryId;
  
  /** In case of add or change, the element to add. */
  private ProxyPageElement element;
  
  /** The id of the element to query. */
  private final UUID messageUUID;
  
  /** The method of the element to query. */
  private final String method;
  
  /** The complete url of the element to query. */
  private final String completeUrl;
  
  /** The name of the consistency to use. */
  private final String consistency_name;

  /** The page key. */
  private PageKey correspondingKey;

  /**
   * Creates a new cache query, based on the given element.
   * @param mOperation The cache operation.
   * @param mElement The element to use.
   * @param mConsistency_name The name of the consistency.
   * @throws ArgumentException If the element does not have an request.
   * @throws NullPointerException If any argument is null.
   */
  public CacheQuery( CacheQueryOperation mOperation,
                     ProxyPageElement mElement,
                     String mConsistency_name ) throws
      ArgumentException, NullPointerException
  {
    if( ( mConsistency_name != null) && (mElement != null) &&
          (mOperation != null) )
    {
      if(mElement.hasRequest())
      {
        operation = mOperation;
        queryId = Generators.timeBasedGenerator().generate();
        element = mElement;
        messageUUID = mElement.getRequest().getMessageId();
        method = mElement.getRequest().getMethod();
        completeUrl = mElement.getRequest().getCompleteUri();
        consistency_name = mConsistency_name;
        this.correspondingKey = new PageKey(messageUUID, completeUrl);
      }
      else throw new ArgumentException("The element must have an request.");
    }
    else throw new NullPointerException("The given arguments cannot be null.");
  }

  /**
   * Creates a new cache query.
   * @param mOperation The cache operation.
   * @param mMessageUUID The message id.
   * @param mMethod The method name. (can be null or get)
   * @param mCompleteUrl The complete url.
   * @param mConsistency_name The name of the consistency.
   */
  public CacheQuery( CacheQueryOperation mOperation,
                     UUID mMessageUUID, String mMethod, String mCompleteUrl,
                     String mConsistency_name )
  {
    if( ( mConsistency_name != null) && (mOperation != null) &&
        (mCompleteUrl != null) && ( mMessageUUID != null ) )
    {
      operation = mOperation;
      queryId = Generators.timeBasedGenerator().generate();
      messageUUID = mMessageUUID;
      method = (mMethod == null) ? "get" : mMethod;
      completeUrl = mCompleteUrl;
      consistency_name = mConsistency_name;
      this.correspondingKey = new PageKey(messageUUID, completeUrl);
    }
    else throw new NullPointerException("The given arguments cannot be null.");
  }

  // Query Id
  
  /**
   * Returns the unique identifier of this query request.
   * @return The identifier of this request.
   */
  public UUID getQueryId( )
  {
    return this.queryId;
  }
  
  // Consistency name
  
  /**
   * Returns the consistency name associated with this query.
   * @return The consistency name.
   */
  public String getConsistencyName( )
  {
    return this.consistency_name;
  }

  //Page Key

  /**
   * Returns the page key corresponding to this query.
   * @return The query page key.
   */
  public PageKey getPageKey()
  {
    return this.correspondingKey;
  }

  // URI
  
  /**
   * Returns the complete url to use. (host + http completeUrl request)
   * @return The complete url to use.
   */
  public String getCompleteUrl( )
  {
    return this.completeUrl;
  }
  
  // Method
  
  /**
   * Returns the method to use.
   * @return The method to search for. 
   */
  public String getMethod( )
  {
    return this.method;
  }
  
  // Id
  
  /**
   * Returns the request id to use.
   * @return The request id to search for.
   */
  public UUID getMessageUUID( )
  {
    return this.messageUUID;
  }
  
  // Proxy page
  
  /**
   * Returns the proxy page associated with this query.
   * @return The proxy page or null if no page exists.
   */
  public ProxyPageElement getElement( )
  {
    return this.element;
  }
  
  // Operation
  
  /**
   * Returns the operation associated with this query. 
   * @return The operation
   */
  public CacheQueryOperation getOperation( )
  {
    return this.operation;
  }
  
  /**
   * Returns an string representing this query.
   * @return An string representation of this query.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    final StringBuilder sb = new StringBuilder( );
    sb.append("{ ");
    sb.append( format( "id: %s, ", this.queryId.toString() ) );
    sb.append( format( "Uri: %s, ", this.completeUrl ) );
    sb.append( format( "Type: %s ", this.operation.toString() ) );
    sb.append("}");
    return sb.toString( );
  }

  /**
   * Checks if the given object is equal to this query.
   * @param o The other object to compare.
   * @return True, if both objects are equal, false otherwise.
   */
  @Override
  public boolean equals( Object o )
  {
    if( this == o ) return true;
    if( o == null || getClass() != o.getClass() ) return false;

    com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery that =
      ( com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery ) o;

    if( element != null )
    {
      if( completeUrl.equals( that.completeUrl ) &&
            consistency_name.equals( that.consistency_name ) &&
            correspondingKey.equals( that.correspondingKey ) &&
            !!element.equals( that.element ) &&
            messageUUID.equals( that.messageUUID ) &&
            method.equals( that.method ) && operation == that.operation &&
            queryId.equals( that.queryId ) ) return true;
      else return false;
    }
    else
    {
      if( completeUrl.equals( that.completeUrl ) &&
            consistency_name.equals( that.consistency_name ) &&
            correspondingKey.equals( that.correspondingKey ) &&
            !( that.element != null ) &&
            messageUUID.equals( that.messageUUID ) &&
            method.equals( that.method ) && operation == that.operation &&
            queryId.equals( that.queryId ) ) return true;
      else return false;
    }

  }

  /**
   * Calculates and returns an hash code for this query.
   * @return The calculated query hash code.
   */
  @Override
  public int hashCode()
  {
    return Objects.hashCode( operation, queryId, element, messageUUID, method,
                             completeUrl, consistency_name );
  }
}
