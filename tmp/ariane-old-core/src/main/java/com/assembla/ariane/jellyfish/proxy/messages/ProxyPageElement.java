/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages;

import java.io.Serializable;
import java.util.*;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Duration;
import com.fasterxml.uuid.Generators;

/**
 * This class represents an Proxy Page.
 * @author Fat Cat
 * @version 5
 * @since 0.0.1
 */
public class ProxyPageElement implements Serializable
{
  /** The proxy element version.  */
  private static final long serialVersionUID = 5105578560812443443L;

  private UUID currentUUID;
  
  private ProxyRequestMessage request;
  
  private ProxyResponseMessage response;
  
  private ProxyRequestState state;
  
  private final HashMap< String, String > extra;
  
  private ProxyMessage transitoryData;
  
  private TransitoryDataState transitoryDataState;

  /**
   * Creates a new empty proxy page. 
   */
  public ProxyPageElement( )
  {
    this.extra = new HashMap< String, String >( );
    this.request = new ProxyRequestMessage( UUID.randomUUID( ) );
    this.response = new ProxyResponseMessage( UUID.randomUUID( ) );
    this.state = ProxyRequestState.NEW;
    this.transitoryData = null;
    this.transitoryDataState = TransitoryDataState.NONE;
    this.currentUUID = Generators.timeBasedGenerator( ).generate( );
  }
  
  // State
  
  /**
   * Returns the current page element state.
   * @return the current state.
   */
  public synchronized ProxyRequestState getState( )
  {
    return this.state;
  }
  
  /**
   * Changes the state of this page element.
   * @param state The new state of this page element.
   */
  public void setState( final ProxyRequestState state )
  {
    if ( state != null )
    {
      this.state = state;
    }
  }
  
  // Request
  
  /**
   * Returns the page element request.
   * @return The page element request.
   */
  public synchronized ProxyRequestMessage getRequest( )
  {
    return this.request;
  }
  
  /**
   * Changes the request to the given one.
   * @param request The new request.
   */
  public void setRequest( final ProxyRequestMessage request )
  {
    if ( request != null )
    {
      this.request = request;
      this.currentUUID = this.request.getMessageId( );
    }
  }
  
  /**
   * Returns if this page has an valid request.
   * @return True, if it has.
   */
  public boolean hasRequest( )
  {
    return this.request != null;
  }
  
  // Response
  
  /**
   * Returns the page element response.
   * @return The page element response.
   */
  public synchronized ProxyResponseMessage getResponse( )
  {
    return this.response;
  }
  
  /**
   * Changes the response to the given one.
   * @param response The new response.
   */
  public void setResponse( final ProxyResponseMessage response )
  {
    if ( response != null )
    {
      this.response = response;
    }
  }
  
  /**
   * Checks if this page has an valid response.
   * @return True, if it has.
   */
  public boolean hasResponse( )
  {
    return this.response != null;
  }
  
  // Transitory data
  
  /**
   * Returns the transitory data as a response.
   * @return the response or null if the transitory data is not a response.
   */
  public synchronized ProxyResponseMessage getTransitoryResponse( )
  {
    if ( this.transitoryDataState == TransitoryDataState.RESPONSE )
    {
      return ( ProxyResponseMessage ) this.transitoryData;
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Returns the transitory data as a request.
   * @return the request or null if the transitory data is not a request.
   */
  public synchronized ProxyRequestMessage getTransitoryRequest( )
  {
    if ( this.transitoryDataState == TransitoryDataState.REQUEST )
    {
      return ( ProxyRequestMessage ) this.transitoryData;
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Sets the transitory data as a response.
   * @param transitoryData The response to set.
   */
  public void setTransitoryData( final ProxyResponseMessage transitoryData )
  {
    if ( transitoryData != null )
    {
      this.transitoryData = transitoryData;
      this.transitoryDataState = TransitoryDataState.RESPONSE;
    }
  }
  
  /**
   * Sets the transitory data as a request.
   * @param transitoryData The request to set.
   */
  public void setTransitoryData( final ProxyRequestMessage transitoryData )
  {
    if ( transitoryData != null )
    {
      this.transitoryData = transitoryData;
      this.transitoryDataState = TransitoryDataState.REQUEST;
    }
  }
  
  /**
   * Clears any data in the transitory message field.
   */
  public void emptyTransitoryData( )
  {
    if ( this.transitoryDataState != TransitoryDataState.NONE )
    {
      this.transitoryDataState = TransitoryDataState.NONE;
      this.transitoryData = null;
    }
  }
  
  // Extra
  
  /**
   * Checks if the extra value with the given name is known.
   * @param name The name of the extra value.
   * @return True, if it is.
   */
  public boolean containsExtra( final String name )
  {
    return this.extra.containsKey( name );
  }
  
  /**
   * Clears the extra field values.
   */
  public void clearExtra( )
  {
    this.extra.clear( );
  }
  
  /**
   * Returns an list with the names of the extra values.
   * @return An read-only list of extra value names.
   */
  public Set< String > getExtraNames( )
  {
    return Collections.unmodifiableSet( this.extra.keySet( ) );
  }
  
  /**
   * Checks if the extra value list is empty.
   * @return True, if it is.
   */
  public boolean isExtraEmpty( )
  {
    return this.extra.isEmpty( );
  }
  
  /**
   * Returns the extra value with the given name or null if it is unknown.
   * @param name The name of the extra value.
   * @return The value of the extra value.
   */
  public String getExtra( final String name )
  {
    if ( this.containsExtra( name ) )
    {
      return this.extra.get( name );
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Returns the number of known extra values.
   * @return The number of extra values.
   */
  public int extraCount( )
  {
    return this.extra.size( );
  }
  
  /**
   * Adds the given extra value if it does not exist.
   * @param name The name of the extra value.
   * @param value The value of the extra value.
   */
  public void addExtra( final String name, final String value )
  {
    if ( !this.containsExtra( name ) )
    {
      this.extra.put( name, value );
    }
  }
  
  /**
   * Removes the extra value with the given name.  
   * @param name The name of the extra value.
   */
  public void removeExtra( final String name )
  {
    if ( this.containsExtra( name ) )
    {
      this.extra.remove( name );
    }
  }
  
  // RequestId
  
  /**
   * Returns the current page element id.
   * @return The page element id.
   */
  public synchronized UUID getCurrentUUID( )
  {
    return this.currentUUID;
  }
  
  // Proxy Calculations
  
  /**
   * Calculates the corrected initial age, as specified by the http standard.
   * @return The corrected initial age.
   */
  public long calculateCorrectedInitialAge( )
  {
    if ( !this.containsExtra( "CorrectedInitialAge" ) )
    {
      setCorrectedReceivedInitialAge( );
    }
    
    long result = -1;
    
    try
    {
      result = Long.parseLong( this.getExtra( "CorrectedInitialAge" ) );
    }
    catch( final NumberFormatException ex )
    {
      result = -1;
    }
    return result;
  }
  
  /**
   * Sets the corrected received initial age extra field, using the 
   * standard calculations.
   */
  private void setCorrectedReceivedInitialAge( )
  {
    if ( this.hasRequest( ) && this.hasResponse( ) && 
         this.getRequest( ).hasRequestTime( ) && 
         this.getResponse( ).hasResponseTime( ) )
    {
      try
      {
        final long received_age = this.getResponse( ).getCorrectedReceivedAge( );
        final DateTime response_time = this.getResponse( ).getResponseTime( );
        final DateTime request_time = this.getRequest( ).getRequestTime( );
        
        if ( response_time.isAfter( request_time ) )
        {
          
          final long periodInSeconds = Math.round( new Duration( 
              request_time, response_time ).getMillis( ) / ( float ) 
              DateTimeConstants.MILLIS_PER_SECOND );
          
          final long initage = received_age + periodInSeconds;
          this.addExtra( "CorrectedInitialAge", Long.toString( initage ) );
        }
      }
      catch( final IllegalArgumentException ex )
      {
        this.addExtra( "CorrectedInitialAge", Long.toString( -1 ) );
      }
    }
    this.addExtra( "CorrectedInitialAge", Long.toString( -1 ) );
  }
  
  /**
   * Calculates the resident time of this page.
   * @return The current resident time of this page.
   */
  public long calculateResidentTime( )
  {
    long result = -1;
    
    try
    {
      if ( this.hasResponse( ) && this.getResponse( ).hasResponseTime( ) )
      {
        result = Math.round( 
            new Duration( this.getResponse( ).getResponseTime( ), 
                DateTime.now( ) ).getMillis( ) / ( float ) DateTimeConstants.MILLIS_PER_SECOND );
      }
    }
    catch( final IllegalArgumentException ex )
    {
      result = -1;
    }
    
    return result;
  }
  
  /**
   * Calculates the page current age, using http standard calculations.
   * @return The page current age.
   */
  public long calculateCurrentAge( )
  {
    final long correctedInitialAge = this.calculateCorrectedInitialAge( );
    final long residentTime = this.calculateResidentTime( );
    
    if ( ( correctedInitialAge >= 0 ) && ( residentTime >= 0 ) )
    {
      return correctedInitialAge + residentTime;
    }
    else
    {
      return -1;
    }
  }
  
  /**
   * Calculates the freshness lifetime of a page, using the http standard 
   * methods.<br/>
   * According to the http standard, this method tries to:<br/>
   * <ol>
   *  <li>Check if there is the max_value cache control header, and return that</li>
   *  <li>Check if there is an expires and date header and use the subtraction between
   *      the expires and the date header as the return value.</li>
   *  <li>Check if there is the date and the last modified header, and return 
   *      the following value: <br/>
   *      <code>date_value - ( last_modified_value * 0.1 )</code></li>
   *  <li>Return an value that corresponds to 1 day, assuming that the
   *      days have 24 hours, the hours have 60 minutes and an minute has 
   *      60 seconds.</li>
   * </ol><br/>
   * The following algorithm is followed from the first to the last method.<br/>
   * If max-stale is included, we add that to the freshness lifetime<br/>
   * Note that since this is a shared cache, both s-max-age and max-age can be 
   * used, so if one is missing the other is used by the indicated order.
   * 
   * @return The page freshness lifetime.
   */
  public long calculateFreshnessLifetime( )
  {
    long maxStale = 0;
    if ( this.hasRequest( ) && this.getRequest( ).containsExtra( "max-stale" ) )
    {
      maxStale = Long.parseLong( this.getRequest( ).getExtra( "max-stale" ) );
    }
    final long secondCondition = trySecondCondition( );
    final long thirdCondition = tryThirdCondition( );
    if ( this.hasResponse( ) && this.getResponse( ).containsExtra( "max-age" ) )
    {
      return Long.parseLong( this.getResponse( ).getExtra( "max-age" ) ) + maxStale;
    }
    else if ( this.hasRequest( ) && this.getRequest( ).containsExtra( "max-age" ) )
    {
      return Long.parseLong( this.getRequest( ).getExtra( "max-age" ) ) + maxStale;
    }
    else if ( secondCondition >= 0 )
    {
      return secondCondition + maxStale;
    }
    else if ( thirdCondition >= 0 )
    {
      return thirdCondition + maxStale;
    }
    else
    {
      return JellyfishConstants.MAX_STATE_TIME + maxStale;
    }
  }
  
  /**
   * Method to execute the third condition of the freshness lifetime, that is,
   * to check if there is the date and the last modified header, and return 
   * the following value: <code>date_value - ( last_modified_value * 0.1 )</code>.
   * @return The calculated value, of -1 if some error happens.
   */
  private long tryThirdCondition( )
  {
    if ( this.hasResponse( ) && this.getResponse( ).containsHeader( "Date" ) && 
         this.getResponse( ).containsHeader( "Last-Modified" ) )
    {
      try
      {
        final DateTime lastModifiedValue = DateTime.parse( 
            this.getResponse( ).getHeader( "Last-Modified" ), 
            JellyfishConstants.HTTP_FORMATTER );
        
        final DateTime dateValue = DateTime.parse( 
            this.getResponse( ).getHeader( "Date" ), 
            JellyfishConstants.HTTP_FORMATTER );
        
        final long periodInSeconds = Math.round( 
            new Duration( 
                lastModifiedValue, dateValue ).getMillis( ) / ( float ) DateTimeConstants.MILLIS_PER_SECOND ); 
        
        return Math.round( periodInSeconds * 0.1 );
      }
      catch( final IllegalArgumentException ex )
      {
        return -1;
      }
    }
    return -1;
  }
  
  /**
   * Method to execute the second condition of the freshness lifetime, that is,
   * to check if there is an expires and date header and use the subtraction between
   * the expires and the date header as the return value.
   * @return The valvulated value or -1 if some error happens.
   */
  private long trySecondCondition( )
  {
    if ( this.hasResponse( ) && 
         this.getResponse( ).containsHeader( "Expires" ) && 
         this.getResponse( ).containsHeader( "Date" ) )
    {
      try
      {
        final DateTime expiresValue = DateTime.parse( 
            this.getResponse( ).getHeader( "Expires" ), 
            JellyfishConstants.HTTP_FORMATTER );
        
        final DateTime dateValue = DateTime.parse( 
            this.getResponse( ).getHeader( "Date" ), 
            JellyfishConstants.HTTP_FORMATTER );

        return Math.round(
          new Duration( dateValue, expiresValue ).getMillis( ) / ( float )
            DateTimeConstants.MILLIS_PER_SECOND );
      }
      catch( final IllegalArgumentException ex )
      {
        return -1;
      }
    }
    return -1;
  }
  
  /**
   * Calculates the expiration time, using the http standard calculations.
   * @return The expiration time of the page.
   */
  public long calculateExpirationTime( )
  {
    final long freshness_lifetime = this.calculateFreshnessLifetime( );
    final long current_age = this.calculateCurrentAge( );
    try
    {
      if ( this.hasResponse( ) && this.getResponse( ).hasResponseTime( ) && 
          ( freshness_lifetime >= 0 ) && ( current_age >= 0 ) )
      {
        final long response_time = Math.round( 
            new Duration( 
                this.getResponse( ).getResponseTime( ) ).getMillis( ) / 
                ( float ) DateTimeConstants.MILLIS_PER_SECOND );
        
        return response_time + ( freshness_lifetime - current_age );
      }
    }
    catch( final IllegalArgumentException ex )
    {
      return -1;
    }
    return -1;
  }
  
  // Proxy Cleaning
  
  /**
   * Removes all hop-to-hop headers.
   * @param isRequest If the request headers are the ones to be removed.
   */
  public void removeHopToHopHeaders( final boolean isRequest )
  {
    if ( this.hasRequest( ) && isRequest )
    {
      this.getRequest( ).removeHopToHopHeaders( );
    }
    else if ( this.hasResponse( ) && !isRequest )
    {
      this.getResponse( ).removeHopToHopHeaders( );
    }
  }
  
}
