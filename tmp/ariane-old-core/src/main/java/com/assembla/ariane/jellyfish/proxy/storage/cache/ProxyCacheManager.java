/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.storage.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import com.google.common.annotations.VisibleForTesting;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.util.collections.cache.CacheMessageProcessingQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.exceptions.JellyfishInterruptedException;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponse;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponseOperation;
import com.assembla.ariane.jellyfish.proxy.storage.cache.util.CacheList;
import com.assembla.ariane.jellyfish.proxy.storage.cache.util.PageKey;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.search.Attribute;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.Result;
import net.sf.ehcache.search.Results;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that manages all the existing caches.
 * @author Fat Cat
 * @since 0.0.5
 * @version 1
 */
public class ProxyCacheManager extends AbstractModule
{
  
  /** An list of storage areas. */
  private CacheList< PageKey, ProxyPageElement > caches;
  
  /** The logger object. */
  private final Logger logger;
  
  /**
   * Creates a new cache manager.
   * @param config The configuration to use.
   * @throws ConfigurationException If some error happens. 
   */
  public ProxyCacheManager( final HierarchicalConfiguration config ) throws ConfigurationException
  {
    super( config );
    this.logger = LoggerFactory.getLogger( ProxyCacheManager.class );
    
    final SubnodeConfiguration subconf = config.configurationAt( "caches" );
    final net.sf.ehcache.CacheManager manager = net.sf.ehcache.CacheManager.create( 
        config.getString( "caches[@configuration]", null ) );
      
    this.caches = new CacheList<>( manager );
    this.caches.configure( subconf );
  }
  
  /**
   * Creates a new cache manager.
   * @param configfile The configuration to use.
   * @throws ConfigurationException If some error happens.
   */
  @VisibleForTesting
  public ProxyCacheManager( final String configfile ) throws ConfigurationException
  {
    super( null );
    this.logger = LoggerFactory.getLogger( ProxyCacheManager.class );
    final net.sf.ehcache.CacheManager manager = net.sf.ehcache.CacheManager.create( configfile );
    this.caches = new CacheList<>( manager );
  }
  
  /**
   * Stops the cache manager.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override( )
  public void stop( )
  {
    this.logger.info( "Trying to stop the cache manager." );
    this.setRunning( false );
  }
  
  /**
   * Starts the cache manager.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override( )
  public void run( )
  {
    CacheQuery query;
    CacheResponse resp = null;
    this.setRunning( true );
    while( true )
    {
      query = null;
      if(tryShutdown( )) return;
      
      //Retrieve a query request.
      this.logger.debug( "Retrieving a query request" );
      try
      {
        while( query == null )
        {  
          query = CacheMessageProcessingQueue.instance( ).getRequests( ).poll( 
              JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS );
          if(tryShutdown( )) return;
        } 
      }
      catch( final InterruptedException e )
      {
        //Do nothing
      }
      
      if ( query != null )
      {
        this.logger.info( "Processing a query request for url {}", 
            query.getCompleteUrl( ) );
        
        this.logger.info( "The query has an id of {}", query.getQueryId( ) );

        try
        {
          switch( query.getOperation( ) )
          {
            case ADD:
              resp = this.addQueryHandler( query );
              break;
            case CHANGE:
              resp = this.changeQueryHandler( query );
              break;
            case QUERY:
              resp = this.getQueryHandler( query );
              break;
            case REMOVE:
              resp = this.removeQueryHandler( query );
              break;
            default:
              break;
          }
          if(tryShutdown( )) return;
        }
        catch( JellyfishInterruptedException ex)
        {
          return;
        }

        if(tryShutdown( )) return;
        
        try
        {
          CacheMessageProcessingQueue.instance( ).getResponses( ).put( resp );
        }
        catch( final InterruptedException e )
        {
          //Do nothing
        }

        if(tryShutdown( )) return;
        
      }
      
    }
  }

  /**
   * Handles an get query.
   * @param query The query to handle.
   * @return An response to the query request.
   * @throws JellyfishInterruptedException If the manager should shutdown.
   */
  private CacheResponse getQueryHandler( CacheQuery query )
    throws JellyfishInterruptedException
  {
    logger.debug( "In get method for query id {}",
                  query.getQueryId().toString() );
    if(this.tryShutdown( )) throw new JellyfishInterruptedException();

    if ( !this.caches.contains( query.getConsistencyName() ) )
    {
      return new CacheResponse( query.getOperation(), query.getQueryId(),
                                CacheResponseOperation.ERROR );
    }
    else
    {
      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      //Get all of the matching entries...
      List< ProxyPageElement > elements = get(query.getConsistencyName(),
                                              query.getPageKey());

      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      if(elements.isEmpty())
      {
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.FAIL);
      }
      else
      {
        //if there is at least one element...
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.OK, elements.get(0) );
      }

    }
  }

  /**
   * Handles an remove query.
   * @param query The query to handle
   * @return An response to the query request.
   * @throws JellyfishInterruptedException If the manager should shutdown.
   */
  private CacheResponse removeQueryHandler( CacheQuery query )
    throws JellyfishInterruptedException
  {
    logger.debug( "In remove method for query id {}",
                  query.getQueryId().toString() );

    if(this.tryShutdown( )) throw new JellyfishInterruptedException();

    if ( !this.caches.contains( query.getConsistencyName() ) )
    {
      return new CacheResponse( query.getOperation(), query.getQueryId(),
                                CacheResponseOperation.ERROR );
    }
    else
    {

      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      List< ProxyPageElement > elements = remove(query.getConsistencyName(),
                                                 query.getPageKey());

      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      if(elements.isEmpty())
      {
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.FAIL);
      }
      else
      {
        //if there is at least one element...
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.OK, elements.get(0) );
      }
    }
  }

  /**
   * Handles an change query.
   * @param query The query to handle.
   * @return An response to the query request.
   * @throws JellyfishInterruptedException If the manager should shutdown.
   */
  private CacheResponse changeQueryHandler( CacheQuery query )
    throws JellyfishInterruptedException
  {
    if(this.tryShutdown( )) throw new JellyfishInterruptedException();

    if ( query.getElement() == null )
    {
      return new CacheResponse( query.getOperation(), query.getQueryId(),
                                CacheResponseOperation.ERROR );
    }

    if(this.tryShutdown( )) throw new JellyfishInterruptedException();

    if ( !this.caches.contains( query.getConsistencyName() ) )
    {
      return new CacheResponse( query.getOperation(), query.getQueryId(),
                                CacheResponseOperation.ERROR );
    }
    else
    {
      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      List< ProxyPageElement > elements = change(query.getConsistencyName(),
                                                 query.getPageKey(),
                                                 query.getElement());

      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      if(elements.isEmpty())
      {
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.FAIL);
      }
      else
      {
        //if there is at least one element...
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.OK, elements.get(0) );
      }
    }
  }

  /**
   * Handles an add query.
   * @param query The query to handle.
   * @return An response to the query request.
   * @throws JellyfishInterruptedException If the manager should shutdown.
   */
  private CacheResponse addQueryHandler( CacheQuery query )
    throws JellyfishInterruptedException
  {
    if(this.tryShutdown( )) throw new JellyfishInterruptedException();

    if ( query.getElement() == null )
    {
      return new CacheResponse( query.getOperation(), query.getQueryId(),
                                CacheResponseOperation.ERROR );
    }

    if(this.tryShutdown( )) throw new JellyfishInterruptedException();

    if ( !this.caches.contains( query.getConsistencyName() ) )
    {
      return new CacheResponse( query.getOperation(), query.getQueryId(),
                                CacheResponseOperation.ERROR );
    }
    else
    {
      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      boolean success = add(query.getConsistencyName(),
                            query.getPageKey(),
                            query.getElement());

      if(this.tryShutdown( )) throw new JellyfishInterruptedException();

      if(success)
      {
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.OK);
      }
      else
      {
        return new CacheResponse(query.getOperation(), query.getQueryId(),
                                 CacheResponseOperation.FAIL );
      }
    }
  }

  /**
   * Sees if the cache manager has to shutdown.
   */
  private boolean tryShutdown( )
  {
    if ( !this.isRunning( ) )
    {
      this.logger.debug( "Emptying all the caches." );
      //Empty all caches
      this.caches.shutdown( );
      
      this.logger.debug( "Clearing all the queries." );
      //Empty all queries
      CacheMessageProcessingQueue.instance( ).clearAll( );
      this.logger.info( "Cache manager terminated." );
      return true;
    }
    else return false;
  }

  /**
   * Returns the cache mapping.
   * @return The cache mapping.
   */
  @VisibleForTesting
  public final CacheList< PageKey, ProxyPageElement > getCaches( )
  {
    return this.caches;
  }

  //Cache list operations

  /**
   * Checks if any cache of the given type contains the given key.
   * @param consistencyName The name of the associated consistency.
   * @param key The key to use.
   * @return True, if it has, false otherwise.
   */
  public boolean contains( String consistencyName, PageKey key )
  {
    if ( !this.caches.contains( consistencyName ) )
    {
      return false;
    }
    else
    {
      try
      {
        Cache cache = this.caches.get(consistencyName);

        //Attribute<String> pageId = cache.getSearchAttribute( "pageId" );
        Attribute<String> uri = cache.getSearchAttribute( "uri" );

        Query query = cache.createQuery();
        query.includeKeys();
        query.addCriteria(uri.ilike(key.getUri()) ).maxResults(1).end();

        Results results = query.execute();

        return results.size() != 0;
      }
      catch( CacheException ex)
      {
        return false;
      }
    }
  }

  /**
   * Returns the value present in a cache.
   * @param consistencyName The consistency name.
   * @param key The key to use.
   * @return The values associated to the key, or an empty list.
   */
  public List<ProxyPageElement> get( String consistencyName, PageKey key )
  {
    if ( !this.contains( consistencyName, key ) )
    {
      return Collections.emptyList();
    }
    else
    {
      try
      {
        Cache cache = this.caches.get(consistencyName);

        Attribute<String> uri = cache.getSearchAttribute("uri");

        Query query = cache.createQuery();
        query.includeKeys();
        query.addCriteria(uri.ilike(key.getUri()) ).maxResults(1).end();

        Results results = query.execute();

        List<ProxyPageElement> queryResults= new ArrayList<>(results.size());

        Object obj;
        for( Result result : results.all() )
        {
          this.logger.debug( "The key is {}", result.getKey( ) );
          obj = this.caches.get( consistencyName, (PageKey) result.getKey( ));
          queryResults.add((ProxyPageElement) obj);
        }
        return queryResults;
      }
      catch(CacheException ex)
      {
        return Collections.emptyList();
      }
    }
  }

  /**
   * Adds the given key/value pair to the cache associated with the given consistency.
   * @param consistencyName The consistency name.
   * @param key The entry key.
   * @param value The entry value.
   * @return True on success, false otherwise.
   */
  public boolean add( String consistencyName, PageKey key,
                      ProxyPageElement value )
  {
    return this.contains( consistencyName, key ) ||
             this.caches.add( consistencyName, key, value );
  }

  /**
   * Tries to change the given key/value pair to the cache associated
   * with the given consistency.
   * @param consistencyName The cache type.
   * @param key The entry key.
   * @param value The entry value.
   * @return The old values of the entry, or empty list if there are no values.
   */
  public List<ProxyPageElement> change( String consistencyName, PageKey key,
                                        ProxyPageElement value )
  {
    if ( this.contains( consistencyName, key ) )
    {
      List<ProxyPageElement> old_value = this.get( consistencyName, key );
      if(!old_value.isEmpty())
      {
        try
        {
          if(!this.caches.change( consistencyName, key, value ))
          {
            return Collections.emptyList();
          }
        }
        catch( final CacheException e )
        {
          return Collections.emptyList();
        }
        return old_value;
      }
      else return Collections.emptyList();
    }
    else return Collections.emptyList();
  }

  /**
   * Tries to remove the given key from the cache associated with the given consistency.
   * @param consistencyName The consistency name.
   * @param key The entry key.
   * @return The removed pages, or empty list if nothing was removed.
   */
  public List<ProxyPageElement> remove( String consistencyName, PageKey key )
  {
    if ( this.contains( consistencyName, key ) )
    {
      List<ProxyPageElement> element = this.get( consistencyName, key );
      if(!element.isEmpty())
      {
        if(!this.caches.remove( consistencyName, key ))
        {
          return Collections.emptyList();
        }
        else return element;
      }
      else return Collections.emptyList();
    }
    else
    {
      return Collections.emptyList();
    }
  }

}
