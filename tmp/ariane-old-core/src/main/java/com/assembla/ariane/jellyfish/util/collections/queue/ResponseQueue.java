/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.queue;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents an queue of responses.
 * @author Fat Cat
 * @param <Response> The response type.
 * @version 1
 * @since 0.0.3
 */
public class ResponseQueue< Response >
{
  protected BlockingQueue< Response > receiver;
  
  protected Logger logger;
  
  /**
   * Creates a new response queue, given it's size.
   * @param size The size of the new queue.
   */
  protected ResponseQueue( final int size )
  {
    this.receiver = new LinkedBlockingQueue< Response >( size );
    this.logger = LoggerFactory.getLogger( ResponseQueue.class );
  }
  
  /**
   * Adds a new response to the queue.
   * @param response The response to add.
   * @throws IllegalStateException If the queue cannot hold more responses. 
   */
  public void add( final Response response ) throws IllegalStateException
  {
    this.logger.debug( "Adding a response." );
    if ( !this.receiver.contains( response ) )
    {
      this.receiver.add( response );
    }
  }
  
  /**
   * Clears the response queue.
   */
  public void clear( )
  {
    this.logger.debug( "Clearing the responses." );
    this.receiver.clear( );
  }
  
  /**
   * Checks if the given response is in this queue.
   * @param response The response to check for.
   * @return True, if the response exists.
   */
  public boolean contains( final Response response )
  {
    return this.receiver.contains( response );
  }
  
  /**
   * Retrieves the request at the head of the response without removing it.
   * @return The request at the head of the response.
   */
  public Response element( )
  {
    return this.receiver.element( );
  }
  
  /**
   * Returns true if the response queue is empty.
   * @return True if the response queue is empty.
   */
  public boolean isEmpty( )
  {
    return this.receiver.isEmpty( );
  }
  
  /**
   * Returns an response queue iterator.
   * @return The response queue iterator.
   */
  public Iterator< Response > iterator( )
  {
    return this.receiver.iterator( );
  }
  
  /**
   * Adds the specified response to the queue.
   * @param e The response to add to the queue.
   * @return True, if successful.
   */
  public boolean offer( final Response e )
  {
    this.logger.debug( "Adding a response." );
    return this.receiver.offer( e );
  }
  
  /**
   * Adds the specified response, waiting the given amount of time.
   * @param e The response to add to the queue.
   * @param timeout The maximum time to wait.
   * @param unit The unit of the timeout value.
   * @return True, on success.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public boolean offer( final Response e, final long timeout, final TimeUnit unit ) throws InterruptedException
  {
    this.logger.debug( "Adding a response." );
    return this.receiver.offer( e, timeout, unit );
  }
  
  /**
   * Retrieves but does not remove the response at the head of the queue.
   * @return The response at the head of the queue or null if no such thing exist.
   */
  public Response peek( )
  {
    return this.receiver.peek( );
  }
  
  /**
   * Retrieves and removes the response at the head of this queue.
   * @return The response at the head of this queue or null.
   */
  public Response poll( )
  {
    this.logger.debug( "Removing a response." );
    return this.receiver.poll( );
  }
  
  /**
   * Retrieves and removes the response at the head of this queue, 
   * waiting the specified amount of time.
   * @param timeout The maximum time to wait.
   * @param unit The timeout unit.
   * @return The response at the head of this queue or null.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public Response poll( final long timeout, final TimeUnit unit ) throws InterruptedException
  {
    return this.receiver.poll( timeout, unit );
  }
  
  /**
   * Adds a new request to this queue waiting until the space if available.
   * @param response The response to add to this queue.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public void put( final Response response ) throws InterruptedException
  {
    this.logger.debug( "Adding a response." );
    this.receiver.put( response );
  }
  
  /**
   * Returns the number of responses that can be added to this 
   * queue so that the capacity of this queue is not bypassed.
   * 
   * @return The number of remaining responses.
   */
  public int remainingCapacity( )
  {
    return this.receiver.remainingCapacity( );
  }
  
  /**
   * Retrieves and removes an response from the head of the pool.
   * @return The response at the head of the queue.
   * @throws NoSuchElementException If there is no response in the queue.
   */
  public Response remove( ) throws NoSuchElementException
  {
    this.logger.debug( "Removing a response." );
    return this.receiver.remove( );
  }
  
  /**
   * Returns the number of available responses.
   * @return The number of available responses.
   */
  public int count( )
  {
    return this.receiver.size( );
  }
  
  /**
   * Retrieves and removes an response form the queue 
   * waiting for it to become available.
   * 
   * @return The retrieved response.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public Response take( ) throws InterruptedException
  {
    this.logger.debug( "Removing a response." );
    return this.receiver.take( );
  }
}
