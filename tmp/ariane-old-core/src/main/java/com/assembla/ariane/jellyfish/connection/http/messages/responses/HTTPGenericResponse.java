/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.messages.responses;

import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import com.assembla.ariane.jellyfish.connection.http.messages.HTTPGenericMessage;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.WarningValue;
import com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse;

/**
 * This is a generic http response.
 * @author Fat Cat
 * @version 2
 * @since 0.0.6
 */
public class HTTPGenericResponse extends HTTPGenericMessage implements GenericResponse
{
  
  /** The response. */
  protected final HttpResponse response;
  
  /** The warnings. */
  protected List< WarningValue > warnings;
  
    /**
   * Creates a new client response.
   * @param requestId The id of this response.
   */
  public HTTPGenericResponse( final UUID requestId )
  {
    this( requestId, new DefaultHttpResponse( HttpVersion.HTTP_1_1, HttpResponseStatus.OK ) );
    this.warnings = Collections.synchronizedList( new ArrayList< WarningValue >( JellyfishConstants.DEFAULT_ARRAY_LIST_SIZE ) );
  }
  
  /**
   * Creates a new generic response, based on a HttpResponse object.
   * @param requestId The requestId of the response.
   * @param response The other HttpResponse.
   */
  public HTTPGenericResponse( final UUID requestId, final HttpResponse response )
  {
    super( response, requestId );
    this.response = response;
    this.warnings = Collections.synchronizedList( new ArrayList< WarningValue >( JellyfishConstants.DEFAULT_ARRAY_LIST_SIZE ) );
  }

  // Response code stuff
  
  /**
   * Returns the current http response code.
   * @return The response code.
   */
  public HttpResponseStatus getNativeResponseCode( )
  {
    return this.response.getStatus( );
  }

  /**
   * Returns the current http response code as a string.
   * @return The response code.
   */
  @Override( )
  public String getResponseCode( )
  {
    return this.response.getStatus( ).toString( );
  }

  /**
   * Returns the current http response code as a integer.
   * @return The response code.
   */
  @Override( )
  public int getResponseCodeAsInteger( )
  {
    return this.response.getStatus( ).getCode( );
  }

  /**
   * Changes the http response code.
   * @param responsecode The new response code.
   */
  public void setResponseCode( final HttpResponseStatus responsecode )
  {
    if ( ( responsecode != null ) && ( !this.response.getStatus( ).equals( responsecode ) ) )
    {
      this.response.setStatus( responsecode );
    }
  }

  /**
   * Changes the response code of this message.
   * @param code The response code integer.
   * @param message The response code message. (not used)
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#setResponseCode(int, java.lang.String)
   */
  @Override( )
  public void setResponseCode( final int code, final String message )
  {
    this.response.setStatus( HttpResponseStatus.valueOf( code ) );
  }
  
  //Response stuff

  /**
   * Returns the inner HTTPResponse object.
   * @return The HTTPResponse object.
   */
  public HttpResponse getResponse( )
  {
    return this.response;
  }

  // Warning stuff
  
  /**
   * Checks if the given warning is present.
   * @param warning The warning to check for.
   * @return True, if it is.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#containsWarning(com.assembla.ariane.jellyfish.proxy.messages.WarningValue)
   */
  @Override( )
  public boolean containsWarning( final WarningValue warning )
  {
    return this.warnings.contains( warning );
  }

  /**
   * Adds the given warning to the list.
   * @param warning The warning to add.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#addWarning(com.assembla.ariane.jellyfish.proxy.messages.WarningValue)
   */
  @Override( )
  public void addWarning( final WarningValue warning )
  {
    if ( !this.containsWarning( warning ) )
    {
      this.warnings.add( warning );
    }
  }

  /**
   * Removes the given warning.
   * @param warning The warning to remove.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#removeWarning(com.assembla.ariane.jellyfish.proxy.messages.WarningValue)
   */
  @Override( )
  public void removeWarning( final WarningValue warning )
  {
    if ( this.containsWarning( warning ) )
    {
      this.warnings.remove( warning );
    }
  }

  /**
   * Returns an list of warnings.
   * @return An read-only list of present warnings.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#getWarnings()
   */
  @Override( )
  public List< WarningValue > getWarnings( )
  {
    return Collections.unmodifiableList( this.warnings );
  }

  /**
   * Returns the number of warnings.
   * @return The number of warning.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#getNumberOfWarnings()
   */
  @Override( )
  public int getNumberOfWarnings( )
  {
    return this.warnings.size( );
  }

  /**
   * Checks if the warning list is empty.
   * @return True, if it is.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#hasAnyWarning()
   */
  @Override( )
  public boolean hasAnyWarning( )
  {
    return this.warnings.isEmpty( );
  }

  /**
   * Empties the warning list.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#clearWarnings()
   */
  @Override( )
  public void clearWarnings( )
  {
    this.warnings.clear( );
  }
  
}
