/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages;

/**
 * This is the state of the transitory message field, it specified whether
 * the transitory message is an request, an response or none of them.
 * @author Fat Cat
 * @version 1
 * @since 0.0.6
 */
enum TransitoryDataState
{
  /**
   * The field is an response.
   */
  RESPONSE,
  /**
   * The field is an request.
   */
  REQUEST,
  /**
   * The field is null.
   */
  NONE
}
