/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages;

import java.io.Serializable;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import org.joda.time.DateTime;

/**
 * This class represents an warning header field.
 * @author Fat Cat
 */
public final class WarningValue implements Serializable
{
  
  /** This class version. */
  private static final long serialVersionUID = 761767589853287234L;
  
  /** The warning code. */
  private int code;
  
  /** The warning agent. */
  private String agent;
  
  /** The warning text. */
  private String text;
  
  /** The warning date. */
  private DateTime date;
  
  /**
   * Creates a new proxy warning. 
   * @param code The warning code.
   * @param agent The warning agent.
   * @param text The warning text.
   * @return The corresponding proxy warning.
   */
  public static WarningValue create( final int code, final String agent, final String text )
  {
    final WarningValue warning = new WarningValue( );
    warning.setAgent( agent );
    warning.setCode( code );
    warning.setText( text );
    return warning;
  }
  
  /**
   * Creates a new proxy warning. 
   * @param code The warning code.
   * @param agent The warning agent.
   * @param text The warning text.
   * @param date The warning date.
   * @return The corresponding proxy warning.
   */
  public static WarningValue create( final int code, final String agent, final String text, final DateTime date )
  {
    final WarningValue warning = new WarningValue( );
    warning.setAgent( agent );
    warning.setCode( code );
    warning.setText( text );
    warning.setDate( date );
    return warning;
  }
  
  /** Constructor to create a new proxy warning. */
  private WarningValue( )
  {
    this.code = 0;
    this.agent = "";
    this.text = "";
    this.date = null;
  }
  
  /**
   * Returns the code of this warning.
   * @return The warning code.
   */
  public int getCode( )
  {
    return this.code;
  }
  
  /**
   * Changes the code of this warning.
   * @param code The new warning code.
   */
  public void setCode( final int code )
  {
    if ( code > 0 )
    {
      this.code = code;
    }
  }
  
  /**
   * Returns the agent of this warning.
   * @return The warning agent.
   */
  public String getAgent( )
  {
    return this.agent;
  }
  
  /**
   * Changes the agent of this warning.
   * @param agent The new warning agent.
   */
  public void setAgent( final String agent )
  {
    if ( agent != null )
    {
      this.agent = agent;
    }
  }
  
  /**
   * Returns the text of this warning.
   * @return The warning text.
   */
  public String getText( )
  {
    return this.text;
  }
  
  /**
   * Changes the text of this warning.
   * @param text The new warning text.
   */
  public void setText( final String text )
  {
    if ( text != null )
    {
      this.text = text;
    }
  }
  
  /**
   * Returns the date of this warning.
   * @return The warning date.
   */
  public DateTime getDate( )
  {
    return this.date;
  }
  
  /**
   * Changes the date of this warning.
   * @param date The new warning date.
   */
  public void setDate( final DateTime date )
  {
    if ( date != null )
    {
      this.date = date;
    }
  }
  
  /**
   * Checks if this warning has a date value.
   * @return True, if it has.
   */
  public boolean hasDate( )
  {
    return this.date != null;
  }
  
  /**
   * Returns an string representation of this warning, according
   * to the rfc format.
   * @return An string representation of this warning.
   * @see java.lang.Object#toString()
   */
  public String toString( )
  {
    final StringBuilder sb = new StringBuilder( );
    sb.append( this.code );
    sb.append( " " );
    sb.append( this.agent );
    sb.append( " " );
    sb.append( this.text );
    
    if ( this.hasDate( ) )
    {
      sb.append( " " );
      sb.append( this.date.toString( JellyfishConstants.HTTP_FORMATTER ) );
    }
    
    return sb.toString( );
  }
  
  /**
   * Checks if the given warning is identical to this one.
   * @param warning The warning to check for.
   * @return True, if they are.
   */
  public boolean equals( final WarningValue warning )
  {
    return this.code == warning.getCode( );
  }
  
  /**
   * Checks if the given object is identical to this one.
   * @param obj The object to check for.
   * @return True, if they are.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals( final Object obj )
  {
    return obj instanceof WarningValue && this.equals( ( WarningValue ) obj );
  }
  
  /**
   * Returns an hash code for this object.
   * @return An hash code for this warning.
   * @see java.lang.Object#hashCode()
   */
  public int hashCode()
  {
    return Integer.toString( this.code ).hashCode( );
  }
  
}
