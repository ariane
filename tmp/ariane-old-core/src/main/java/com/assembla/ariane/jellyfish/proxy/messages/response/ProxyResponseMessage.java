/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages.response;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyMessage;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.WarningValue;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

/**
 * This class represents an response message as used internally by the proxy.
 * @author Fat Cat
 * @version 4
 * @since 0.0.1
 */
public class ProxyResponseMessage extends ProxyMessage implements Serializable, GenericResponse
{
  
  /** The version of this response message. */
  private static final long serialVersionUID = 2438772686246087671L;
  
  private ProxyResponseCode responseCode;
  
  private List< WarningValue > warnings;
  
  /**
   * Creates a new proxy response message.
   * @param requestId The identifier of the request.
   */
  public ProxyResponseMessage( final UUID requestId )
  {
    super( requestId );
    this.responseCode = null;
    this.warnings = new ArrayList<>(
        JellyfishConstants.DEFAULT_ARRAY_LIST_SIZE );
  }
  
  /**
   * Changes the message id of this response.
   * @param messageId The new message id.
   */
  public void setMessageId( final UUID messageId )
  {
    if( messageId != null )
    {
      this.messageUUID = messageId;
    }
  }
  
  /**
   * Returns the response code of this response.
   * @return The response code.
   */
  public ProxyResponseCode getNativeResponseCode( )
  {
    return this.responseCode;
  }
  
  /**
   * Returns the response code as a string.
   * @return The response code.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#getResponseCode()
   */
  @Override( )
  public String getResponseCode( )
  {
    return this.responseCode.toString( );
  }
  
  /**
   * Changes the response code.
   * @param responseCode The new response code.
   */
  public void setResponseCode( final ProxyResponseCode responseCode )
  {
    if ( responseCode != null )
    {
      this.responseCode = responseCode;
    }
  }
  
  /**
   * Returns the response code as a integer.
   * @return The response code value.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#getResponseCodeAsInteger()
   */
  @Override( )
  public int getResponseCodeAsInteger( )
  {
    return this.responseCode.getResponseCode( );
  }

  /**
   * Changes the response code.
   * @param code The response code value.
   * @param message The response code message.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#setResponseCode(int, java.lang.String)
   */
  @Override( )
  public void setResponseCode( final int code, final String message )
  {
    this.responseCode = ProxyResponseCode.create( code, message );
  }
  
  // Response time
  
  /**
   * Returns the response time.
   * @return The date and time of when the response was received.
   */
  public DateTime getResponseTime( )
  {
    if ( this.extra.containsKey( "ResponseTime" ) )
    {
      try
      {
        return DateTime.parse( this.extra.get( "ResponseTime" ) );
      }
      catch( final IllegalArgumentException ex )
      {
        return DateTime.now( );
      }
    }
    else
    {
      return DateTime.now( );
    }
  }
  
  /**
   * Changes the response time.
   * @param datetime The new response time.
   */
  public void setResponseTime( final DateTime datetime )
  {
    if ( datetime != null )
    {
      this.extra.put( "ResponseTime", datetime.toString( ) );
    }
  }
  
  /**
   * Checks if this response has an ResponseTime value.
   * @return True, if it has.
   */
  public boolean hasResponseTime( )
  {
    return this.extra.containsKey( "ResponseTime" );
  }
  
  /**
   * Returns the corrected received age as determined by the http standard.
   * @return The corrected received age
   */
  public long getCorrectedReceivedAge( )
  {
    if ( !this.extra.containsKey( "CorrectedReceivedAge" ) )
    {
      this.setCorrectedReceivedAge( );
    }
    try
    {
      return Long.parseLong( this.extra.get( "ResponseTime" ) );
    }
    catch( final NumberFormatException ex )
    {
      return 0;
    }
  }
  
  /**
   * Sets the corrected received age.
   */
  public void setCorrectedReceivedAge( )
  {
    long sec_val = 0;
    long third_val = 0;
    
    if ( this.hasResponseTime( ) && this.containsHeader( "Date" ) )
    {
      try
      {
        final DateTime responsetime = this.getResponseTime( );
        final DateTime dateValue = DateTime.parse( this.getHeader( "Date" ), 
            JellyfishConstants.HTTP_FORMATTER );
        
        if ( responsetime.isAfter( dateValue ) )
        {
          sec_val = new Period( dateValue, responsetime ).normalizedStandard( 
              PeriodType.seconds( ) ).getSeconds( );
        }
      }
      catch( final IllegalArgumentException ex )
      {
        sec_val = 0;
      }
    }
    
    if ( this.containsHeader( "age" ) )
    {
      try
      {
        third_val = Long.parseLong( this.getHeader( "age" ) );
      }
      catch( final NumberFormatException ex )
      {
        third_val = 0;
      }
    }
    
    final long corrected_age = Math.max( Math.max( 0, sec_val ), third_val );
    
    if ( this.containsExtra( "CorrectedReceivedAge" ) )
    {
      this.removeExtra( "CorrectedReceivedAge" );
    }
    
    this.addExtra( "CorrectedReceivedAge", Long.toString( corrected_age ) );
  }
  
  // Warning
  
  /**
   * Checks if the given warning is present.
   * @param warning The warning to check for.
   * @return True, if it is.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#containsWarning(com.assembla.ariane.jellyfish.proxy.messages.WarningValue)
   */
  @Override( )
  public boolean containsWarning( final WarningValue warning )
  {
    return this.warnings.contains( warning );
  }
  
  /**
   * Adds the given warning to the list.
   * @param warning The warning to add.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#addWarning(com.assembla.ariane.jellyfish.proxy.messages.WarningValue)
   */
  @Override( )
  public void addWarning( final WarningValue warning )
  {
    if ( !this.containsWarning( warning ) )
    {
      this.warnings.add( warning );
    }
  }
  
  /**
   * Replaces the existing warnings with the new ones.
   * @param warnings The new substitute warnings.
   */
  public void replaceWarnings( final List<WarningValue> warnings )
  {
    this.warnings.clear( );
    this.warnings.addAll( warnings );
  }
  
  /**
   * Removes the given warning.
   * @param warning The warning to remove.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#removeWarning(com.assembla.ariane.jellyfish.proxy.messages.WarningValue)
   */
  @Override( )
  public void removeWarning( final WarningValue warning )
  {
    if ( this.containsWarning( warning ) )
    {
      this.warnings.remove( warning );
    }
  }
  
  /**
   * Returns an list of warnings.
   * @return An read-only list of present warnings.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#getWarnings()
   */
  @Override( )
  public List< WarningValue > getWarnings( )
  {
    return Collections.unmodifiableList( this.warnings );
  }
  
  /**
   * Returns the number of warnings.
   * @return The number of warning.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#getNumberOfWarnings()
   */
  @Override( )
  public int getNumberOfWarnings( )
  {
    return this.warnings.size( );
  }
  
  /**
   * Checks if the warning list is empty.
   * @return True, if it is.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#hasAnyWarning()
   */
  @Override( )
  public boolean hasAnyWarning( )
  {
    return this.warnings.isEmpty( );
  }
  
  /**
   * Empties the warning list.
   * @see com.assembla.ariane.jellyfish.proxy.messages.response.GenericResponse#clearWarnings()
   */
  @Override( )
  public void clearWarnings( )
  {
    this.warnings.clear( );
  }
  
  /**
   * Clears all temporary warnings.<br/>
   * These are warnings with code 1xx.
   */
  public void clearTemporaryWarnings( )
  {
    final Iterator< WarningValue > it = this.warnings.iterator( );
    
    while( it.hasNext( ) )
    {
      final int code = it.next( ).getCode( );
      if ( ( code >= 100 ) && ( code < 200 ) )
      {
        it.remove( );
      }
    }
  }
  
  // Utilities
  
  /**
   * Clones this proxy response message.
   * @return A new identical proxy response message.
   */
  public ProxyResponseMessage copy( )
  {
    try
    //main clone method uses serialization
    {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream( );
      final ObjectOutputStream out = new ObjectOutputStream( baos );
      out.writeObject( this );
      
      final ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray( ) );
      final ObjectInputStream ois = new ObjectInputStream( bais );
      return ( ProxyResponseMessage ) ois.readObject( );
    }
    catch( final IOException | ClassNotFoundException e ) //An alternate clone method, just in case...
    {
      final ProxyResponseMessage response = new ProxyResponseMessage( this.messageUUID );
      
      response.setContent( this.getContent( ) );
      response.setDockName( this.getDockName( ) );
      
      response.setProtocol( ProxyProtocol.create( 
          this.proxyProtocol.getMajor( ), 
          this.proxyProtocol.getMinor( ), 
          this.proxyProtocol.getProtocolName( ) ) );
      
      response.setResponseTime( DateTime.parse( 
          this.getResponseTime( ).toString( ) ) );
      
      response.setResponseCode( ProxyResponseCode.create( 
          this.responseCode.getResponseCode( ), 
          this.responseCode.getMessage( ), 
          this.responseCode.getException( ) ) );
      
      //Add the headers
      for ( String header : this.getHeaderNames( ) )
      {
        for ( String value : this.getHeaders( header ) )
        {
          response.addHeader( header, value );
        }
      }
      
      //Add the extra
      for ( String extraname : this.getExtraValueNames( ) )
      {
        response.addExtra( extraname, this.getExtra( extraname ) );
      }
      
      //Add the ranges
      for ( Pair< Integer, Integer > range : this.getRanges( ) )
      {
        response.addRange( range );
      }
      
      //Add the warnings
      for( WarningValue warn : this.warnings )
      {
        if( warn.getDate( ) == null )
        {
          response.addWarning( 
              WarningValue.create( 
                  warn.getCode( ), warn.getAgent( ), warn.getText( ) ) );
        }
        else
        {
          response.addWarning( 
              WarningValue.create( warn.getCode( ), 
                  warn.getAgent( ), warn.getText( ), warn.getDate( ) ) );
        }
      }
      
      return response;
    }
  }
  
  /**
   * Removes all hop-to-hop headers.
   */
  public void removeHopToHopHeaders( )
  {
    for ( String headerName : this.getHopHeaders( ) )
    {
      this.removeHeader( headerName );
    }
  }
  
}
