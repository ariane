/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.standard;

import java.net.MalformedURLException;
import java.net.URL;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.RequestType;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * An proxy no-cache action/processor that implements an fully transparent proxy without
 * touching or caching the request.<br/>
 * This is used in case of requests other than get or head that aren't conditional.
 * @author Fat Cat
 * @since 0.0.6
 * @version 1
 */
public class NoCacheProxyAction extends ProxyAction
{
  
  /**
   * Creates a new no-cache proxy.
   * @param successor The next proxy action processor.
   * @param config The configuration to use for this no cache proxy.
   */
  public NoCacheProxyAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( NoCacheProxyAction.class );
  }
  
  /**
   * Checks if this processor can handle the given request.<br/>
   * This processor returns true, if:
   * <ol>
   *  <li>The request is not conditional</li>
   *  <li>The request is not an range request</li>
   *  <li>The request is not cacheable and/or its method is different from get or head</li>
   * </ol> 
   * @param aRequest The request to check for.
   * @return True, if the conditions above are satisfied.
   * @throws IllegalArgumentException Never thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return ( !RequestType.isCacheable( aRequest ) || !RequestType.isGetOrHeadRequest( aRequest ) ) &&
           !RequestType.isConditionalRequest( aRequest ) && !RequestType.isRangeRequest( aRequest );
  }
  
  /**
   * This method handles a request, by asking the original server, for
   * a response and directly send it to the client AKA a fully transparent proxy.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    try
    {
      final Object[ ] args = { aRequest.getCurrentUUID( ), aRequest.getRequest( ).getUri( ), aRequest.getState( ).toString( ) };
      
      this.logger.info( "Received an request:\nID: {}\nURI: {}\nState: {}", args );
      
      //Check if this is the first round...
      if ( aRequest.getState( ).equals( ProxyRequestState.PROCESSING ) )
      {
        this.handleMoreInfoRequest( aRequest );
      }
      else if ( aRequest.getState( ).equals( ProxyRequestState.MORE_INFO_RECEIVED ) )
      {
        this.handleMoreInfoReceivedRequest( aRequest );
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Error in the transparent proxy action.", ex );
    }
  }
  
  /**
   * Method called whenever a response is received from a server.
   * @param aRequest The request with the server answer.
   */
  private void handleMoreInfoReceivedRequest( final ProxyPageElement aRequest )
  {
    this.logger.info( "Sending request {} to server.", aRequest.getRequest( ).getMessageId( ) );
    
    //Place the received response as this request response and send it... 
    final ProxyResponseMessage response = aRequest.getTransitoryResponse( );
    
    aRequest.setResponse( response );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    this.logger.debug( "Request {} dock is {}",  aRequest.getRequest( ).getMessageId( ), aRequest.getRequest( ).getDockName( ) );
    aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
    aRequest.getResponse( ).setMessageId( aRequest.getRequest( ).getMessageId( ) );
    
    aRequest.setState( ProxyRequestState.PROCESSED );
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Method called whenever the request is new.
   * @param aRequest The request needing more information from the server.
   */
  private void handleMoreInfoRequest( final ProxyPageElement aRequest )
  {
    this.logger.info( "Preparing request {} to be sent to the server", aRequest.getRequest( ).getMessageId( ) );
        
    //If it is place the request as a server request and ask for more info...
    final ProxyRequestMessage request = aRequest.getRequest( );
    
    URL host;
    int port = JellyfishConstants.DEFAULT_HTTP_PORT;
    try
    {
      host = new URL( request.getCompleteUri( ) );
      port = host.getPort( );
      if ( port == -1 )
      {
        port = JellyfishConstants.DEFAULT_HTTP_PORT;
      }
    }
    catch( final MalformedURLException e )
    {
      //Do nothing
    }
    
    this.logger.debug( "Server port for request {} is {}", aRequest.getRequest( ).getMessageId( ), port );
    
    //Notice that since the server port can be different than the standard 80,
    //we will extract the port in order to use it in the HTTPServerSender channel.
    request.addExtra( "server-port", Integer.toString( port ) );
    aRequest.setTransitoryData( request );
    
    aRequest.setState( ProxyRequestState.MORE_INFO );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} was sent to server", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Returns the id of this processor.
   * @return The processor id that is "No-Cache Proxy Action".
   */
  @Override( )
  public String getActionID( )
  {
    return "No-Cache Proxy Action";
  }
}
