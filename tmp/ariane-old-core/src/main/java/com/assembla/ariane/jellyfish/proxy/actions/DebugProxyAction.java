/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions;

import java.io.File;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * An proxy action/processor that serializes the request and sends back an
 * empty response.
 * Currently this request is decommissioned, and does nothing...
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 * @deprecated
 */
public class DebugProxyAction extends ProxyAction
{
  
  /**
   * Creates a new debug proxy action.
   * @param successor The next proxy action processor.
   * @param config The processor configuration.
   */
  public DebugProxyAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( DebugProxyAction.class );
  }
  
  /**
   * Checks if this processor can handle the given request.<br/>
   * This always returns false.
   * @param aRequest The request to check.
   * @return Always, false.
   * @throws IllegalArgumentException Never thrown
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return false;
  }
  
  /**
   * This method handles a request, that in the case of this class is
   * saving a serialized version of the request in page_save_dir/requestId.xml.<br/>
   * And then, send an empty response, with error code 200.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    this.logger.info( "Received an request with id {}", aRequest.getCurrentUUID( ) );
  }
  
  /**
   * Returns the id of this processor.
   * @return The processor id, that is "Debug Proxy Action".
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#getActionID()
   */
  @Override( )
  public String getActionID( )
  {
    return "Debug Proxy Action";
  }
  
}
