/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.client;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPipelineFactory;
import io.netty.channel.Channels;
import io.netty.channel.socket.nio.NioServerSocketChannelFactory;
import io.netty.handler.codec.http.HttpChunkAggregator;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageQueue;
import com.assembla.ariane.jellyfish.util.collections.client.HTTPChannelHolder;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.LoggerFactory;

/**
 * This class receives the http requests from a client.
 * @author Fat Cat
 * @since 0.0.1
 * @version 3
 */
public class HTTPClientReceptor extends AbstractModule implements ChannelPipelineFactory, Runnable
{
  
  /** The current port. */
  private int currentPort;
  
  /** The bootstrap. */
  private ServerBootstrap bootstrap;
  
  /** The dock name where this receptor belongs. */
  private String dockName;
  
  /**
   * Creates a new http receptor.
   * @param config The configuration to use.
   * @param dockName The name of the dock.
   */
  public HTTPClientReceptor( final HierarchicalConfiguration config, final String dockName )
  {
    super( config );
    this.logger = LoggerFactory.getLogger( HTTPClientReceptor.class );
    this.dockName = dockName;
    this.currentPort = config.getInt( "port-number", 0 );
    this.logger.info( "The receptor port is {}", this.currentPort );
  }
  
  /**
   * Configures a new channel pipeline.
   * @return An channel pipeline for this receptor.
   * @throws Exception If something happens.
   * @see io.netty.channel.ChannelPipelineFactory#getPipeline()
   */
  @Override()
  public ChannelPipeline getPipeline( ) throws Exception
  {
    final ChannelPipeline pipeline = Channels.pipeline( );
    
    pipeline.addLast( "decoder", new HttpRequestDecoder( ) );
    pipeline.addLast( "aggregator", new HttpChunkAggregator( this.config.getInt( "max-message-size", JellyfishConstants.DEFAULT_MAX_MESSAGE_SIZE ) ) );
    pipeline.addLast( "encoder", new HttpResponseEncoder( ) );
    pipeline.addLast( "compressor", new HttpContentCompressor( ) );
    pipeline.addLast( "jelly", new HTTPClientChannelHandler( this.dockName ) );
    
    return pipeline;
  }
  
  /**
   * Starts the http client receptor.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override()
  public synchronized void run( )
  {
    this.logger.info( "Starting the http client receptor." );
    this.setRunning( true );
    this.bootstrap = new ServerBootstrap( new NioServerSocketChannelFactory( Executors.newCachedThreadPool( ), Executors.newCachedThreadPool( ) ) );
      
    this.bootstrap.setPipelineFactory( this );
    final Channel serverChannel = this.bootstrap.bind( new InetSocketAddress( this.currentPort ) );
    this.logger.info( "We are listing to stuff..." );
    HTTPChannelHolder.instance( ).getAllChannels( ).add( serverChannel );    
  }
  
  /**
   * Stops the http client receptor.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override()
  public synchronized void stop( )
  {
    this.logger.info( "We are stopping the http client receptor..." );
    this.setRunning( false );
    HTTPChannelHolder.instance( ).clear( );
    HTTPChannelHolder.instance( ).getAllChannels().close();
    ClientMessageQueue.instance( ).getRequests().clear();
    if( this.bootstrap != null )
    {
      this.bootstrap.releaseExternalResources( );
    }
  }
}
