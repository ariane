/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.exceptions;

/**
 * This class represents an exception in a method argument.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public class ArgumentException extends Exception
{
  
  /** The class version. */
  private static final long serialVersionUID = 742469394949327765L;

  /**
   * Creates a new argument exception.
   */
  public ArgumentException( )
  {
    super( " There was an argument exception. " );
  }
  
  /**
   * Creates a new argument exception.
   * @param message The argument exception message.
   */
  public ArgumentException( final String message )
  {
    super( message );
  }
  
  /**
   * Creates a new argument exception. 
   * @param ex The inner exception.
   */
  public ArgumentException( final Throwable ex )
  {
    super( " There was an argument exception. ", ex );
  }
  
  /**
   * Creates a new argument exception. 
   * @param message The exception message.
   * @param ex The inner exception.
   */
  public ArgumentException( final String message, final Throwable ex )
  {
    super( message, ex );
  }
  
}
