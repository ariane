/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.server;

import com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.slf4j.LoggerFactory;

/**
 * This class implements an queue of server requests and responses.
 * @author Fat Cat
 * @version 2
 * @since 0.0.2
 */
public final class ServerMessageQueue extends JellyfishQueue< ProxyRequestMessage, ProxyResponseMessage >
{
  
  /** The singleton. */
  private static final ServerMessageQueue inst = new ServerMessageQueue( );
  
  /**
   * Method to return this singleton.
   * @return The singleton.
   */
  public static ServerMessageQueue instance( )
  {
    return ServerMessageQueue.inst;
  }
  
  /**
   * Private constructor to create a new queue.
   */
  private ServerMessageQueue( )
  {
    super( );
    this.logger = LoggerFactory.getLogger( ServerMessageQueue.class );
  }
  
  /**
   * Returns the size of the request queue. (1024)
   * @return The size of the request queue (1024 for now)
   * @see com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue#sendSize()
   */
  protected int sendSize( )
  {
    return JellyfishConstants.DEFAULT_QUEUE_SIZE;
  }
  
  /**
   * Returns the size of the response queue. (1024)
   * @return The size of the request queue (1024 for now) 
   * @see com.assembla.ariane.jellyfish.util.collections.queue.JellyfishQueue#receiveSize()
   */
  protected int receiveSize( )
  {
    return JellyfishConstants.DEFAULT_QUEUE_SIZE;
  }
  
}
