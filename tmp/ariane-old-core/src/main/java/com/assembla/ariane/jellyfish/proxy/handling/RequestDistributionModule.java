/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.handling;

import java.util.concurrent.TimeUnit;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * Helper class to run the consistency modules in other thread.
 * @author Fat Cat
 * @since 0.0.1
 * @version 3
 */
public class RequestDistributionModule extends AbstractModule
{
  
  /** The current start handler. */
  private IHandler< ProxyPageElement > startingHandler;
  
  /**
   * Constructor to create a request processing module.
   * @param config The configuration object
   */
  public RequestDistributionModule( final HierarchicalConfiguration config )
  {
    super( config );
    this.logger = LoggerFactory.getLogger( RequestDistributionModule.class );
  }
  
  /**
   * Stops the request distributor.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override( )
  public synchronized void stop( )
  {
    this.logger.info( "Trying to stop the request distributor." );
    this.setRunning( false );
  }
  
  /**
   * Runs this request distributor.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override( )
  public void run( )
  {
    try
    {
      ProxyRequestMessage request = null;
      ProxyPageElement element = null;
      this.setRunning( true );
      this.logger.info( "Request distributor is running!" );
      while( true )
      {
        request = null;
        
        if ( !this.isRunning( ) )
        {
          this.logger.info( "Request distributor is stopped" );
          break;
        }
        
        //Check if the processing request list has anything...
        this.logger.debug( "Checking client requests..." );
        try
        {
          while( request == null )
          {  
            request = ClientMessageQueue.instance( ).getRequests( ).poll( 
                JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS );
            if ( !this.isRunning( ) )
            {
              this.logger.info( "Request distributor is stopped" );
              break;
            }
          }
        }
        catch( final InterruptedException e )
        {
        }
        
        if ( request != null )
        {
          this.logger.info( "There is a new request with id {}", request.getMessageId( ) );
          element = new ProxyPageElement( );
          element.setRequest( request );
          element.setState( ProxyRequestState.PROCESSING );
          
          if ( !this.isRunning( ) )
          {
            this.logger.info( "Request distributor is stopped" );
            break;
          }
          
          //start the default handler...
          if ( this.startingHandler != null )
          {
            this.startingHandler.start( element ); //this will exit when it is processed...
          }
          else
          {
            this.logger.warn( "Sorry no starting handler..." );
            throw new NullPointerException( "Sorry no starting handler..." );
          }
        }
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Exception in RequestDistributorModule", ex );
    }
  }
  
  /**
   * Returns the starting handler.
   * @return The starting hander.
   */
  public IHandler< ProxyPageElement > getStartingHandler( )
  {
    return this.startingHandler;
  }
  
  /**
   * Changes the starting handler.
   * @param handler The new starting handler.
   */
  public void setStartingHandler( final IHandler< ProxyPageElement > handler )
  {
    if ( handler != null )
    {
      this.startingHandler = handler;
    }
  }
}
