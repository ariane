/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.docks;

import org.apache.commons.configuration.HierarchicalConfiguration;

/**
 * This class represents an empty dock.
 * @author Fat Cat
 * @since 0.0.1
 * @version 1
 */
public class NullDockImpl extends Dock
{
  
  /**
   * Creates a new null dock.
   * @param config The configuration to use.
   */
  public NullDockImpl( HierarchicalConfiguration config )
  {
    super( null );
    this.name = "NULL";
  }
  
  /**
   * Starts the null dock (does nothing).
   * @param startClient Whether or not to start the client.
   * @param startServer Whether or not to start the server.
   * @throws Exception If some error happens. 
   */
  @Override()
  public void start( final boolean startClient, final boolean startServer ) throws Exception 
  { 
    if( startClient )
    {
      this.isClientStarted = true;
    }
    if( startServer )
    {
      this.isServerStarted = true;
    }    
  }

  /**
   * Stops the null dock (does nothing).
   * @param stopClient Whether or not to stop the client.
   * @param stopServer Whether or not to stop the server.
   * @throws Exception If some error happens.   
   * @see com.assembla.ariane.jellyfish.connection.docks.Dock#stop(boolean, boolean)
   */
  @Override()
  public void stop( final boolean stopClient, final boolean stopServer ) throws Exception 
  { 
    if( stopClient )
    {
      this.isClientStarted = false;
    }
    if( stopServer )
    {
      this.isServerStarted = false;
    }
  }
  
}
