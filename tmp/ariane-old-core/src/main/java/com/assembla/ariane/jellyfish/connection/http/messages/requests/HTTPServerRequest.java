/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.messages.requests;

import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpVersion;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import org.slf4j.LoggerFactory;

/**
 * This class represents an http server request.
 * @author Fat Cat
 * @version 5
 * @since 0.0.2
 */
public class HTTPServerRequest extends HTTPGenericRequest
{
  
  /**
   * Converts an http request to a generic proxy request.
   * @param http_request The http request to convert.
   * @return An corresponding generic proxy request.
   */
  public static ProxyRequestMessage messageConverter( HTTPServerRequest http_request )
  {
    ProxyRequestMessage request = new ProxyRequestMessage( http_request.getMessageId( ) );
    
    request.setMethod( http_request.getMethod() );
    request.setProtocol( ProxyProtocol.create(
      http_request.getNativeProtocolVersion().getMajorVersion(),
      http_request.getNativeProtocolVersion().getMinorVersion(),
      http_request.getNativeProtocolVersion().getProtocolName() ) );
    
    request.setContent( http_request.getContent() );
    request.setDockName( http_request.getDockName() );
    
    //Complete URI
    StringBuffer sb = new StringBuffer( );
    if ( !http_request.getUri( ).contains( http_request.getHeader( "Host" ) ) )
    {      
      sb.append( http_request.getHeader( "Host" ) );
    }
    sb.append( http_request.getUri( ) );
    
    request.setCompleteUri( sb.toString( ) );
    
    //Relative URI
    sb = new StringBuffer( );
    
    String req = http_request.getUri( );
    if ( http_request.getUri( ).contains( http_request.getHeader( "Host" ) ) )
    {
      if ( !http_request.getHeader( "Host" ).startsWith( "http://" ) )
      {
        sb.append( "http://" );
      }
      sb.append( http_request.getHeader( "Host" ) );
      req = http_request.getUri( ).replaceFirst( sb.toString( ), "" );
    }
    
    request.setUri( req );
    
    //Keep-alive    
    if ( http_request.isKeepAlive( ) )
    {
      request.setKeepAlive( );
    }
    
    //Headers
    for ( String headerName : http_request.getHeaderNames( ) )
    {
      request.addHeader( headerName, http_request.getHeader( headerName ) );
    }
    
    //Extra values
    for ( String extraName : http_request.getExtraValueNames( ) )
    {
      request.addExtra( extraName, http_request.getExtra( extraName ) );
    }
    
    //Ranges
    for ( Pair< Integer, Integer > pair : http_request.getRanges( ) )
    {
      request.addRange( pair );
    }

    //Hop Headers
    for ( String hopHeader : http_request.getHopHeaders( )  )
    {
      request.addHopHeader( hopHeader );
    }
    
    return request;
  }
  
  /**
   * Converts an generic proxy request to a http request.
   * @param proxyreq The generic proxy request.
   * @return The corresponding http request.
   */
  public static HTTPServerRequest messageConverter( final ProxyRequestMessage proxyreq )
  {
    final HTTPServerRequest request = new HTTPServerRequest( proxyreq.getMessageId( ) );
    
    request.setContent( proxyreq.getContent( ) );
    request.setDockName( proxyreq.getDockName( ) );
    request.setProtocolVersion( HttpVersion.valueOf( proxyreq.getProtocolVersion( ) ) );
    request.setMethod( HttpMethod.valueOf( proxyreq.getMethod( ) ) );
    request.setUri( proxyreq.getUri( ) );
    
    if ( proxyreq.isKeepAlive( ) )
    {
      request.setKeepAlive( );
    }
    
    //extra values
    for ( String extraName : proxyreq.getExtraValueNames( ) )
    {
      request.addExtra( extraName, proxyreq.getExtra( extraName ) );
    }
    
    //header names
    for ( String headerName : proxyreq.getHeaderNames( ) )
    {
      request.addHeader( headerName, proxyreq.getHeader( headerName ) );
    }
            
    //ranges
    for ( Pair< Integer, Integer > pair : proxyreq.getRanges() )
    {
      request.addRange( pair );
    }
    
    //hop headers
    for ( String hopHeader : proxyreq.getHopHeaders( ) )
    {
      request.addHopHeader( hopHeader );
    }
        
    // We have to relativise the uri, from the request, since
    // unlike what the http 1.1 rfc says, most servers do not know
    // how to handle absolute url's...
    // Whatever you do DO NOT DELETE THIS PIECE OF CODE, UNLESS:
    // * You test it very carefully, and it works with everything
    //   (google, wikipedia, random blogs)
    String req = proxyreq.getUri( );
    if ( proxyreq.getUri( ).contains( proxyreq.getHeader( "Host" ) ) )
    {
      final StringBuilder sb = new StringBuilder( );
      if ( !proxyreq.getHeader( "Host" ).startsWith( "http://" ) )
      {
        sb.append( "http://" );
      }
      sb.append( proxyreq.getHeader( "Host" ) );
      req = proxyreq.getUri( ).replaceFirst( sb.toString( ), "" );
    }
    request.getNativeRequest( ).setUri( req );
    
    return request;
  }
  
  /**
   * Adds the remaining headers to the request. 
   * @param httpServerRequest The request to add the headers.
   * @return The request with all the headers.
   */
  public static HTTPServerRequest addRemainingHeaders( final HTTPServerRequest httpServerRequest )
  {
    if( httpServerRequest.hasRanges( ) )
    { 
      final StringBuilder sb = new StringBuilder( );
      sb.append( "bytes=" );
      for ( Pair< Integer, Integer > pair : httpServerRequest.getRanges( ) )
      {
        if ( ( pair.getFirst( ) == -1 ) && ( pair.getSecond( ) != -1 ) ) // max only
        {
          sb.append( String.format( "-%d", pair.getSecond( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) == -1 ) ) // min only
        {
          sb.append( String.format( "%d-", pair.getFirst( ) ) );
        }
        else if ( ( pair.getFirst( ) != -1 ) && ( pair.getSecond( ) != -1 ) ) // both
        {
          sb.append( String.format( "%d-%d", pair.getFirst( ), pair.getSecond( ) ) );
        }
      }
      httpServerRequest.addHeader( "Range", sb.toString( ) );    
    }    
    return httpServerRequest;
  }
  
  /**
   * Creates a new empty server request, given the request id.
   * @param messageID The request id.
   */
  public HTTPServerRequest( final UUID messageID )
  {
    super( messageID );
    this.logger = LoggerFactory.getLogger( HTTPServerRequest.class );
  }
  
  // Id
  
  /**
   * Changes the request id of this request.
   * @param requestId The new request id.
   */
  public void setRequestId( final UUID requestId )
  {
    if ( ( requestId != null ) && ( !this.messageId.equals( requestId ) ) )
    {
      this.messageId = requestId;
    }
  }
  
  // Regular
  
  /**
   * Checks if these two objects are equal.
   * @param obj The object to compare to.
   * @return True, if they are.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  {
    return obj instanceof HTTPServerRequest &&
      this.messageId.equals( ( ( HTTPServerRequest ) obj ).getMessageId() );
  }
  
  /**
   * Returns an hash code for this request.
   * @return the hash of this server request.
   * @see java.lang.Object#hashCode()
   */
  public int hashCode( )
  {
    return this.messageId.hashCode( );
  }
  
  /**
   * Returns the inner http request.
   * @return The http request.
   */
  public HttpRequest getNativeRequest( )
  {
    return this.request;
  }
  
  //Utilities
  
  /**
   * Clones this http client response message.
   * @return A new identical http client response message.
   */
  public HTTPServerRequest copy( )
  {
    try
    //main clone method uses serialization
    {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream( );
      final ObjectOutputStream out = new ObjectOutputStream( baos );
      out.writeObject( this );
      
      final ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray( ) );
      final ObjectInputStream ois = new ObjectInputStream( bais );
      return ( HTTPServerRequest ) ois.readObject( );
    }
    catch( final IOException | ClassNotFoundException e ) //An alternate clone method, just in case...
    {
      final HTTPServerRequest request = new HTTPServerRequest( this.messageId );
      
      request.setContent( this.getContent( ).copy( ) );
      request.setDockName( this.getDockName( ) );
      request.setProtocolVersion( HttpVersion.valueOf( this.getProtocolVersion() ) );
      request.setMethod( HttpMethod.valueOf( this.getMethod() ) );
      request.setUri( this.getUri( ) );
      
      if ( request.isKeepAlive( ) )
      {
        request.setKeepAlive( );
      }
      
      //Add the headers
      for ( String header : this.getHeaderNames( ) )
      {
        for ( String value : this.getNativeRequest( ).getHeaders( header ) )
        {
          request.getNativeRequest( ).addHeader( header, value );
        }
      }
      
      //Add the extra
      for ( String extra_name : this.getExtraValueNames( ) )
      {
        request.addExtra( extra_name, this.getExtra( extra_name ) );
      }
      
      //Add the ranges
      for ( Pair< Integer, Integer > range : this.getRanges( ) )
      {
        request.addRange( range );
      }
      
      //Add the hop headers
      for ( String hopHeader : this.getHopHeaders( ) )
      {
        request.addHopHeader( hopHeader );
      }
      
      return request;
    }
  }
  
}
