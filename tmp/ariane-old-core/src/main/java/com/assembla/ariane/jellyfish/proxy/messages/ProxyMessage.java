/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages;

import io.netty.buffer.ChannelBuffer;
import io.netty.buffer.ChannelBuffers;
import io.netty.handler.codec.base64.Base64;
import io.netty.util.CharsetUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import com.google.common.base.Strings;
import com.assembla.ariane.jellyfish.util.collections.Pair;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;

/**
 * This class represents an proxy message, that can contain headers, extra values,
 * and so on.
 * @author Fat Cat
 * @version 5
 * @since 0.0.1
 */
public abstract class ProxyMessage implements Serializable, GenericMessage
{
  
  /** The proxy message version.  */
  private static final long serialVersionUID = 5905965362614690706L;

  /** The message id. */
  protected UUID messageUUID;
  
  /** The header list. */
  protected HashMap< String, List< String >> headers;
  
  /** The extra field list. */
  protected HashMap< String, String > extra;
  
  /** The range list. */
  protected List< Pair< Integer, Integer >> range;
  
  /** The content. */
  protected String base64Content;
  
  /** The proxy protocol. */
  protected ProxyProtocol proxyProtocol;
  
  /** The hop header list. */
  protected SortedSet< String > hopHeaders;
  
  /**
   * Protected constructor that receives the current requestID.
   * @param requestID The current requestID.
   */
  protected ProxyMessage( final UUID requestID )
  {
    this.messageUUID = requestID;
    this.headers = new HashMap<>( );
    this.base64Content = "";
    this.extra = new HashMap<>( );
    
    this.range = new ArrayList<>(
        JellyfishConstants.DEFAULT_ARRAY_LIST_SIZE );
    
    this.hopHeaders = new TreeSet<>( );
  }
  
  //Dockname stuff
  
  /**
   * Sets the dockname associated with this message.
   * @param dockName The name of the dock.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#setDockName(java.lang.String)
   */
  @Override( )
  public void setDockName( final String dockName )
  {
    this.extra.put( "dock-name", dockName );
  }
  
  /**
   * Returns the dockname associated with this message.
   * @return The name of the associated dock.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getDockName()
   */
  @Override( )
  public String getDockName( )
  {
    if ( this.extra.containsKey( "dock-name" ) )
    {
      return this.extra.get( "dock-name" );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }
  
  /**
   * Checks if the named header exists or not.
   * @param name The header name.
   * @return true if the header exists, false otherwise.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#containsHeader(java.lang.String)
   */
  @Override( )
  public boolean containsHeader( final String name )
  {
    return this.headers.containsKey( name );
  }
  
  /**
   * Adds a new header to the header list.<br/>
   * If the header already exists, a new value is added.<br/>
   * If the value is null, then nothing is added.
   * @param name The header to add
   * @param value The value of the header.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addHeader(java.lang.String, java.lang.String)
   */
  @Override( )
  public void addHeader( final String name, final String value )
  {
    if ( value == null )
    {
      return;
    }
    
    if ( this.containsHeader( name ) )
    {
      this.headers.get( name ).add( value );
    }
    else
    {
      final List< String > values = new LinkedList<>( );
      values.add( value );
      this.headers.put( name, values );
    }
  }
  
  /**
   * Returns the first value the header with the given name.
   * @param name The name of the header to return.
   * @return The first value of the header, or empty string if the header is unknown.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getHeader(java.lang.String)
   */
  @Override( )
  public String getHeader( final String name )
  {
    if ( this.containsHeader( name ) )
    {
      return this.headers.get( name ).get( 0 );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }
  
  /**
   * Returns an set containing the names of all known headers.
   * @return An read-only set with the names of all headers.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getHeaderNames()
   */
  @Override( )
  public Set< String > getHeaderNames( )
  {
    return Collections.unmodifiableSet( this.headers.keySet( ) );
  }
  
  /**
   * Returns all the values of the given header in a read-only list. 
   * @param name The name of the header.
   * @return The values of the given header, of null if the header unknown.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getHeaders(java.lang.String)
   */
  @Override( )
  public List< String > getHeaders( final String name )
  {
    if ( this.containsHeader( name ) )
    {
      return Collections.unmodifiableList( this.headers.get( name ) );
    }
    else
    {
      return Collections.emptyList( );
    }
  }
  
  /**
   * Returns the value of the given header.
   * @param name The name of the header.
   * @param index The position of the header value (if multiple values exist)
   * @return The value of the header in the given index.
   */
  public String getHeader( final String name, final int index )
  {
    if ( this.containsHeader( name ) && ( this.headers.get( name ).size( ) > index ) && ( index >= 0 ) )
    {
      return this.headers.get( name ).get( index );
    }
    else
    {
      return "";
    }
  }
  
  /**
   * Returns the number of different values for the given header.
   * @param name The header name.
   * @return The number of values, or -1 is the header is unknown.
   */
  public int getHeaderValueCount( final String name )
  {
    if ( this.containsHeader( name ) )
    {
      return this.headers.get( name ).size( );
    }
    else
    {
      return -1;
    }
  }
  
  /**
   * Removes all the values of the given header, except the one in the
   * given index.
   * The size of the value list of the header after this method is always
   * 1, unless an add is issued.
   * @param name The name of the header.
   * @param index The index of the value to keep.
   */
  public void collapseHeader( final String name, final int index )
  {
    if ( this.containsHeader( name ) && ( this.headers.get( name ).size( ) > index ) && ( index >= 0 ) )
    {
      for ( int i = 0; i < this.headers.get( name ).size( ); i++ )
      {
        if ( i != index )
        {
          this.headers.get( name ).remove( i );
        }
      }
    }
  }
  
  /**
   * Returns the number of headers.
   * @return The header number.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getHeaderCount()
   */
  @Override( )
  public int getHeaderCount( )
  {
    return this.headers.size( );
  }

  /**
   * Changes the given header, to the given value.<br/>
   * If the value is null, then nothing is changed.
   * @param name The header name.
   * @param value The new header value. 
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#changeHeader(java.lang.String, java.lang.String)
   */
  @Override( )
  public void changeHeader( final String name, final String value )
  {
    if( value == null )
    {
      return;
    }
    
    if( this.containsHeader( name ) )
    {
      this.removeHeader( name );
      this.addHeader( name, Strings.nullToEmpty( value ) );
    }
  }

  /**
   * Removes the header with the given name.<br/>
   * If the header is null, than nothing is removed.
   * @param name The name of the header to remove.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#removeHeader(java.lang.String)
   */
  @Override( )
  public void removeHeader( final String name )
  {
    if ( this.containsHeader( name ) )
    {
      this.headers.remove( name );
    }
  }
  
  /**
   * Clears the existing headers.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#clearHeaders()
   */
  @Override( )
  public void clearHeaders( )
  {
    this.headers.clear( );
  }
  
  // Protocol content
  
  /**
   * Returns the current content as a channel buffer.
   * @return The current content.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getContent()
   */
  @Override( )
  public ChannelBuffer getContent( )
  {
    if( ( this.base64Content == null ) || this.base64Content.isEmpty( ) )
    {
      return ChannelBuffers.EMPTY_BUFFER;
    }
    else
    {
      final ChannelBuffer cb = ChannelBuffers.copiedBuffer( this.base64Content, CharsetUtil.US_ASCII );
      return Base64.decode( cb );
    }
  }
  
  /**
   * Returns the current content as a string.
   * @return The current content.
   */
  public String getContentString( )
  {
    if( this.base64Content == null )
    {
      return JellyfishConstants.EMPTY_STRING;
    }
    else
    {
      ChannelBuffer cb = ChannelBuffers.copiedBuffer( this.base64Content, CharsetUtil.US_ASCII );
      cb = Base64.decode( cb );
      return cb.toString( CharsetUtil.US_ASCII );
    }
  }
  
  /**
   * Sets the current content as a channel buffer.
   * @param content The new content.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#setContent(io.netty.buffer.ChannelBuffer)
   */
  @Override( )
  public void setContent( final ChannelBuffer content )
  {
    if( content == null )
    {
      this.base64Content = Base64.encode( ChannelBuffers.EMPTY_BUFFER ).toString( CharsetUtil.US_ASCII );
    }
    else
    {  
      this.base64Content = Base64.encode( content ).toString( CharsetUtil.US_ASCII );
    }
  }
  
  /**
   * Sets the current content given a string containing it. 
   * @param content The new content.
   */
  public void setContentString( final String content )
  {
    if( content == null )
    {
      this.setContent( ChannelBuffers.EMPTY_BUFFER );
    }
    else
    {
      this.setContent( ChannelBuffers.copiedBuffer( content, CharsetUtil.UTF_8 ) );
    }
  }
  
  /**
   * Returns the size of this content in bytes.
   * @return The size of this content in bytes.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getContentLength()
   */
  @Override( )
  public long getContentLength( )
  {
    return this.getContentString( ).length( );
  }
  
  // Extra values.

  /**
   * Checks if the given extra value exists.
   * @param name The extra value name.
   * @return True, if it exists.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#containsExtra(java.lang.String)
   */
  @Override( )
  public boolean containsExtra( final String name )
  {
    return this.extra.containsKey( name );
  }
  
  /**
   * Checks if the extra value list has values.
   * @return True, if it has.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#hasExtraValues()
   */
  @Override( )
  public boolean hasExtraValues( )
  {
    return !this.extra.isEmpty( );
  }
  
  /**
   * Adds the given extra value if it does not exist.
   * @param name The extra value name.
   * @param value The value of the extra value.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addExtra(java.lang.String, java.lang.String)
   */
  @Override( )
  public void addExtra( final String name, final String value )
  {
    if ( !this.containsExtra( name ) )
    {
      this.extra.put( name, value );
    }
  }
  
  /**
   * Returns the value of the given extra value.
   * @param name The name of the extra value.
   * @return The value of the extra value, or empty string if it is unknown.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getExtra(java.lang.String)
   */
  @Override( )
  public String getExtra( final String name )
  {
    if ( this.containsExtra( name ) )
    {
      return this.extra.get( name );
    }
    else
    {
      return JellyfishConstants.EMPTY_STRING;
    }
  }
  
  /**
   * Returns the names of the extra values.
   * @return An read-only set with the names of the extra values.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getExtraValueNames()
   */
  @Override( )
  public Set< String > getExtraValueNames( )
  {
    return Collections.unmodifiableSet( this.extra.keySet( ) );
  }
  
  /**
   * Clears all the extra values.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#clearExtra()
   */
  @Override( )
  public void clearExtra( )
  {
    this.extra.clear( );
  }
  
  /**
   * Removes the given extra value.
   * @param name The name of the extra value.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#removeExtra(java.lang.String)
   */
  @Override( )
  public void removeExtra( final String name )
  {
    if ( this.containsExtra( name ) )
    {
      this.extra.remove( name );
    }
  }
  
  /**
   * Changes the specified extra value.
   * @param name The name of the extra value to change.
   * @param value The new value of this extra value.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#changeExtra(java.lang.String, java.lang.String)
   */
  @Override( )
  public void changeExtra( final String name, final String value )
  {
    if( value == null )
    {
      return;
    }
    
    if( this.containsExtra( name ) )
    {
      this.removeExtra( name );
      this.addExtra( name, value );
    }
  }

  /**
   * Returns the number of extra values.
   * @return The number of extra values.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getExtraValueCount()
   */
  @Override( )
  public int getExtraValueCount( )
  {
    return this.extra.size( );
  }
  
  // Message Id
  
  /**
   * Returns the message id.
   * @return The message id.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getMessageId()
   */
  @Override( )
  public UUID getMessageId( )
  {
    return this.messageUUID;
  }
  
  /**
   * Returns the message id as a string.
   * @return The message id.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getMessageIdAsString()
   */
  @Override( )
  public String getMessageIdAsString( )
  {
    return this.messageUUID.toString( );
  }
  
  // Ranges

  /**
   * Adds a new range that does not have a lower bound.
   * @param max The upper bound of the range.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addRangeMax(int)
   */
  @Override( )
  public void addRangeMax( final int max )
  {
    if ( !this.containsRange( -1, max ) )
    {
      this.range.add( new Pair<>( -1, max ) );
    }
  }
  
  /**
   * Adds a new range that does not have a upper bound.
   * @param min The lower bound of the range.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addRange(int)
   */
  @Override( )
  public void addRange( final int min )
  {
    if ( !this.containsRange( min, -1 ) )
    {
      this.range.add( new Pair<>( min, -1 ) );
    }
  }
  
  /**
   * Adds a new range.
   * @param min The lower bound of the range.
   * @param max The upper bound of the range.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addRange(int, int)
   */
  @Override( )
  public void addRange( final int min, final int max )
  {
    if ( !this.containsRange( min, max ) )
    {
      this.range.add( new Pair<>( min, max ) );
    }
  }
  
  /**
   * Adds a new range.
   * @param range A pair representing the new range lower and upper bounds.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addRange(com.assembla.ariane.jellyfish.util.collections.Pair)
   */
  @Override( )
  public void addRange( final Pair< Integer, Integer > range )
  {
    if ( !this.containsRange( range ) )
    {
      this.range.add( range );
    }
  }
  
  /**
   * Checks if the given range is in the range list.
   * @param range The range to check.
   * @return True, if the range is in the list.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#containsRange(com.assembla.ariane.jellyfish.util.collections.Pair)
   */
  @Override( )
  public boolean containsRange( final Pair< Integer, Integer > range )
  {
    for ( final Pair< Integer, Integer > current_range : this.range )
    {
      if ( current_range.equals( range ) )
      { 
        return true; 
      }
    }
    return false;
  }
  
  /**
   * Checks if the given range is in the list.
   * @param min The lower bound of the range.
   * @param max The upper bound of the range.
   * @return True, if the range is in the list.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#containsRange(java.lang.Integer, java.lang.Integer)
   */
  @Override( )
  public boolean containsRange( final Integer min, final Integer max )
  {
    return this.containsRange( new Pair<>( min, max ) );
  }
  
  /**
   * Adds the given list to this range list.
   * @param ranges The list of ranges to add.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addAllRanges(java.util.Collection)
   */
  @Override( )
  public void addAllRanges( final Collection< Pair< Integer, Integer >> ranges )
  {
    for ( final Pair< Integer, Integer > range : ranges )
    {
      this.addRange( range );
    }
  }
  
  /**
   * Returns an unmodifiable list containing all ranges.
   * @return An range list.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getRanges()
   */
  @Override( )
  public List< Pair< Integer, Integer >> getRanges( )
  {
    return Collections.unmodifiableList( this.range );
  }
  
  /**
   * Clears the range list.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#clearRanges()
   */
  @Override( )
  public void clearRanges( )
  {
    this.range.clear( );
  }
  
  /**
   * Returns the number of ranges.
   * @return The range number.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getRangeNumber()
   */
  @Override( )
  public int getRangeNumber( )
  {
    return this.range.size( );
  }
  
  /**
   * Checks if there is any range, in this request.
   * @return True, if there is.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#hasRanges()
   */
  @Override( )
  public boolean hasRanges( )
  {
    return !this.range.isEmpty( );
  }
  
  // Protocol
  
  /**
   * Returns the current protocol information.
   * @return The current protocol.
   */
  public ProxyProtocol getProtocol( )
  {
    return this.proxyProtocol;
  }
  
  /**
   * Changes the protocol information.
   * @param proxyProtocol The new protocol object.
   */
  public void setProtocol( final ProxyProtocol proxyProtocol )
  {
    if ( proxyProtocol != null )
    {
      this.proxyProtocol = proxyProtocol;
    }
  }
  
  /**
   * Returns the protocol version as a string.
   * @return The protocol version.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getProtocolVersion()
   */
  @Override( )
  public String getProtocolVersion( )
  {
    return this.proxyProtocol.toString( );
  }

  /**
   * Changes the protocol version given a string.
   * @param version The new protocol version.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#setProtocolVersion(java.lang.String)
   */
  @Override( )
  public void setProtocolVersion( final String version )
  {
    this.proxyProtocol = ProxyProtocol.create( version );
  }
  
  // Hop Headers
  
  /**
   * Checks if the given hop header exists.
   * @param header The header name.
   * @return True, if it does.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#containsHopHeader(java.lang.String)
   */
  @Override( )  
  public boolean containsHopHeader( final String header )
  {
    return this.hopHeaders.contains( header );
  }
  
  /**
   * Adds the given hop header if the header does not exist.
   * @param header The header name.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#addHopHeader(java.lang.String)
   */
  @Override( )  
  public void addHopHeader( final String header )
  {
    if ( !this.containsHopHeader( header ) )
    {
      this.hopHeaders.add( header );
    }
  }
  
  /**
   * Returns an sorted set with the hop headers.
   * @return An read-only sorted set with the hop headers.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getHopHeaders()
   */
  @Override( )
  public SortedSet<String> getHopHeaders( )
  {
    return Collections.unmodifiableSortedSet( this.hopHeaders );
  }
  
  /**
   * Returns the number of hop headers.
   * @return The number of hop headers.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#getNumberOfHopHeaders()
   */
  @Override( )  
  public int getNumberOfHopHeaders( )
  {
    return this.hopHeaders.size( );
  }
  
  /**
   * Removes the given hop header.
   * @param header The hop header to remove.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#removeHopHeader(java.lang.String)
   */
  @Override( )  
  public void removeHopHeader( final String header )
  {
    if( this.containsHopHeader( header ) )
    {
      this.hopHeaders.remove( header );
    }
  }
  
  /**
   * Empties the hop header list.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#clearHopHeaders()
   */
  @Override( )  
  public void clearHopHeaders( )
  {
    this.hopHeaders.clear( );
  }
  
  /**
   * Checks if there are any hop headers.
   * @return True, if there are.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#hasHopHeaders()
   */
  @Override( )
  public boolean hasHopHeaders( )
  {
    return !this.hopHeaders.isEmpty( );
  }
  
  // Utilities
  
  /**
   * Merges the headers, of the given message.<br/>
   * Meaning that any headers similar to the existing ones are 
   * going to replace the ones in this request.
   * @param message The message to merge.
   */
  public void mergeHeaders( final ProxyMessage message )
  {
    for ( String name : message.getHeaderNames( ) )
    {
      if ( this.containsHeader( name ) )
      {
        this.removeHeader( name );
        
        for ( String value : message.getHeaders( name ) )
        {
          this.addHeader( name, value );
        }
      }
    }
  }

  // Keep-alive stuff
  
  /**
   * Changes this message so that it is a keep-alive message.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#setKeepAlive()
   */
  @Override( )
  public void setKeepAlive( )
  {
    if( this.containsExtra( "Keep-alive" ) )
    {
      this.removeExtra( "Keep-alive" );
    }
    this.addExtra( "Keep-alive", Boolean.toString( true ) );
  }

  /**
   * Changes this message so that it is NOT a keep-alive message.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#unsetKeepAlive()
   */
  @Override( )
  public void unsetKeepAlive( )
  {
    if( this.containsExtra( "Keep-alive" ) )
    {
      this.removeExtra( "Keep-alive" );
    }
    this.addExtra( "Keep-alive", Boolean.toString( false ) );
  }

  /**
   * Checks if this message is a keep-alive message.
   * @return True, if it is.
   * @see com.assembla.ariane.jellyfish.proxy.messages.GenericMessage#isKeepAlive()
   */
  @Override( )
  public boolean isKeepAlive( )
  {
    return this.containsExtra( "Keep-alive" ) &&
             Boolean.parseBoolean( this.getExtra( "Keep-alive" ) );
  }
  
}
