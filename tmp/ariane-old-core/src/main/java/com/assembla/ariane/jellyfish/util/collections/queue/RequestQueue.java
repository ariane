/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.queue;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that represents an queue of requests.
 * @author Fat Cat
 * @param <Request> The request type.
 * @version 2
 * @since 0.0.3
 */
public class RequestQueue< Request >
{
  /** An list of messages to process. */
  private final BlockingQueue< Request > sender;
  
  /** The size of the request queue.*/
  private int size;
  
  /** The logger to use. */
  private Logger logger;
  
  /**
   * Creates a new request queue given it's size.
   * @param size The size of the queue.
   */
  public RequestQueue( final int size )
  {
    this.sender = new LinkedBlockingQueue< Request >( size );
    this.size = size;
    this.logger = LoggerFactory.getLogger( RequestQueue.class );
  }
  
  /**
   * Adds a new request to the queue.
   * @param request The request to add
   * @throws IllegalStateException Exception raised if the request could not be
   *   added due to problems with the queue size.
   */
  public void add( final Request request ) throws IllegalStateException
  {
    this.logger.debug( "Adding a new request." );
    if ( !this.sender.contains( request ) )
    {
      this.sender.add( request );
    }
  }
  
  /**
   * Clears the request queue.
   */
  public void clear( )
  {
    this.logger.debug( "Clearing all the requests." );
    this.sender.clear( );
  }
  
  /**
   * Checks if the queue contains the given request.
   * @param request the request to check for.
   * @return true, if successful.
   */
  public boolean contains( final Request request )
  {
    return this.sender.contains( request );
  }
  
  /**
   * Retrieves the element at the head of this queue, without removing it.
   * @return The head element of this queue.
   */
  public Request element( )
  {
    return this.sender.element( );
  }
  
  /**
   * Checks if the requests queue is empty.
   * @return true, if is empty.
   */
  public boolean isEmpty( )
  {
    return this.sender.isEmpty( );
  }
  
  /**
   * Returns the iterator to iterate the request queue.
   * @return The requests queue iterator.
   */
  public Iterator< Request > iterator( )
  {
    return this.sender.iterator( );
  }
  
  /**
   * Adds an element to this queue and returns whether or not is was added.
   * @param e The request to add.
   * @return true, if successful.
   */
  public boolean offer( final Request e )
  {
    this.logger.debug( "Adding a new request." );
    return this.sender.offer( e );
  }
  
  /**
   * Adds the element to this queue and waits until the element 
   * is successfully inserted.
   * 
   * @param e The request to add.
   * @param timeout The maximum waiting time.
   * @param unit The unit of the timeout value.
   * @return true, if successful.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public boolean offer( final Request e, final long timeout, final TimeUnit unit ) throws InterruptedException
  {
    return this.sender.offer( e, timeout, unit );
  }
  
  /**
   * Retrieves the head of the queue or returns null if it does not exist.
   * @return The request in the head of this queue.
   */
  public Request peek( )
  {
    return this.sender.peek( );
  }
  
  /**
   * Retrieves and removes the head of this queue if the queue is not empty.
   * @return The request in the head of this queue or null is the queue is empty.
   */
  public Request poll( )
  {
    this.logger.debug( "Removing a request." );
    return this.sender.poll( );
  }
  
  /**
   * Retrieves and removes the head of this queue, waiting the 
   * specified time until the request is available.
   * 
   * @param timeout The maximum waiting time.
   * @param unit The unit of the timeout value.
   * @return The request in the head or null if the queue is empty.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public Request poll( final long timeout, final TimeUnit unit ) throws InterruptedException
  {
    return this.sender.poll( timeout, unit );
  }
  
  /**
   * Inserts the given request while waiting for space to be available. 
   * @param e The request to add.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public void put( final Request e ) throws InterruptedException
  {
    this.logger.debug( "Adding a request." );
    this.sender.put( e );
  }
  
  /**
   * Returns the number of requests that can be added without 
   * violating the size dimension of this queue.
   * @return The number of remaining requests.
   */
  public int remainingCapacity( )
  {
    return this.sender.remainingCapacity( );
  }
  
  /**
   * Retrieves and removes the request at the head of the queue. 
   * @return The request at the head.
   * @throws NoSuchElementException If there is no available request.
   */
  public Request remove( ) throws NoSuchElementException
  {
    this.logger.debug( "Removing a request." );
    return this.sender.remove( );
  }
  
  /**
   * Returns the number of available requests. 
   * @return The number of available requests.
   */
  public int count( )
  {
    return this.sender.size( );
  }
  
  /**
   * Retrieves and removes the request at the head of the queue, 
   * waiting the amount of time until the operation is successful.
   * 
   * @return The request at the head of the queue.
   * @throws InterruptedException If an interrupted exception occurred.
   */
  public Request take( ) throws InterruptedException
  {
    this.logger.debug( "Removing a request." );
    return this.sender.take( );
  }
  
  /**
   * Returns the maximum number of requests in this queue.
   * @return The maximum number of queue requests.
   */
  public int getMaxSize( )
  {
    return this.size;
  }
  
}
