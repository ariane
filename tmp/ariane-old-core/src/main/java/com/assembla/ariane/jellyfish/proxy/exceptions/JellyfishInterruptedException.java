package com.assembla.ariane.jellyfish.proxy.exceptions;

/**
 * Exception launched whenever a method is interrupted.
 * @author Fat Cat
 * @version 1
 * @since 0.0.7
 */
public class JellyfishInterruptedException extends Exception
{
  /** The version id. */
  private static final long serialVersionUID = -2603975394827345364L;

  /**
   * Create a new interrupted exception.
   */
  public JellyfishInterruptedException()
  {
    super("The method was interrupted.");
  }

  /**
   * Create a new interrupted exception.
   * @param message The exception message.
   */
  public JellyfishInterruptedException( String message )
  {
    super( message );
  }

  /**
   * Create a new interrupted exception.
   * @param message The exception message.
   * @param mThrowable The inner exception.
   */
  public JellyfishInterruptedException( String message, Throwable mThrowable )
  {
    super( message, mThrowable );
  }

  /**
   * Create a new interrupted exception.
   * @param mThrowable The inner exception.
   */
  public JellyfishInterruptedException( Throwable mThrowable )
  {
    super( "The method was interrupted.", mThrowable );
  }
}
