/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages.requests;

import com.assembla.ariane.jellyfish.proxy.messages.GenericMessage;

/**
 * This interface represents an generic request.
 * @author Fat Cat
 */
public interface GenericRequest extends GenericMessage
{
  
  //Method stuff
  
  /**
   * Returns the http method as a string.
   * @return An string representing the http method.
   */
  String getMethod( );
  
  /**
   * Sets the http method.
   * @param method The http method as a string.
   */
  void setMethod( String method );
  
  //Uri stuff
  
  /**
   * Returns the uri of this request.
   * @return The uri of this request.
   */
  String getUri( );
  
  /**
   * Changes the uri of this request.
   * @param uri The new uri of this request.
   */
  void setUri( String uri );
  
}
