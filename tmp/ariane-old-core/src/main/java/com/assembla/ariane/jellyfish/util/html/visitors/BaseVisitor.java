/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.html.visitors;

import org.htmlparser.Remark;
import org.htmlparser.Tag;
import org.htmlparser.Text;
import org.htmlparser.tags.*;
import org.htmlparser.visitors.NodeVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is an base visitor that uses an NodeVisitor,
 * to provide an API capable of handling Link tags, body tags,
 * etc.; in a more specific way than a NodeVisitor. 
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public abstract class BaseVisitor extends NodeVisitor
{
  
  protected Logger logger;
  
  /**
   * Creates a new BaseVisitor.<br/>
   * Please override if you want to customize this constructor,
   * or if you want to create a logger using your class instead
   * of this one.
   */
  public BaseVisitor( )
  {
    this.logger = LoggerFactory.getLogger( NodeVisitor.class );
  }
  
  /**
   * Creates a new BaseVisitor.<br/>
   * Please override if you want to customize this constructor,
   * or if you want to create a logger using your class instead
   * of this one. 
   * @param recurseChildren If this visitor should recurse thought children.
   */
  public BaseVisitor( final boolean recurseChildren )
  {
    super( recurseChildren );
    this.logger = LoggerFactory.getLogger( NodeVisitor.class );
  }
  
  /**
   * Creates a new BaseVisitor.<br/>
   * Please override if you want to customize this constructor,
   * or if you want to create a logger using your class instead
   * of this one. 
   * @param recurseChildren If true, the visitor will visit children, 
   * otherwise only the top level nodes are recursed.
   * @param recurseSelf If true, the visitor will visit the top level node.
   */
  public BaseVisitor( final boolean recurseChildren, final boolean recurseSelf )
  {
    super( recurseChildren, recurseSelf );
    this.logger = LoggerFactory.getLogger( NodeVisitor.class );
  }

  /**
   * Use this method if you want to do something special 
   * during initialization.
   * @see org.htmlparser.visitors.NodeVisitor#beginParsing()
   */
  @Override( )
  public void beginParsing( )
  {
    super.beginParsing( );
    this.logger.debug( "Beginning the parsing..." );
  }

  /**
   * Method that is called in order to visit an tag.
   * @param tag The visited tag.
   * @see org.htmlparser.visitors.NodeVisitor#visitTag(org.htmlparser.Tag)
   */
  @Override( )
  public final void visitTag( final Tag tag )
  {
    super.visitTag( tag );
    
    this.logger.debug( "Visiting tag {}", tag.getTagName( ).toLowerCase( ) );
    
    if( tag instanceof AppletTag )
    {
      this.visitAppletTag( ( AppletTag ) tag );
    }
    else if( tag instanceof BaseHrefTag )
    {
      this.visitBaseTag( ( BaseHrefTag ) tag );
    }
    else if( tag instanceof BodyTag )
    {
      this.visitBodyTag( ( BodyTag ) tag );
    }
    else if( tag instanceof Bullet )
    {
      this.visitBulletTag( ( Bullet ) tag );
    }
    else if( tag instanceof BulletList )
    {
      this.visitBulletListTag( ( BulletList ) tag );
    }
    else if( tag instanceof DefinitionList )
    {
      this.visitDefinitionListTag( ( DefinitionList ) tag );
    }
    else if( tag instanceof DefinitionListBullet )
    {
      this.visitDefinitionListBulletTag( ( DefinitionListBullet ) tag );
    }    
    else if( tag instanceof Div )
    {
      this.visitDivTag( ( Div ) tag );
    }
    else if( tag instanceof DoctypeTag )
    {
      this.visitDoctypeTag( ( DoctypeTag ) tag );
    }
    else if( tag instanceof FormTag )
    {
      this.visitFormTag( ( FormTag ) tag );
    }
    else if( tag instanceof FrameSetTag )
    {
      this.visitFrameSetTag( ( FrameSetTag ) tag );
    }
    else if( tag instanceof FrameTag )
    {
      this.visitFrameTag( ( FrameTag ) tag );
    }
    else if( tag instanceof HeadingTag )
    {
      this.visitHeadingTag( ( HeadingTag ) tag );
    }
    else if( tag instanceof HeadTag )
    {
      this.visitHeadTag( ( HeadTag ) tag );
    }
    else if( tag instanceof Html )
    {
      this.visitHtmlTag( ( Html ) tag );
    }
    else if( tag instanceof ImageTag )
    {
      this.visitImageTag( ( ImageTag ) tag );
    }
    else if( tag instanceof InputTag )
    {
      this.visitInputTag( ( InputTag ) tag );
    }
    else if( tag instanceof JspTag )
    {
      this.visitJspTag( ( JspTag ) tag );
    }
    else if( tag instanceof LabelTag )
    {
      this.visitLabelTag( ( LabelTag ) tag );
    }
    else if( tag instanceof LinkTag )
    {
      this.visitLinkTag( ( LinkTag ) tag );
    }
    else if( tag instanceof MetaTag )
    {
      this.visitMetaTag( ( MetaTag ) tag );
    }
    else if( tag instanceof ObjectTag )
    {
      this.visitObjectTag( ( ObjectTag ) tag );
    }
    else if( tag instanceof OptionTag )
    {
      this.visitOptionTag( ( OptionTag ) tag );
    }
    else if( tag instanceof ParagraphTag )
    {
      this.visitParagraphTag( ( ParagraphTag ) tag );
    }
    else if( tag instanceof ProcessingInstructionTag )
    {
      this.visitProcessingInstructionTag( ( ProcessingInstructionTag ) tag );
    }
    else if( tag instanceof ScriptTag )
    {
      this.visitScriptTag( ( ScriptTag ) tag );
    }
    else if( tag instanceof SelectTag )
    {
      this.visitSelectTag( ( SelectTag ) tag );
    }
    else if( tag instanceof Span )
    {
      this.visitSpanTag( ( Span ) tag );
    }
    else if( tag instanceof StyleTag )
    {
      this.visitStyleTag( ( StyleTag ) tag );
    }    
    else if( tag instanceof TableColumn )
    {
      this.visitTableColumnTag( ( TableColumn ) tag );
    }
    else if( tag instanceof TableHeader )
    {
      this.visitTableHeaderTag( ( TableHeader ) tag );
    }
    else if( tag instanceof TableRow )
    {
      this.visitTableRowTag( ( TableRow ) tag );
    }
    else if( tag instanceof TableTag )
    {
      this.visitTableTag( ( TableTag ) tag );
    }
    else if( tag instanceof TextareaTag )
    {
      this.visitTextareaTag( ( TextareaTag ) tag );
    }
    else if( tag instanceof TitleTag )
    {
      this.visitTitleTag( ( TitleTag ) tag );
    }
    else
    {
      this.visitGenericTag( tag );
    }
    
    this.logger.debug( "Visited tag {}", tag.getTagName( ).toLowerCase( ) );
  }

  /**
   * This method visits an applet tag.<br/>
   * Override if you want to do something useful.
   * @param appletTag The visited applet tag.
   */
  protected void visitAppletTag( final AppletTag appletTag )
  {
    this.logger.debug( "Visiting an applet tag." );
  }

  /**
   * This method visits an base tag.<br/>
   * Override if you want to do something useful.
   * @param baseTag The visited base tag.
   */
  protected void visitBaseTag( final BaseHrefTag baseTag )
  {
    this.logger.debug( "Visiting an base tag." );
  }

  /**
   * This method visits an body tag.<br/>
   * Override if you want to do something useful.
   * @param bodyTag The visited body tag.
   */
  protected void visitBodyTag( final BodyTag bodyTag )
  {
    this.logger.debug( "Visiting an body tag." );
  }

  /**
   * This method visits an bullet list tag.<br/>
   * Override if you want to do something useful.
   * @param bulletListTag The bullet list tag to visit.
   */
  protected void visitBulletListTag( final BulletList bulletListTag )
  {
    this.logger.debug( "Visiting an bullet list tag." );
  }

  /**
   * This method visits an bullet tag.<br/>
   * Override if you want to do something useful.
   * @param bulletTag The bullet tag to visit.
   */
  protected void visitBulletTag( final Bullet bulletTag )
  {
    this.logger.debug( "Visiting an bullet tag." );
  }

  /**
   * This method visits an definition list bullet tag.<br/>
   * Override if you want to do something useful.
   * @param definitionListBulletTag The definition list bullet tag to visit.
   */
  protected void visitDefinitionListBulletTag( final DefinitionListBullet definitionListBulletTag )
  {
    this.logger.debug( "Visiting an definition list bullet tag." );
  }

  /**
   * This method visits an definition list tag.<br/>
   * Override if you want to do something useful.
   * @param definitionListTag The definition list tag to visit.
   */
  protected void visitDefinitionListTag( final DefinitionList definitionListTag )
  {
    this.logger.debug( "Visiting an definition list tag." );
  }

  /**
   * This method visits an div tag.<br/>
   * Override if you want to do something useful.
   * @param divTag The div tag to visit.
   */
  protected void visitDivTag( final Div divTag )
  {
    this.logger.debug( "Visiting an definition list tag." );
  }

  /**
   * Method to visit an doctype tag.<br/>
   * Override if you want to do something useful.
   * @param tag The doctype tag to visit
   */
  protected void visitDoctypeTag( final DoctypeTag tag )
  {
    this.logger.debug( "Visiting an doctype tag." );
  }

  /**
   * Method to visit an form tag.<br/>
   * Override if you want to do something useful.
   * @param tag The form tag to visit
   */
  protected void visitFormTag( final FormTag tag )
  {
    this.logger.debug( "Visiting an form tag." );
  }

  /**
   * Method to visit an frameset tag.<br/>
   * Override if you want to do something useful.
   * @param tag The frameset tag to visit
   */
  protected void visitFrameSetTag( final FrameSetTag tag )
  {
    this.logger.debug( "Visiting an frameset tag." );
  }

  /**
   * Method to visit an frame tag.<br/>
   * Override if you want to do something useful.
   * @param tag The frame tag to visit
   */
  protected void visitFrameTag( final FrameTag tag )
  {
    this.logger.debug( "Visiting an frame tag." );
  }

  /**
   * Method to visit an generic tag.<br/>
   * Override if you want to do something useful.
   * @param tag The generic tag to visit
   */
  protected void visitGenericTag( final Tag tag )
  {
    this.logger.debug( "Visiting an generic tag" );
  }

  /**
   * Method to visit an heading tag.<br/>
   * Override if you want to do something useful.
   * @param tag The heading tag to visit
   */
  protected void visitHeadingTag( final HeadingTag tag )
  {
    this.logger.debug( "Visiting an heading tag" );
  }

  /**
   * Method to visit an head tag.<br/>
   * Override if you want to do something useful.
   * @param tag The head tag to visit
   */
  protected void visitHeadTag( final HeadTag tag )
  {
    this.logger.debug( "Visiting an head tag" );
  }

  /**
   * Method to visit an html tag.<br/>
   * Override if you want to do something useful.
   * @param tag The html tag to visit
   */
  protected void visitHtmlTag( final Html tag )
  {
    this.logger.debug( "Visiting an html tag" );
  }

  /**
   * Method to visit an image tag tag.<br/>
   * Override if you want to do something useful.
   * @param tag The image tag tag to visit
   */
  protected void visitImageTag( final ImageTag tag )
  {
    this.logger.debug( "Visiting an image tag tag" );
  }

  /**
   * Method to visit an input tag.<br/>
   * Override if you want to do something useful.
   * @param tag The input tag to visit
   */
  protected void visitInputTag( final InputTag tag )
  {
    this.logger.debug( "Visiting an input tag" );
  }

  /**
   * Method to visit an JSP tag.<br/>
   * Override if you want to do something useful.
   * @param tag The JSP tag to visit
   */
  protected void visitJspTag( final JspTag tag )
  {
    this.logger.debug( "Visiting an JSP tag" );
  }

  /**
   * Method to visit an label tag.<br/>
   * Override if you want to do something useful.
   * @param tag The label tag to visit
   */
  protected void visitLabelTag( final LabelTag tag )
  {
    this.logger.debug( "Visiting an label tag" );
  }

  /**
   * Method to visit an link tag.<br/>
   * Override if you want to do something useful.
   * @param tag The link tag to visit
   */
  protected void visitLinkTag( final LinkTag tag )
  {
    this.logger.debug( "Visiting an link tag" );
  }

  /**
   * Method to visit an meta tag.<br/>
   * Override if you want to do something useful.
   * @param tag The meta tag to visit
   */
  protected void visitMetaTag( final MetaTag tag )
  {
    this.logger.debug( "Visiting an meta tag" );
  }

  /**
   * Method to visit an object tag.<br/>
   * Override if you want to do something useful.
   * @param tag The object tag to visit
   */
  protected void visitObjectTag( final ObjectTag tag )
  {
    this.logger.debug( "Visiting an object tag" );
  }

  /**
   * Method to visit an option tag.<br/>
   * Override if you want to do something useful.
   * @param tag The option tag to visit
   */
  protected void visitOptionTag( final OptionTag tag )
  {
    this.logger.debug( "Visiting an option tag" );
  }

  /**
   * Method to visit an paragraph tag.<br/>
   * Override if you want to do something useful.
   * @param tag The paragraph tag to visit
   */
  protected void visitParagraphTag( final ParagraphTag tag )
  {
    this.logger.debug( "Visiting an paragraph tag" );
  }

  /**
   * Method to visit an instruction processing tag.<br/>
   * Override if you want to do something useful.
   * @param tag The instruction processing tag to visit
   */
  protected void visitProcessingInstructionTag( final ProcessingInstructionTag tag )
  {
    this.logger.debug( "Visiting an instruction processing tag" );
  }

  /**
   * Method to visit an script tag.<br/>
   * Override if you want to do something useful.
   * @param tag The script tag to visit
   */
  protected void visitScriptTag( final ScriptTag tag )
  {
    this.logger.debug( "Visiting an script tag" );
  }

  /**
   * Method to visit an select tag.<br/>
   * Override if you want to do something useful.
   * @param tag The select tag to visit
   */
  protected void visitSelectTag( final SelectTag tag )
  {
    this.logger.debug( "Visiting an select tag" );
  }

  /**
   * Method to visit an span tag.<br/>
   * Override if you want to do something useful.
   * @param tag The span tag to visit
   */
  protected void visitSpanTag( final Span tag )
  {
    this.logger.debug( "Visiting an span tag" );
  }

  /**
   * Method to visit an style tag.<br/>
   * Override if you want to do something useful.
   * @param tag The style tag to visit
   */
  protected void visitStyleTag( final StyleTag tag )
  {
    this.logger.debug( "Visiting an style tag" );
  }

  /**
   * Method to visit an table column tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table column tag to visit
   */
  protected void visitTableColumnTag( final TableColumn tag )
  {
    this.logger.debug( "Visiting an table column tag" );
  }

  /**
   * Method to visit an table header tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table header tag to visit
   */
  protected void visitTableHeaderTag( final TableHeader tag )
  {
    this.logger.debug( "Visiting an table header tag" );
  }

  /**
   * Method to visit an table row tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table row tag to visit
   */
  protected void visitTableRowTag( final TableRow tag )
  {
    this.logger.debug( "Visiting an table row tag" );
  }

  /**
   * Method to visit an table tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table tag to visit
   */
  protected void visitTableTag( final TableTag tag )
  {
    this.logger.debug( "Visiting an table tag" );
  }

  /**
   * Method to visit an text area tag.<br/>
   * Override if you want to do something useful.
   * @param tag The text area tag to visit
   */
  protected void visitTextareaTag( final TextareaTag tag )
  {
    this.logger.debug( "Visiting an text area tag" );
  }

  /**
   * Method to visit an title tag.<br/>
   * Override if you want to do something useful.
   * @param tag The title tag to visit
   */
  protected void visitTitleTag( final TitleTag tag )
  {
    this.logger.debug( "Visiting an title tag" );
  }

  /**
   * This method is called to visit an end tag.
   * @param tag The visited end tag.
   * @see org.htmlparser.visitors.NodeVisitor#visitEndTag(org.htmlparser.Tag)
   */
  @Override( )
  public final void visitEndTag( final Tag tag )
  {
    super.visitEndTag( tag );
    
    this.logger.debug( "Visiting end tag {}", tag.getTagName( ).toLowerCase( ) );
    
    if( tag instanceof AppletTag )
    {
      this.visitAppletEndTag( ( AppletTag ) tag );
    }
    else if( tag instanceof BaseHrefTag )
    {
      this.visitBaseEndTag( ( BaseHrefTag ) tag );
    }
    else if( tag instanceof BodyTag )
    {
      this.visitBodyEndTag( ( BodyTag ) tag );
    }
    else if( tag instanceof Bullet )
    {
      this.visitBulletEndTag( ( Bullet ) tag );
    }
    else if( tag instanceof BulletList )
    {
      this.visitBulletListEndTag( ( BulletList ) tag );
    }
    else if( tag instanceof DefinitionList )
    {
      this.visitDefinitionListEndTag( ( DefinitionList ) tag );
    }
    else if( tag instanceof DefinitionListBullet )
    {
      this.visitDefinitionListBulletEndTag( ( DefinitionListBullet ) tag );
    }    
    else if( tag instanceof Div )
    {
      this.visitDivEndTag( ( Div ) tag );
    }
    else if( tag instanceof DoctypeTag )
    {
      this.visitDoctypeEndTag( ( DoctypeTag ) tag );
    }
    else if( tag instanceof FormTag )
    {
      this.visitFormEndTag( ( FormTag ) tag );
    }
    else if( tag instanceof FrameSetTag )
    {
      this.visitFrameSetEndTag( ( FrameSetTag ) tag );
    }
    else if( tag instanceof FrameTag )
    {
      this.visitFrameEndTag( ( FrameTag ) tag );
    }
    else if( tag instanceof HeadingTag )
    {
      this.visitHeadingEndTag( ( HeadingTag ) tag );
    }
    else if( tag instanceof HeadTag )
    {
      this.visitHeadEndTag( ( HeadTag ) tag );
    }
    else if( tag instanceof Html )
    {
      this.visitHtmlEndTag( ( Html ) tag );
    }
    else if( tag instanceof ImageTag )
    {
      this.visitImageEndTag( ( ImageTag ) tag );
    }
    else if( tag instanceof InputTag )
    {
      this.visitInputEndTag( ( InputTag ) tag );
    }
    else if( tag instanceof JspTag )
    {
      this.visitJspEndTag( ( JspTag ) tag );
    }
    else if( tag instanceof LabelTag )
    {
      this.visitLabelEndTag( ( LabelTag ) tag );
    }
    else if( tag instanceof LinkTag )
    {
      this.visitLinkEndTag( ( LinkTag ) tag );
    }
    else if( tag instanceof MetaTag )
    {
      this.visitMetaEndTag( ( MetaTag ) tag );
    }
    else if( tag instanceof ObjectTag )
    {
      this.visitObjectEndTag( ( ObjectTag ) tag );
    }
    else if( tag instanceof OptionTag )
    {
      this.visitOptionEndTag( ( OptionTag ) tag );
    }
    else if( tag instanceof ParagraphTag )
    {
      this.visitParagraphEndTag( ( ParagraphTag ) tag );
    }
    else if( tag instanceof ProcessingInstructionTag )
    {
      this.visitProcessingInstructionEndTag( ( ProcessingInstructionTag ) tag );
    }
    else if( tag instanceof ScriptTag )
    {
      this.visitScriptEndTag( ( ScriptTag ) tag );
    }
    else if( tag instanceof SelectTag )
    {
      this.visitSelectEndTag( ( SelectTag ) tag );
    }
    else if( tag instanceof Span )
    {
      this.visitSpanEndTag( ( Span ) tag );
    }
    else if( tag instanceof StyleTag )
    {
      this.visitStyleEndTag( ( StyleTag ) tag );
    }    
    else if( tag instanceof TableColumn )
    {
      this.visitTableColumnEndTag( ( TableColumn ) tag );
    }
    else if( tag instanceof TableHeader )
    {
      this.visitTableHeaderEndTag( ( TableHeader ) tag );
    }
    else if( tag instanceof TableRow )
    {
      this.visitTableRowEndTag( ( TableRow ) tag );
    }
    else if( tag instanceof TableTag )
    {
      this.visitTableEndTag( ( TableTag ) tag );
    }
    else if( tag instanceof TextareaTag )
    {
      this.visitTextareaEndTag( ( TextareaTag ) tag );
    }
    else if( tag instanceof TitleTag )
    {
      this.visitTitleEndTag( ( TitleTag ) tag );
    }
    else
    {
      this.visitGenericEndTag( tag );
    }
    
    this.logger.debug( "Visited end tag {}", tag.getTagName( ).toLowerCase( ) );
  }

  /**
   * Method to visit an applet end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The applet end tag to visit
   */

  protected void visitAppletEndTag( final AppletTag tag )
  {
    this.logger.debug( "Visiting an applet end tag" );
  }

  /**
   * Method to visit an base end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The base end tag to visit
   */
  protected void visitBaseEndTag( final BaseHrefTag tag )
  {
    this.logger.debug( "Visiting an base end tag" );
  }

  /**
   * Method to visit an body end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The body end tag to visit
   */
  protected void visitBodyEndTag( final BodyTag tag )
  {
    this.logger.debug( "Visiting an body end tag" );
  }

  /**
   * Method to visit an bullet end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The bullet end tag to visit
   */
  protected void visitBulletEndTag( final Bullet tag )
  {
    this.logger.debug( "Visiting an bullet end tag" );
  }

  /**
   * Method to visit an bullet list end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The bullet list end tag to visit
   */
  protected void visitBulletListEndTag( final BulletList tag )
  {
    this.logger.debug( "Visiting an bullet list end tag" );
  }

  /**
   * Method to visit an definition list end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The definition list end tag to visit
   */
  protected void visitDefinitionListBulletEndTag( final DefinitionListBullet tag )
  {
    this.logger.debug( "Visiting an definition list end tag" );
  }

  /**
   * Method to visit an definition lists end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The definition lists tag to visit
   */
  protected void visitDefinitionListEndTag( final DefinitionList tag )
  {
    this.logger.debug( "Visiting an definition lists tag" );
  }

  /**
   * Method to visit an div end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The div end tag to visit
   */
  protected void visitDivEndTag( final Div tag )
  {
    this.logger.debug( "Visiting an div end tag" );
  }

  /**
   * Method to visit an doctype end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The doctype end tag to visit
   */
  protected void visitDoctypeEndTag( final DoctypeTag tag )
  {
    this.logger.debug( "Visiting an doctype end tag" );
  }

  /**
   * Method to visit an form end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The form end tag to visit
   */
  protected void visitFormEndTag( final FormTag tag )
  {
    this.logger.debug( "Visiting an form end tag" );
  }

  /**
   * Method to visit an frame end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The frame end tag to visit
   */
  protected void visitFrameEndTag( final FrameTag tag )
  {
    this.logger.debug( "Visiting an frame end tag" );
  }

  /**
   * Method to visit an frameset end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The frameset end tag to visit
   */
  protected void visitFrameSetEndTag( final FrameSetTag tag )
  {
    this.logger.debug( "Visiting an frameset end tag" );
  }

  /**
   * Method to visit an head end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The head end tag to visit
   */
  protected void visitHeadEndTag( final HeadTag tag )
  {
    this.logger.debug( "Visiting an head end tag" );
  }

  /**
   * Method to visit an heading end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The heading end tag to visit
   */
  protected void visitHeadingEndTag( final HeadingTag tag )
  {
    this.logger.debug( "Visiting an heading end tag" );
  }

  /**
   * Method to visit an html end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The html end tag to visit
   */
  protected void visitHtmlEndTag( final Html tag )
  {
    this.logger.debug( "Visiting an html end tag" );
  }

  /**
   * Method to visit an image end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The image end tag to visit
   */
  protected void visitImageEndTag( final ImageTag tag )
  {
    this.logger.debug( "Visiting an image end tag" );
  }

  /**
   * Method to visit an input end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The input end tag to visit
   */
  protected void visitInputEndTag( final InputTag tag )
  {
    this.logger.debug( "Visiting an input end tag" );
  }

  /**
   * Method to visit an JSP end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The JSP end tag to visit
   */
  protected void visitJspEndTag( final JspTag tag )
  {
    this.logger.debug( "Visiting an JSP end tag" );
  }

  /**
   * Method to visit an label end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The label end tag to visit
   */
  protected void visitLabelEndTag( final LabelTag tag )
  {
    this.logger.debug( "Visiting an label end tag" );
  }

  /**
   * Method to visit an link end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The link end tag to visit
   */
  protected void visitLinkEndTag( final LinkTag tag )
  {
    this.logger.debug( "Visiting an link end tag" );
  }

  /**
   * Method to visit an meta end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The meta end tag to visit
   */
  protected void visitMetaEndTag( final MetaTag tag )
  {
    this.logger.debug( "Visiting an meta end tag" );
  }

  /**
   * Method to visit an object end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The object end tag to visit
   */
  protected void visitObjectEndTag( final ObjectTag tag )
  {
    this.logger.debug( "Visiting an object end tag" );
  }

  /**
   * Method to visit an option end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The option end tag to visit
   */
  protected void visitOptionEndTag( final OptionTag tag )
  {
    this.logger.debug( "Visiting an option end tag" );
  }

  /**
   * Method to visit an paragraph end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The paragraph end tag to visit
   */
  protected void visitParagraphEndTag( final ParagraphTag tag )
  {
    this.logger.debug( "Visiting an %s tag" );
  }

  /**
   * Method to visit an processing instruction end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The processing instruction end tag to visit
   */
  protected void visitProcessingInstructionEndTag( final ProcessingInstructionTag tag )
  {
    this.logger.debug( "Visiting an processing instruction end tag" );
  }

  /**
   * Method to visit an script end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The script end tag to visit
   */
  protected void visitScriptEndTag( final ScriptTag tag )
  {
    this.logger.debug( "Visiting an script end tag" );
  }

  /**
   * Method to visit an select end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The select end tag to visit
   */
  protected void visitSelectEndTag( final SelectTag tag )
  {
    this.logger.debug( "Visiting an select end tag" );
  }

  /**
   * Method to visit an span end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The span end tag to visit
   */
  protected void visitSpanEndTag( final Span tag )
  {
    this.logger.debug( "Visiting an span end tag" );
  }

  /**
   * Method to visit an style end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The style end tag to visit
   */
  protected void visitStyleEndTag( final StyleTag tag )
  {
    this.logger.debug( "Visiting an style end tag" );
  }

  /**
   * Method to visit an table column end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table column end tag to visit
   */
  protected void visitTableColumnEndTag( final TableColumn tag )
  {
    this.logger.debug( "Visiting an table column end tag" );
  }

  /**
   * Method to visit an table end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table end tag to visit
   */
  protected void visitTableEndTag( final TableTag tag )
  {
    this.logger.debug( "Visiting an table end tag" );
  }

  /**
   * Method to visit an table header end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table header end tag to visit
   */
  protected void visitTableHeaderEndTag( final TableHeader tag )
  {
    this.logger.debug( "Visiting an table header end tag" );
  }

  /**
   * Method to visit an table row end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The table row end tag to visit
   */
  protected void visitTableRowEndTag( final TableRow tag )
  {
    this.logger.debug( "Visiting an table row end tag" );
  }

  /**
   * Method to visit an text area end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The text area end tag to visit
   */
  protected void visitTextareaEndTag( final TextareaTag tag )
  {
    this.logger.debug( "Visiting an text area end tag" );
  }
  

  /**
   * Method called to visit an title end tag.<br/>
   * Override if you want to do something useful.
   * @param titleTag The title tag that has ended.
   */
  protected void visitTitleEndTag( final TitleTag titleTag )
  {
    this.logger.debug( "Visiting an title tag end." );
  }
  

  /**
   * Method called to visit an generic end tag.<br/>
   * Override if you want to do something useful.
   * @param tag The tag that has ended.
   */
  protected void visitGenericEndTag( final Tag tag )
  {
    this.logger.debug( "Visiting an generic end tag." );
  }

  /**
   * Method called in order to visit an text node.<br/>
   * Override if you want to do something useful.
   * @param string The string represented by this text node.
   * @see org.htmlparser.visitors.NodeVisitor#visitStringNode(org.htmlparser.Text)
   */
  @Override( )
  public void visitStringNode( final Text string )
  {
    super.visitStringNode( string );
    this.logger.debug( "Visiting string node." );
  }

  /**
   * Method called in order to visit an comment.<br/>
   * Override if you want to do something useful.
   * @param remark The visited comment.
   * @see org.htmlparser.visitors.NodeVisitor#visitRemarkNode(org.htmlparser.Remark)
   */
  @Override( )
  public void visitRemarkNode( final Remark remark )
  {
    super.visitRemarkNode( remark );
    this.logger.debug( "Visiting comment node." );
  }

  /**
   * Method called whenever the parser finishes parsing the document.<br/>
   * Override if you want to do something useful.
   * @see org.htmlparser.visitors.NodeVisitor#finishedParsing()
   */
  @Override( )
  public void finishedParsing( )
  {
    super.finishedParsing( );
    this.logger.debug( "Finished parsing the document." );
  }

  /**
   * Checks if the given object is equal to this one.
   * @param obj The object to compare.
   * @return True, if the visitors are equal.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public abstract boolean equals( Object obj );

  /**
   * Calculates and returns an hash code for this object.
   * @return The calculated hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public abstract int hashCode( );

  /**
   * Returns an string representation of this visitor.
   * @return An string representation of this visitor.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    return "Base Visitor";
  }
}
