/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.cache;

import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that holds a mapping between the uri and the uuid of pages in cache.
 * @author Fat Cat
 * @version 2
 * @since 0.0.5
 */
public final class CacheUriIdMap
{
  /** Variable that stores the mapping holder. */
  private final SortedMap< String, SortedSet< UUID >> mappings;
  
  /** An logger object. */
  private final Logger logger;
  
  /** The singleton object. */
  private static final CacheUriIdMap inst = new CacheUriIdMap( );
  
  /**
   * Returns the instance of this singleton.
   * @return The id mapping singleton.
   */
  public static CacheUriIdMap instance( )
  {
    return CacheUriIdMap.inst;
  }
  
  /**
   * Creates a new map.
   */
  private CacheUriIdMap( )
  {
    this.mappings = Collections.synchronizedSortedMap( new TreeMap< String, SortedSet< UUID >>( ) );
    this.logger = LoggerFactory.getLogger( CacheUriIdMap.class );
  }
  
  /**
   * Clears all mappings.
   */
  public void clear( )
  {
    this.logger.debug( "Cleaning all the entries." );
    this.mappings.clear( );
  }
  
  /**
   * Checks if the given uri is contained in this mapping.
   * @param uri The uri name
   * @return True, if it is.
   */
  public boolean contains( final String uri )
  {
    return this.mappings.containsKey( uri );
  }
  
  /**
   * Returns the id of the associated uri.
   * @param uri The uri name.
   * @return The associated ids, or null if the uris are unknown.
   */
  public SortedSet< UUID > getId( final String uri )
  {
    if ( this.contains( uri ) ) 
    {
      return Collections.unmodifiableSortedSet( this.mappings.get( uri ) );
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Checks if this mapping is empty.
   * @return True, if it is.
   */
  public boolean isEmpty( )
  {
    return this.mappings.isEmpty( );
  }
  
  /**
   * Adds a new uri and id pair to the map.
   * @param uri The uri name.
   * @param requestId The associated id.
   */
  public void add( final String uri, final UUID requestId )
  {
    if ( !this.contains( uri ) )
    {
      this.mappings.put( uri, new TreeSet< UUID >( ) );
    }
    this.mappings.get( uri ).add( requestId );
  }
  
  /**
   * Removes the mapping for the given uri.
   * @param uri The uri name to remove.
   * @param requestId The id of the request to remove.
   * @return True, on success.
   */
  public boolean remove( final String uri, final UUID requestId )
  {
    if ( this.mappings.containsKey( uri ) && this.mappings.get( uri ).contains( requestId ) )
    {
      this.mappings.get( uri ).remove( requestId );
      this.logger.debug( "Removing the entry {}", requestId.toString( ) );
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Returns the number of entries in this mapping.
   * @return The entry number.
   */
  public int size( )
  {
    int total = 0;
    for ( final String uri : this.mappings.keySet( ) )
    {
      total += this.mappings.get( uri ).size( );
    }
    return total;
  }
  
  /**
   * Removes the mapping for the given uri.
   * @param uri The uri to remove.
   * @return True, on success.
   */
  public boolean removeAll( final String uri )
  {
    if ( this.contains( uri ) )
    {
      this.mappings.get( uri ).clear( );
      this.logger.debug( "Removing the entries with method {}", uri );
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Returns the number of entries in this mapping.
   * @param uri The name of the uri. 
   * @return The entry number.
   */
  public int sizeEntry( final String uri )
  {
    if ( this.contains( uri ) ) 
    {
      return this.mappings.get( uri ).size( );
    }
    else
    {
      return -1;
    }
  }
  
  /**
   * Returns the internal map containing the entries.
   * @return The internal map in read-only mode.
   */
  public synchronized Map< String, SortedSet< UUID >> getMappings( )
  {
    return Collections.unmodifiableSortedMap( this.mappings );
  }
}
