/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.server;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that holds a map of all requests sent grouped by host name.
 * @author Fat Cat
 * @since 0.0.2
 * @version 3
 */
public final class HTTPSentIdHolder
{
  /** Variable that stores the mapping holder. */
  private final Map< String, UUID > mappings;
  
  /** An logger object. */
  private final Logger logger;
  
  /** The singleton object. */
  private static final HTTPSentIdHolder inst = new HTTPSentIdHolder( );
  
  private final ChannelGroup channels;
  
  /**
   * Returns the instance of this singleton. 
   * @return The id mapping singleton.
   */
  public static HTTPSentIdHolder instance( )
  {
    return HTTPSentIdHolder.inst;
  }
  
  /**
   * Creates a new map.
   */
  private HTTPSentIdHolder( )
  {
    this.mappings = Collections.synchronizedMap( new HashMap< String, UUID >( ) );
    this.logger = LoggerFactory.getLogger( HTTPSentIdHolder.class );
    this.channels = new DefaultChannelGroup( );
  }
  
  /**
   * Returns the instance of the channel group.
   * @return the channel group.
   */
  public synchronized ChannelGroup getChannels( )
  {
    return this.channels;
  }
  
  /**
   * Clears all mappings.
   */
  public void clear( )
  {
    this.logger.debug( "Cleaning all the entries." );
    this.mappings.clear( );
  }
  
  /**
   * Checks if the given host is contained in this mapping.
   * @param host The host name
   * @return True, if it is.
   */
  public boolean contains( final String host )
  {
    return this.mappings.containsKey( host );
  }
  
  /**
   * Returns the id of the last request sent to the given host.
   * @param host The host name.
   * @return The id of the last request sent, or null if the host is unknown.
   */
  public UUID getId( final String host )
  {
    if ( this.contains( host ) ) 
    {
      return this.mappings.get( host );
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Checks if this mapping is empty.
   * @return True, if it is.
   */
  public boolean isEmpty( )
  {
    return this.mappings.isEmpty( );
  }
  
  /**
   * Adds the given request id to the mapping, changing the current value.
   * @param requestId The request id to associate.
   * @param host The name of the host.
   */
  public void add( final String host, final UUID requestId )
  {
    if ( this.contains( host ) )
    {
      this.logger.debug( "Modifying the entry {}", requestId.toString( ) );
    }
    else
    {
      this.logger.debug( "Adding an entry {}", requestId.toString( ) );
    }
    this.mappings.put( host, requestId );
  }
  
  /**
   * Removes the mapping for the given hosting.
   * @param host The host name to remove.
   * @return The associated id if any or null if the host is unknown.
   */
  public UUID remove( final String host )
  {
    if ( this.contains( host ) )
    {
      final UUID entry = this.mappings.remove( host );
      this.logger.debug( "Removing the entry {}", entry.toString( ) );
      return entry;
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Returns the number of entries in this mapping.
   * @return The entry number.
   */
  public int size( )
  {
    return this.mappings.size( );
  }
  
  /**
   * Returns the internal map containing the entries.
   * @return The internal map in read-only mode.
   */
  public synchronized Map< String, UUID > getMappings( )
  {
    return Collections.unmodifiableMap( this.mappings );
  }
}
