/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.vfc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import org.htmlparser.Parser;
import org.htmlparser.util.ParserException;
import com.assembla.ariane.jellyfish.util.html.visitors.StandardVFCLinkVisitor;
import com.assembla.ariane.jellyfish.util.html.visitors.VFCLinkInformation;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import com.assembla.ariane.jellyfish.proxy.vfc.Pivot;
import com.assembla.ariane.jellyfish.proxy.vfc.User;
import com.assembla.ariane.jellyfish.proxy.vfc.Users;
import com.assembla.ariane.jellyfish.proxy.vfc.VFCPage;
import com.assembla.ariane.jellyfish.proxy.vfc.VFCVector;

/**
 * This is an utility class with some commonly used VFC functions.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public final class VFCUtilityFunctions
{
  
  /** Hidden Constructor, do not make public. */
  private VFCUtilityFunctions( )
  {
    
  }
  
  /**
   * Checks if the given response is an html or xhtml page.
   * @param response The response to check for.
   * @return True if it is.
   */
  public static boolean isHtmlPage( final ProxyResponseMessage response )
  {
    if( response.containsHeader( "Content-Type" ) )
    {
      final String mediaType = response.getHeader( "Content-Type" ).split( ";" )[0];
      
      return mediaType.equalsIgnoreCase( "text/html" ) || 
             mediaType.equalsIgnoreCase( "application/xhtml+xml" );
    }
    return false;
  }
  
  /**
   * Parses the HTML page.
   * @param serverResponse The server response.
   * @return The visitor after visiting the given page.
   * @throws ParserException If some parsing error happens.
   */
  public static StandardVFCLinkVisitor parsePage( final ProxyResponseMessage serverResponse ) throws ParserException
  {
    final StandardVFCLinkVisitor visitor = new StandardVFCLinkVisitor( );
    visitor.clear( );
    final Parser parser = Parser.createParser( 
        serverResponse.getContentString( ), 
        VFCUtilityFunctions.getResponseEncoding( serverResponse ) );
    
    parser.visitAllNodesWith( visitor );
    
    return visitor;
  }
  
  /**
   * Returns the encoding of the given response.
   * @param response The response to use.
   * @return The encoding of the response, or null if the response does not
   * have an encoding.
   */
  private static String getResponseEncoding( final ProxyResponseMessage response )
  {
    if( response.containsHeader( "Content-Type" ) )
    {
      final String[] mediaType = response.getHeader( "Content-Type" ).split( ";" );
      
      if( ( mediaType.length >= 2 ) && ( mediaType[1].startsWith( "charset" ) ) )
      {
        return mediaType[1].split( "=" )[0];
      }
    }
    return null;
  }
  
  /**
   * Adds the given page to the pivot.<br/>
   * This method will also add the given page to the list of child pages of
   * the parent (if the page is unknown) and add a new vector for
   * the given pivot to the given page.
   * @param pivot The pivot to add the page to.
   * @param info The information about the page to be added.
   * @param parentPage The parent of the page to be added.
   */
  private static void addPageFor( final Pivot pivot, final VFCLinkInformation info, 
                                 final VFCPage parentPage )
  {
        
    final VFCPage currentPage = info.getPage( parentPage );
    
    VFCVector parentVector;
    
    parentPage.addChildPage( currentPage );
    
    //check the visited link type
    switch( info.getLinkType( ) )
    {
      case ALINK: //(check) if the page is an alink
        //(check) if the vector is from pivot (distance == 0)
        parentVector = parentPage.getVector( pivot.getPage( ).getUrl( ) );
        if( parentVector.getDistance( ) == 0 )
        {
          //set the vfc vector as parent + 2, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ) + 2, 0, 0 ) );
          
        }
        else //else (vector is not pivot)
        {
          //set the vfc vector as parent + 1, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
        }
        break;
      case FRAME:
        //set the vfc vector as parent, 0, 0
        parentVector = parentPage.getVector( pivot.getPage( ).getUrl( ) );
        currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ), 0, 0 ) );
        break;
      case IMAGE:
        //(check) if the vector is from pivot (distance == 0)
        parentVector = parentPage.getVector( 
            pivot.getPage( ).getUrl( ) );
        if( parentVector.getDistance( ) == 0 )
        {
          //set the vfc vector as parent + 1, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
          
        }
        else //else (vector is not pivot)
        {
          //set the vfc vector as parent, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ), 0, 0 ) );
          
        }
        break;
      case LINK:
        //(check) if the vector is from pivot (distance == 0)
        parentVector = parentPage.getVector( 
            pivot.getPage( ).getUrl( ) );
        if( parentVector.getDistance( ) == 0 )
        {
          //set the vfc vector as parent + 1, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
          
        }
        else //else (vector is not pivot)
        {
          //set the vfc vector as parent, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ), 0, 0 ) );
          
        }
        break;
      case SCRIPT:
        //(check) if the vector is from pivot (distance == 0)
        parentVector = parentPage.getVector( 
            pivot.getPage( ).getUrl( ) );
        if( parentVector.getDistance( ) == 0 )
        {
          //set the vfc vector as parent + 1, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
          
        }
        else //else (vector is not pivot)
        {
          //set the vfc vector as parent, 0, 0
          currentPage.addVector( pivot.getPage( ).getUrl( ), 
              new VFCVector( parentVector.getDistance( ), 0, 0 ) );
          
        }
        break;
      case UNKNOWN:
        //set the vfc vector as parent + 1, 0, 0
        parentVector = parentPage.getVector( 
            pivot.getPage( ).getUrl( ) );
        currentPage.addVector( pivot.getPage( ).getUrl( ), 
            new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
        break;
      default:
        break;
    }
    
    pivot.addNewPage( currentPage );    
  }
  
  /**
   * This method merges the parsed pages with the list of child pages of the 
   * given parent page.
   * @param parentPage The parent page that was parsed.
   * @param visitor The result of the parsing.
   */
  public static void mergeParsedPages( final VFCPage parentPage, final StandardVFCLinkVisitor visitor )
  {
    final SortedSet< VFCLinkInformation > visitedPages = visitor.getLinkSet( );
    
    //Iterate in the list of child pages
    List< VFCPage > toRemove = new ArrayList<>(
        Math.min( visitor.getSize( ), parentPage.getNumberChildPages( ) ) );
    
    for( VFCPage page : parentPage.getChildPages( ) )
    {
      //(check) if the page is NOT in the visited link list
      if( !visitor.containsUrl( page.getUrl( ) ) )
      {
        toRemove.add( page );        
      }
    }
    
    final Collection< List< Pivot > > parentPivots = 
        Users.instance( ).getPivotsFor( parentPage ).values( );
    
    for( VFCPage page : toRemove )
    {
      
      for( List< Pivot > pivots : parentPivots )
      {
      
        for( Pivot pivot : pivots )
        {
        
          //remove the vfc vector for this pivot
          page.removeVector( pivot.getPage( ).getUrl( ) );
          
          //remove this page from this pivot
          pivot.removePage( page );
        
        }
        
      }
      
      //remove this page from the list of child pages
      parentPage.removeChildPage( page.getUrl( ) );
      
    }
    
    toRemove.clear();
    
    //Iterate in the list of new child pages
    for( VFCLinkInformation info : visitedPages )
    {
      //(check) if the page is NOT a current child
      if( parentPage.containsChildPage( info.getUrlName( ) ) )
      {
        for( List< Pivot > pivots : parentPivots )
        {
          for( Pivot pivot : pivots )
          {
            //add this page to this pivot, a vector to the given pivot
            // and the page to the list of child pages of the given parent
            // page.
            VFCUtilityFunctions.addPageFor( pivot, info, parentPage );
          }
        }
      }
    }
  }
  
  /**
   * Adds the parsed pages in the respective consistency zones.
   * @param parentPage The parsed page (that is parent of all inserted pages). 
   * @param visitor The visitor that visited the parent page.
   */
  public static void addNewParsedPages( final VFCPage parentPage, final StandardVFCLinkVisitor visitor )
  {
    //add the parsed pages in the respective consistency zones
    final SortedSet< VFCLinkInformation > visitedPages = visitor.getLinkSet( );
    
    //get the pivots for the parent page
    final Map< User, List< Pivot > > pivots = Users.instance( ).getPivotsFor( parentPage );
    
    //for each page pivot (this pivot is already included) 
    for( List< Pivot > pivotList : pivots.values( ) )
    {
      for( Pivot pivot : pivotList )
      {
        for( VFCLinkInformation info : visitedPages )
        {
          //Add each page to the each pivot
          VFCUtilityFunctions.addPageFor( pivot, info, parentPage );
        }
      }
    }
  }
  
}
