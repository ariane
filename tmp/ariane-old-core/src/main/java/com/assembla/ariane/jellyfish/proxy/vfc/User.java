/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.vfc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Objects;

/**
 * This class represents an User.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public class User implements Serializable
{
  
  /** The class version. */
  private static final long serialVersionUID = 5892438774544419077L;

  /** The user name. */
  private String username;
  
  /** The number of requests made by the user. */
  private int frequency;
  
  /** The consistency definitions. */
  private ConsistencyZoneDefinitions definitions;
  
  /** The time of the last request from the user. */
  private DateTime age;
  
  /** An list of pivots of the user. */
  private List<Pivot> pivots;
  
  /** The logger object. */
  private Logger logger = LoggerFactory.getLogger( User.class );
  
  /** An comparator using the user name to compare users. */
  public static final UserNameComparator USER_NAME_COMPARATOR = new UserNameComparator( );
  
  /**
   * Creates a new user.
   * @param username The name of the new user.
   */
  public User( final String username ) 
  { 
    if( username == null )
    {
      throw new NullPointerException( );
    }
    
    this.username = username;
    this.frequency = 0;
    this.age = DateTime.now( );
    this.pivots = Collections.synchronizedList( new LinkedList<Pivot>( ) );
    this.definitions = null;
  }
  
  /**
   * Returns the date of the last user request.
   * @return The last user request date.
   */
  public final synchronized DateTime getAge( )
  {
    return this.age;
  }

  /**
   * Changes the date of the last user request, to now.
   */
  public final synchronized void changeAge( )
  {
    this.age = DateTime.now( );
  }

  /**
   * Returns the user name.
   * @return The name of this user.
   */
  public final synchronized String getUserName( )
  {
    return this.username;
  }

  /**
   * Returns the number of requests made by this user.
   * @return The user request number.
   */
  public final synchronized int getFrequency( )
  {
    return this.frequency;
  }

  /**
   * Returns this user VFC definitions.
   * @return The user definitions.
   */
  public final synchronized ConsistencyZoneDefinitions getDefinitions( )
  {
    return this.definitions;
  }
  
  /**
   * Checks if this user has any valid VFC definitions.
   * @return True, if it has, false otherwise.
   */
  public final synchronized boolean hasDefinitions( )
  {
    return this.definitions != null;
  }
  
  /**
   * Changes the user VFC definitions.
   * @param definitions The new definitions.
   */
  public final synchronized void setDefinitions( final ConsistencyZoneDefinitions definitions )
  {
    if( this.definitions != null )
    {
      this.definitions = definitions;
    }
  }

  /**
   * Returns an read-only list with all of the pivots of this user.
   * @return An list of user pivots.
   */
  public final synchronized List< Pivot > getPivots( )
  {
    return Collections.unmodifiableList( this.pivots );
  }
  
  /**
   * Checks if the given url is a pivot for this user.
   * @param url The url to check.
   * @return True, if the url is a pivot, false otherwise.
   */
  public final synchronized boolean hasPivot( final String url )
  {
    for( Pivot currentPivot : this.pivots )
    {
      if( currentPivot.getPage( ).getUrl( ).equals( url ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Adds the given pivot to this user.<br/>
   * Note that if there is already an pivot with the same url nothing is added.
   * @param pivot The pivot to add.
   */
  public final synchronized void addPivot( final Pivot pivot )
  { 
    for( Pivot currentPivot : this.pivots )
    {
      if( Pivot.PIVOT_URL_COMPARATOR.compare( pivot, currentPivot ) == 0 )
      {
        return;
      }
    }
    this.pivots.add( pivot );
    Collections.sort( this.pivots, Pivot.PIVOT_URL_COMPARATOR );
  }
  
  /**
   * Removes the given pivot from this user.
   * @param pivot The pivot to remove.
   */
  public final synchronized void removePivot( final Pivot pivot )
  {
    if( this.pivots.contains( pivot ) )
    {
      this.pivots.remove( pivot );
      Collections.sort( this.pivots, Pivot.PIVOT_URL_COMPARATOR );
    }
  }
  
  /**
   * Removes the pivot with the given url from this user.
   * @param url The url of the pivot to remove.
   */
  public final synchronized void removePivot( final String url )
  {
    Pivot selectedPivot = null;
    for( Pivot pivot : this.pivots )
    { 
      if( pivot.getPage( ).getUrl( ).equals( url ) )
      {
        selectedPivot = pivot;
        break;
      }
    }
    
    if( selectedPivot != null )
    {
      this.pivots.remove( selectedPivot );
      Collections.sort( this.pivots, Pivot.PIVOT_URL_COMPARATOR );
    }
  }
  
  /**
   * Checks if the given page url is known.<br/>
   * This method returns on first match.
   * @param url The url of the page to check.
   * @return True, if there is an page with the given url.
   */
  public boolean isPageKnown( final String url )
  {
    for( Pivot pivot : this.pivots )
    { 
      if( pivot.isPageKnown( url ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Checks if the given page is known.<br/>
   * This method returns on first match.
   * @param page The page to check for.
   * @return True, if the page is known.
   */
  public boolean isPageKnown( final VFCPage page )
  {
    for( Pivot pivot : this.pivots )
    { 
      if( pivot.isPageKnown( page ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Returns the page with the given url or null if the url is unknown.<br/>
   * If the debug logging is enabled, this method checks if
   * all received pages are equal that is, in the case the url is
   * known by more than one pivot, than then pages from both pivots should be 
   * exactly the same, since we only create one page and distribute it to
   * all pivots that have some relation to it.<br/>
   * See finalize() method on VFCPage, for more info.
   * @param url The url of the page to return.
   * @return The page with the given url, or null if the page does not exist.
   */
  public VFCPage peekPageInformation( final String url ) 
  { 
    
    VFCPage newpage = null;
    VFCPage oldpage = null;
    
    for( Pivot pivot : this.pivots )
    { 
      if( this.logger.isDebugEnabled( ) )
      { 
        newpage = pivot.peekPageInformation( url );
        
        if( ( oldpage != null ) && (  newpage.equals( oldpage ) ) )
        {
          oldpage = newpage;
        }
        else if( oldpage != null )
        {
          this.logger.warn( "There is an duplicated page {} in the consistency zones!", newpage.getUrl() );
        }
        
      }
      else
      {
        oldpage = pivot.peekPageInformation( url );
        
        if( oldpage != null )
        {
          break;
        }
      }
    }
    
    return oldpage;
  }
    
  /**
   * Checks if the given page is fresh or not.<br/>
   * This method returns immediately if the given page is not fresh in
   * some pivot, and does not check all pivots in that case.
   * @param page The page to check for freshness.
   * @return True, if the page is fresh.
   */
  public boolean isPageFresh( final VFCPage page ) 
  { 
    for( Pivot pivot : this.pivots )
    {
      if( !pivot.isPageFresh( page ) )
      {
        return false; 
      }
    }
    return true;
  }
  
  /**
   * Checks if the given page needs to be remapped.<br/>
   * This method returns immediately if the given page needs to be remaped
   * in some pivot, and does not check all pivots in that case.
   * @param page The page to check if it needs to be remapped.
   * @return True, if the page needs to be remapped.
   */
  public boolean isRemapNeeded( final VFCPage page ) 
  { 
    for( Pivot pivot : this.pivots )
    {
      if( pivot.isRemapNeeded( page ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * This method remapps the given page in the pivots that contain it.<br/>
   * Unlike the method isRemapNeeded() this method iterates thought 
   * all the pivot list.
   * @param page The page to remap.
   */
  public void remapAsNeeded( final VFCPage page ) 
  {  
    for( Pivot pivot : this.pivots )
    {
      pivot.remapAsNeeded( page );
    }
  }
  
  /**
   * Returns an read-only list with all the pivots that contain the given page.<br/>
   * Use isPageKnown(page) on each pivot and add if true.
   * @param page The page to return the pivots for.
   * @return An list of pivots for the given page.
   */
  public List<Pivot> getPivotsFor( final VFCPage page ) 
  {  
    final List< Pivot > pivots = new ArrayList<Pivot>( this.pivots.size( ) );
    
    for( Pivot pivot : this.pivots )
    { 
      if( pivot.isPageKnown( page ) )
      {
        this.pivots.add( pivot );
      }
    }
    
    return Collections.unmodifiableList( pivots );
  }

  /**
   * Checks if the given object is equal to this user.
   * @param obj The object to check for.
   * @return True, if the objects are equal.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  { 
    if( ( obj instanceof User ) && ( obj != null ) )
    {
      
      final User other = ( User ) obj;
      
      if( other.getUserName( ).equals( this.username ) &&
          other.getAge( ).equals( this.age ) &&
          ( other.getFrequency( ) == this.frequency ) )
      { 
        final List<Pivot> otherPivots = other.pivots;
        
        if( otherPivots.size( ) == this.pivots.size( ) )
        { 
          return this.pivots.containsAll( otherPivots );
        }
        
      }
      
    }
    return false;
  }

  /**
   * Calculates and returns an hash code for this user.
   * @return The calculated hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this.age, this.frequency, this.pivots, this.username );
  }

  /**
   * Returns an string representation of this user.
   * @return An string representing this user.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    final StringBuffer sb = new StringBuffer( );
    
    sb.append( String.format( "User name: %s\n", this.username ) );
    sb.append( String.format( "User Age: %s\n", this.age.toString( JellyfishConstants.DEBUG_FORMATTER ) ) );
    sb.append( String.format( "User Frequency: %d\n", this.frequency ) );
    sb.append( String.format( "User Pivot Count: %d\n", this.pivots.size( ) ) );
    
    return sb.toString( );
  }
  
  /**
   * This is an comparator that uses the user name to compare two users.
   * @author Fat Cat
   * @since 0.0.7
   * @version 1
   */
  public static final class UserNameComparator implements Comparator<User>, Serializable
  {

    /** The class version. */
    private static final long serialVersionUID = -1343862062107338468L;

    /**
     * Creates a comparator.
     */
    public UserNameComparator( )
    {
      
    }
    
    /**
     * Compares the given users using the user id.
     * @param left The left user.
     * @param right The right user.
     * @return -1, 0 or 1 as the left user id is less than, equal to, or 
     * greater than the right user id.
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override( )
    public int compare( final User left, final User right )
    {
      return left.getUserName( ).compareTo( right.getUserName( ) );
    }
    
  }
  
  
}
