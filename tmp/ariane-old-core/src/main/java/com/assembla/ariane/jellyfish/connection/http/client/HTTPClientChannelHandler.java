/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.client;

import io.netty.buffer.ChannelBuffers;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ExceptionEvent;
import io.netty.channel.MessageEvent;
import io.netty.channel.SimpleChannelUpstreamHandler;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import java.util.Date;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.uuid.Generators;
import com.assembla.ariane.jellyfish.connection.http.messages.requests.HTTPClientRequest;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageQueue;
import com.assembla.ariane.jellyfish.util.collections.client.HTTPChannelHolder;

/**
 * This class given an http request, handles it so that it can be properly 
 * treated.&nbsp;
 * By placing it in a request queue.
 * @author Fat Cat
 * @version 3
 * @since 0.0.1
 */
public class HTTPClientChannelHandler extends SimpleChannelUpstreamHandler
{ 
  
  private Logger logger;
  
  private String dockName;
  
  /**
   * Creates a new request handler.
   * @param dockName The dock where this handler belongs.
   */
  public HTTPClientChannelHandler( final String dockName )
  {
    this.logger = LoggerFactory.getLogger( HTTPClientChannelHandler.class );
    this.dockName = dockName;
  }
  
  /**
   * Method that given an errorcode string, returns the correspnding
   * HttpResponseStatus.
   * 
   * @param errorcode The errorcode string
   * @return The corresponding HttpResponseStatus or BAD_REQUEST if the 
   * string is invalid.
   */
  private static HttpResponseStatus getStatusFromErrorCode( final String errorcode )
  {    
    try
    {
      final int status = Integer.parseInt( errorcode );
      return HttpResponseStatus.valueOf( status );
    }
    catch( final NumberFormatException ex ) 
    { 
      return HttpResponseStatus.BAD_REQUEST; 
    }    
  }
  
  
  /**
   * Method called whenever a message is received.
   * @param ctx The channel context.
   * @param e The message event.
   * @throws Exception If there is some exception.
   */
  @Override( )
  public void messageReceived( ChannelHandlerContext ctx, MessageEvent e ) throws Exception
  {    
    this.logger.info( "Received a new request." );
    final HttpRequest request = ( HttpRequest ) e.getMessage( );    
    final HTTPClientRequest http_request = new HTTPClientRequest(
      request, Generators.timeBasedGenerator( ).generate( ) );
    
    this.logger.debug( "Checking the request." );
    http_request.check();
    this.logger.debug( "Checked the request." );
    
    //Check if there is an error condition...
    if( http_request.containsExtra( "error-number" ) )
    {
      handleError( ctx, http_request );
    }
    
    http_request.setDockName( this.dockName );
    
    //Insert the channel in the channel holder...
    this.logger.debug( "Inserted the channel in the channel holder." );
    HTTPChannelHolder.instance( ).put( http_request.getMessageId(),
                                       e.getChannel() );
    
    //Insert the message in the queue...
    this.logger.debug( "Inserting the request in the request queue." );
    try
    {
      ClientMessageQueue.instance( ).getRequests().put( HTTPClientRequest.messageConverter( http_request ) );
      this.logger.info( "Inserted the request in the request queue." );
    }
    catch( final InterruptedException ex )
    {
      Thread.currentThread( ).interrupt( );
    }
  }

  @Override
  public void exceptionCaught( ChannelHandlerContext ctx, ExceptionEvent e )
    throws Exception
  {
    if( ctx.getChannel().isWritable() )
    {
      this.logger.info( "Request contains an error." );
      final HttpResponse error_response = new DefaultHttpResponse(
        HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST);

      error_response.addHeader( HttpHeaders.Names.CONTENT_TYPE,
                                "text/html; charset=utf-8" );

      HttpHeaders.setDate( error_response, new Date() );

      error_response.addHeader( HttpHeaders.Names.SERVER,
                                JellyfishConstants.SERVER_NAME );

      final StringBuilder sb = new StringBuilder( );
      sb.append( "<html>" )
        .append( "<head>" )
        .append( "<title>400 error</error>" )
        .append( "</head>" )
        .append( "<body>" )
        .append( "<h1>400 error</h1>" )
        .append( "<p>There was an error while parsing the request!</p>")
        .append("<h2>Message</h2>")
        .append( "The request error message is: " )
        .append( e.getCause().getMessage() );

        if( e.getCause().getStackTrace().length > 0 )
        {
          sb.append("<h2>Stack Trace</h2>");
          for( StackTraceElement line : e.getCause().getStackTrace() )
          {
            if(line.isNativeMethod())
            {
              sb.append(String.format("<p>(Native) %s-%s:%d</p>",
                line.getClassName(), line.getMethodName(),
                line.getLineNumber()));
            }
            else
            {
              sb.append(String.format("<p>(Java) %s-%s:%d</p>",
                line.getClassName(), line.getMethodName(),
                line.getLineNumber()));
            }
          }
        }

      sb.append( "</body>" )
        .append( "</html>" );

      error_response.setContent( ChannelBuffers.copiedBuffer( sb.toString(),
                                                              CharsetUtil.UTF_8 ) );

      HttpHeaders.setContentLength( error_response,
                                    error_response.getContent().readableBytes() );

      ChannelFuture future = ctx.getChannel( ).write( error_response );
      future.addListener( ChannelFutureListener.CLOSE );
      this.logger.info( "Sent error response." );
    }
  }

  /**
   * Handles an error in the request parsing.
   * @param ctx The underlying netty context.
   * @param http_request The malformed request.
   */
  private void handleError( ChannelHandlerContext ctx,
                            HTTPClientRequest http_request )
  {
    this.logger.info( "Request contains an error." );
    final HttpResponse error_response = new DefaultHttpResponse(
        HttpVersion.HTTP_1_1,
        getStatusFromErrorCode( http_request.getExtra( "error-number" ) ) );
    
    HttpHeaders.setKeepAlive( error_response, http_request.isKeepAlive() );
    error_response.addHeader( HttpHeaders.Names.CONTENT_TYPE,
                         "text/html; charset=utf-8" );
    
    HttpHeaders.setDate( error_response, new Date() );
    
    error_response.addHeader( HttpHeaders.Names.SERVER,
                         JellyfishConstants.SERVER_NAME );
    
    this.logger.info( "Error is {}", http_request.getExtra( "error-message" ) );
    
    final StringBuilder sb = new StringBuilder( );
    sb.append( "<html>" );
    sb.append( "<head>" );
    sb.append( "<title>" )
      .append( http_request.getExtra( "error-number" ) )
      .append( " error</title>" );
    sb.append( "</head>" );
    sb.append( "<body>" );
    sb.append( "<h1>" )
      .append( http_request.getExtra( "error-number" ) )
      .append( " error</h1>" );
    sb.append( http_request.getExtra( "error-message" ) );
    sb.append( "</body>" );
    sb.append( "</html>" );
    
    error_response.setContent( ChannelBuffers.copiedBuffer( sb.toString(),
                                                       CharsetUtil.UTF_8 ) );
    
    HttpHeaders.setContentLength( error_response,
                                  error_response.getContent().readableBytes() );
    
    ChannelFuture future = ctx.getChannel( ).write( error_response );
    future.addListener( ChannelFutureListener.CLOSE );
    this.logger.info( "Sent error response." );
  }
  
}
