package com.assembla.ariane.jellyfish.proxy.storage.cache.query;

/**
 * Enumeration that represents an query response status.
 * @author Fat Cat
 */
public enum CacheResponseOperation
{
  /**
   * The query response is positive meaning that the
   * request was successfully done.
   */
  OK,
  /**
   * There was an error while processing the query response,
   * use getException() in order to retrieve the generated exception.
   */
  ERROR,
  /**
   * The query response is negative meaning that while there was no errors
   * while processing it, the request couldn't be successfully done.
   */
  FAIL,
  /**
   * Some unknown state happen, look at the logs for more information.
   */
  OTHER
}
