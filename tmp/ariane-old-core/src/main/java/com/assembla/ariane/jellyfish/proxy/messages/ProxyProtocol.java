/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages;

import java.io.Serializable;

/**
 * This class represents an protocol, including information about it's version.
 * @author Fat Cat
 * @version 1
 * @since 0.0.6
 */
public final class ProxyProtocol implements Serializable
{
  
  /** The class version. */
  private static final long serialVersionUID = 7654083938857691044L;
  
  /** The protocol version. */
  private String version;
  
  /** The protocol name. */
  private String protocolName;
  
  /**
   * Creates a new protocol object given the information about the protocol.
   * @param version The version information about the protocol.
   * @param protocolName The protocol name. 
   * @return An protocol object.
   */
  public static ProxyProtocol create( final String version, final String protocolName )
  {
    final ProxyProtocol proto = new ProxyProtocol( );
    proto.setProtocolName( protocolName );
    proto.setVersion( version );
    return proto;
  }
  
  /**
   * Creates a new protocol object given the information about the protocol. 
   * @param major The major version number.
   * @param minor The minor version number.
   * @param protocolName The protocol name. 
   * @return An protocol object.
   */
  public static ProxyProtocol create( final int major, final int minor, final String protocolName )
  {
    final ProxyProtocol proto = new ProxyProtocol( );
    proto.setProtocolName( protocolName );
    proto.setVersion( major, minor );
    return proto;
  }
  
  /**
   * Creates a new protocol object given the information about the protocol. 
   * @param major The major version number.
   * @param minor The minor version number.
   * @param revision Tge revision version number.
   * @param protocolName The protocol name. 
   * @return An protocol object.
   */
  public static ProxyProtocol create( final int major, final int minor, final int revision, final String protocolName )
  {
    final ProxyProtocol proto = new ProxyProtocol( );
    proto.setProtocolName( protocolName );
    proto.setVersion( major, minor, revision );
    return proto;
  }
  
  /**
   * Creates a new protocol object given the information about the protocol.<br/>
   * Note that the following must be true:
   * <code>protocolRepresentation.equals( ProxyProtocol.create( protocolRepresentation ).toString( ) )</code>
   * @param protocolRepresentation An string representation of the protocol.
   * @return An protocol object.
   */
  public static ProxyProtocol create( final String protocolRepresentation )
  {
    final ProxyProtocol proto = new ProxyProtocol( );
    if ( ( protocolRepresentation != null ) && ( protocolRepresentation.length( ) >= 2 ) )
    {
      final String[ ] div = protocolRepresentation.split( "/" );
      proto.setProtocolName( div[0] );
      proto.setVersion( div[1] );
    }
    return proto;
  }
  
  /** Creates a new protocol object. */
  private ProxyProtocol( )
  {
    this.version = "";
    this.protocolName = "";
  }
  
  // Version
  
  /**
   * Returns the version information as a string.
   * @return The version information.
   */
  public String getVersion( )
  {
    return this.version;
  }
  
  /**
   * Returns the major version number.
   * @return The major version number or -1 if the number does not exist.
   */
  public int getMajor( )
  {
    final String[ ] splited = this.version.split( "\\." );
    if ( splited.length >= 1 )
    {
      try
      {
        return Integer.parseInt( splited[ 0 ] );
      }
      catch( final NumberFormatException ex )
      {
        return -1;
      }
    }
    else
    {
      return -1;
    }
  }
  
  /**
   * Returns the minor version number.
   * @return The minor version number or -1 if it does not exist.
   */
  public int getMinor( )
  {
    final String[ ] splited = this.version.split( "\\." );
    if ( splited.length >= 2 )
    {
      try
      {
        return Integer.parseInt( splited[ 1 ] );
      }
      catch( final NumberFormatException ex )
      {
        return -1;
      }
    }
    else
    {
      return -1;
    }
  }
  
  /**
   * Returns the revision version number.
   * @return The revision version number or -1 if it does not exist.
   */
  public int getRevision( )
  {
    final String[ ] splited = this.version.split( "\\." );
    if ( splited.length >= 3 )
    {
      try
      {
        return Integer.parseInt( splited[ 2 ] );
      }
      catch( final NumberFormatException ex )
      {
        return -1;
      }
    }
    else
    {
      return -1;
    }
  }
  
  /**
   * Changes the version string.
   * @param version The new version string.
   */
  public void setVersion( final String version )
  {
    if ( version != null )
    {
      this.version = version;
    }
  }
  
  /**
   * Changes the version number.<br/>
   * Anything in the version is removed even a version string.
   * @param major The new major version number.
   * @param minor The new minor version number.
   */
  public void setVersion( final int major, final int minor )
  {
    final StringBuilder sb = new StringBuilder( );
    sb.append( major );
    sb.append( "." );
    sb.append( minor );
    this.version = sb.toString( );
  }
  
  /**
   * Changes the version number.<br/>
   * Anything in the version is removed even a version string.
   * @param major The new major version number.
   * @param minor The new minor version number.
   * @param revision The new revision version number.
   */
  public void setVersion( final int major, final int minor, final int revision )
  {
    final StringBuilder sb = new StringBuilder( );
    sb.append( major );
    sb.append( "." );
    sb.append( minor );
    sb.append( "." );
    sb.append( revision );
    this.version = sb.toString( );
  }
  
  // Protocol
  
  /**
   * Returns the protocol name.
   * @return The protocol name.
   */
  public String getProtocolName( )
  {
    return this.protocolName;
  }
  
  /**
   * Changes the protocol name.
   * @param protocolName The new protocol name.
   */
  public void setProtocolName( final String protocolName )
  {
    if ( protocolName != null )
    {
      this.protocolName = protocolName;
    }
  }
  
  // Others
  
  /**
   * Returns an string representation of this object.
   * @return An string representation of this object.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString()
  {
    final StringBuilder sb = new StringBuilder( );
    sb.append( this.protocolName.toUpperCase( ) );
    sb.append( "/" );
    sb.append( this.version );
    return sb.toString( );
  }
  
}
