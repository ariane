/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.storage.cache.query;

import java.util.UUID;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import java.io.Serializable;

/**
 * This represents an response from a query.
 * @author Fat Cat
 * @version 3
 * @since 0.0.5
 */
public class CacheResponse implements Serializable
{

  /** The serial version. */
  private static final long serialVersionUID = 6667343606093845935L;

  /** The operation done by the query.  */
  private final CacheQueryOperation cacheQueryOp;
  
  /** The query id. */
  private UUID queryId;
  
  /** The page given as a response. */
  private final ProxyPageElement responseElement;
  
  /** The response code. */
  private final CacheResponseOperation response;
  
  /**
   * Creates a new query response.
   * @param cacheOperation The operation done by the query.
   * @param queryId The query identifier.
   * @param response The response to the query.
   */
  public CacheResponse( final CacheQueryOperation cacheOperation,
                        final UUID queryId,
                        final CacheResponseOperation response )
  {
    this.cacheQueryOp = cacheOperation;
    this.queryId = queryId;
    this.response = response;
    this.responseElement = null;
  }
  
  /**
   * Creates a new query response.
   * @param cacheOperation The operation done by the query.
   * @param queryId The query identifier.
   * @param response The response to the query.
   * @param page The response page.
   */
  public CacheResponse( final CacheQueryOperation cacheOperation,
                        final UUID queryId,
                        final CacheResponseOperation response,
                        final ProxyPageElement page )
  {
    this.cacheQueryOp = cacheOperation;
    this.queryId = queryId;
    this.response = response;
    this.responseElement = page;
  }
  
  /**
   * Returns the operation done in the query.
   * @return The operation done in the query.
   */
  public final com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQueryOperation getCacheQueryOp( )
  {
    return this.cacheQueryOp;
  }
  
  /**
   * Returns the id of the associated query.
   * @return The id of the query.
   */
  public final UUID getQueryId( )
  {
    return this.queryId;
  }

  /**
   * Returns the page element given as a response.
   * @return The response page element.
   */
  public final ProxyPageElement getResponseElement( )
  {
    return this.responseElement;
  }
  
  /**
   * Returns the response code of this query operation.
   * @return The response code of this query operation.
   */
  public final com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponseOperation getResponse( )
  {
    return this.response;
  }
}
