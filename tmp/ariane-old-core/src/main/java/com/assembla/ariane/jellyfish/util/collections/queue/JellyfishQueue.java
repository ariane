/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents an generic request and response queue.
 * @author Fat Cat
 * @param <S> The type of requests.
 * @param <R> The type of the responses.
 * @version 2
 * @since 0.0.3
 */
public abstract class JellyfishQueue< S, R >
{
  /** This is the request queue. */
  protected RequestQueue< S > requests;
  
  /** This is the response queue. */
  protected ResponseQueue< R > responses;
  
  /** This is the logger object. */
  protected Logger logger;
  
  /**
   * Creates a new queue.
   */
  protected JellyfishQueue( )
  {
    this.requests = new RequestQueue< S >( this.sendSize( ) );
    this.responses = new ResponseQueue< R >( this.receiveSize( ) );
    this.logger = LoggerFactory.getLogger( JellyfishQueue.class );
  }
  
  /**
   * Returns the size of the request queue.
   * @return The size of the request queue.
   */
  protected abstract int sendSize( );
  
  /**
   * Returns the size of the response queue.
   * @return The size of the response queue.
   */
  protected abstract int receiveSize( );
  
  /**
   * Clears both the request and response queue.
   */
  public void clearAll( )
  {
    this.requests.clear( );
    this.responses.clear( );
  }
  
  /**
   * Returns the request queue.
   * @return the request queue.
   */
  public RequestQueue< S > getRequests( )
  {
    return this.requests;
  }
  
  /**
   * Returns the response queue.
   * @return the response queue.
   */
  public ResponseQueue< R > getResponses( )
  {
    return this.responses;
  }
}
