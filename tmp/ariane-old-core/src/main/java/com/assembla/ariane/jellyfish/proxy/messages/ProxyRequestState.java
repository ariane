/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages;

/**
 * State of a proxy page element.
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 */
public enum ProxyRequestState
{
  /**
   * The request was received.
   */
  RECEIVED,
  
  /**
   * The request is new.
   */
  NEW,
  
  /**
   * The request is waiting for processing.
   */
  AWAITING_PROCESSING,
  
  /**
   * The request is being processed.
   */
  PROCESSING,
  
  /**
   * The request is waiting for more info from the server.
   */
  MORE_INFO,
  
  /**
   * The request has received information from the server.
   */
  MORE_INFO_RECEIVED,
  
  /**
   * The request has been processed.
   */
  PROCESSED,
  
  /**
   * The request has been sent.
   */
  SENT,
  
  /**
   * The request has been archived.
   */
  ARCHIVED
}
