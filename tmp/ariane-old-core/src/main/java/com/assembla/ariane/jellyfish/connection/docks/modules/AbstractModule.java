/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.docks.modules;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.Logger;

/**
 * Abstract class that is inherited by all the receptors and transmitters,
 * and allows the control of them.
 * @author Fat Cat
 * @since 0.0.1
 * @version 2
 */
public abstract class AbstractModule implements Runnable
{
  /** The configuration. */
  protected HierarchicalConfiguration config;
  
  /** If the module is running. */
  protected boolean running;
  
  /** The logger. */
  protected Logger logger;
  
  /**
   * Changes the state of this module.
   * @param running The new state of this module.
   */
  protected synchronized void setRunning( final boolean running )
  {
    if( this.running != running )
    {
      this.running = running;
    }
  }
  
  /**
   * Constructor used to configure the module.
   * @param config The configuration object.
   */
  public AbstractModule( final HierarchicalConfiguration config )
  {
    this.config = config;
    this.running = false;
  }
  
  /**
   * Method called whenever the thread needs to stop.
   */
  public abstract void stop( );
  
  /**
   * Method called whenever the thread needs to run.
   * @see java.lang.Runnable#run()
   */
  public abstract void run( );

  /**
   * Checks if this module is running or not.
   * @return True if it is, false otherwise
   */
  public synchronized boolean isRunning( )
  {
    return this.running;
  }
  
}
