/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;

/**
 * Utility class that contains methods to check for several request and page
 * types.
 * @author Fat Cat
 * @since 0.0.6
 * @version 1
 */
public final class RequestType
{
  
  /** Private constructor of this utility class. */
  private RequestType( )
  {
  }
  
  /**
   * This method determines if this request is an VFC client operation, that
   * must be satisfied by this cache, such as a pivot addition or removal.
   * @param aRequest The request to check out.
   * @return True, if the request is a client operation, false otherwise.
   */
  public static boolean isClientOperation( final ProxyPageElement aRequest )
  {
    String host = JellyfishConstants.EMPTY_STRING;
    
    if( aRequest.hasRequest( ) && 
        aRequest.getRequest( ).containsHeader( "Host" ) )
    {
      if( aRequest.getRequest( ).getHeader( "Host" ).toLowerCase( ).startsWith( "http://" ) )
      {
        host = aRequest.getRequest( ).getHeader( "Host" );
      }
      else
      {
        host = String.format( "http://%s", aRequest.getRequest( ).getHeader( "Host" ) );
      }
      
      //Translate the address to an IP
      try
      {
        final InetAddress addr = InetAddress.getByName( host );
        addr.getHostAddress( );
        
        //Check if the ip matches any network ip...
        for( NetworkInterface iface : Collections.list( NetworkInterface.getNetworkInterfaces( ) ) )
        {
          for( InetAddress netAddr : Collections.list( iface.getInetAddresses( ) ) )
          {
            if( netAddr.equals( addr ) )
            {
              return true;
            }
          }
        }
      }
      catch( final UnknownHostException | SocketException e )
      { 
      }
    }
    return false;
  }
  
  /**
   * Checks if the given request is a get or head request.
   * @param aRequest The request to check for.
   * @return True, if it is a get or head request.
   */
  public static boolean isGetOrHeadRequest( final ProxyPageElement aRequest )
  {
    boolean retval = false;
    if( aRequest.hasRequest( ) )
    {
      retval = aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) ||
               aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "head" );
    }
    return retval;
  }

  /**
   * Checks if the given request is conditional.
   * @param aRequest The request to check if it is conditional.
   * @return True, if it is.
   */
  public static boolean isConditionalRequest( final ProxyPageElement aRequest )
  {
    boolean retval = false;
    if( aRequest.hasRequest( ) )
    {
      retval = aRequest.getRequest( ).containsHeader( "If-Match" ) || 
            aRequest.getRequest( ).containsHeader( "If-None-Match" ) || 
            aRequest.getRequest( ).containsHeader( "If-Modified-Since" ) || 
            aRequest.getRequest( ).containsHeader( "If-Unmodified-Since" );
    }
    return retval;
  }
  
  /**
   * Checks if the given page is virtually cacheable or not.
   * @param aRequest The page to check for.
   * @return True, if it is virtually cacheable.
   */
  public static boolean isVirtuallyCacheable( final ProxyPageElement aRequest )
  {
    boolean retval = false; 
    if( aRequest.hasRequest( ) )
    {
      retval = !aRequest.getRequest( ).containsExtra( "virtual-no-cache" );
    }    
    if( aRequest.getTransitoryResponse( ) != null ) //the server cache-control is independent from client cache-control.
    {
      retval = !aRequest.getTransitoryResponse( ).containsExtra( "virtual-no-cache" );
    }
    else if( aRequest.hasResponse( ) )
    {
      retval = !aRequest.getRequest( ).containsExtra( "virtual-no-cache" );
    }
    return retval;
  }
  
  /**
   * Checks if the given request is virtually cacheable or not.
   * @param aRequest The request to check for.
   * @return True, if it is virtually cacheable.
   */
  public static boolean isVirtuallyCacheableRequest( final ProxyPageElement aRequest )
  {
    boolean retval = false; 
    if( aRequest.hasRequest( ) )
    {
      retval = !aRequest.getRequest( ).containsExtra( "virtual-no-cache" );
    }    
    return retval;
  }

  /**
   * Checks if the given page is cacheable or not.
   * @param aRequest The page to check for.
   * @return True, if it is cacheable.
   */
  public static boolean isCacheable( final ProxyPageElement aRequest )
  {
    boolean retval = false; 
    if( aRequest.hasRequest( ) )
    {
      retval = !( aRequest.getRequest( ).containsExtra( "no-cache" ) || aRequest.getRequest( ).containsExtra( "virtual-no-cache" ) );
    }    
    if( aRequest.getTransitoryResponse( ) != null ) //the server cache-control is independent from client cache-control.
    {
      retval = !( aRequest.getTransitoryResponse( ).containsExtra( "no-cache" ) || aRequest.getTransitoryResponse( ).containsExtra( "virtual-no-cache" ) );
    }
    else if( aRequest.hasResponse( ) )
    {
      retval = !( aRequest.getResponse( ).containsExtra( "no-cache" ) || aRequest.getRequest( ).containsExtra( "virtual-no-cache" ) );
    }
    return retval;
  }
  
  /**
   * Checks if the given request is cacheable or not.
   * @param aRequest The request to check for.
   * @return True, if it is cacheable.
   */
  public static boolean isCacheableRequest( final ProxyPageElement aRequest )
  {
    boolean retval = false; 
    if( aRequest.hasRequest( ) )
    {
      retval = !( aRequest.getRequest( ).containsExtra( "no-cache" ) || aRequest.getRequest( ).containsExtra( "virtual-no-cache" ) );
    }    
    return retval;
  }

  /**
   * Checks if the given request is a range request.
   * @param aRequest The request to check.
   * @return True, if it is a range request.
   */
  public static boolean isRangeRequest( final ProxyPageElement aRequest )
  {
    boolean retval = false;
    if( aRequest.hasRequest( ) )
    {
      retval = aRequest.getRequest( ).hasRanges( ) ||
            ( aRequest.getRequest( ).hasRanges( ) &&
              aRequest.getRequest( ).containsHeader( "If-Range" ) );
    }
    if( aRequest.hasResponse( ) && retval ) //an range response must have an range request 
    {
      retval = aRequest.getResponse( ).hasRanges( );
    }
    else if( ( aRequest.getTransitoryResponse( ) != null ) && retval )
    {
      retval = aRequest.getTransitoryResponse( ).hasRanges( );
    }
    return retval;
  }
  
}
