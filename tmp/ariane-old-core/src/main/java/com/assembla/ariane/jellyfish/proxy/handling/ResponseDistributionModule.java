/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.handling;

import java.util.concurrent.TimeUnit;
import com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule;
import com.assembla.ariane.jellyfish.util.collections.ProxyMessageHolder;
import com.assembla.ariane.jellyfish.util.collections.server.ServerMessageQueue;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;

/**
 * Module that collects the responses from the servers and sends them back
 * to the processing modules.
 * @author Fat Cat
 * @version 3
 * @since 0.0.2
 */
public class ResponseDistributionModule extends AbstractModule
{
  
  /** The current start handler. */
  private IHandler< ProxyPageElement > startingHandler;
  
  /**
   * Constructor for an response distributor module.
   * @param config The configuration to use.
   */
  public ResponseDistributionModule( final HierarchicalConfiguration config )
  {
    super( config );
    this.logger = LoggerFactory.getLogger( ResponseDistributionModule.class );
  }
  
  /**
   * Stops the response distribution module.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#stop()
   */
  @Override( )
  public void stop( )
  {
    this.logger.info( "Trying to stop the response distributor." );
    this.setRunning( false );
  }
  
  /**
   * Starts the response distribution module.
   * @see com.assembla.ariane.jellyfish.connection.docks.modules.AbstractModule#run()
   */
  @Override( )
  public void run( )
  {
    try
    {
      ProxyResponseMessage element = null;
      this.setRunning( true );
      this.logger.info( "Response distributor is running!" );
      while( true )
      {
        element = null;
        
        if ( !this.isRunning( ) )
        {
          this.logger.info( "Response distributor is stopped" );
          break;
        }
        
        //Check if the processing response list has anything...
        this.logger.debug( "Checking response distributor..." );
        
        try
        {
          while( element == null )
          {  
            element = ServerMessageQueue.instance( ).getResponses( ).poll( 
                JellyfishConstants.DEFAULT_WAITING_TIME, TimeUnit.SECONDS );
            
            if ( !this.isRunning( ) )
            {
              this.logger.info( "Response distributor is stopped" );
              break;
            }
          }
        }
        catch( final InterruptedException e )
        {
        }
        
        if ( element != null )
        {
          this.logger.info( "There is an response for {}", element.getMessageIdAsString( ) );
          
          //Since an response when received isn't associated with an proxy page
          //we use an proxy page map, that contains all the pages with requests 
          //that where sent to the server, in order to associate the response with 
          //the proxy page.
          
          if ( ProxyMessageHolder.instance( ).contains( element.getMessageId( ) ) )
          {
            final ProxyPageElement page = ProxyMessageHolder.instance( ).remove( element.getMessageId( ) );
            
            if( !element.hasResponseTime( ) )
            {
              element.setResponseTime( page.getResponse( ).getResponseTime( ) );
            }
            page.setTransitoryData( element );
            page.setState( ProxyRequestState.MORE_INFO_RECEIVED );
            
            this.logger.info( "Server distributor is sendind response with id {}", page.getRequest( ).getMessageId( ) );
            
            if ( !this.isRunning( ) )
            {
              this.logger.info( "Response distributor is stopped" );
              break;
            }
            
            //start the default handler...
            if ( this.startingHandler != null )
            {
              this.startingHandler.start( page ); //this will exit when it is processed...
            }
            else
            {
              this.logger.warn( "Sorry no starting handler..." );
              throw new NullPointerException( "Sorry no starting handler..." );
            }
          }
          else
          {
            this.logger.warn( "There is a response for an unwanted message {}", element.getMessageId( ) );
            if ( !this.isRunning( ) )
            {
              this.logger.info( "Response distributor is stopped" );
              break;
            }
            try
            {
              ServerMessageQueue.instance( ).getResponses( ).put( element );
            }
            catch( final InterruptedException e )
            {
            }
          }
        }
      }
    }
    catch( final Exception ex )
    {
      this.logger.error( "Exception in ResponseDistributorModule", ex );
    }
  }
  
  /**
   * Returns the starting handler.
   * @return The starting hander.
   */
  public IHandler< ProxyPageElement > getStartingHandler( )
  {
    return this.startingHandler;
  }
  
  /**
   * Changes the starting handler.
   * @param handler The new starting handler.
   */
  public void setStartingHandler( final IHandler< ProxyPageElement > handler )
  {
    if ( handler != null )
    {
      this.startingHandler = handler;
    }
  }
}
