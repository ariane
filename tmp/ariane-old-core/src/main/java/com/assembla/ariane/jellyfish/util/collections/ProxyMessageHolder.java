/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that holds a map of all requests sent grouped by host name.
 * @author Fat Cat
 * @version 2
 * @since 0.0.2
 */
public final class ProxyMessageHolder
{
  /** Variable that stores the mapping holder. */
  private final Map< UUID, ProxyPageElement > mappings;
  
  /** An logger object. */
  private final Logger logger;
  
  /** The singleton object. */
  private static final ProxyMessageHolder inst = new ProxyMessageHolder( );
  
  /**
   * Returns the instance of this singleton. 
   * @return The id mapping singleton.
   */
  public static ProxyMessageHolder instance( )
  {
    return ProxyMessageHolder.inst;
  }
  
  /**
   * Creates a new map.
   */
  private ProxyMessageHolder( )
  {
    this.mappings = Collections.synchronizedMap( new HashMap< UUID, ProxyPageElement >( ) );
    this.logger = LoggerFactory.getLogger( ProxyMessageHolder.class );
  }
  
  /**
   * Clears all mappings.
   */
  public void clear( )
  {
    this.logger.debug( "Cleaning all the entries." );
    this.mappings.clear( );
  }
  
  /**
   * Checks if the given requestId is contained in this mapping.
   * @param requestId The request id
   * @return True, if it is.
   */
  public boolean contains( final UUID requestId )
  {
    return this.mappings.containsKey( requestId );
  }
  
  /**
   * Returns the page element associated with the given id.
   * @param requestId The request id
   * @return The page element associated with the id, or null if the host is unknown.
   */
  public ProxyPageElement get( final UUID requestId )
  {
    if ( this.contains( requestId ) ) 
    {
      return this.mappings.get( requestId );
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Checks if this mapping is empty.
   * @return True, if it is.
   */
  public boolean isEmpty( )
  {
    return this.mappings.isEmpty( );
  }
  
  /**
   * Adds the given page element to the mapping, changing the current value.
   * @param element The page element to add (the requestId of the element is used)
   */
  public void add( final ProxyPageElement element )
  {
    if ( this.contains( element.getCurrentUUID( ) ) )
    {
      this.logger.debug( "Modifying the entry {}", element.getCurrentUUID( ).toString( ) );
    }
    else
    {
      this.logger.debug( "Adding an entry {}", element.getCurrentUUID( ).toString( ) );
    }
    this.mappings.put( element.getCurrentUUID( ), element );
  }
  
  /**
   * Removes the mapping for the given request id.
   * @param requestId The request id.
   * @return The associated page element pair if any or null if the id is unknown.
   */
  public ProxyPageElement remove( final UUID requestId )
  {
    if ( this.contains( requestId ) )
    {
      final ProxyPageElement entry = this.mappings.remove( requestId );
      this.logger.debug( "Removing the entry {}", entry.getCurrentUUID( ) );
      return entry;
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Sets the request time for the given page id.
   * @param requestId The request id of the page.
   * @param requestTime The page request time.
   */
  public void setRequestTime( final UUID requestId, final DateTime requestTime )
  {
    if( this.contains( requestId ) )
    {
      this.get( requestId ).getRequest( ).setRequestTime( requestTime );
    }
  }
  
  /**
   * Sets the response time for the given page id.
   * @param requestId The request id of the page.
   * @param responseTime The page response time.
   */
  public void setResponseTime( final UUID requestId, final DateTime responseTime )
  {
    if( this.contains( requestId ) )
    {
      if( this.get( requestId ).getTransitoryResponse( ) != null )
      {
        this.get( requestId ).getTransitoryResponse( ).setResponseTime( responseTime );
      }
      else
      {
        this.get( requestId ).getResponse( ).setResponseTime( responseTime );
      }
    }
  }
  
  /**
   * Returns the number of entries in this mapping.
   * @return The entry number.
   */
  public int size( )
  {
    return this.mappings.size( );
  }
  
  /**
   * Returns the internal map containing the entries.
   * @return The internal map in read-only mode.
   */
  public synchronized Map< UUID, ProxyPageElement > getMappings( )
  {
    return Collections.unmodifiableMap( this.mappings );
  }
}
