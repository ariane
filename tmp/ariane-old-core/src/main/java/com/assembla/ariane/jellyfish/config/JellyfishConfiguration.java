/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.config;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class initializes the jellyfish configuration.
 * @author Fat Cat
 * @since 0.0.1
 * @version 3 
 */
public class JellyfishConfiguration
{
  /** The logger. */
  private final Logger logger = LoggerFactory.getLogger( JellyfishConfiguration.class );
    
  private XMLConfiguration config;
  
  /**
   * Instantiates a new empty jellyfish configuration.
   */
  public JellyfishConfiguration()
  {
    this.config = new XMLConfiguration( );
  }
  
  /**
   * Instantiates a new jellyfish configuration, using the specified configfile.
   * @param configfile The name of the configuration file to use.
   * @throws IOException If the file cannot be read.
   * @throws ConfigurationException If there is some error.
   */
  public JellyfishConfiguration( final String configfile ) throws ConfigurationException, IOException
  {
    this.config = new XMLConfiguration( );
    this.init( configfile );
  }
  
  /**
   * Checks if the given key is in this configuration object.
   * @param key the key to check
   * @return true, if successful
   */
  public boolean containsKey( final String key )
  {
    return this.config.containsKey( key );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public BigDecimal getBigDecimal( final String key, final BigDecimal defaultValue )
  {
    return this.config.getBigDecimal( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public BigInteger getBigInteger( final String key, final BigInteger defaultValue )
  {
    return this.config.getBigInteger( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public boolean getBoolean( final String key, final boolean defaultValue )
  {
    return this.config.getBoolean( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public byte getByte( final String key, final byte defaultValue )
  {
    return this.config.getByte( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public double getDouble( final String key, final double defaultValue )
  {
    return this.config.getDouble( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public float getFloat( final String key, final float defaultValue )
  {
    return this.config.getFloat( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public int getInt( final String key, final int defaultValue )
  {
    return this.config.getInt( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public List< Object > getList( final String key, final List< Object > defaultValue )
  {
    return this.config.getList( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public long getLong( final String key, final long defaultValue )
  {
    return this.config.getLong( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public short getShort( final String key, final short defaultValue )
  {
    return this.config.getShort( key, defaultValue );
  }
  
  /**
   * Returns the value of the given key.
   * @param key The key of the value to return.
   * @param defaultValue the default value
   * @return The value of the key.
   */
  public String getString( final String key, final String defaultValue )
  {
    return this.config.getString( key, defaultValue );
  }
  
  /**
   * Initializes this configuration singleton...
   * @param config_filename The configuration filename
   * @throws IOException If the filename does not exist
   * @throws ConfigurationException If there was an exception while parsing the file
   */
  public void init( final String config_filename ) throws IOException, ConfigurationException
  {
    //the file was already initialized
    if( !this.config.isEmpty( ) ) 
    {
      return;
    }
    
    if( ( new File( config_filename ) ).exists( ) )
    {
      this.config = new XMLConfiguration( config_filename );
      
      this.config.setThrowExceptionOnMissing( false );
      this.config.setLogger( LogFactory.getLog( this.getClass( ) ) );
    }
    else
    {
      this.logger.error( "Configuration file does not exist, exiting..." );
      throw new IOException( "The configuration file does not exist" );
    }
  }
  
  /**
   * Checks if the configurations are empty.
   * @return true, if they are empty.
   */
  public boolean isEmpty( )
  {
    return this.config.isEmpty( );
  }

  /**
   * Returns the inner configuration object.
   * @return An read-only apache configuration object.
   */
  public final HierarchicalConfiguration getConfig()
  {
    return this.config;
  }
}
