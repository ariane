/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.http.server;

import io.netty.buffer.ChannelBuffers;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ExceptionEvent;
import io.netty.channel.MessageEvent;
import io.netty.channel.SimpleChannelUpstreamHandler;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import java.util.UUID;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import com.assembla.ariane.jellyfish.connection.http.messages.responses.HTTPServerResponse;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.util.collections.ProxyMessageHolder;
import com.assembla.ariane.jellyfish.util.collections.server.HTTPSentIdHolder;
import com.assembla.ariane.jellyfish.util.collections.server.ServerMessageQueue;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class handles all responses from a server.
 * @author Fat Cat
 * @version 2
 * @since 0.0.2
 */
public class HTTPServerChannelHandler extends SimpleChannelUpstreamHandler
{

  /** The name of the containing dock. */
  private String dockName;
  
  /** The logger object. */
  private final Logger logger;
  
  /**
   * Method called whenever an message is received from the server.
   * @param ctx The channel handler context.
   * @param e The event data.
   * @throws Exception If some processing error happens.
   */
  @Override( )
  public void messageReceived( ChannelHandlerContext ctx,
                               MessageEvent e ) throws Exception
  {
    try
    {
      final String hostname = e.getChannel( ).getRemoteAddress( ).toString( );
      final UUID requestId = HTTPSentIdHolder.instance( ).getId( hostname );
      
      //Set the response time...
      ProxyMessageHolder.instance( ).setResponseTime( requestId, DateTime.now( ) );
      
      //Add the response to the http server queue.
      HTTPServerResponse http_response = new HTTPServerResponse(
        requestId, ( HttpResponse ) e.getMessage( ) );
      http_response.setDockName( this.dockName );
      http_response.check();
      ServerMessageQueue.instance().getResponses().put(
        HTTPServerResponse.messageConverter( http_response ) );
      
      //Close the connection
      e.getChannel( ).close( );
    }
    catch( final Exception ex )
    {
      this.logger.error( "Error parsing the server response.", ex );
    }
  }

  @Override
  public void exceptionCaught( ChannelHandlerContext ctx,
                               ExceptionEvent e )
    throws Exception
  {
    if( ctx.getChannel().isWritable() )
    {
      this.logger.info( "Request contains an error." );
      HttpResponse error_response = new DefaultHttpResponse(
         HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR);

      error_response.addHeader( HttpHeaders.Names.CONTENT_TYPE,
                                "text/html; charset=utf-8" );

      HttpHeaders.setDate( error_response, new java.util.Date() );

      error_response.addHeader( HttpHeaders.Names.SERVER,
                                JellyfishConstants.SERVER_NAME );

      final StringBuilder sb = new StringBuilder( );
      sb.append( "<html>" )
        .append( "<head>" )
        .append( "<title>500 error</error>" )
        .append( "</head>" )
        .append( "<body>" )
        .append( "<h1>500 error</h1>" )
        .append( "<p>There was an error while parsing the response!</p>")
        .append("<h2>Message</h2>")
        .append( "The request error message is: " )
        .append( e.getCause().getMessage() );

      if( e.getCause().getStackTrace().length > 0 )
      {
        sb.append("<h2>Stack Trace</h2>");
        for( StackTraceElement line : e.getCause().getStackTrace() )
        {
          if(line.isNativeMethod())
          {
            sb.append(String.format("<p>(Native) %s-%s:%d</p>",
              line.getClassName(), line.getMethodName(), line.getLineNumber()));
          }
          else
          {
            sb.append(String.format("<p>(Java) %s-%s:%d</p>",
              line.getClassName(), line.getMethodName(), line.getLineNumber()));
          }
        }
      }

      sb.append( "</body>" )
        .append( "</html>" );

      error_response.setContent(
        ChannelBuffers.copiedBuffer( sb.toString(), CharsetUtil.UTF_8 ) );

      HttpHeaders.setContentLength( error_response,
        error_response.getContent().readableBytes() );

      ChannelFuture future = ctx.getChannel( ).write( error_response );
      future.addListener( ChannelFutureListener.CLOSE );
      this.logger.info( "Sent error response." );
    }
  }


  /**
   * Creates a new http server channel handler.
   * @param dockName The name of the associated dock.
   */
  public HTTPServerChannelHandler( String dockName )
  {
    this.logger = LoggerFactory.getLogger( HTTPServerChannelHandler.class );
    this.dockName = dockName;
  }
}
