/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.vfc;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import com.fasterxml.uuid.Generators;
import com.google.common.base.Objects;
import com.assembla.ariane.jellyfish.util.collections.cache.CacheMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.html.visitors.VisitedLinkType;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQueryOperation;

/**
 * This class represents an VFCPage. 
 * @author Fat Cat
 * @version 1
 * @since 0.0.7
 */
public final class VFCPage implements Serializable
{

  /** The class version. */
  private static final long serialVersionUID = -8733754716360266433L;

  /** The type of this page. */
  private VisitedLinkType pageType;
  
  /** The url of the page. */
  private String url;
  
  /** The id of the page. */
  private UUID pageId;
  
  /** The VFCVector of the page. */
  private Map<String, VFCVector> vector;
  
  /** The child pages. */
  private Set< VFCPage > linkedPages;
  
  /** Is this page cached? */
  private boolean isCached;
  
  /** An comparator that takes in attention only the page url. */
  public static final VFCPageUrlComparator URLComparator = new VFCPageUrlComparator( );
  
  /**
   * Constructor that creates a new empty page.
   */
  public VFCPage( )
  {
    this.url = JellyfishConstants.EMPTY_STRING;
    this.isCached = false;
    this.linkedPages = Collections.synchronizedSet( Collections.newSetFromMap( new java.util.WeakHashMap< VFCPage, Boolean >( ) ) );
    this.pageId = Generators.timeBasedGenerator( ).generate( );
    this.vector = new HashMap<>( );
    this.pageType = VisitedLinkType.UNKNOWN; 
  }
  
  /**
   * Constructor that creates a new page.
   * @param url The url of the new page.
   * @param pageId The id of this page.
   */
  public VFCPage( final String url, final UUID pageId )
  {
    this.url = ( url != null ) ? url : JellyfishConstants.EMPTY_STRING;
    this.isCached = false;
    this.linkedPages = Collections.synchronizedSet( Collections.newSetFromMap( new java.util.WeakHashMap< VFCPage, Boolean >( ) ) );
    this.pageId = ( pageId != null ) ? pageId : Generators.timeBasedGenerator( ).generate( );
    this.vector = new HashMap<>( );
    this.pageType = VisitedLinkType.UNKNOWN;
  }
  
  /**
   * Returns the type of this page.
   * @return The page type.
   */
  public synchronized VisitedLinkType getPageType( )
  {
    return this.pageType;
  }

  /**
   * Checks if this page is cached or not.
   * @return True, if it is.
   */
  public synchronized boolean isCached( )
  {
    return this.isCached;
  }
  
  /**
   * Sets this page as cached.
   */
  public synchronized void setCached( )
  {
    if ( !this.isCached )
    {
      this.isCached = true;
    }
  }
  
  /**
   * Unsets this page as cached.
   */
  public synchronized void unsetCached( )
  {
    if ( this.isCached )
    {
      this.isCached = false;
    }
  }
  
  /**
   * Returns the url of this page.<br/>
   * Note that the url, of an empty page is the empty string.
   * @return The page url.
   */
  public synchronized String getUrl( )
  {
    return this.url;
  }
  
  /**
   * Returns the id of this page.
   * @return The page id.
   */
  public synchronized UUID getPageId( )
  {
    return this.pageId;
  }
  
  /**
   * Changes the id of this page to the given one.
   * @param pageId The new page id.
   */
  public synchronized void changePageId( final UUID pageId )
  {
    if( !this.pageId.equals( pageId ) )
    {
      this.pageId = pageId;
    }
  }
  
  /**
   * Returns the VFCVector of this page.
   * @param pivotUrl The url of the pivot to return.
   * @return The page VFCVector.
   */
  public synchronized VFCVector getVector( final String pivotUrl )
  {
    return this.vector.get( pivotUrl );
  }
  
  /**
   * Adds a new VFCVector to the list.
   * @param pivotUrl The pivot of the new VFCVector.
   * @param vector The new VFCVector.
   */
  public synchronized void addVector( final String pivotUrl, final VFCVector vector )
  {
    if( !this.vector.containsKey( pivotUrl ) )
    {
      this.vector.put( pivotUrl, vector );
    }
  }
  
  /**
   * Removes the vector of the given pivot.
   * @param pivotUrl The pivot whose vector is to be removed.
   */
  public synchronized void removeVector( final String pivotUrl )
  {
    if( this.vector.containsKey( pivotUrl ) )
    {
      this.vector.remove( pivotUrl );
    }
  }
  
  /**
   * Checks if there is an vector for the given pivot.
   * @param pivotUrl The pivot to look for.
   * @return True, if there is an vector for the given pivot.
   */
  public synchronized boolean containsVector( final String pivotUrl )
  {
    return this.vector.containsKey( pivotUrl );
  }
  
  /**
   * Checks if there are vectors for the given pivots.
   * @param pivotUrl The pivots to look for.
   * @return True, if there are vectors for the given pivots.
   */
  public synchronized boolean containsAllVectors( final Collection< String > pivotUrl )
  {
    for( String value : pivotUrl )
    {
      if( !this.containsUrl( value ) )
      {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Returns the number of vectors of this page.
   * @return The page vector number
   */
  public synchronized int getNumberVectors( )
  {
    return this.vector.size( );
  }
  
  /**
   * Returns an read-only map with all mapping between pivot and VFC vectors.
   * @return The pivot/vector map.
   */
  public synchronized Map< String, VFCVector > getVectorMap( )
  {
    return Collections.unmodifiableMap( this.vector );
  }
  
  /**
   * Returns an read-only set with all the child pages.
   * @return An set with all child pages.
   */
  public synchronized Set< VFCPage > getChildPages( )
  {
    return Collections.unmodifiableSet( this.linkedPages );
  }
  
  /**
   * Checks if an child page with the given url exists.
   * @param url The url of the page to check.
   * @return True, if it does.
   */
  private synchronized boolean containsUrl( final String url )
  {
    for( VFCPage child : this.linkedPages )
    {
      if( child.getUrl( ).equals( url ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Checks if the given page has the same vfc vectors as this page.
   * @param other The other page to check.
   * @return True, if the other page has the same vectors.
   */
  private boolean areVectorsEqual( final VFCPage other )
  {
    if( other.containsAllVectors( this.vector.keySet( ) ) && 
        ( other.getNumberVectors( ) == this.getNumberVectors( ) ) )
    {
      for( String pivot : this.vector.keySet( ) )
      {
        if( other.getVector( pivot ).getDistance( ) != this.getVector( pivot ).getDistance( ) )
        {
          return false;
        }
      }
      return true;
    }
    return false;
  }
  
  /**
   * Checks if the given page is a child of this page.
   * @param other The other page to check.
   * @return True, if the other page can be a child of this page.
   */
  private boolean isChildPage( final VFCPage other )
  {
    if( other.containsAllVectors( this.vector.keySet( ) ) && 
        ( other.getNumberVectors( ) == this.getNumberVectors( ) ) )
    {
      for( String pivot : this.vector.keySet( ) )
      {
        if( other.getVector( pivot ).getDistance( ) < this.getVector( pivot ).getDistance( ) )
        {
          return false;
        }
      }
      return true;
    }
    return false;
  }
  
  /**
   * Adds a new child page.<br/>
   * An child page is added if and only if, its distance is compatible
   * with this page (that is page.distance >= this.distance) and
   * if there is no page with the same url.
   * @param page The child page to add.
   */
  public synchronized void addChildPage( final VFCPage page )
  {
    //Check if the distance of the page is >= this.distance.
    if ( ( page != null ) && 
         !this.containsUrl( page.getUrl( ) ) && 
         this.isChildPage( page ) ) 
    {
      //The set id ordered by page url, so...
      this.linkedPages.add( page );
    }
  }
  
  /**
   * Removes the given child page.<br/>
   * If the page is not found, then nothing is done. 
   * @param page The page to remove.
   */
  public synchronized void removeChildPage( final VFCPage page )
  {
    //Check if the given page exists...
    if( this.linkedPages.contains( page ) )
    {
      this.linkedPages.remove( page );
    }
  }
  
  /**
   * Returns an estimate of the number of child pages.
   * @return An estimate of the number of child pages.
   */
  public synchronized int getNumberChildPages( )
  {
    return this.linkedPages.size( );
  }
  
  /**
   * Checks if the given page is in the list of child pages.
   * @param page The page to check.
   * @return True, if the page is in the list of pages.
   */
  public synchronized boolean containsChildPage( final VFCPage page )
  {
    for( VFCPage child : this.linkedPages )
    {
      if( child.equals( page ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Checks if there is an page with the given url in the list of
   * child pages.
   * @param url The url to look for.
   * @return True, if the url is a child page.
   */
  public synchronized boolean containsChildPage( final String url )
  {
    for( VFCPage child : this.linkedPages )
    {
      if( child.getUrl( ).equals( url ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Removes the page with the given url.<br/>
   * If an page with the given url is not found, 
   * then nothing is done.<br/>
   * Also only one page is removed.
   * @param url The url of the page to remove.
   */
  public synchronized void removeChildPage( final String url )
  { 
    VFCPage pageToRemove = null;
    
    //Check if the given url exists...
    for( VFCPage child : this.linkedPages )
    {
      if( child.getUrl( ).equals( url ) )
      {
        pageToRemove = child;
        break;
      }
    }
    
    if( pageToRemove != null )
    {
      this.linkedPages.remove( pageToRemove );
    } 
  }
  
  /**
   * Checks if the given object is equal to this page.
   * @param obj The object to check.
   * @return True, if it is.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override( )
  public boolean equals( final Object obj )
  {
    if( obj instanceof com.assembla.ariane.jellyfish.proxy.vfc.VFCPage )
    {      
      final VFCPage other = ( VFCPage ) obj;
      if( ( other.isCached( ) == this.isCached ) &&
          other.getPageId( ).equals( this.pageId ) &&
          other.getUrl( ).equals( this.url ) &&
          this.areVectorsEqual( other ) )
      { 
        boolean containsAllPages = false;
        for( VFCPage page : this.linkedPages )
        {
          if( other.getChildPages( ).contains( page ) )
          {
            containsAllPages = true;
          }
        }
        return containsAllPages;
      }
    }
    return false;
  }

  /**
   * Calculates and returns an hash code for this page.
   * @return The calculated hash code.
   * @see java.lang.Object#hashCode()
   */
  @Override( )
  public int hashCode( )
  {
    return Objects.hashCode( this.isCached, this.linkedPages, this.pageId, this.url, this.vector );
  }

  /**
   * Returns an string representation of this page object.
   * @return An string representing this object.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {    
    final StringBuilder sb = new StringBuilder();
    sb.append("{ ");
    sb.append( String.format( "URL: %s,", this.url ) );
    sb.append( String.format( "Cached: %s, ", Boolean.toString( this.isCached
    ) ) );
    sb.append( String.format( "ID: %s, ", this.pageId.toString( ) ) );
    sb.append( String.format( "VFCVector-Number: %d, ",
                              this.getNumberVectors( ) ) );
    sb.append( String.format( "Linked-Page-Number: %d ",
                              this.linkedPages.size( ) ) );
    sb.append("}");
    return sb.toString( );
  }

  /**
   * Removes the associated cached page.<br/>
   * 
   * This is not unsafe!<br/>&nbsp;<br/>
   *
   * Let me explain:<br/>&nbsp;<br/>
   * 
   * The structures used to store VFC data are really an direct acyclic graph (DAG),
   * in that each VFCPage is really a reference to the same page 
   * (like all objects in java).<br/>&nbsp;<br/>
   * 
   * The point, is that we need that whenever an VFCPage is *really* 
   * deleted (no more uses in the DAG),
   * then if that page has cached data, then we must destroy all the cached 
   * data with the page, why?<br/>&nbsp;<br/>
   *
   * Because suppose, that we don't delete the cached page only the 
   * VFCPage (for example when a pivot or user 
   * is removed), then at some other time, when a pivot uses that same page, 
   * he will detect that there is no
   * VFCPage for that URL and will retrieve the page from the server, but 
   * when he tries to cache it,
   * hell will break loose, since there is an cached page (that is potentially 
   * stale and different from the 
   * new one), and nothing will be cached, so that we will have an new VFCPage 
   * with an old cached page hence
   * stored data is mismatched and potentially stale.
   * @throws Throwable Never thrown.
   * @see java.lang.Object#finalize()
   */
  @Override( )
  protected void finalize( ) throws Throwable
  {
    try
    {
      super.finalize( );
      if ( this.isCached )
      {
        //Remove the cached page...
        final CacheQuery query = new CacheQuery( CacheQueryOperation.REMOVE,
           this.getPageId(), "get", this.url, "vfc");
        try
        {
          CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
        }
        catch( final InterruptedException ex )
        {
          //Do nothing
        }
        
        try
        {
          CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
        }
        catch( final InterruptedException ex )
        {
          //Do nothing
        }
      }
    }
    catch( final Exception ex )
    {
      //Do nothing
    }
  }
  
  /**
   * This class is an comparator, that takes into attention only 
   * the url of the pages.
   * @author Fat Cat
   * @version 1
   * @since 0.0.7
   */
  public static class VFCPageUrlComparator implements Comparator<VFCPage>, Serializable
  {
    
    /** The comparator version. */
    private static final long serialVersionUID = -2458579658529911508L;

    /**
     * Creates a new comparator.
     */
    public VFCPageUrlComparator( )
    {
      
    }
    
    /**
     * Compares the given pages according to their url.
     * @param left The left page to compare.
     * @param right The right page to compare.
     * @return The value 0 if the right page url is equal to the left page url; 
     * a value less than 0 if the left page url is lexicographically less than the 
     * right page url; and a value greater than 0 if the left page url is 
     * lexicographically greater than the right page url.
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override( )
    public int compare( final VFCPage left, final VFCPage right )
    {
      return left.getUrl( ).compareTo( right.getUrl( ) );
    }
    
  }

  /**
   * Creates a new page based on the type of link and parent. 
   * @param parentPage The parent of the new page.
   * @param visitedLinkType The type of the new page.
   * @param url The complete url of the new page.
   * @param pageId The id of the page.
   * @return The new page.
   */
  public static VFCPage createUsingParent( final VFCPage parentPage, 
        final VisitedLinkType visitedLinkType,
        final String url, 
        final UUID pageId )
  {
    final VFCPage page = new VFCPage( url, pageId );
    
    //check the visited link type
    switch( visitedLinkType )
    {
      case ALINK: //(check) if the page is an alink
        //for each vector in the parent page
        for( String pivot : parentPage.getVectorMap( ).keySet( ) )
        {
          //(check) if the vector is from pivot (distance == 0)
          final VFCVector parentVector = parentPage.getVector( pivot ); 
          
          if( parentVector.getDistance( ) == 0 )
          {
            //set the vfc vector as parent + 2, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ) + 2, 0, 0 ) );
            
          }
          else //else (vector is not pivot)
          {
            //set the vfc vector as parent + 1, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
            
          }
        }
        break;
      case FRAME:
        //for each vector in the parent page
        for( String pivot : parentPage.getVectorMap( ).keySet( ) )
        {
          
          //set the vfc vector as parent, 0, 0
          final VFCVector parentVector = parentPage.getVector( pivot );
          page.addVector( pivot, 
              new VFCVector( parentVector.getDistance( ), 0, 0 ) );
          
        }
        break;
      case IMAGE:
        //for each vector in the parent page
        for( String pivot : parentPage.getVectorMap( ).keySet( ) )
        {
        
          //(check) if the vector is from pivot (distance == 0)
          final VFCVector parentVector = parentPage.getVector( pivot ); 
          
          if( parentVector.getDistance( ) == 0 )
          {
            //set the vfc vector as parent + 1, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
            
          }
          else //else (vector is not pivot)
          {
            //set the vfc vector as parent, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ), 0, 0 ) );
            
          }
        }
        break;
      case LINK:
        //for each vector in the parent page
        for( String pivot : parentPage.getVectorMap( ).keySet( ) )
        {
        
          //(check) if the vector is from pivot (distance == 0)
          final VFCVector parentVector = parentPage.getVector( pivot ); 
          
          if( parentVector.getDistance( ) == 0 )
          {
            //set the vfc vector as parent + 1, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
            
          }
          else //else (vector is not pivot)
          {
            //set the vfc vector as parent, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ), 0, 0 ) );
            
          }
        }
        break;
      case SCRIPT:
        //for each vector in the parent page
        for( String pivot : parentPage.getVectorMap( ).keySet( ) )
        {
        
          //(check) if the vector is from pivot (distance == 0)
          final VFCVector parentVector = parentPage.getVector( pivot );
          
          if( parentVector.getDistance( ) == 0 )
          {
            //set the vfc vector as parent + 1, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
            
          }
          else //else (vector is not pivot)
          {
            //set the vfc vector as parent, 0, 0
            page.addVector( pivot, 
                new VFCVector( parentVector.getDistance( ), 0, 0 ) );
            
          }
        }
        break;
      case UNKNOWN:
        //for each vector in the parent page
        for( String pivot : parentPage.getVectorMap( ).keySet( ) )
        {
          //set the vfc vector as parent + 1, 0, 0
          final VFCVector parentVector = parentPage.getVector( pivot );
          
          page.addVector( pivot, 
              new VFCVector( parentVector.getDistance( ) + 1, 0, 0 ) );
          
        }
        break;
      default:
        break;
    }
    
    return page;
  }
  
  /**
   * Resets the recency of all vectors, ie. puts the counter back to zero.
   */
  public void resetRecency( )
  {
    for( String pivot : this.vector.keySet( ) )
    {
      this.vector.get( pivot ).resetRecency( );
    }
  }
  
  /**
   * Increments the recency of all vectors in one.
   */
  public void incrementRecency( )
  {
    for( String pivot : this.vector.keySet( ) )
    {
      this.vector.get( pivot ).incrementRecency( );
    }
  }
  
  /**
   * Increments the frequency of all vectors in one.
   */
  public void incrementFrequency( )
  {
    for( String pivot : this.vector.keySet( ) )
    {
      this.vector.get( pivot ).incrementFrequency( );
    }
  }
  
  
}
