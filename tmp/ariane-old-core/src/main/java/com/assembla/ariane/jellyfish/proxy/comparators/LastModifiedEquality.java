/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.comparators;

import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import org.joda.time.DateTime;

/**
 * Compares two validators, using the last modified validation. 
 * @author Fat Cat
 * @since 0.0.6
 * @version 1
 */
public final class LastModifiedEquality implements Equality< String >
{
 
  /** The singleton object. */
  private static final Equality< String > inst = new LastModifiedEquality( );
  
  /**
   * Returns the modified equality operator singleton.
   * @return The newly created last modified operator singleton.
   */
  public static Equality< String > instance( )
  {
    return LastModifiedEquality.inst;
  }
  
  /**
   * Creates a new last modified validator.
   */
  private LastModifiedEquality( )
  {
    //Does nothing...
  }
  
  /**
   * Checks if both last modified validators are similar, that is,
   * if first (received last modified) > second (existing last modified).
   * @param received The received validator.
   * @param existing The existing validator.
   * @return True, if they are.
   * @see com.assembla.ariane.jellyfish.proxy.comparators.Equality#equals(java.lang.Object, java.lang.Object)
   */
  @Override( )
  public boolean equals( final String received, final String existing )
  {
    //Check if both validators are last modified fields...
    if ( this.isValidator( received ) && this.isValidator( existing ) )
    { 
      try
      {
        final DateTime receivedDateValidator = DateTime.parse( received, JellyfishConstants.HTTP_FORMATTER );
        final DateTime existingDateValidator = DateTime.parse( existing, JellyfishConstants.HTTP_FORMATTER );
        return receivedDateValidator.isAfter( existingDateValidator ) || receivedDateValidator.equals( existingDateValidator );
      }
      catch( final IllegalArgumentException ex )
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Checks if the given validator is a last modified validator.
   * @param candidate The validator to check for.
   * @return True, if the validator is a last modified validator.
   * @see com.assembla.ariane.jellyfish.proxy.comparators.Equality#isValidator(java.lang.Object)
   */
  @Override( )
  public boolean isValidator( final String candidate )
  {
    if ( candidate.matches( "([a-zA-Z]{3}),\\s([0-9]{2})\\s([a-zA-Z]{3})\\s([0-9]{4})\\s([0-9]{2}):([0-9]{2}):([0-9]{2})\\sGMT" ) )
    {
      //This is a date field...
      return true;
    }
    else
    {
      //This is a weak or compatible e-tag, by exclusion...
      return false;
    }
  }
  
}
