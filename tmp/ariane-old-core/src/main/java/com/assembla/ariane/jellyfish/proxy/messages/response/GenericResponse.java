/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages.response;

import java.util.List;
import com.assembla.ariane.jellyfish.proxy.messages.GenericMessage;
import com.assembla.ariane.jellyfish.proxy.messages.WarningValue;

/**
 * This interface represents an generic response.
 * @author Fat Cat
 */
public interface GenericResponse extends GenericMessage
{
  
  //Response code stuff
  
  /**
   * Returns the current http response code as a string.
   * @return The response code.
   */
  String getResponseCode( );
  
  /**
   * Returns the current http response code as a integer.
   * @return The response code.
   */
  int getResponseCodeAsInteger( );
  
  /**
   * Changes the http response code.
   * @param code The new response code value.
   * @param message The new response code message.
   */
  void setResponseCode( int code, String message );
  
  //Warnings stuff
  
  /**
   * Checks if the given warning is present.
   * @param warning The warning to check for.
   * @return True, if it is.
   */
  boolean containsWarning( final WarningValue warning );
  
  /**
   * Adds the given warning to the list.
   * @param warning The warning to add.
   */
  void addWarning( final WarningValue warning );
  
  /**
   * Removes the given warning.
   * @param warning The warning to remove.
   */
  void removeWarning( final WarningValue warning );
  
  /**
   * Returns an list of warnings.
   * @return An read-only list of present warnings.
   */
  List< WarningValue > getWarnings( );
  
  /**
   * Returns the number of warnings.
   * @return The number of warning. 
   */
  int getNumberOfWarnings( );
  
  /**
   * Checks if the warning list is empty.
   * @return True, if it is.
   */
  boolean hasAnyWarning( );
  
  /**
   * Empties the warning list.
   */
  void clearWarnings( );
  
}
