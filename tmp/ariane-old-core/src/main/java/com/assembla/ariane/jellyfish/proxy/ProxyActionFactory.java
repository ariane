/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;

/**
 * This class is a processor factory. 
 * @author Fat Cat
 * @version 2
 * @since 0.0.1
 */
public final class ProxyActionFactory
{
  
  @SuppressWarnings( "rawtypes" )
  private final Map< String, Class > knownActionFactories;
  
  /** The singleton instance. */
  private static final ProxyActionFactory inst = new ProxyActionFactory( );
  
  /**
   * Returns the instance of this singleton.
   * @return The factory singleton.
   */
  public static ProxyActionFactory instance( )
  {
    return ProxyActionFactory.inst;
  }
  
  /**
   * Creates a new proxy factory.
   */
  @SuppressWarnings( "rawtypes" )
  private ProxyActionFactory( )
  {
    this.knownActionFactories = new HashMap< String, Class >( );
  }
  
  /**
   * Clears all known processor types.
   */
  public void clear( )
  {
    this.knownActionFactories.clear( );
  }
  
  /**
   * Checks if the processor with the given id is known.
   * @param key The key of the processor to check.
   * @return True, if it is.
   */
  public boolean contains( final String key )
  {
    return this.knownActionFactories.containsKey( key );
  }
  
  /**
   * Returns the processor type associated with the given key.
   * @param key The key to use.
   * @return The processor type class associated with the key.
   */
  @SuppressWarnings( "rawtypes" )
  public Class get( final String key )
  {
    return this.knownActionFactories.get( key );
  }
  
  /**
   * Checks if the list of known processors is empty.
   * @return True, if it is.
   */
  public boolean isEmpty( )
  {
    return this.knownActionFactories.isEmpty( );
  }
  
  /**
   * Returns an set with all known processor ids.
   * @return An read-only set with all the processor ids.
   */
  public Set< String > keys( )
  {
    return Collections.unmodifiableSet( this.knownActionFactories.keySet( ) );
  }
  
  /**
   * Adds a new processor to the factory list.
   * @param key The processor id.
   * @param value The processor class to add.
   */
  @SuppressWarnings( "rawtypes" )
  public void add( final String key, final Class value )
  {
    if ( !this.contains( key ) )
    {
      this.knownActionFactories.put( key, value );
    }
  }
  
  /**
   * Removes the given processor from the factory.
   * @param key The processor id.
   * @return The processor type.
   */
  @SuppressWarnings( "rawtypes" )
  public Class remove( final Object key )
  {
    return this.knownActionFactories.remove( key );
  }
  
  /**
   * Returns the number of known processors.
   * @return The number of processors.
   */
  public int size( )
  {
    return this.knownActionFactories.size( );
  }
  
  /**
   * Creates a new processor.
   * @param type The id of the processor.
   * @param successor The processor sucessor.
   * @param config The configuration for the processor to use.
   * @return A new proxy processor.
   */
  @SuppressWarnings( "unchecked" )
  public ProxyAction get( final String type, final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    if ( this.contains( type ) )
    {
      try
      {
        return ( ProxyAction ) this.knownActionFactories.get( type ).getConstructor( IHandler.class, HierarchicalConfiguration.class ).newInstance( successor, config );
      }
      catch( final InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e )
      {
      }
    }
    return JellyfishConstants.NullProxyAction;
  }
}
