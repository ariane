/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.main;

import java.util.Locale;
import com.assembla.ariane.jellyfish.connection.docks.Dock;
import com.assembla.ariane.jellyfish.connection.docks.NullDockImpl;
import com.assembla.ariane.jellyfish.proxy.NullProxyActionImpl;
import com.assembla.ariane.jellyfish.proxy.vfc.VFCVector;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import com.google.common.io.Files;

/**
 * This class contains some constants used globally in the com.assembla.ariane.jellyfish code.
 * @author Fat Cat
 */
public final class JellyfishConstants
{
  
  /**
   * This is an enumeration that specified constants for common http error 
   * responses.
   * @author Fat Cat
   * @since 0.0.7
   * @version 1
   */
  public enum HTTPErrorCodes
  {
    /**
     * Represents an not modified server response.
     */
    NOT_MODIFIED( 304 ),
    
    /**
     * Represents an bad request server response.
     */
    BAD_REQUEST( 400 ),
    
    /**
     * Represents an bad gateway response.<br/>
     * Used when there is no connection to a server.
     */
    BAD_GATEWAY( 502 ),
    
    /**
     * Represents an multiple choices message.<br/>
     * It is also the upper bound to the valid cacheable responses.
     */
    MULTIPLE_CHOICES( 300 ),
    
    /**
     * Represents an gateway timeout error code.
     */
    GATEWAY_TIMEOUT( 504 ),
    
    /**
     * Represents an server error code.
     */
    SERVER_ERROR( 500 ),
    
    /**
     * Represents an OK server response.
     */
    OK( 200 );
    
    /**
     * The error code integer corresponding to the enum.
     */
    public final int errorcode;

    /**
     * Associates an errorcode with an enum value.
     * @param errorcode The error code to associate.
     */
    HTTPErrorCodes( final int errorcode )
    {
      this.errorcode = errorcode;
    }
  }
  
  
  /**
   * Private constructor, for a utility class.
   */
  private JellyfishConstants()
  {
    
  }
  
  /**
   * This is the warning code used when there is an connection problem to the
   * server.
   */
  public static final int REVALIDATION_FAILED = 111;
  
  /**
   * This is the VFC vector for a initial pivot: (0, 0, 0).
   */
  public static final VFCVector PIVOT_VFC_VECTOR = new VFCVector( 0, 0, 0 );
  
  /**
   * The http date formatter, that formats an datetime object using http format.
   */
  public static final DateTimeFormatter HTTP_FORMATTER = DateTimeFormat.forPattern( "EEE, dd MMM yyyy HH:mm:ss 'GMT'" ).withLocale( Locale.US ).withZoneUTC( );
  
  /**
   * This is the debug date formatter, that formatts an datetime object using 
   * the format of the following example:
   * 
   * 03/08/2000 13:24:09:234 PM
   * 
   * The native locale is used, with UTC zone.
   */
  public static final DateTimeFormatter DEBUG_FORMATTER = DateTimeFormat.forPattern(
     "dd/MM/yyyy HH:mm:ss:SSSS a" ).withLocale( Locale.getDefault( ) ).withZoneUTC( );

  /**
   * This is the name of this cache.
   */
  public static final String CACHE_NAME = "Ariane Jellyfish";

  /**
   * This is the name of the current operating system.
   */
  public static final String OS_NAME = System.getenv( "os.name" );

  /**
   * This is the cache version.
   */
  public static final String CACHE_VERSION = "0.0.7";

  /**
   * This is the server name string, for use in http.
   */
  public static final String SERVER_NAME = String.format("%s/%s (%s)",
    CACHE_NAME, CACHE_VERSION, OS_NAME);

  /**
   * The default http port of a server.<br/>
   * By default it is 80.
   */
  public static final int DEFAULT_HTTP_PORT = 80;
  
  /**
   * The default size of a queue.<br/>
   * By default it is 1024 items.
   */
  public static final int DEFAULT_QUEUE_SIZE = 1024;
  
  /**
   * The null dock value.
   */
  public static final Dock NullDock = new NullDockImpl( null );
  
  /**
   * This is an empty string.
   */
  public static final String EMPTY_STRING = new String();
  
  /**
   * This is the default timeout value for threads waiting for queue messages.<br/>
   * By default it is 6 seconds.
   */
  public static final int DEFAULT_WAITING_TIME = 6;

  /**
   * This is the default size of an array list.<br/>
   * It is used in the definition of the range list, for example.<br/>
   * By default it is 6 items.
   */
  public static final int DEFAULT_ARRAY_LIST_SIZE = 6;
  
  /**
   * This is the default maximum total size of an http message, with all 
   * chunks included.<br/>
   * This should be equal or higher than DEFAULT_MAX_HEADER_LINE_SIZE +
   * DEFAULT_MAX_HEADER_SIZE + DEFAULT_MAX_CONTENT_SIZE.<br/>
   * By default is it equal to the sum of the three fields above, or more
   * or less 11 MB.
   */
  public static final int DEFAULT_MAX_MESSAGE_SIZE = 10534912;
  
  /**
   * This is the default maximum size of an http message header line.<br/>
   * By default is is 16 KB.
   */
  public static final int DEFAULT_MAX_HEADER_LINE_SIZE = 16384;
  
  /**
   * This is the default maximum size of an the http headers.<br/>
   * By default it is 32 KB.
   */
  public static final int DEFAULT_MAX_HEADER_SIZE = 32768;
  
  /**
   * This is the default maximum size of the message contents.<br/>
   * By default it is 10 MB.
   */
  public static final int DEFAULT_MAX_CONTENT_SIZE = 10485760;
  
  /**
   * This is the default save directory for the debug proxy action.<br/>
   * By default it is an directory created in the user temporary directory.
   */
  public static final String DEFAULT_DEBUG_PROXY_SAVE_DIR = Files.createTempDir( ).getAbsolutePath( );
  
  /**
   * The null proxy action value.
   */
  public static final NullProxyActionImpl NullProxyAction = new NullProxyActionImpl( null, null );
  
  /**
   * This is the maximum stale time of a response in cache, if there is no
   * other value to use.<br/>
   * By default it is 2 days.
   */
  public static final int MAX_STATE_TIME = 86400;
  
}
