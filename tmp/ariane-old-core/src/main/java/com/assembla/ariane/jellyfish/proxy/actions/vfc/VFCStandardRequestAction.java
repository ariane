/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions.vfc;

import io.netty.buffer.ChannelBuffers;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponseOperation;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.htmlparser.util.ParserException;
import org.joda.time.DateTime;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.slf4j.LoggerFactory;
import com.assembla.ariane.jellyfish.util.collections.cache.CacheMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.client.ClientMessageProcessingQueue;
import com.assembla.ariane.jellyfish.util.collections.vfc.VFCPageList;
import com.assembla.ariane.jellyfish.util.html.visitors.StandardVFCLinkVisitor;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.RequestType;
import com.assembla.ariane.jellyfish.proxy.exceptions.ArgumentException;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyProtocol;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import com.assembla.ariane.jellyfish.proxy.messages.requests.ProxyRequestMessage;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseCode;
import com.assembla.ariane.jellyfish.proxy.messages.response.ProxyResponseMessage;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQuery;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheQueryOperation;
import com.assembla.ariane.jellyfish.proxy.storage.cache.query.CacheResponse;
import com.assembla.ariane.jellyfish.proxy.vfc.User;
import com.assembla.ariane.jellyfish.proxy.vfc.Users;
import com.assembla.ariane.jellyfish.proxy.vfc.VFCPage;

/**
 * This action handles an standard request using VFC.<br/>
 * That is an request that is not to this server and is not conditional
 * is not a range request and is virtually cacheable.<br/>
 * An request is virtually cacheable when it is not an upgrade request,
 * does not have trailers, is not an 100-continue expect message.<br/>
 * Since VFC does not cares about cache-control or pragma, requests/responses
 * that wouldn't be cacheable in other other servers, will be cached by
 * this scheme.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public class VFCStandardRequestAction extends ProxyAction
{
  
  /**
   * Creates a new VFC standard request action.
   * @param successor The next action in the list.
   * @param config The configuration of this action.
   */
  public VFCStandardRequestAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.logger = LoggerFactory.getLogger( VFCStandardRequestAction.class );
  }
  
  /**
   * Checks if the given request can be processed by this action.<br/>
   * As told in the class documentation, this action can be processed, if
   * it is not for this server, it is not conditional, it is not a range 
   * request, is virtually cacheable and is a get or head request. 
   * @param aRequest The request to look for.
   * @return True, if the request can be handled by this action.
   * @throws IllegalArgumentException Never Thrown.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#canHandle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return !RequestType.isClientOperation( aRequest ) && 
           !RequestType.isConditionalRequest( aRequest ) && 
           !RequestType.isRangeRequest( aRequest ) && 
            RequestType.isVirtuallyCacheableRequest( aRequest ) && 
            RequestType.isGetOrHeadRequest( aRequest );
  }
  
  /**
   * Handles this request, using the VFC approach.
   * @param aRequest The request to handle.
   * @throws IllegalArgumentException If some error happens.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#handle(com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement)
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    //if request is new
    if ( aRequest.getState( ) == ProxyRequestState.PROCESSING )
    {
      processRequest( aRequest );
    }
    else if ( aRequest.getState( ) == ProxyRequestState.MORE_INFO_RECEIVED ) //else (the request is from the server)
    {
      processReceivedResponse( aRequest );
    }
  }

  /**
   * Processes the client request.
   * @param aRequest The client request.
   */
  private void processRequest( final ProxyPageElement aRequest )
  {
    final ProxyRequestMessage request = aRequest.getRequest( );
    
    //(check) if the page is known for the given user 
    //user name is in header X-USERNAME
    VFCPage page = null;
    User user = null;
    if ( request.containsHeader( "X-USERNAME" ) && 
        Users.instance( ).hasUserName( request.getHeader( "X-USERNAME" ) ) )
    {
      final String username = request.getHeader( "X-USERNAME" );
      user = Users.instance( ).getUserFor( username );
      page = user.peekPageInformation( aRequest.getRequest( ).getCompleteUri( ) );
      
    }
    
    if ( page != null )
    {
      processVFCPageRequest( aRequest, page, user );
    }
    else
    //else (the page is not known)
    {
      
      //set an extra-value (direct=true)
      aRequest.addExtra( "direct", Boolean.toString( Boolean.TRUE ) );
      
      //create an unconditional server request (+4) (send the client request)
      this.sendToServer( aRequest, aRequest.getRequest( ) );
    }
  }

  /**
   * Processes an VFCPage client request.
   * @param aRequest The client request.
   * @param page The VFCPage.
   * @param user The client user name.
   */
  private void processVFCPageRequest( final ProxyPageElement aRequest, 
      final VFCPage page, final User user )
  {
    //(check) if the page is already cached (isCached)
    if ( page.isCached( ) )
    {
      
      //get the cached page
      final CacheResponse cacheResponse = this.getCachedPage( aRequest );
      if ( cacheResponse.getResponse( ) == CacheResponseOperation.OK )
      {
        processCachedRequest( aRequest, page, user, cacheResponse );
      }
      else //else (there is no cached page)
      {
        //unset the cached attribute
        page.unsetCached( );
        
        if ( VFCPageList.instance( ).change( page.getUrl( ), page ) == null )
        {
          VFCPageList.instance( ).add( page );
        }
        
        //create an unconditional server request (+4) (send the client request)
        this.sendToServer( aRequest, aRequest.getRequest( ) );
      }
    }
    else
    //else (if the page is not cached)
    {
      //create an unconditional server request (+4) (send the client request)
      this.sendToServer( aRequest, aRequest.getRequest( ) );
    }
  }

  /**
   * Processes an VFC cached request. 
   * @param aRequest The client request.
   * @param page The VFCPage.
   * @param user The client user name.
   * @param cacheResponse The cached response.
   */
  private void processCachedRequest( final ProxyPageElement aRequest, 
      final VFCPage page, final User user, final CacheResponse cacheResponse )
  {
    //(check) if the page is fresh (page vfc-vector < consistency vfc-vector)
    if ( user.isPageFresh( page ) )
    {
      //add one to the frequency counter
      page.incrementFrequency( );
      
      //add one to the recency of all pages
      VFCPageList.instance( ).incrementRecency( );
      
      if ( VFCPageList.instance( ).change( page.getUrl( ), page ) == null )
      {
        VFCPageList.instance( ).add( page );
      }
      
      //check (if the page needs to he remaped) (remap all users) (the page has not change)
      Users.instance( ).remapAsNeeded( page );
      
      //return the cached page to the user (2+)
      this.sendClienResponse( aRequest, 
          cacheResponse.getResponseElement( ).getResponse( ) );
      
    }
    else //else (if page is not fresh)
    {
      //create an conditional server request (or a normal one it does not matter) (+4 incomplete)
      this.logger.info( "Request {} is being sent to the server", 
          aRequest.getRequest( ).getMessageId( ) );
      
      final ProxyRequestMessage conditionalRequest = aRequest.getRequest( ).copy( );
      
      if ( cacheResponse.getResponseElement( ).
             getResponse( ).containsHeader( "ETag" ) )
      {
        this.logger.debug( "Request {} etag is {}", 
            aRequest.getRequest( ).getMessageId( ), 
            cacheResponse.getResponseElement( ).getResponse( ).
              getHeader( "ETag" ) );
        
        conditionalRequest.addHeader( "If-None-Match", 
            cacheResponse.getResponseElement( ).getResponse( ).
              getHeader( "ETag" ) );
      }
      
      if ( cacheResponse.getResponseElement( ).
            getResponse( ).containsHeader( "Last-Modified" ) )
      {
        this.logger.debug( "Request {} last-modified is {}", 
            aRequest.getRequest( ).getMessageId( ), 
            cacheResponse.getResponseElement( ).getResponse( ).
              getHeader( "Last-Modified" ) );
        
        conditionalRequest.addHeader( "If-Modified-Since", 
            cacheResponse.getResponseElement( ).getResponse( ).
              getHeader( "Last-Modified" ) );
      }
      
      sendToServer( aRequest, conditionalRequest );
    }
  }

  /**
   * Processes the received server response.
   * @param aRequest The proxy page.
   */
  private void processReceivedResponse( final ProxyPageElement aRequest )
  {
    try
    {
      //if direct is not set
      if ( !aRequest.containsExtra( "direct" ) )
      {
        //get the server response
        final ProxyResponseMessage serverResponse = aRequest.
            getTransitoryResponse( ).copy( );
        
        //get the corresponding page (from consistency map)
        final VFCPage correspondingPage = VFCPageList.instance( ).get( 
            aRequest.getRequest( ).getCompleteUri( ) );
        
        //(check) if there is an VFCPage
        if ( correspondingPage != null )
        {
          
          //(check) if the response is 304 (unmodified)
          if ( serverResponse.getResponseCodeAsInteger( ) == 
              JellyfishConstants.HTTPErrorCodes.NOT_MODIFIED.errorcode )
          {
            processUnmodifiedResponse( aRequest, correspondingPage );
          }
          else if ( 
              ( serverResponse.getResponseCodeAsInteger( ) >= 
                JellyfishConstants.HTTPErrorCodes.OK.errorcode ) && 
              ( serverResponse.getResponseCodeAsInteger( ) < 
                  JellyfishConstants.HTTPErrorCodes.MULTIPLE_CHOICES.errorcode ) )
          {
            //else (if the response is not an error)
            
            processOkResponse( aRequest, serverResponse, correspondingPage );
          }
          else //else (if the response is an error)
          {
            //return the received page to the user (+3)
            this.sendClienResponse( aRequest, serverResponse );
          }
          
        }
        else //else (there isn't an VFCPage)
        {
          //send an error message to the user
          final ProxyResponseMessage response = new ProxyResponseMessage( 
              aRequest.getRequest( ).getMessageId( ) );
          
          response.unsetKeepAlive( );
          
          response.setResponseCode( 
              ProxyResponseCode.create( 
                  JellyfishConstants.HTTPErrorCodes.SERVER_ERROR.errorcode, 
                  "Server Error" ) );
          
          response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
          response.setResponseTime( DateTime.now( ) );
          
          response.addHeader( "Date", response.getResponseTime( ).toString( 
              JellyfishConstants.HTTP_FORMATTER ) );
          
          response.addHeader( "Server", "com/assembla/ariane/jellyfish" );
          response.addHeader( "Content-Type", "text/html; charset=utf-8" );
          
          final String content = response.getNativeResponseCode( ).getHtmlPage( 
              "Operation unsucessful<br/> VFC Cache error." );
          
          response.addHeader( "Content-Lenght", Integer.toString( 
              content.length( ) ) );
          
          response.setContentString( content );
          
          sendClienResponse( aRequest, response );
        }
      }
      else
      //else (direct is set)
      {
        //return the received page to the user (+3)
        this.sendClienResponse( aRequest, aRequest.getTransitoryResponse( ) );
      }
    }
    catch( final ParserException ex )
    {
      //return the received page to the user (+3)
      this.sendClienResponse( aRequest, aRequest.getTransitoryResponse( ) );
    }
  }

  /**
   * Processes an OK server response.
   * @param aRequest The proxy page.
   * @param serverResponse The server response.
   * @param correspondingPage The VFCPage corresponding to the server response.
   * @throws ParserException 
   */
  private void processOkResponse( final ProxyPageElement aRequest, 
      final ProxyResponseMessage serverResponse,
      final VFCPage correspondingPage ) throws ParserException
  {
    //(check) if the page is listed as cached
    final CacheResponse cacheResponse = this.getCachedPage( aRequest );
    if ( cacheResponse.getResponse( ) == CacheResponseOperation.OK )
    {
      
      this.parseChangedPage( aRequest, serverResponse, correspondingPage );
      
      //return the received page to the user (+3)
      this.sendClienResponse( aRequest, serverResponse );
      
    }
    else //else (the page is listed as not cached)
    {
      
      //if the page is set to be cached
      if ( correspondingPage.isCached( ) )
      {
        //unset the cache attribute
        correspondingPage.unsetCached( );
        
        if ( VFCPageList.instance( ).change( correspondingPage.getUrl( ), correspondingPage ) == null )
        {
          VFCPageList.instance( ).add( correspondingPage );
        }
      }
      
      this.parseNewPage( aRequest, serverResponse, correspondingPage );
      
      //return the received page to the user (+3)
      this.sendClienResponse( aRequest, serverResponse );
      
    }
  }
  
  /**
   * Parses the given new server response.<br/>
   * This method checks if the page is an html page, then if it is
   * parses the html page, extracts the links, images, scripts and stylesheet
   * urls and adds them to the VFCPage as child pages and to the pivots
   * containing the parent page.
   * @param aRequest The proxy request page.
   * @param serverResponse The response from the server.
   * @param correspondingPage The VFC page corresponding to the server response.
   * @throws ParserException If there is an error parsing the html.
   */
  private void parseNewPage( final ProxyPageElement aRequest, 
                             final ProxyResponseMessage serverResponse, 
                             final VFCPage correspondingPage ) throws ParserException
  {
    //(check) if the page is an html page
    if ( VFCUtilityFunctions.isHtmlPage( serverResponse ) )
    {
      //parse the page
      final StandardVFCLinkVisitor visitor = 
          VFCUtilityFunctions.parsePage( serverResponse );
      
      //add the parsed pages in the respective consistency zones
      VFCUtilityFunctions.addNewParsedPages( correspondingPage, visitor );
    }
    
    //set the vfc-vector as (current_distance, 0, current_frequency + 1)
    correspondingPage.incrementFrequency( );
    correspondingPage.resetRecency( );
    
    //add one to the recency of all pages (remember that this is an lazy algorithm)
    VFCPageList.instance( ).incrementRecency( );
    
    //remap the page in the consistency zones (of all users)
    Users.instance( ).remapAsNeeded( correspondingPage );
    
    //cache the new page
    final ProxyPageElement cachedPage = new ProxyPageElement( );
    cachedPage.setRequest( aRequest.getRequest( ).copy( ) );
    cachedPage.setResponse( serverResponse );
    cachedPage.setState( ProxyRequestState.ARCHIVED );
    cachedPage.emptyTransitoryData( );
    
    if ( this.addCachedPage( cachedPage ).getResponse( ) == 
        CacheResponseOperation.OK )
    {
      correspondingPage.setCached( );
      correspondingPage.changePageId( 
          aRequest.getRequest( ).getMessageId( ) );
    }
    
    //Reupdate the changed page.
    if ( VFCPageList.instance( ).change( 
        correspondingPage.getUrl( ), correspondingPage ) == null )
    {
      VFCPageList.instance( ).add( correspondingPage );
    }
  }
  
  /**
   * This method parses an changed server response.<br/>
   * This method checks if the page is an html page, then if it is
   * parses the html page, extracts the links, images, scripts and stylesheet
   * urls and merges them to the parent page child pages and to the pivots
   * containing the parent page.
   * @param aRequest The proxy request page.
   * @param serverResponse The response from the server.
   * @param correspondingPage The VFC page corresponding to the server response.
   * @throws ParserException If there is an error parsing the html.
   */
  private void parseChangedPage( final ProxyPageElement aRequest, 
                                 final ProxyResponseMessage serverResponse, 
                                 final VFCPage correspondingPage ) throws ParserException
  {
    //(check) if the page is an html page
    if ( VFCUtilityFunctions.isHtmlPage( serverResponse ) )
    {
      //parse the page
      final StandardVFCLinkVisitor visitor = VFCUtilityFunctions.parsePage( serverResponse );
      
      //merge the new associated pages in the users consistency zones 
      //and delete the non-existing ones
      VFCUtilityFunctions.mergeParsedPages( correspondingPage, visitor );
      
    }
    
    //set the vfc-vectors as (current_distance, 0, current_frequency + 1)
    correspondingPage.incrementFrequency( );
    correspondingPage.resetRecency( );
    
    //add one to the recency of all pages 
    // (remember that this is an lazy algorithm, so we don't need to check if
    //  pages are well mapped, into the consistency zones, we will do that
    //  when there is a request for a page and if the page is not fresh)
    VFCPageList.instance( ).incrementRecency( );
    
    //remap the page in the consistency zones (of all users)
    Users.instance( ).remapAsNeeded( correspondingPage );
    
    //cache the new page
    final ProxyPageElement cachedPage = new ProxyPageElement( );
    cachedPage.setRequest( aRequest.getRequest( ).copy( ) );
    cachedPage.setResponse( serverResponse );
    cachedPage.setState( ProxyRequestState.ARCHIVED );
    cachedPage.emptyTransitoryData( );
    
    if ( this.changeCachedPage( cachedPage ).getResponse( ) == CacheResponseOperation.OK )
    {
      correspondingPage.setCached( );
    }
    
    //Reupdate the changed page.
    if ( VFCPageList.instance( ).change( correspondingPage.getUrl( ), correspondingPage ) == null )
    {
      VFCPageList.instance( ).add( correspondingPage );
    }
  }

  /**
   * Processes an unmodified server response.
   * @param aRequest The proxy page.
   * @param correspondingPage The VFCPage corresponding to the response.
   */
  private void processUnmodifiedResponse( final ProxyPageElement aRequest, final VFCPage correspondingPage )
  {
    //set the vfc-vector as (current_distance, 0, current_frequency)
    correspondingPage.resetRecency( );
    
    //remap the page in the consistency zones (remap all users)
    if ( VFCPageList.instance( ).change( correspondingPage.getUrl( ), correspondingPage ) == null )
    {
      VFCPageList.instance( ).add( correspondingPage );
    }
    
    Users.instance( ).remapAsNeeded( correspondingPage );
    
    //get the cached page
    final CacheResponse cacheResponse = this.getCachedPage( aRequest );
    if ( cacheResponse.getResponse( ) == CacheResponseOperation.OK )
    {
      //remove any 1xx warning fields,
      final ProxyResponseMessage response = cacheResponse.getResponseElement( ).getResponse( ).copy( );
      
      this.logger.info( "Cleaning temporary warning fields from request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
      response.clearTemporaryWarnings( );
      
      //combine the headers of both page responses, meaning that headers present,
      // in the new response replace the ones in the old one.
      this.logger.info( "Combining the received headers from request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
      response.mergeHeaders( aRequest.getTransitoryResponse( ) );
      response.replaceWarnings( aRequest.getTransitoryResponse( ).getWarnings( ) );
      
      //remove any hop-to-hop headers,
      this.logger.info( "Removing hop headers from request {}", aRequest.getRequest( ).getMessageIdAsString( ) );
      response.removeHopToHopHeaders( );
      
      //return the cached page to the user (2+)
      this.sendClienResponse( aRequest, response );
      
    }
    else
    {
      //unset the cached attribute
      correspondingPage.unsetCached( );
      
      if ( VFCPageList.instance( ).change( correspondingPage.getUrl( ), correspondingPage ) == null )
      {
        VFCPageList.instance( ).add( correspondingPage );
      }
      
      //send an error message to the user
      final ProxyResponseMessage response = new ProxyResponseMessage( 
          aRequest.getRequest( ).getMessageId( ) );
      
      response.unsetKeepAlive( );
      
      response.setResponseCode( 
          ProxyResponseCode.create( JellyfishConstants.HTTPErrorCodes.SERVER_ERROR.errorcode, 
              "Server Error" ) );
      
      response.setProtocol( ProxyProtocol.create( 1, 1, "HTTP" ) );
      response.setResponseTime( DateTime.now( ) );
      
      response.addHeader( "Date",
          response.getResponseTime( ).toString( JellyfishConstants.HTTP_FORMATTER ) );
      
      response.addHeader( "Server", JellyfishConstants.SERVER_NAME );
      response.addHeader( "Content-Type", "text/html; charset=utf-8" );
      
      final String content = response.getNativeResponseCode( ).getHtmlPage( 
          "Operation unsucessful<br/> Cache error." );
      
      response.addHeader( "Content-Lenght", Integer.toString( content.length( ) ) );
      response.setContentString( content );
      
      sendClienResponse( aRequest, response );
    }
  }
  
  /**
   * Sends an response to the client.
   * @param aRequest The client request.
   * @param response The response for the request.
   */
  private void sendClienResponse( final ProxyPageElement aRequest, final ProxyResponseMessage response )
  {
    this.logger.debug( "Request {} is {}", 
        aRequest.getRequest( ).getMessageId( ), 
        aRequest.getRequest( ).getMethod( ) );
    
    //If the method is get
    if ( aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      //we return the full cached page to the user.
      aRequest.setResponse( response );
    }
    else
    //else if the method is head. (remember that other methods aren't cacheable)
    {
      //we return only the headers...
      final ProxyResponseMessage headerResponse = response.copy( );
      headerResponse.setContent( ChannelBuffers.EMPTY_BUFFER );
      aRequest.setResponse( headerResponse );
    }
    
    //send the result to the client.
    aRequest.setState( ProxyRequestState.PROCESSED );
    
    //Because a request can come from any dock, than an response must be 
    //associated to that dock so that it can be sent by the sender associated
    //with the dock.
    this.logger.debug( "Request {} dock is {}", 
        aRequest.getRequest( ).getMessageId( ), 
        aRequest.getRequest( ).getDockName( ) );
    
    aRequest.getResponse( ).setDockName( aRequest.getRequest( ).getDockName( ) );
    
    aRequest.getResponse( ).setMessageId( aRequest.getRequest( ).getMessageId( ) );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} is processed!", aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Sends an request to the server asking for more information.
   * @param aRequest The request page to use. 
   * @param request The request to send.
   */
  private void sendToServer( final ProxyPageElement aRequest, final ProxyRequestMessage request )
  {
    this.logger.info( "Preparing request {} to be sent to the server", 
        aRequest.getRequest( ).getMessageId( ) );
    
    URL host;
    int port = JellyfishConstants.DEFAULT_HTTP_PORT;
    try
    {
      host = new URL( aRequest.getRequest( ).getCompleteUri( ) );
      port = host.getPort( );
      if ( port == -1 )
      {
        port = JellyfishConstants.DEFAULT_HTTP_PORT;
      }
    }
    catch( final MalformedURLException e )
    {
      //Do nothing
    }
    
    this.logger.debug( "Server port for request {} is {}", 
        aRequest.getRequest( ).getMessageId( ), port );
    
    //Notice that since the server port can be different than the standard 80,
    //we will extract the port in order to use it in the HTTPServerSender channel.
    request.addExtra( "server-port", Integer.toString( port ) );
    aRequest.setTransitoryData( request );
    
    aRequest.setState( ProxyRequestState.MORE_INFO );
    
    try
    {
      ClientMessageProcessingQueue.instance( ).getResponses( ).put( aRequest );
      this.logger.info( "Request {} was sent to server", 
          aRequest.getRequest( ).getMessageId( ) );
    }
    catch( final InterruptedException e )
    {
      //Do nothing
    }
  }
  
  /**
   * Returns the page in cache, if existing.
   * @param aRequest The request to use for the query.
   * @return The page in cache or null, if something happens.
   */
  private CacheResponse getCachedPage( final ProxyPageElement aRequest )
  {
    CacheResponse response = null;
    final CacheQuery query = new CacheQuery(CacheQueryOperation.QUERY,
      aRequest.getCurrentUUID(), aRequest.getRequest().getMethod(),
      aRequest.getRequest().getCompleteUri(), "vfc");
    try
    {
      CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( 
          "The VFC standard cache consistency was interrupted.", ex );
    }
    
    try
    {
      response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
    }
    catch( final InterruptedException ex )
    {
      this.logger.debug( 
          "The VFC standard cache consistency was interrupted.", ex );
    }
    
    return response;
  }
  
  /**
   * Adds the page to the cache.
   * @param aRequest The request to use for the query.
   * @return The response of the cache.
   */
  private CacheResponse addCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isVirtuallyCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      CacheResponse response = null;
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.ADD, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return new CacheResponse( CacheQueryOperation.ADD, UUID.randomUUID( ),
          CacheResponseOperation.ERROR );
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The VFC standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The VFC standard cache consistency was interrupted.", ex );
      }
      
      return response;
    }
    return new CacheResponse( CacheQueryOperation.ADD, UUID.randomUUID( ), 
        CacheResponseOperation.ERROR );
  }
  
  /**
   * Changes the page in cache, to the given one.
   * @param aRequest The request to use for the query.
   * @return The response of the cache.
   */
  private CacheResponse changeCachedPage( final ProxyPageElement aRequest )
  {
    if ( RequestType.isVirtuallyCacheable( aRequest ) && 
         aRequest.getRequest( ).getMethod( ).equalsIgnoreCase( "get" ) )
    {
      CacheResponse response = null;
      final CacheQuery query;
      try
      {
        query = new CacheQuery( CacheQueryOperation.CHANGE, aRequest, "http");
      }
      catch( ArgumentException e )
      {
        //Do nothing.
        return new CacheResponse( CacheQueryOperation.ADD, UUID.randomUUID( ),
           CacheResponseOperation.ERROR );
      }
      try
      {
        CacheMessageProcessingQueue.instance( ).getRequests( ).put( query );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      
      try
      {
        response = CacheMessageProcessingQueue.instance( ).getResponses( ).take( );
      }
      catch( final InterruptedException ex )
      {
        this.logger.debug( "The standard cache consistency was interrupted.", ex );
      }
      return response;
    }
    return new CacheResponse( CacheQueryOperation.ADD, UUID.randomUUID( ), 
        CacheResponseOperation.ERROR );
  }
  
  /**
   * Returns the string containing the action id of this proxy.<br/>
   * The string is "VFC Standard Request Action".
   * @return The id of this action.
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#getActionID()
   */
  @Override( )
  public String getActionID( )
  {
    return "VFC Standard Request Action";
  }
  
}
