/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.actions;

import java.io.File;
import com.assembla.ariane.jellyfish.proxy.ProxyAction;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyPageElement;
import com.assembla.ariane.jellyfish.proxy.messages.ProxyRequestState;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.perfectjpattern.core.api.behavioral.chainofresponsibility.IHandler;
import org.perfectjpattern.core.behavioral.chainofresponsibility.AllHandleStrategy;
import org.slf4j.LoggerFactory;

/**
 * An proxy action/processor that serializes the request and returns an 
 * empty 200 OK response.<br/>
 * Currently this request is decommissioned, and does nothing...
 * @author Fat Cat
 * @version 2
 * @since 0.0.2
 * @deprecated
 */
public class SaveProxyAction extends ProxyAction
{
  
  /**
   * Creates a new save proxy action.
   * @param successor The next proxy action processor...
   * @param config The configuration of this processor.
   */
  public SaveProxyAction( final IHandler< ProxyPageElement > successor, final HierarchicalConfiguration config )
  {
    super( successor, config );
    this.setChainStrategy( AllHandleStrategy.getInstance( ) );
    this.logger = LoggerFactory.getLogger( SaveProxyAction.class );
  }
  
  /**
   * Checks if this processor can handle this request.<br/>
   * This always returns false...
   * @param aRequest The request to check for.
   * @return False, always.
   * @throws IllegalArgumentException Never thrown.
   */
  @Override( )
  public boolean canHandle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    return false;
  }
  
  /**
   * Concatenates the given path parts.
   * @param path1 The first part of the path.
   * @param path2 The second part of the path.
   * @return The concatenated path.
   */
  private String joinPath( final String path1, final String path2 )
  {
    final File finalpath = new File( path1, path2 );
    return finalpath.getPath( );
  }
  
  /**
   * This method handles a request, that in the case of this class is
   * saving a serialized version of the request in page_save_dir/requestId.xml.
   * @param aRequest The request to serialize.
   * @throws IllegalArgumentException If some error happens.
   */
  @Override( )
  public void handle( final ProxyPageElement aRequest ) throws IllegalArgumentException
  {
    this.logger.info( "Received an request with id {}", aRequest.getCurrentUUID( ) );
    if ( aRequest.getState( ).equals( ProxyRequestState.MORE_INFO_RECEIVED ) )
    {
      //Does nothing...
    }
  }
  
  /**
   * Returns the id of this processor.
   * @return The processor id, that is "Save Proxy Action".
   * @see com.assembla.ariane.jellyfish.proxy.ProxyAction#getActionID()
   */
  @Override( )
  public String getActionID( )
  {
    return "Save Proxy Action";
  }
  
}
