package com.assembla.ariane.jellyfish.proxy.storage.cache.util;

import java.io.Serializable;
import java.util.UUID;
import com.google.common.base.Objects;
import static java.lang.String.format;

/**
 * This class represents an key object in the cache storage system.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public class PageKey implements Serializable
{

  /** The version id. */
  private static final long serialVersionUID = -2442220178751301128L;

  /**
   * The id of the page represented by this key.
   */
  private final UUID pageId;

  /**
   * The uri of the page represented by this key.
   */
  private final String uri;

  /**
   * Creates a new page key.
   * @param mPageId The page id.
   * @param mUri The page uri.
   */
  public PageKey( java.util.UUID mPageId, String mUri )
  {
    if( (mPageId != null) && (mUri != null) )
    {
      this.pageId = mPageId;
      this.uri = mUri;
    }
    else throw new NullPointerException("One of the given arguments is null");
  }

  /**
   * Returns the id of the page.
   * @return The page id.
   */
  public java.util.UUID getPageId()
  {
    return pageId;
  }

  /**
   * Returns the uri of the page.
   * @return The page uri.
   */
  public String getUri()
  {
    return uri;
  }

  /**
   * Checks if the given object is equal to this key.
   * @param o The object to check.
   * @return True, if the objects are equal.
   */
  @Override( )
  public boolean equals( Object o )
  {
    if( this == o ) return true;
    if( o == null || getClass() != o.getClass() ) return false;

    com.assembla.ariane.jellyfish.proxy.storage.cache.util.PageKey pageKey =
      ( com.assembla.ariane.jellyfish.proxy.storage.cache.util.PageKey ) o;

    return pageId.equals( pageKey.pageId ) && uri.equals( pageKey.uri );

  }

  /**
   * Generates an hash code for this key.
   * @return The key hash code.
   */
  @Override( )
  public int hashCode()
  {
    return Objects.hashCode( pageId, uri );
  }

  /**
   * Returns an string representation of this object.
   * @return An string representing this object.
   */
  public String toString( )
  {
    StringBuilder sb = new StringBuilder();
    sb.append( "{ " );
    sb.append(format( "pageId: %s, ", pageId ));
    sb.append(format( "uri: %s ", uri ));
    sb.append("}");
    return sb.toString();
  }

}
