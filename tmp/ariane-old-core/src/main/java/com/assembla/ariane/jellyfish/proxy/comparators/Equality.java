/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.comparators;

/**
 * Interface used to specify an equality operator for an validator.
 * @author Fat Cat
 * @param <T> The equality value.
 */
public interface Equality< T >
{
  
  /**
   * Checks if both arguments are equal.
   * @param first The first argument to compare.
   * @param second The second argument to compare.
   * @return True, if they are.
   */
  boolean equals( T first, T second ); 
  
  /**
   * Checks if the given argument is a validator according to this equality
   * principle.
   * @param val The value to check for a valid validator.
   * @return True, if the value is a valid validator.
   */
  boolean isValidator( T val );
  
}
