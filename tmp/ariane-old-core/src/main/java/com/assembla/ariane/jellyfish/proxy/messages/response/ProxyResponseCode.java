/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.messages.response;

import java.io.Serializable;

/**
 * This object represents an proxy response code.
 * @author Fat Cat
 * @version 1
 * @since 0.0.6
 */
public final class ProxyResponseCode implements Serializable
{
  
  /** The class version.  */
  private static final long serialVersionUID = -6608638133612619353L;

  /** The status or response code. */
  private int responseCode;
  
  /** The optional associated exception. */
  private Exception exception;
  
  /** The associated message. */
  private String message;
  
  /**
   * Creates a new proxy response code.<br/>
   * Note that there is no exception associated with the returned,
   * proxy response code.
   * @param responseCode The numerical response code.
   * @param message The response code message.
   * @return The corresponding proxy response code.
   */
  public static ProxyResponseCode create( final int responseCode, final String message )
  {
    final ProxyResponseCode code = new ProxyResponseCode( );
    code.setMessage( message );
    code.setResponseCode( responseCode );
    return code;
  }
  
  /**
   * Creates a new proxy response code.<br/>
   * Note that the message, of this response code is the same as
   * the message of the given exception or the string
   * "Invalid exception given" if the exception is null.
   * @param responseCode The numerical response code.
   * @param ex The associated exception.
   * @return The corresponding proxy response code.
   */
  public static ProxyResponseCode create( final int responseCode, final Exception ex )
  {
    final ProxyResponseCode code = new ProxyResponseCode( );
    code.setMessage( ( ex != null ) ? ex.getMessage( ) : "Invalid exception given" );
    code.setException( ex );
    code.setResponseCode( responseCode );
    return code;
  }
  
  /**
   * Creates a new proxy response code.
   * @param responseCode The numerical response code.
   * @param message The response code message.
   * @param ex The associated exception.
   * @return The corresponding proxy response code.
   */
  public static ProxyResponseCode create( final int responseCode, final String message, final Exception ex )
  {
    final ProxyResponseCode code = new ProxyResponseCode( );
    code.setMessage( message );
    code.setException( ex );
    code.setResponseCode( responseCode );
    return code;
  }
  
  /** Creates a new proxy response code. */
  private ProxyResponseCode( )
  {
    this.responseCode = -1;
    this.exception = null;
    this.message = null;
  }
  
  // Response code
  
  /**
   * Returns the numerical response code.
   * @return The numerical response code.
   */
  public int getResponseCode( )
  {
    return this.responseCode;
  }
  
  /**
   * Changes the numerical response code.
   * @param responseCode The new numerical response code.
   */
  public void setResponseCode( final int responseCode )
  {
    if ( responseCode > 0 )
    {
      this.responseCode = responseCode;
    }
  }
  
  // Exception
  
  /**
   * Returns the associated exception, or null if there is 
   * no associated exception.
   * @return The associated exception.
   */
  public Exception getException( )
  {
    return this.exception;
  }
  
  /**
   * Changes the associated exception.
   * @param ex The new associated exception.
   */
  public void setException( final Exception ex )
  {
    if ( ex != null )
    {
      this.exception = ex;
    }
  }
  
  /**
   * Checks if there is an associated exception.
   * @return True, if there is.
   */
  public boolean hasException( )
  {
    return this.exception != null;
  }
  
  // Message
  
  /**
   * Returns the response message.
   * @return The response message.
   */
  public String getMessage( )
  {
    return this.message;
  }
  
  /**
   * Changes the response message.
   * @param message The new response message.
   */
  public void setMessage( final String message )
  {
    if ( ( message != null ) && ( message.length( ) > 0 ) )
    {
      this.message = message;
    }
  }
  
  // HTML error page
  
  /**
   * Returns an string containing a html representation,
   * of the associated error.
   * @param description An optional human description. 
   * @return The associated html represention page.
   */
  public String getHtmlPage( final String description )
  {
    final StringBuilder sb = new StringBuilder( );
    
    sb.append( "<html>" );
    sb.append( "<head>" );
    sb.append( "<title>" );
    sb.append( this.getMessage( ) );
    sb.append( "</title>" );
    sb.append( "</head>" );
    sb.append( "<body>" );
    sb.append( "<h1>" );
    sb.append( this.getMessage( ) );
    sb.append( "</h1>" );
    sb.append( "<br/>&nbsp;" );
    if ( description != null )
    {
      sb.append( "<h2>Description</h2>" );
      sb.append( description );
      sb.append( "<br/>&nbsp;" );
    }
    if ( this.hasException( ) )
    {
      sb.append( "<h2>Detailed Error: </h2>" );
      sb.append( this.exception.getMessage( ) );
      sb.append( "<br/>&nbsp;" );
      sb.append( "<h2>Stack Trace</h2>" );
      sb.append( "<br/>&nbsp;" );
      sb.append( "<p>" );
      for ( StackTraceElement element : this.exception.getStackTrace( ) )
      {
        sb.append( element.toString( ) );
      }
      sb.append( "</p>" );
      sb.append( "<br/>&nbsp;" );
    }
    sb.append( "</body>" );
    sb.append( "</html>" );
    
    return sb.toString( );
  }
  
  /**
   * Returns an string representation of this response code.
   * @return An string representation of this response code.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    final StringBuilder sb = new StringBuilder( );
    sb.append( this.responseCode );
    sb.append( " " );
    sb.append( this.message );
    return sb.toString( );
  }
  
}
