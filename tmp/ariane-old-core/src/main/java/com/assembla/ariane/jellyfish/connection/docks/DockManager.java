/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.connection.docks;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Set;
import com.assembla.ariane.jellyfish.main.JellyfishConstants;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the class that manages all of the existing docks.
 * @author Fat Cat
 * @since 0.0.1
 * @version 2
 */
public final class DockManager
{
  
  /** The existing docks. */
  private final HashMap< String, Dock > docks;
  
  /** The Logger. */
  private Logger logger;
  
  /** Is the dock manager started? */
  private boolean isStarted;
  
  /** The dock manager singleton. */
  private static final DockManager inst = new DockManager( );
  
  /**
   * Returns the dock manager singleton.
   * @return The singleton.
   */
  public static DockManager instance( ) 
  { 
    return DockManager.inst; 
  }
  
  /**
   * Creates a new dock manager.
   */
  private DockManager( ) 
  { 
    this.docks = new HashMap< String, Dock >( ); 
    this.logger = LoggerFactory.getLogger( DockManager.class );
  }
  
  /**
   * Adds a new dock to the dock manager.
   * @param key The name of the dock
   * @param dock The new dock
   */
  public void add( final String key, final Dock dock )
  {
    if( !this.containsKey( key ) ) 
    { 
      this.docks.put( key, dock ); 
    }
  }
  
  /**
   * Clears all the existing docks.
   */
  public void clear( )
  {
    this.docks.clear( );
  }
  
  /**
   * Configures this dock manager, by filling it with the configuration.
   * @param configuration The jellyfish configuration object
   * @throws InvocationTargetException If an dock cannot be created.
   */
  public void configure( final HierarchicalConfiguration configuration ) throws InvocationTargetException
  {
    this.logger.info( "Configuring the dock manager" );
    for( final HierarchicalConfiguration dockInfo : configuration.configurationsAt( "dock" ) )
    {      
      final Dock dock = DockFactory.instance( ).get( dockInfo.getString( "[@type]", "NULL" ), dockInfo );      
      this.logger.info( "Added a new {} dock", Integer.toString( dock.getDockType( ) ) );
      
      if( ( dock != JellyfishConstants.NullDock ) && ( !this.containsKey( dock.getName( ) ) ) )
      {
        this.add( dock.getName( ), dock );
      }
    }
  }
  
  /**
   * Checks if the dock manager has the specified key.
   * @param key The key to check
   * @return True if the key exists.
   */
  public boolean containsKey( final String key )
  {
    return this.docks.containsKey( key );
  }
  
  /**
   * Returns the dock corresponding to the given key.
   * @param key The name of the dock to return.
   * @return The corresponding dock or the null dock if no such key exists.
   */
  public Dock get( final String key )
  {
    if( this.docks.containsKey( key ) ) 
    {
      return this.docks.get( key );
    }
    else
    {
      return JellyfishConstants.NullDock;
    }
  }
  
  /**
   * Checks if the dock manager is empty.
   * @return True if the dock manager is empty, false otherwise
   */
  public boolean isEmpty( )
  {
    return this.docks.isEmpty( );
  }
  
  /**
   * Checks if this dock is started.
   * @return True, if it is.
   */
  public synchronized boolean isStarted( )
  {
    return this.isStarted;
  }
  
  /**
   * Returns an set containing the names of all docks inside this dock manager.
   * @return An set of dock names.
   */
  public Set< String > keySet( )
  {
    return this.docks.keySet( );
  }
  
  /**
   * Removes the dock corresponding to the given key.
   * @param key The name of the dock to remove
   * @return The removed dock or null dock if the key is invalid.
   */
  public Dock remove( final String key )
  { 
    if( this.docks.containsKey( key ) ) 
    {
      return this.docks.remove( key );
    }
    else
    {
      return JellyfishConstants.NullDock;
    }
  }
  
  /**
   * Returns the number of known docks.
   * @return The number of existing docks
   */
  public int size( )
  {
    return this.docks.size( );
  }
  
  /**
   * Starts this dock manager and all of the contained docks.
   * @throws Exception If some error happens.
   */
  public void start( ) throws Exception
  {
    this.logger.info( "Starting the dock manager" );
    if( !this.isStarted )
    {
      for( final Dock dock : this.docks.values( ) )
      {
        dock.start( true, true );
        this.logger.info( "Started the dock {}", dock.getName( ) );
      }
      this.isStarted = true;
    }
  }
  
  /**
   * Stops this dock manager and all of the contained docks.
   * @throws Exception If some error happens.
   */
  public void stop( ) throws Exception
  {
    this.logger.info( "Stopping the dock manager" );
    if( this.isStarted )
    {
      for( final Dock dock : this.docks.values( ) )
      {
        dock.stop( true, true );
        this.logger.info( "Stopped the dock {}", dock.getName( ) );
      }
      this.isStarted = false;
    }
  }
}
