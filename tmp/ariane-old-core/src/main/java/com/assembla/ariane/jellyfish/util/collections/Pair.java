/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import com.google.common.base.Objects;

/**
 * This class represents an serializable pair.
 * @author Fat Cat
 * @version 3
 * @since 0.0.1
 * @param <K> The first item of this pair.
 * @param <V> The second item of this pair.
 */
@XmlType( name = "pair" )
public class Pair< K, V > implements Serializable
{
  
  /** The version id of this object. */
  private static final long serialVersionUID = -6734791974625576856L;

  /** This is the first value of this pair. */
  @XmlElement( name = "first" )
  private K key;
  
  /** This is the second value of this pair. */
  @XmlElement( name = "second" )
  private V value;
  
  /**
   * Creates a new pair, with the default values.
   */
  public Pair( )
  {
    this.key = null;
    this.value = null;
  }
  
  /**
   * Creates a new pair.
   * @param key The first item of the pair.
   * @param value The second item of the pair.
   */
  public Pair( final K key, final V value )
  {
    this.key = key;
    this.value = value;
  }
  
  /**
   * Changes the key of the pair.
   * @param key The new key of the pair.
   */
  public void setKey( final K key )
  {
    if ( key != this.key )
    {
      this.key = key;
    }
  }
  
  /**
   * Returns the key of the pair.
   * @return The pair key.
   */
  @XmlTransient( )
  public K getKey( )
  {
    return this.key;
  }
  
  /**
   * Returns the value of the pair.
   * @return The pair value.
   */
  @XmlTransient( )
  public V getValue( )
  {
    return this.value;
  }
  
  /**
   * Changes the value of the pair.
   * @param value The new pair value.
   */
  public void setValue( final V value )
  {
    if ( value != this.value )
    {
      this.value = value;
    }
  }
  
  /**
   * Returns the first value of this pair.
   * @return The first value of this pair.
   */
  @XmlTransient( )
  public K getFirst( )
  {
    return this.key;
  }
  
  /**
   * Changes the first value of this pair.
   * @param first The first value of this pair.
   */
  public void setFirst( final K first )
  {
    this.setKey( first );
  }
  
  /**
   * Returns the second value of this pair.
   * @return The second value of this pair.
   */
  @XmlTransient( )
  public V getSecond( )
  {
    return this.value;
  }
  
  /**
   * Changes the second value of this pair.
   * @param second The new second value of this pair.
   */
  public void setSecond( final V second )
  {
    this.setValue( second );
  }
  
  /**
   * Checks if these objects are equal.
   * @param obj The other object to compare to.
   * @return True, if they are.
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @SuppressWarnings( "unchecked" )
  public boolean equals( final Object obj )
  {
    if ( obj instanceof Pair )
    {
      final Pair< Object, Object > pair = ( Pair< Object, Object > ) obj;
      return ( pair.getFirst( ).equals( this.key ) ) && ( pair.getSecond( ).equals( this.value ) );
    }
    return false;
  }
  
  /**
   * Calculates and returns an hash code for this pair.
   * @return The calculated hash code of this pair.
   * @see java.lang.Object#hashCode()
   */
  public int hashCode( )
  {
    return Objects.hashCode( this.key, this.value );
  }

  /**
   * Returns an text representation of this pair.
   * @return An string representing this pair.
   * @see java.lang.Object#toString()
   */
  @Override( )
  public String toString( )
  {
    return String.format( "(%s, %s)", 
        this.key.toString( ), this.value.toString( ) );
  }
  
}
