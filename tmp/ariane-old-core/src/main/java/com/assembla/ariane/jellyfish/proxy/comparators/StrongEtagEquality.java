/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.proxy.comparators;

/**
 * Compares two validators, using the string etag validation. 
 * @author Fat Cat
 * @since 0.0.6
 * @version 1
 */
public final class StrongEtagEquality implements Equality< String >
{
  
  /** The singleton object. */
  private static final Equality< String > inst = new StrongEtagEquality();
  
  /**
   * Returns the strong etag equality operator singleton.
   * @return The strong etag operator singleton.
   */
  public static Equality< String > instance( )
  {
    return StrongEtagEquality.inst;
  }
  
  /**
   * Creates a new strong etag validator.
   */
  private StrongEtagEquality( )
  {
    //Does nothing...
  }
  
  /**
   * Checks if both validators are equal.
   * @param first The first validator.
   * @param second The second validator.
   * @return True, if they are.
   * @see com.assembla.ariane.jellyfish.proxy.comparators.Equality#equals(java.lang.Object, java.lang.Object)
   */
  @Override( )
  public boolean equals( final String first, final String second )
  {
    //Check if both validators are strong etags...
    if( this.isValidator( first ) && this.isValidator( second ) )
    {      
      //strong etags should be strings with the same size...
      if( first.length( ) != second.length( ) )
      {
        return false;
      }
      else
      {        
        //Compare characters bit by bit...
        for( int i = 0; i < first.length( ); i++ )
        {
          if( first.charAt( i ) != second.charAt( i ) )
          {
            return false;
          }
        }        
        return true;        
      }
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Checks if the given validator is a strong e-tag validator.
   * @param candidate The validator to check for.
   * @return True, if the validator is a strong e-tag.
   * @see com.assembla.ariane.jellyfish.proxy.comparators.Equality#isValidator(java.lang.Object)
   */
  @Override( )
  public boolean isValidator( final String candidate )
  {
    if ( candidate.matches( "([a-zA-Z]{3}),\\s([0-9]{2})\\s([a-zA-Z]{3})\\s([0-9]{4})\\s([0-9]{2}):([0-9]{2}):([0-9]{2})\\sGMT" ) )
    {
      //This is a date field...
      return false;
    }
    else if ( candidate.startsWith( "W/" ) )
    {
      //This is a weak e-tag...
      return false;
    }
    else
    {
      //This is a strong e-tag, by exclusion...
      return true;
    }
  }
  
}
