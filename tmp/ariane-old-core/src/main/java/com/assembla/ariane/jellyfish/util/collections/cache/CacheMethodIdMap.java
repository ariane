/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.util.collections.cache;

import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that holds a mapping between the method and the uuid of pages in cache.
 * @author Fat Cat
 * @since 0.0.5
 * @version 2
 */
public final class CacheMethodIdMap
{
  /** Variable that stores the mapping holder. */
  private final SortedMap< String, SortedSet< UUID >> mappings;
  
  /** An logger object. */
  private final Logger logger;
  
  /** The singleton object. */
  private static final CacheMethodIdMap inst = new CacheMethodIdMap( );
  
  /**
   * Returns the instance of this singleton. 
   * @return The id mapping singleton.
   */
  public static CacheMethodIdMap instance( )
  {
    return CacheMethodIdMap.inst;
  }
  
  /**
   * Creates a new map.
   */
  private CacheMethodIdMap( )
  {
    this.mappings = Collections.synchronizedSortedMap( new TreeMap< String, SortedSet< UUID >>( ) );
    this.logger = LoggerFactory.getLogger( CacheMethodIdMap.class );
  }
  
  /**
   * Clears all mappings.
   */
  public void clear( )
  {
    this.logger.debug( "Cleaning all the entries." );
    this.mappings.clear( );
  }
  
  /**
   * Checks if the given method is contained in this mapping.
   * @param method The method name.
   * @return True, if it is.
   */
  public boolean contains( final String method )
  {
    return this.mappings.containsKey( method );
  }
  
  /**
   * Returns the ids of the associated method.
   * @param method The method name.
   * @return The associated ids, or null if the method is unknown.
   */
  public SortedSet< UUID > getId( final String method )
  {
    if ( this.contains( method ) ) 
    {
      return Collections.unmodifiableSortedSet( this.mappings.get( method ) );
    }
    else
    {
      return null;
    }
  }
  
  /**
   * Checks if this mapping is empty.
   * @return True, if it is.
   */
  public boolean isEmpty( )
  {
    return this.mappings.isEmpty( );
  }
  
  /**
   * Adds a new method and id pair to the map.
   * @param method The method name.
   * @param requestId The associated id.
   */
  public void add( final String method, final UUID requestId )
  {
    if ( !this.contains( method ) )
    {
      this.mappings.put( method, new TreeSet< UUID >( ) );
    }
    if ( !this.mappings.get( method ).contains( requestId ) )
    {
      this.mappings.get( method ).add( requestId );
    }
  }
  
  /**
   * Removes the mapping for the given method.
   * @param method The method name to remove.
   * @param requestId The associated id.
   * @return True, on success.
   */
  public boolean remove( final String method, final UUID requestId )
  {
    if ( this.contains( method ) && this.mappings.get( method ).contains( requestId ) )
    {
      this.mappings.get( method ).remove( requestId );
      this.logger.debug( "Removing the entry {}", requestId.toString( ) );
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Returns the number of entries in this mapping.
   * @return The entry number.
   */
  public int size( )
  {
    return this.mappings.size( );
  }
  
  /**
   * Removes the mapping for the given method.
   * @param method The method name to remove.
   * @return True, on success.
   */
  public boolean removeAll( final String method )
  {
    if ( this.contains( method ) )
    {
      this.mappings.get( method ).clear( );
      this.logger.debug( "Removing the entries with method {}", method );
      return true;
    }
    else
    {
      return false;
    }
  }
  
  /**
   * Returns the number of entries in this mapping.
   * @param method The method name.
   * @return The entry number.
   */
  public int sizeEntry( final String method )
  {
    if ( this.contains( method ) ) 
    {
      return this.mappings.get( method ).size( );
    }
    else
    {
      return -1;
    }
  }
  
  /**
   * Returns the internal map containing the entries.
   * @return The internal map in read-only mode.
   */
  public synchronized Map< String, SortedSet< UUID >> getMappings( )
  {
    return Collections.unmodifiableSortedMap( this.mappings );
  }
}
