/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.test;

import com.assembla.ariane.jellyfish.connection.docks.Dock;
import org.apache.commons.configuration.HierarchicalConfiguration;

/**
 * Mock dock.
 * 
 * @author Fat Cat
 */
public class TestDock extends Dock
{
  
  public boolean hasClient;
  
  public boolean hasServer;
  
  /**
   * Creates a new mock.
   * @param config The configure.
   */
  public TestDock( final HierarchicalConfiguration config )
  {
    super( config );
    this.dockType = 2;
    for ( final HierarchicalConfiguration socketInfo : config.configurationsAt( "entry" ) )
    {
      if ( socketInfo.getString( "[@type]", "" ).equals( "client" ) )
      {
        this.hasClient = true;
      }
      else if ( socketInfo.getString( "[@type]", "" ).equals( "server" ) )
      {
        this.hasServer = true;
      }
    }
  }
  
  /**
   * Starts the mock.
   * @param startClient Client.
   * @param startServer Server.
   * @throws Exception ex. 
   * @see com.assembla.ariane.jellyfish.connection.docks.Dock#start(boolean, boolean)
   */
  @Override( )
  public void start( final boolean startClient, final boolean startServer ) throws Exception
  {
    if ( this.hasClient && startClient && !this.isClientStarted )
    {
      this.isClientStarted = true;
    }
    if ( this.hasServer && startServer && !this.isServerStarted )
    {
      this.isServerStarted = true;
    }
  }
  
  /**
   * Stops the mock.
   * @param stopClient Client.
   * @param stopServer Server.
   * @throws Exception ex. 
   * @see com.assembla.ariane.jellyfish.connection.docks.Dock#stop(boolean, boolean)
   */
  @Override( )
  public void stop( final boolean stopClient, final boolean stopServer ) throws Exception
  {
    if ( this.hasClient && stopClient && this.isClientStarted )
    {
      this.isClientStarted = false;
    }
    if ( this.hasServer && stopServer && this.isServerStarted )
    {
      this.isServerStarted = false;
    }
  }
  
}
