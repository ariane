/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.test;

import com.assembla.ariane.jellyfish.config.JellyfishConfiguration;
import junit.framework.Assert;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the configuration.
 * @author Fat Cat
 */
public class JellyfishConfigurationTest
{
  
  /**
   * Creates something.
   */
  public JellyfishConfigurationTest( )
  {
    
  }
  
  private JellyfishConfiguration configuration;
  
  /**
   * Creates a new configuration based on a test configuration file.
   * @throws java.lang.Exception If some error happens.
   */
  @Before( )
  public void setUp( ) throws Exception
  {
    this.configuration = new JellyfishConfiguration( );
    this.configuration.init( "src/test/resources/TestConfigurationFile.xml" );
  }
  
  /**
   * Destroys the created configuration.
   * @throws java.lang.Exception If some error happens.
   */
  @After( )
  public void tearDown( ) throws Exception
  {
    this.configuration = null;
  }
  
  /**
   * Sees if the configuration file was opened correctly.
   */
  @Test( )
  public void openTest( )
  {
    Assert.assertFalse( this.configuration.isEmpty( ) );
  }
  
  /**
   * Tries to retrieve some simple values from the configuration file.
   */
  @Test( )
  public void retrieveTest( )
  {
    Assert.assertEquals( "Test1", this.configuration.getConfig( ).getString( "name" ) );
    Assert.assertEquals( "Today", this.configuration.getConfig( ).getString( "date" ) );
    Assert.assertEquals( 12, this.configuration.getConfig( ).getInt( "number" ) );
    Assert.assertEquals( 56, this.configuration.getConfig( ).getInt( "age" ) );
    Assert.assertEquals( 12, this.configuration.getConfig( ).getInt( "foo[@type]" ) );
    Assert.assertEquals( "Test", this.configuration.getConfig( ).getString( "foo.name" ) );
    Assert.assertEquals( ( float ) 4.5, this.configuration.getConfig( ).getFloat( "foo.age" ), 0.1 );
    Assert.assertEquals( "hours", this.configuration.getConfig( ).getString( "foo.age[@units]" ) );
  }
  
  /**
   * Tries to retrieve some complex values from the configuration file.
   */
  @Test( )
  public void retrieveComplex( )
  {
    for ( final HierarchicalConfiguration config : this.configuration.getConfig( ).configurationsAt( "foo.compounds" ) )
    {
      Assert.assertEquals( "Test", config.getString( "compound.name" ) );
    }
  }
}
