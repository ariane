/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import com.assembla.ariane.jellyfish.util.collections.server.HTTPSentIdHolder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test class for testing the HTTPSentIdHolder class.
 * @author Fat Cat
 */
public final class HTTPSentIdHolderTest
{
  
  /**
   * Clear the sample HTTPSentIdHolder.
   * @throws java.lang.Exception error.
   */
  @After( )
  public void tearDown( ) throws Exception
  {
    HTTPSentIdHolder.instance( ).clear( );
  }
  
  /**
   * Test method for {@link com.assembla.ariane.jellyfish.util.collections.server.HTTPSentIdHolder#getId(java.lang.String)}.
   */
  @Test( )
  public void testGetId( )
  {
    final List< UUID > idList = new ArrayList<>( 8 );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.o.test", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.g.test", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.h.test", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.i.test", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.y.test", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.f.test", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "123.067.090", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.a.test", idList.get( idList.size( ) - 1 ) );
    
    idList.add( UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.m.test", idList.get( idList.size( ) - 1 ) );
    
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.o.test" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.g.test" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.h.test" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.i.test" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.y.test" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.f.test" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "123.067.090" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.a.test" ) ) );
    Assert.assertTrue( idList.contains( HTTPSentIdHolder.instance( ).getId( "www.m.test" ) ) );
  }
  
  /**
   * Test method for 
   */
  @Test( )
  public void testAdd( )
  {
    HTTPSentIdHolder.instance( ).add( "www.o.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.g.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.h.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.i.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.y.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.f.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "123.067.090", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.a.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.m.test", UUID.randomUUID( ) );
    
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.o.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.g.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.h.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.i.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.y.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.f.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "123.067.090" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.a.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.m.test" ) );
    Assert.assertFalse( HTTPSentIdHolder.instance( ).contains( "www.ra.test" ) );
    Assert.assertFalse( HTTPSentIdHolder.instance( ).contains( "www.s.test" ) );
    Assert.assertFalse( HTTPSentIdHolder.instance( ).contains( "www.fr.test" ) );
  }
  
  /**
   * Test method for {@link com.assembla.ariane.jellyfish.util.collections.server.HTTPSentIdHolder#remove(java.lang.String)}.
   */
  @Test( )
  public void testRemove( )
  {
    HTTPSentIdHolder.instance( ).add( "www.o.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.g.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.h.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.i.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.y.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.f.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "123.067.090", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.a.test", UUID.randomUUID( ) );
    HTTPSentIdHolder.instance( ).add( "www.m.test", UUID.randomUUID( ) );
    
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "123.067.090" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.a.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.m.test" ) );
    
    HTTPSentIdHolder.instance( ).remove( "123.067.090" );
    
    Assert.assertFalse( HTTPSentIdHolder.instance( ).contains( "123.067.090" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.a.test" ) );
    Assert.assertTrue( HTTPSentIdHolder.instance( ).contains( "www.m.test" ) );
  }
  
}
