/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.test;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.TreeMap;
import com.assembla.ariane.jellyfish.proxy.storage.cache.util.CacheList;
import net.sf.ehcache.CacheManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the cache list for an consistency. 
 * @author Fat Cat
 */
public class CacheListTest
{
  
  /**
   * Constructor.
   */
  public CacheListTest( )
  {
    
  }
  
  private CacheList< Integer, String > clist;
  
  private CacheManager manager;
  
  private TreeMap< Integer, String > addedItems;
  
  /**
   * Fills the map with values.
   * @param numberItems The number of items.
   * @throws NoSuchAlgorithmException If some error happens.
   */
  private void fillMap( final int numberItems ) throws NoSuchAlgorithmException
  {
    final SecureRandom random = SecureRandom.getInstance( "SHA1PRNG" );
    for ( int i = 0; i < numberItems; i++ )
    {
      this.addedItems.put( random.nextInt( 6000 ), Long.toString( Double.doubleToLongBits( random.nextDouble( ) ) ) );
    }
  }
  
  /**
   * Changes the map.
   * @throws NoSuchAlgorithmException If some error happens.
   */
  private void changeMap( ) throws NoSuchAlgorithmException
  {
    final SecureRandom random = SecureRandom.getInstance( "SHA1PRNG" );
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      this.addedItems.put( key, Long.toString( Double.doubleToLongBits( random.nextDouble( ) ) ) );
    }
  }
  
  /**
   * Sets up the test.
   * @throws java.lang.Exception If some error happens.
   */
  @Before( )
  public void setUp( ) throws Exception
  {
    this.addedItems = new TreeMap<>( );
    final File file = new File( "src/test/resources/ehcache.xml" ).getAbsoluteFile( );
    this.manager = CacheManager.create( file.toString( ) );
    this.clist = new CacheList<>( this.manager );
  }
  
  /**
   * Tears down the test.
   * @throws java.lang.Exception If some error happens.
   */
  @After( )
  public void tearDown( ) throws Exception
  {
    this.clist.clearLists( );
    this.addedItems.clear( );
    this.addedItems = null;
  }
  
  /**
   * Tests the add, contains, get and size methods of the cache, in order
   * to see if one can add a cache named after a consistency.
   * @throws Exception If something strange happens.
   */
  @Test( )
  public void testContainsGetSizeAddCache( ) throws Exception
  {
    this.clist.add( "test", "test" );
    
    Assert.assertTrue( this.clist.contains( "test" ) );
    Assert.assertEquals( "test", this.clist.get( "test" ).getName( ) );
    Assert.assertEquals( 1, this.clist.size( ) );
  }
  
  /**
   * Tests the remove method, to see if a consistency can be removed. 
   * @throws Exception If some error happens.
   */
  @Test( )
  public void testRemoveCache( ) throws Exception
  {
    this.clist.add( "test", "test" );
    
    Assert.assertTrue( this.clist.contains( "test" ) );
    Assert.assertNotNull( this.clist.get( "test" ) );
    Assert.assertEquals( "test", this.clist.get( "test" ).getName( ) );
    
    this.clist.remove( "test" );
    
    Assert.assertFalse( this.clist.contains( "test" ) );
    Assert.assertNull( this.clist.get( "test" ) );
  }
  
  /**
   * Adds a new item to the cache.
   * @throws Exception If some error happens.
   */
  @Test( )
  public void testAddItemCache( ) throws Exception
  {
    this.clist.add( "test", "test" );
    
    this.fillMap( 12 ); //Adds 12 items to the cache (only memory should be used)
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      this.clist.add( "test", key, this.addedItems.get( key ) );
    }
    
    Assert.assertEquals( this.addedItems.size( ), this.clist.size( "test" ) );
    
    this.clist.get( "test" ).flush( ); //The items were flushed to the disk
    
  }
  
  /**
   * Returns an item from the cache.
   * @throws Exception If some error happens
   */
  @Test( )
  public void testGetItemCache( ) throws Exception
  {
    this.clist.add( "test", "test" );
    
    this.fillMap( 12 ); //Adds 12 items to the cache (only memory should be used)
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      Assert.assertTrue( this.clist.add( "test", key, this.addedItems.get( key
      ) ) );
    }
    
    Assert.assertEquals( this.addedItems.size( ), this.clist.size( "test" ) );
    
    this.clist.get( "test" ).flush( ); //The items were flushed to the disk
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      Assert.assertEquals( this.addedItems.get( key ), this.clist.get( "test", key ) );
    }
  }

  /**
   * Changes an item from the cache.
   * @throws Exception If some error happens.
   */
  @Test( )
  public void testChangeItemCache( ) throws Exception
  {
    this.clist.add( "test", "test" );
    
    this.fillMap( 12 ); //Adds 12 items to the cache (only memory should be used)
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      Assert.assertTrue( this.clist.add( "test", key, this.addedItems.get( key
      ) ) );
    }
    
    Assert.assertEquals( this.addedItems.size( ), this.clist.size( "test" ) );
    
    this.changeMap( ); //Same keys, different values.
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      this.clist.change( "test", key, this.addedItems.get( key ) );
    }
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      Assert.assertEquals( this.addedItems.get( key ),
                           this.clist.get( "test", key ) );
    }
    
    this.clist.get( "test" ).flush( ); //The items were flushed to the disk
    
    this.changeMap( ); //Same keys, different values.
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      this.clist.change( "test", key, this.addedItems.get( key ) ); //change on disk
    }
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      Assert.assertEquals( this.addedItems.get( key ),
                           this.clist.get( "test", key ) ); //get on disk
    }
  }

  /**
   * Removes an item from the cache.
   * @throws Exception If some error happens.
   */
  @Test( )
  public void testRemoveItemCache( ) throws Exception
  {
    this.clist.add( "test", "test" );
    
    this.fillMap( 12 ); //Adds 12 items to the cache (only memory should be used)
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      Assert.assertTrue( this.clist.add( "test", key,
                                           this.addedItems.get( key ) ) );
    }
    
    Assert.assertEquals( this.addedItems.size( ), this.clist.size( "test" ) );
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      this.clist.remove( "test", key );
    }
    
    Assert.assertEquals( 0, this.clist.size( "test" ) );
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      Assert.assertTrue( this.clist.add( "test",
                                           key, this.addedItems.get( key ) ) );
    }
    
    Assert.assertEquals( this.addedItems.size( ), this.clist.size( "test" ) );
    
    this.clist.get( "test" ).flush( ); //The items were flushed to the disk
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      this.clist.remove( "test", key ); //Remove from disk...
    }
    
    Assert.assertEquals( 0, this.clist.size( "test" ) );
  }

}
