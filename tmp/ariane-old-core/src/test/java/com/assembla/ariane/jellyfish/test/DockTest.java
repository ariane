/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.test;

import com.assembla.ariane.jellyfish.config.JellyfishConfiguration;
import com.assembla.ariane.jellyfish.connection.docks.DockFactory;
import com.assembla.ariane.jellyfish.connection.docks.DockManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the docking system aka the dock manager.
 * 
 * @author Fat Cat
 */
public class DockTest
{
  
  /**
   * Constructor.
   */
  public DockTest( )
  {
    
  }
  
  /**
   * Initializes the dock manager singleton.
   * @throws java.lang.Exception error.
   */
  @Before( )
  public void setUp( ) throws Exception
  {
    DockFactory.instance( ).add( "test", TestDock.class );
    DockManager.instance( ).clear( );
  }
  
  /**
   * Cleans the dock manager.
   * @throws java.lang.Exception error
   */
  @After( )
  public void tearDown( ) throws Exception
  {
    DockFactory.instance( ).remove( "test" );
    DockManager.instance( ).clear( );
  }
  
  /**
   * Test using a configuration with one test dock.
   * @throws Exception error 
   */
  @Test( )
  public void simpleTest( ) throws Exception
  {
    DockManager.instance( ).configure( new JellyfishConfiguration( "src/test/resources/TestConfigurationOne.xml" ).getConfig( ) );
    Assert.assertFalse( DockManager.instance( ).isEmpty( ) );
    
    Assert.assertTrue( DockManager.instance( ).containsKey( "test1" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test1" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).start( );
    
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).stop( );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
  }
  
  /**
   * Test using a configuration with two test docks.
   * @throws Exception error 
   */
  @Test( )
  public void oneNullOneTest( ) throws Exception
  {
    DockManager.instance( ).configure( new JellyfishConfiguration( "src/test/resources/TestConfigurationTwo.xml" ).getConfig( ) );
    Assert.assertFalse( DockManager.instance( ).isEmpty( ) );
    
    Assert.assertTrue( DockManager.instance( ).containsKey( "test1" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test1" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertTrue( DockManager.instance( ).containsKey( "test2" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test2" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).start( );
    
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).stop( );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
  }
  
  /**
   * Test using a configuration with one test dock and one client test dock.
   * @throws Exception error
   */
  @Test( )
  public void testDockOneHalfTest( ) throws Exception
  {
    DockManager.instance( ).configure( new JellyfishConfiguration( "src/test/resources/TestConfigurationThree.xml" ).getConfig( ) );
    Assert.assertFalse( DockManager.instance( ).isEmpty( ) );
    
    Assert.assertTrue( DockManager.instance( ).containsKey( "test1" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test1" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertTrue( DockManager.instance( ).containsKey( "test2" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test2" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).start( );
    
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).stop( );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
  }
  
  /**
   * Test using a configuration with one test dock, without using the manager to
   * start the docks.
   * 
   * @throws Exception error 
   */
  @Test( )
  public void complexTest( ) throws Exception
  {
    DockManager.instance( ).configure( new JellyfishConfiguration( "src/test/resources/TestConfigurationOne.xml" ).getConfig( ) );
    Assert.assertFalse( DockManager.instance( ).isEmpty( ) );
    
    Assert.assertTrue( DockManager.instance( ).containsKey( "test1" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test1" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).start( true, false );
    
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).stop( true, true );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).start( false, true );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).stop( true, false );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).start( true, false );
    
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).stop( true, true );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
  }
  
  /**
   * Test using a configuration with one client test dock and one complete test dock, 
   * without using the manager to start the docks.
   * 
   * @throws Exception error 
   */
  @Test( )
  public void complexTwoDocksTest( ) throws Exception
  {
    DockManager.instance( ).configure( new JellyfishConfiguration( "src/test/resources/TestConfigurationFour.xml" ).getConfig( ) );
    Assert.assertFalse( DockManager.instance( ).isEmpty( ) );
    
    //Full test dock
    Assert.assertTrue( DockManager.instance( ).containsKey( "test1" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test1" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    //Client test dock
    Assert.assertTrue( DockManager.instance( ).containsKey( "test2" ) );
    Assert.assertEquals( 2, DockManager.instance( ).get( "test2" ).getDockType( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).start( true, false );
    DockManager.instance( ).get( "test2" ).start( true, false );
    
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertTrue( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).stop( true, true );
    DockManager.instance( ).get( "test2" ).stop( true, true );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).start( false, true );
    DockManager.instance( ).get( "test2" ).start( false, true );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).stop( true, false );
    DockManager.instance( ).get( "test2" ).stop( true, false );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).start( true, false );
    DockManager.instance( ).get( "test2" ).start( true, false );
    
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertTrue( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertTrue( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
    
    DockManager.instance( ).get( "test1" ).stop( true, true );
    DockManager.instance( ).get( "test2" ).stop( true, true );
    
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test1" ).IsServerStarted( ) );
    
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsClientStarted( ) );
    Assert.assertFalse( DockManager.instance( ).get( "test2" ).IsServerStarted( ) );
  }
  
}
