/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.test;

import static org.junit.Assert.*;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import com.assembla.ariane.jellyfish.util.html.visitors.StandardVFCLinkVisitor;
import com.assembla.ariane.jellyfish.util.html.visitors.VFCLinkInformation;
import com.assembla.ariane.jellyfish.util.html.visitors.VisitedLinkType;
import org.htmlparser.Parser;
import org.htmlparser.util.ParserException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the VFC link visitor.
 * @author Fat Cat
 * @since 0.0.7
 * @version 1
 */
public class VFCLinkVisitorTest
{
  
  private StandardVFCLinkVisitor visitor;
  
  /**
   * Sets up the test.
   * @throws java.lang.Exception aa
   */
  @Before( )
  public void setUp( ) throws Exception
  {
    this.visitor = new StandardVFCLinkVisitor( );
  }
  
  /**
   * Clears the test.
   * @throws java.lang.Exception aa
   */
  @After( )
  public void tearDown( ) throws Exception
  {
    this.visitor = null;
  }
  
  /**
   * This test uses the visitor with some html pages that contain various types
   * of a tag links. 
   * @throws ParserException 
   */
  @Test( )
  public void linkTest( ) throws ParserException
  {
    
    Set< VFCLinkInformation > links = this.parseFile( 
        "src/test/resources/AbsoluteHttpFtpLinks.html" );
    assertEquals( links.size( ), 15 );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple1.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple2.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple3.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple4.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple6.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple7.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple8.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple9.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple10.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple11.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple12.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple13.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple14.php", links ) );
    
    links = null;
    links = this.parseFile( "src/test/resources/AbsoluteHttpLinks.html" );
    assertEquals( links.size( ), 15 );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple1.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple2.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple3.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple4.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple6.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple7.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple8.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple9.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple10.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple11.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple12.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple13.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple14.php", links ) );
    
    links = null;
    links = this.parseFile( "src/test/resources/AbsoluteHttpsHttpFtpLinks.html" );
    assertEquals( links.size( ), 15 );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple1.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple2.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple3.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple4.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple6.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple7.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple8.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple9.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple10.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple11.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple12.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple13.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple14.php", links ) );
    
    links = null;
    links = this.parseFile( "src/test/resources/AbsoluteRelativeHttpLinks.html" );
    assertEquals( links.size( ), 14 );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple2.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple3.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple4.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple6.php", links ) );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple7.php", links ) );
    
    final String path = new File( 
        "src/test/resources/AbsoluteRelativeHttpLinks.html" ).
        getAbsoluteFile( ).getParentFile( ).getPath( ).replaceAll( "\\\\", "/" );

    assertTrue( 
        VFCLinkVisitorTest.containsUrl(
            String.format( 
                "file://localhost/%s/%s", path, "simple.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( 
                "file://localhost/%s/%s", path, "simple1.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( 
                "file://localhost/%s/%s", path, "simple2.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( 
                "file://localhost/%s/%s", path, "simple3.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( 
                "file://localhost/%s/%s", path, "simple4.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( 
                "file://localhost/%s/%s", path, "simple5.php" ), links ) );
    
    links = null;
    links = this.parseFile( "src/test/resources/LinklessPage.html" );
    assertEquals( links.size( ), 0 );
    
    links = null;
    links = this.parseFile( "src/test/resources/RepeatedAbsoluteRelativeHttpLinks.html" );
    assertEquals( links.size( ), 11 );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple1.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple1.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple2.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple2.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple3.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple3.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple5.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple6.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple6.php", links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple1.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple3.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple4.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple5.php" ), links ) );
    
    links = null;
    links = this.parseFile( "src/test/resources/RepeatedAbsoluteRelativeHttpFtpLinks.html" );

    assertEquals( links.size( ), 15 );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple.php", links ) );

    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple4.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple4.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple5.php", links ) );

    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple14.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple14.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple13.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple13.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple11.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple11.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple10.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple10.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple9.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple9.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple8.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple8.php", links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple1.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple3.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple7.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple12.php" ), links ) );
    
  }
  
  /**
   * This test uses the visitor with some html pages that contain various types
   * of a tag links and images. 
   * @throws ParserException 
   */
  @Test( )
  public void imageTest( ) throws ParserException
  {
    
    final String path = new File( 
        "src/test/resources/RepeatedAbsoluteRelativeHttpFtpLinksImages.html" ).
        getAbsoluteFile( ).getParentFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    Set< VFCLinkInformation > links = this.parseFile( 
        "src/test/resources/RepeatedAbsoluteRelativeHttpFtpLinksImages.html" );
    
    assertEquals( links.size( ), 21 );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple2.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple2.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple4.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple4.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple5.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple6.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple6.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple8.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple8.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple9.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple9.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple10.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple10.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple11.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple11.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple13.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple13.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple14.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple14.php", links ) );

    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img1.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img1.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img2.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img2.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img3.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img3.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img4.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img4.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img5.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img5.png", links ) );
       
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple1.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple3.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple7.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple12.php" ), links ) );
    
    links = null;
    links = this.parseFile(
        "src/test/resources/RepeatedAbsoluteRelativeHttpFtpLinksRepeatedImages.html" );
    
    assertEquals( links.size( ), 20 );
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple2.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple2.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple4.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple4.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple5.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple5.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple6.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple6.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple8.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple8.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple9.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple9.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple10.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple10.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple11.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple11.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple13.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple13.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/simple14.php", links ) );
    assertEquals( VisitedLinkType.ALINK, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/simple14.php", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img1.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img1.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img3.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img3.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img4.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img4.png", links ) );
    
    assertTrue( VFCLinkVisitorTest.containsUrl( "http://www.example.com/img5.png", links ) );
    assertEquals( VisitedLinkType.IMAGE, VFCLinkVisitorTest.getTypeFor( "http://www.example.com/img5.png", links ) );
       
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple1.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple3.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple7.php" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "simple12.php" ), links ) );
    
  }
  
  /**
   * This test uses the visitor with an html page that contains frames. 
   * @throws ParserException 
   */
  @Test( )
  public void frameTest( ) throws ParserException
  {
    
    final String path = new File( 
        "src/test/resources/Frame.html" ).
        getAbsoluteFile( ).getParentFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    final Set< VFCLinkInformation > links = this.parseFile( 
        "src/test/resources/Frame.html" );
    
    assertEquals( links.size( ), 4 );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "Frame_files/overview-frame.htm" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "Frame_files/allclasses-frame.htm" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "Frame_files/overview-summary.htm" ), links ) );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s/%s", path, "overview-summary.html" ), links ) );
  }
  
  /**
   * This test uses the visitor with an html page that contains link and script 
   * tags. 
   * @throws ParserException 
   * @throws IOException 
   */
  @Test( )
  public void linkScriptTest( ) throws ParserException, IOException
  {
    
    final Set< VFCLinkInformation > links = this.parseFile( 
        "src/test/resources/LinkScript.html" );
    
    assertEquals( links.size( ), 10 );
    
    String path = new File( 
        "src/test/resources/../../resources/css/ext-all.css" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/Feed-Viewer.css" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/../../bootstrap.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/viewer/FeedPost.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/viewer/FeedDetail.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/viewer/FeedGrid.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/viewer/FeedInfo.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/viewer/FeedPanel.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/viewer/FeedViewer.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl( 
            String.format( "file://localhost/%s", path ), links ) );
    
    path = new File( 
        "src/test/resources/viewer/FeedWindow.js" ).
        getAbsoluteFile( ).getCanonicalFile( ).getPath( ).replaceAll( "\\\\", "/" );
    
    assertTrue( 
        VFCLinkVisitorTest.containsUrl(
            String.format( "file://localhost/%s", path ), links ) );
  }
  
  /**
   * Checks if the given url is in the list.
   * @param url The url to look for.
   * @param list The list to check.
   * @return True, if the url is in the list.
   */
  private static boolean containsUrl( final String url, 
      final Set< VFCLinkInformation > list )
  {
    for( VFCLinkInformation info : list )
    {
      if( info.getUrlName( ).equals( url ) )
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Returns the type for the given url.
   * @param url The url to look for.
   * @param list The list containing the data.
   * @return The type of the given url.
   */
  private static VisitedLinkType getTypeFor( final String url, 
      final Set< VFCLinkInformation > list )
  {
    for( VFCLinkInformation info : list )
    {
      if( info.getUrlName( ).equals( url ) )
      {
        return info.getLinkType( );
      }
    }
    return VisitedLinkType.UNKNOWN;
  }
  
  /**
   * Parses the given file.
   * @param filename The file to parse.
   * @return The result link/type list.
   * @throws ParserException If some parse error happens.
   */
  private Set< VFCLinkInformation > parseFile( final String filename ) throws ParserException
  {
    this.visitor.clear( );
    final File file = new File( filename );
    if( file.exists( ) )
    { 
      final Parser parser = new Parser( filename );
      parser.visitAllNodesWith( this.visitor );
      return this.visitor.getLinkSet( );
    }
    return Collections.emptySet( );
  }
  
}
