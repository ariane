/*******************************************************************************
 * Copyright 2012 Fat Cat
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.assembla.ariane.jellyfish.test;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.TreeMap;
import com.assembla.ariane.jellyfish.proxy.storage.cache.util.CacheList;
import net.sf.ehcache.CacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the cache list for an consistency. 
 * @author Fat Cat
 */
public class PersistentCacheListTest
{
  
  /**
   * ...
   */
  public PersistentCacheListTest( )
  {
    
  }
  
  private CacheList< Integer, String > clist;
  
  private CacheManager manager;
  
  private TreeMap< Integer, String > addedItems;
  
  /**
   * fills map.
   * @param numberItems The number of items.
   * @throws NoSuchAlgorithmException Some error.
   */
  private void fillMap( final int numberItems ) throws NoSuchAlgorithmException
  {
    final SecureRandom random = SecureRandom.getInstance( "SHA1PRNG" );
    for ( int i = 0; i < numberItems; i++ )
    {
      this.addedItems.put( random.nextInt( 6000 ), Long.toString( Double.doubleToLongBits( random.nextDouble( ) ) ) );
    }
  }
  
  /**
   * Sets up.
   * @throws java.lang.Exception Some error.
   */
  @Before( )
  public void setUp( ) throws Exception
  {
    this.addedItems = new TreeMap<>( );
    final File file = new File( "src/test/resources/ehcache.xml" ).getAbsoluteFile( );
    this.manager = CacheManager.create( file.toString( ) );
    this.clist = new CacheList<>( this.manager );
  }

  private boolean finished;
  
  /**
   * Tears down.
   * @throws java.lang.Exception Some error.
   */
  @After( )
  public void tearDown( ) throws Exception
  {
    if ( !this.finished )
    {
      this.clist.clearLists( );
      this.addedItems.clear( );
      this.addedItems = null;
    }
  }
  
  /**
   * Adds a new item to the cache.
   * @throws Exception If some error happens.
   */
  @Test( )
  public void testAddItemCache( ) throws Exception
  {
    this.clist.add( "test", "test" );
    
    this.fillMap( 12 ); //Adds 12 items to the cache (only memory should be used)
    
    for ( final Integer key : this.addedItems.keySet( ) )
    {
      this.clist.add( "test", key, this.addedItems.get( key ) );
    }
    
    this.clist.get( "test" ).flush( ); //The items were flushed to the disk
    
  }
  
}
