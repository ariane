/******************************************************************************
 * Copyright 2012 Fat Cat                                                     *
 *                                                                            *
 * Licensed under the Apache License, Version 2.0 (the "License");            *
 * you may not use this file except in compliance with the License.           *
 * You may obtain a copy of the License at                                    *
 *                                                                            *
 *    http://www.apache.org/licenses/LICENSE-2.0                              *
 *                                                                            *
 * Unless required by applicable law or agreed to in writing, software        *
 * distributed under the License is distributed on an "AS IS" BASIS,          *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 * See the License for the specific language governing permissions and        *
 * limitations under the License.                                             *
 ******************************************************************************/

package com.assembla.coyote.ariane.jellyfish.utilities;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Annotation that specifies that the class it appears in does not have
 * instances and defined only static attributes and methods.<br/>
 * The java class Math is an utility class.<br/>
 * This is equivalent to the UML stereotype Utility.
 * @author Fat Cat
 * @since 0.0.8
 * @version 1
 */
@Documented
@Target({ ElementType.TYPE})
public @interface Utility
{
}
